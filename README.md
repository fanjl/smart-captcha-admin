# README #

后台管理系统的重新实现

### What is this repository used? ###

* spring-boot
* mybaits
* freemarker

### 做了些什么? ###

* 去除了admin-dao以及website-cache，captcha-cache等依赖包
* 整合到springboot的整体项目中，定制web项目避免admin-dao过于臃肿
* 视图从jsp切换为freemarker。
* 两个数据源分布在两个路径中，不用像admin一样重新启动一个spring容器加载数据源。
* 使用json web token替换session
* 加入spring security做权限控制


### Who do I talk to? ###

* 樊晋龙