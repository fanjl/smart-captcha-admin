package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.GroupProvider;
import com.touclick.captcha.smart.admin.model.MemberGroup;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface GroupMapper {
	@SelectProvider(type=GroupProvider.class,method="getGroupByMid")
	public List<MemberGroup> getGroupByMid(@Param(value = "mid") int mid);
	
	@InsertProvider(type=GroupProvider.class,method="save")
	public void save(@Param(value = "mg") MemberGroup mg);
	
	@SelectProvider(type=GroupProvider.class,method="getAllGroup")
	@Results({
		@Result(property = "gid",column = "gid"),
		@Result(property = "adsList",column = "gid",javaType = List.class,many = @Many(select = "AdsMapper.getAdsByGid"))
	})
	public List<MemberGroup> getAllGroup();
}
