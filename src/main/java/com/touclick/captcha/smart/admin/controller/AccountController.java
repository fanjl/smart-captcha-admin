package com.touclick.captcha.smart.admin.controller;

import com.touclick.captcha.smart.admin.model.Member;
import com.touclick.captcha.smart.admin.model.MemberInfo;
import com.touclick.captcha.smart.admin.service.inter.IAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping(value="/account")
public class AccountController {
	@Autowired
	private IAccount iAccount;
	
	@RequestMapping(value="accountlist")
	public String getAccountList(HttpSession session, Model model){
		Member m = (Member) session.getAttribute("member");
		List<MemberInfo> list = iAccount.getAccountList(m.getMid());
		model.addAttribute("list",list);
		return "account/accountlist";
	}
	@ResponseBody
	@RequestMapping(value="addaccount")
	public String addAccount(HttpServletRequest request){
		Member m = (Member)request.getSession().getAttribute("member");
		int accounttype= Integer.parseInt(request.getParameter("accounttype"));
		String account = request.getParameter("account");
		boolean checkAccount = iAccount.checkAccount(accounttype,account);
		if(!checkAccount){
			return "false";
		}else{
			int result = iAccount.saveAccount(m.getMid(),accounttype,account);
			if(result>0){
				return "true";
			}else{
				return "false";
			}
		}
	}
	@ResponseBody
	@RequestMapping(value="delaccount")
	public String delaccount(HttpServletRequest request){
		int id = Integer.parseInt(request.getParameter("id"));
		int result = iAccount.delAccount(id);
		if(result>0){
			return "true";
		}else{
			return "false";
		}
	}
	@ResponseBody
	@RequestMapping(value="haveAccount")
	public String haveAccount(HttpServletRequest request){
		Member m = (Member)request.getSession().getAttribute("member");
		List<MemberInfo> list = iAccount.getAccountList(m.getMid());
		if(list.size()>0){
			return "true";
		}else{
			return "false";
		}
	}
	@ResponseBody
	@RequestMapping(value="defaultAccount")
	public String defaultAccount(HttpServletRequest request){
		Member m = (Member)request.getSession().getAttribute("member");
		int id = Integer.parseInt(request.getParameter("id"));
		iAccount.defaultAccount(m.getMid(),id);
		return "true";
	}
}
