package com.touclick.captcha.smart.admin.model;

public enum MemberInfoType {
	aliaccount("支付宝", 1);
    // 成员变量  
    private String name;
    private int index;  
    /**
     * 构造方法
     * @param name
     * @param index
     */
    private MemberInfoType(String name, int index) {
        this.name = name;  
        this.index = index;  
    }
    /**返回属性名
     * @param index
     * @return
     */
    public static String getName(int index) {
        for (com.touclick.captcha.smart.admin.model.MemberInfoType c : com.touclick.captcha.smart.admin.model.MemberInfoType.values()) {
            if (c.getIndex() == index) {  
                return c.name();  
            }  
        }  
        return null;  
    }  
    /**
     * 返回字段含义
     * @param index
     * @return
     */
    public static String getComment(int index){
    	for (com.touclick.captcha.smart.admin.model.MemberInfoType c : com.touclick.captcha.smart.admin.model.MemberInfoType.values()) {
            if (c.getIndex() == index) {  
                return c.name;  
            }  
        }  
        return null;  
    }
    /**
     * 返回索引值
     * @param name
     * @return
     */
    public static int getIndex(String name){
    	for (com.touclick.captcha.smart.admin.model.MemberInfoType c : com.touclick.captcha.smart.admin.model.MemberInfoType.values()) {
            if (c.name().equals(name)) {  
                return c.index;
            }
    	}
        return 0;
    }
    public String getName() {
        return name;  
    }  
    public void setName(String name) {
        this.name = name;  
    }  
    public int getIndex() {  
        return index;  
    }  
    public void setIndex(int index) {  
        this.index = index;  
    }  
}
