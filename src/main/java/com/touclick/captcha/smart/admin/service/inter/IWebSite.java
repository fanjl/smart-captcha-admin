package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.WebSite;
import com.touclick.captcha.smart.admin.model.WebSiteInfo;

import java.util.List;

public interface IWebSite {
	public void addWebSite(WebSite website);
	public int addWebSiteTemp(WebSite website);
	public int addWebSiteInfo(WebSiteInfo wsi);
	public boolean existWebSite(String waddress);
	public int getWebSiteCountByMid(int mid);
	
	/**
	 * 
	* @author wangpeng
	* @Title: existWebSiteByMid 
	* @Description: TODO(检查网址是否是当前账号内的) 
	* @param @param waddress
	* @param @param mid
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws
	 */
	public boolean existWebSiteByMid(String waddress, int mid);
	
	public List<WebSite> getWebSiteList(int mid);
	
	
	public WebSite getWebSiteByPubkey(String pubkey);
	/**
     * 是否仅使用网站私有图库
     * @param wid
     * @return
     */
    public boolean isOnlyOwnerPic(int wid);
    
    /**
     * 网站是否可以放入广告图片
     * @param wid
     * @return
     */
    public boolean isNeedAds(int wid);
    
    /**
     * 查询所有
     * @return
     */
    public List<WebSite> findAll();
    
    public List<WebSiteInfo> findAllWebSiteInfo();
    public List<WebSiteInfo> findAllWebSiteInfo(int wid, int type);
    
    public List<WebSiteInfo> findWebSiteInfoByWid(int wid);
    
	/**
	 * 
	* @author fanjl
	* @Title: updateRemoveStatus 
	* @param @param wid
	* @param @param value           根据type判断websiteinfo表的type，例如8 去除logo，9去除超链接，10开启行为监控，从enum中getIndex获取。
	* @param @param isRemoveLogo    是否设置了去除logo或者help
	* @return void    返回类型 
	* @throws
	 */
    public void updateRemoveStatus(int wid, boolean value, int type);
    
    
	public void updateCustomPic(int wid, boolean custompic);
	/**
	 * 
	* @author wangpeng
	* @Title: updateCtypeAndSkin 
	* @Description: TODO(修改验证码类型和皮肤) 
	* @param @param wid
	* @param @param ctype
	* @param @param skin    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	public void updateCtypeAndSkin(int wid, int ctype, String skin);
	
	public WebSite getWebSiteByWid(int wid);
	
	public void delWebSite(int wid);
	/**
	 * 重置秘钥
	 * @param wid
	 * @param token
	 */
	public void resetToken(int wid, String token);
	/**
	 * 
	* @author wangpeng
	* @Title: updateVerify 
	* @Description: TODO(验证网站通过后更新验证标识) 
	* @param @param wid
	* @param @param verfiyflag    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	public void updateVerify(int wid, int verfiyflag);
	/**
	 * 
	* @author wangpeng
	* @Title: getWebsiteInfo 
	* @Description: TODO(根据网站获取网站信息)
	* @param @param wid
	* @param @return    设定文件 
	* @return List<WebSiteInfo>    返回类型 
	* @throws
	 */
	public List<WebSiteInfo> getWebsiteInfo(int wid);
	/**
	 * 
	* @author wangpeng
	* @Title: updateWebSiteInfo 
	* @Description: TODO(根据网站id和type更新value) 
	* @param     设定文件 
	* @return void    返回类型 
	* @throws
	 */
	public void updateWebSiteInfo(int wid, int type, String value);
	
	/**
	 * 
	* @author wangpeng
	* @Title: delCustom 
	* @Description: TODO(根据网站id和type删除websiteinfo的记录) 
	* @param     设定文件 
	* @return void    返回类型 
	* @throws
	 */
	public void delCustom(int wid, int type);
	/**
	 * 
	* @author wangpeng
	* @Title: udpateCversionByWid 
	* @Description: TODO(网站修改版本号) 
	* @param @param wid
	* @param @param cversion    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	public void udpateCversionByWid(int wid, String cversion);
	/**
	 * 
	* @author wangpeng
	* @Title: udpateCversionByMid 
	* @Description: TODO(更新用户下所有网站版本号) 
	* @param @param mid
	* @param @param cversion    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	public void udpateCversionByMid(int mid, String cversion);
	/**
	 * 
	* @author wangpeng
	* @Title: udpateCversionByPubkey 
	* @Description: TODO(更新公钥对应的网站版本号) 
	* @param @param pubkey
	* @param @param cversion    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	public void udpateCversionByPubkey(String pubkey, String cversion);
	
	public void updateCversionCTypeByPubkey(String pubkey, String cversion, int ctype);
	
	public int udpateCversionByPlatformCversion(String platform, String srcVersion, String desVersion);
	
	public List<WebSite> getWebSiteByPlatformCversion(String platform, String cversion);
	
	public List<WebSite> getAllWebSite();
	
	public List<String> getOldVersionWebSite();
}
