package com.touclick.captcha.smart.admin.utils;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class SendCloudUtils {
    private static final Logger LOGGER = Logger.getLogger(SendCloudUtils.class);
    private final static String url_template = "http://sendcloud.sohu.com/webapi/mail.send_template.json";
    private final static String apiUser = "Fanjl_test_VItWPO";
    private final static String apiKey = "GcBfgspAO4TQoLWJ";

    public static String sendActiveMail(String token, String toEmail, String realName) {
        String urlStr = "http://121.40.85.118:8888/member/active?token=" + token;
        String vars = "{\"to\": [\"" + toEmail + "\"],\"sub\":{\"%name%\": [\"" + realName + "\"],\"%url%\":[\""
                + urlStr + "\"]}}";
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("api_user", apiUser));
        nvps.add(new BasicNameValuePair("api_key", apiKey));
        nvps.add(new BasicNameValuePair("from", "admin@touclick.com"));
        nvps.add(new BasicNameValuePair("substitution_vars", vars));
        nvps.add(new BasicNameValuePair("subject", "active email"));
        nvps.add(new BasicNameValuePair("template_invoke_name", "test_template_active"));
        nvps.add(new BasicNameValuePair("fromname", "touclick"));

        try {
            String result = GetHttpClient.getHttpPost(url_template, new UrlEncodedFormEntity(nvps));
            return result;
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e);
        }
        return null;
    }
}
