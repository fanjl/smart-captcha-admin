package com.touclick.captcha.smart.admin.model;

public class AlipayDetail {
	private String liushui;						//流水号
	private String ali_person_account;		//收款方帐号
	private String realname;					//真实姓名
	private String money;							//付款金额
	private String content="";						//备注说明
	public String getLiushui() {
		return liushui;
	}
	public void setLiushui(String liushui) {
		this.liushui = liushui;
	}
	public String getAli_person_account() {
		return ali_person_account;
	}
	public void setAli_person_account(String ali_person_account) {
		this.ali_person_account = ali_person_account;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
    public String toString(){
		return liushui+"^"+ali_person_account+"^"+realname+"^"+money+"^"+content;
	}
}
