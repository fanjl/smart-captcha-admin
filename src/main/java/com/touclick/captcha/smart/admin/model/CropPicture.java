package com.touclick.captcha.smart.admin.model;

public class CropPicture {
	private String imgUrl; // your image path (the one we recieved after
							// successfull upload)
	private int imgInitW; // your image original width (the one we recieved
							// after upload)
	private int imgInitH; // your image original height (the one we recieved
							// after upload)
	private double imgW; // your new scaled image width
	private double imgH; // your new scaled image height
	private int imgX1; // top left corner of the cropped image in relation to
						// scaled image
	private int imgY1; // top left corner of the cropped image in relation to
						// scaled image
	private int cropW; // cropped image width
	private int cropH; // cropped image height
	private int rotation; // 旋转角度
 
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public int getImgInitW() {
		return imgInitW;
	}

	public void setImgInitW(int imgInitW) {
		this.imgInitW = imgInitW;
	}

	public int getImgInitH() {
		return imgInitH;
	}

	public void setImgInitH(int imgInitH) {
		this.imgInitH = imgInitH;
	}

	public double getImgW() {
		return imgW;
	}

	public void setImgW(double imgW) {
		this.imgW = imgW;
	}

	public double getImgH() {
		return imgH;
	}

	public void setImgH(double imgH) {
		this.imgH = imgH;
	}

	public int getImgX1() {
		return imgX1;
	}

	public void setImgX1(int imgX1) {
		this.imgX1 = imgX1;
	}

	public int getImgY1() {
		return imgY1;
	}

	public void setImgY1(int imgY1) {
		this.imgY1 = imgY1;
	}

	public int getCropW() {
		return cropW;
	}

	public void setCropW(int cropW) {
		this.cropW = cropW;
	}

	public int getCropH() {
		return cropH;
	}

	public void setCropH(int cropH) {
		this.cropH = cropH;
	}

	public int getRotation() {
		return rotation;
	}

	public void setRotation(int rotation) {
		this.rotation = rotation;
	}

	@Override
	public String toString() {
		return "imgInitW=" + imgInitW + "&imgInitH=" + imgInitH + "&imgW=" + imgW + "&imgH=" + imgH + "&imgX1=" + imgX1
				+ "&imgY1=" + imgY1 + "&cropW=" + cropW + "&cropH=" + cropH + "&rotation=" + rotation;
	}
}
