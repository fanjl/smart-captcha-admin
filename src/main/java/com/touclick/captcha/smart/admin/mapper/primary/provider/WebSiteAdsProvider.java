package com.touclick.captcha.smart.admin.mapper.primary.provider;

import java.util.Map;

public class WebSiteAdsProvider {
	 
	public String getWebSiteAdsListByWid(Map<String,Object> paras){
		return "select aid from website_ads t where t.wid=#{wid} and t.type = #{type}";
	}
	
	public String save(){
		return "insert into website_ads(wid,aid,type,time) values(#{wsa.wid},#{wsa.aid},#{wsa.type},#{wsa.time})";
	}
}
