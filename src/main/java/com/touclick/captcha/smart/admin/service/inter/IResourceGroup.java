package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.Group;

import java.util.List;

public interface IResourceGroup {
    int insert(Group group);
    
    List<Group> getGroupByWid(int wid);
    
    /**
     * 根据 website id 获取group id list
     *
     * @param wid website id
     * @return group id list
     */
    List<Integer> getGidsByWid(int wid);

    /**
     * 根据 website id 获取 group id list ，指定 ctype
     *
     * @param wid   website id
     * @param ctype captcha type
     * @return group id list
     */
    List<Integer> getGidsByWid(int wid, int ctype);

    /**
     * 根据 group id 获取实体
     * @param gid groupid
     * @return Group Entity
     */
    Group getGroupById(int gid);
}
