package com.touclick.captcha.smart.admin.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * Created by fanjl on 2017/2/14.
 */
@Configuration
@MapperScan(basePackages = AdminConfig.PACKAGE,sqlSessionFactoryRef = "adminSqlSessionFactory")
public class AdminConfig {
    static final String PACKAGE = "com.touclick.captcha.smart.admin.mapper.primary";
    @Bean(name = "adminDataSource")
    @Primary
    @ConfigurationProperties(prefix = "datasource.primary")
    public DataSource dataSourceMaster() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "adminTransactionManager")
    @Primary
    public DataSourceTransactionManager adminTransactionManager() {
        return new DataSourceTransactionManager(dataSourceMaster());
    }

    @Bean(name = "adminSqlSessionFactory")
    @Primary
    public SqlSessionFactory rdsSqlSessionFactory(@Qualifier("adminDataSource") DataSource adminDataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(adminDataSource);
        return sessionFactory.getObject();
    }

}

