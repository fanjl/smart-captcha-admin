package com.touclick.captcha.smart.admin.mapper.primary.provider;

import java.util.Map;

public class AccountProvider {
	public String getAccountList(Map<String,Object> paras){
		return "select * from member_info t where t.mid = #{mid}";
	}
	public String checkAccount(Map<String,Object> paras){
		return "select * from member_info t where t.type=#{accounttype} and t.value=#{account}";
	}
	public String saveAccount(Map<String,Object> paras){
		return "insert into member_info(mid,type,value,useflag) values(#{mid},#{accounttype},#{account},#{useflag})";
	}
	public String delAccount(Map<String,Object> paras){
		return "delete from member_info where id=#{id}";
	}
	public String updateAccount(Map<String,Object> paras){
		return "update member_info t set t.useflag=0 where t.mid=#{mid}";
	}
	public String defaultAccount(Map<String,Object> paras){
		return "update member_info t set t.useflag=1 where t.id=#{id}";
	}
}
