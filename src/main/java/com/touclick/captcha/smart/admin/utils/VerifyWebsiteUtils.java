package com.touclick.captcha.smart.admin.utils;

import org.apache.commons.codec.digest.DigestUtils;

public class VerifyWebsiteUtils {
	public static boolean verifyByWaddress(String waddress){
		String response = GetHttpClient.getHttpClient(waddress + "/verify.html");
		String answer = DigestUtils.md5Hex(waddress+DateUtils.today());
		System.out.println("answer="+answer);
		System.out.println("response="+response);
		if (answer.trim().equals(response.trim())) {
			return true;
		} else {
			return false;
		}
	} 
}
