package com.touclick.captcha.smart.admin.model;

public enum VipPictureTypeEnum {
	Game("游戏", 12), Plant("植物", 5), Animal("萌宠", 6), Build("建筑",7),
	Animation("动漫", 8), Sports("体育",9),Car("汽车", 10), Beauty("美女", 11), 
	Scenery("风景", 13), Festival("节日", 14), Techinal("科技",1);
    final String displayName;
    final int id;
    private VipPictureTypeEnum(String displayName, int id) {
        this.displayName = displayName;
        this.id = id;
    }
	public String getDisplayName() {
		return displayName;
	}
	public int getId() {
		return id;
	}
}
