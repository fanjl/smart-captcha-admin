package com.touclick.captcha.smart.admin.model;

import java.util.Date;

/**
 * Created by foxer on 2016/10/17.
 */
public class Group {
    private int gid;//group id 自增字段

    private int ctype;//ctype
    private int mid;//member id, 归属id，表示谁创建
    private int wid;//website id,网站id，并不是总有值，特指该gid 与某个网站关联

    //TODO 是否可以在这里直接应用枚举类型
    private int type;//枚举 GroupTypeEnum
    
    private Date createtime;

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public int getCtype() {
        return ctype;
    }

    public void setCtype(int ctype) {
        this.ctype = ctype;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getWid() {
        return wid;
    }

    public void setWid(int wid) {
        this.wid = wid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

}
