package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.LtypeData;
import com.touclick.captcha.smart.admin.model.Tall;

import java.util.List;

public interface IData {
	public int getTodaySv(int mid);
	/**
	 * 获取上一天的广告完成量
	 * @param ads
	 * @return
	 */
	public Integer getYestodayAsv(String ads);
	
	public long getTotalGByWids(List<Integer> wids);
	 
	public List<LtypeData> getDataByDateRange(int wid, long begin, long end);
	
	public List<LtypeData> getDataByDateRangeMonth(int wid, long begin, long end);
	
	public List<LtypeData> getDataByDateRangeYear(int wid, long begin, long end);
	
	public List<LtypeData> getDataByDateRangeWeek(int wid, long begin, long end);
	
	public List<Tall> getAllData(int ltype);
	
	public int getAvgHourDataByLtype(int wid, int ltype);
	
	public int getAvgDayDataByLtype(int wid, int ltype);
}
