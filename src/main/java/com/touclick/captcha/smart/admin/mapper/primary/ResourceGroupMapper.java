package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.ResourceGroupProvider;
import com.touclick.captcha.smart.admin.model.Group;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.StatementType;

import java.util.List;

public interface ResourceGroupMapper {
    @InsertProvider(type = ResourceGroupProvider.class,method = "insert")
    @Options(flushCache=Options.FlushCachePolicy.TRUE,statementType= StatementType.PREPARED,useGeneratedKeys=true,keyProperty="group.gid",keyColumn="group.gid")
    public int insert(@Param(value = "group") Group group);
    
    @SelectProvider(type=ResourceGroupProvider.class,method="getGroupByWid")
    public List<Group> getGroupByWid(@Param(value = "wid") int wid);
    
    @SelectProvider(type=ResourceGroupProvider.class,method="getGidsByWid")
    public List<Integer> getGidsByWid(@Param(value = "wid") int wid);
    
    @SelectProvider(type=ResourceGroupProvider.class,method="getGidsByWidAndCtype")
    public List<Integer> getGidsByWidAndCtype(@Param(value = "wid") int wid, @Param(value = "ctype") int ctype);
    
    @SelectProvider(type=ResourceGroupProvider.class,method="getGroupById")
    public Group getGroupById(@Param(value = "gid") int gid);
    
    
    

}
