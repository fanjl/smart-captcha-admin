package com.touclick.captcha.smart.admin.mapper.secondary.provider;

import com.touclick.captcha.smart.admin.model.Ads;
import com.touclick.captcha.smart.admin.utils.Pagination;

import java.util.Map;

/**
 * Ads provider
 *
 * @author bing.liu
 */
public class AdsProvider {
	public String insert(Map<String, Ads> params) {
		return "INSERT INTO ads ("
				+ "ctype,useflag,createtime,deletetime,"
				+ "mid,wid,vid,ispub,backgroud,spic,notetext,"
				+ "atype,protects,succurl,logourl,imgtexttype,transparent,printfont"
				+ ") VALUES("
				+ "#{ads.ctype},#{ads.useflag},#{ads.createtime},#{ads.deletetime},"
				+ "#{ads.mid},#{ads.wid},#{ads.vid},#{ads.ispub},#{ads.backgroud,jdbcType=BLOB},#{ads.spic,jdbcType=BLOB},#{ads.notetext,jdbcType=BLOB},"
				+ "#{ads.atype},#{ads.protects,jdbcType=BLOB},#{ads.succurl},#{ads.logourl},#{ads.imgtexttype},#{ads.transparent},#{ads.printfont}"
				+ ")";
	}

	public String selectByAid(Map<String, Integer> params) {
		return "SELECT * FROM ads where aid = #{aid}";
	}

	public String selectByAids(Map<String,String> params) {
	    String value = params.get("aids").toString();
		return "SELECT * FROM ads where aid in ("+value+")";
	}

	public String selectByAtype(Map<String, Integer> param) {
		return "SELECT * FROM ads where atype = #{atype}";
	}

	public String selectByCtype(Map<String, Integer> params) {
		return "SELECT * FROM ads where ctype = #{ctype}";
	}

	public String selectByMid(Map<String, Integer> params) {
		return "SELECT * FROM ads where mid = #{mid}";
	}

	public String selectAll() {
		return "SELECT * FROM ads";
	}

	public String getAdsByMid(Map<String, Integer> params) {
		return "select * from ads t where t.mid=#{mid} and t.atype=1";
	}

	public String getAidByMid(Map<String, Integer> params) {
		return "select t.aid from ads t where t.mid=#{mid} and t.atype=1";
	}

	public String getMemberGroupConfByAds(Map<String, String> params) {
		return "select * from member_group t1 where t1.gid in (select DISTINCT t.gid from group_ads t where t.aid in (#{ads}))";
	}

	public String getAidByGid(Map<String, Integer> params) {
		return "select t.aid from group_ads t where t.gid=#{gid}";
	}

	public String selectAllAid() {
		return "SELECT aid FROM ads ";
	}

	public String getNotAds() {
		return "select * from ads t where t.atype=0";
	}
	public String delAds(){
		return "delete from ads where mid=#{mid} and aid=#{aid}";
	}

	public String getAdsByWid() {
		return "select aid from ads t where t.atype=1 and t.wid = #{wid}";
	}

	public String getDefaultAdsAndPrivateAds() {
		return "select aid from ads t where t.atype=0 or t.wid = #{wid}";
	}

	public String getAidByVid(){
		return "select t.aid from ads t where t.vid=#{vid} and t.useflag=1";
	}

	public String getAdsByGid(){
		return "select * from ads t where t.aid in (select aid from group_ads where gid = #{gid})";
	}
	
	public String getAdsListByWid(){
        return "select * from ads t where t.atype=1 and t.wid = #{wid}";
	}
	public String getAdsByPagination(Map<String, Pagination<Ads>> params){
	    int pageNow = 0;
	    int pageSize = 10;
	    for(Map.Entry<String, Pagination<Ads>> entry : params.entrySet()){
	        pageNow = entry.getValue().getPageNo();
	        pageSize = entry.getValue().getPageSize();
	    }
        return "select * from ads t where t.atype != 0 limit "+pageNow+", "+pageSize+"";
    }
	
	public String countTotal(){
        return "select count(*) from ads t where t.atype != 0 ";
    }
	
	
	public String getDistinctWidWithTime(){
        return "select  wid,min(createtime) as createtime,ctype from ads where createtime not between #{start} and #{end} GROUP BY wid";
    }
	
	
}
