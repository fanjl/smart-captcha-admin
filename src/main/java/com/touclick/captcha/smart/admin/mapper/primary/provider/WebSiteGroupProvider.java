package com.touclick.captcha.smart.admin.mapper.primary.provider;

import java.util.Map;

public class WebSiteGroupProvider {
	 
	public String getWebSiteGroupListByWid(Map<String,Object> paras){
		return "select * from website_group t where t.wid=#{wid}";
	}
	
	public String getGidByWidAndType(Map<String,Object> paras){
		return "select gid from website_group t where t.wid=#{wid} and t.type=#{type}";
	}
	
	public String save(){
		return "insert into website_group(wid,gid,type) values(#{wsg.wid},#{wsg.gid},#{wsg.type})";
	}
	
	public String delete(){
		return "delete from website_group where wid=#{wsg.wid} and gid=#{wsg.gid} and type=#{wsg.type}";
	}
}
