package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.WebSiteAdsProvider;
import com.touclick.captcha.smart.admin.model.WebSiteAds;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface WebSiteAdsMapper {
	@SelectProvider(type = WebSiteAdsProvider.class,method = "getWebSiteAdsListByWid")
	public List<Integer> getWebSiteAdsListByWid(@Param(value = "wid") int wid, @Param(value = "type") int type);
	
	@InsertProvider(type = WebSiteAdsProvider.class,method = "save")
	public void save(@Param(value = "wsa") WebSiteAds wsa);
}
