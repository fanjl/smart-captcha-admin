package com.touclick.captcha.smart.admin.mapper.primary.provider;


import com.touclick.captcha.smart.admin.model.AccessLogArea;
import com.touclick.captcha.smart.admin.model.AccessLogDeviceType;
import com.touclick.captcha.smart.admin.model.AccessLogSystem;

import java.util.Map;

public class AccessLogAreaProvider {
    public String insert(Map<String, AccessLogArea> params) {
        return "INSERT INTO accesslog_area ("
                + "pubkey,ip,date,count,browser,system,devicetype,area"
                + ") VALUES("
                + "#{accesslogarea.pubkey},#{accesslogarea.ip},#{accesslogarea.date},#{accesslogarea.count},"
                + "#{accesslogarea.browser,jdbcType=VARCHAR},#{accesslogarea.system,jdbcType=VARCHAR},#{accesslogarea.devicetype,jdbcType=VARCHAR},#{accesslogarea.area,jdbcType=VARCHAR}"
                + ")";
    }
    
    public String insertDevice(Map<String, AccessLogDeviceType> params) {
        return "INSERT INTO accesslog_device ("
                + "pubkey,ip,date,count,devicetype"
                + ") VALUES("
                + "#{accesslogdevicetype.pubkey},#{accesslogdevicetype.ip},#{accesslogdevicetype.date},#{accesslogdevicetype.count},"
                + "#{accesslogdevicetype.devicetype,jdbcType=VARCHAR}"
                + ")";
    }
    
    public String insertSystem(Map<String, AccessLogSystem> params) {
        return "INSERT INTO accesslog_system ("
                + "pubkey,ip,date,count,operationsystem"
                + ") VALUES("
                + "#{accesslogsystem.pubkey},#{accesslogsystem.ip},#{accesslogsystem.date},#{accesslogsystem.count},"
                + "#{accesslogsystem.operationsystem,jdbcType=VARCHAR}"
                + ")";
    }
    
    public String getAreaListByPubkeyTime(Map<String, String> params) {
        return "select * from accesslog_area where pubkey=#{pubkey} and date between #{start} and #{end}";
    }
    
    public String getSystemListByPubkeyTime(Map<String, String> params) {
        return "select * from accesslog_system where pubkey=#{pubkey} and date between #{start} and #{end}";
    }
    
}
