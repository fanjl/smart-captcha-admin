package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.MemberGroupProvider;
import com.touclick.captcha.smart.admin.model.MemberGroup;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface MemberGroupMapper {
	@SelectProvider(type = MemberGroupProvider.class,method = "findAll")
	public List<MemberGroup> findAll();
	
	@SelectProvider(type = MemberGroupProvider.class,method = "findByGid")
	public MemberGroup findByGid(@Param(value = "gid") int gid);
	
	@SelectProvider(type = MemberGroupProvider.class,method = "findAllGid")
	public List<Integer> findAllGid();
}
