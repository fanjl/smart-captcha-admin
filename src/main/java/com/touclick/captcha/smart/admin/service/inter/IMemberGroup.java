package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.MemberGroup;

import java.util.List;

public interface IMemberGroup {
	public List<MemberGroup> findAll();
	
	public MemberGroup findByGid(int gid);
	
	public List<Integer> findAllGid();
	
}
