package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.MoneyProvider;
import com.touclick.captcha.smart.admin.model.CheckDrawMoney;
import com.touclick.captcha.smart.admin.model.DrawMoney;
import com.touclick.captcha.smart.admin.model.HistoryCost;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import java.math.BigDecimal;
import java.util.List;

public interface MoneyMapper {
	@SelectProvider(type = MoneyProvider.class,method="getEarnings")
	public Double getEarnings(@Param(value = "mid") int mid);
	
	@SelectProvider(type = MoneyProvider.class,method="getHistoryCost")
	public List<HistoryCost> getHistoryCost(@Param(value = "mid") int mid);
	
	@SelectProvider(type = MoneyProvider.class,method="getHsitoryBill")
	public List<HistoryCost> getHsitoryBill(@Param(value = "mid") int mid);
	
	@SelectProvider(type = MoneyProvider.class,method="getDrawMoneyRecord")
	public List<DrawMoney> getDrawMoneyRecord(@Param(value = "mid") int mid);
	
	@SelectProvider(type = MoneyProvider.class,method="getCountMoneyByDateRange")
	public Double getCountMoneyByDateRange(@Param(value = "mid") int mid, @Param(value = "starttime") String starttime, @Param(value = "endtime") String endtime);
	
	@UpdateProvider(type = MoneyProvider.class,method="updateDrawflag")
	public void updateDrawflag(@Param(value = "mid") int mid, @Param(value = "starttime") String starttime, @Param(value = "endtime") String endtime);
	
	@UpdateProvider(type = MoneyProvider.class,method="updateAllDrawflag")
	public void updateAllDrawflag(@Param(value = "mid") int mid);
	
	@InsertProvider(type = MoneyProvider.class,method="insertDrawMoneyRecord")
	public void insertDrawMoneyRecord(@Param(value = "mid") int mid, @Param(value = "money") Double money, @Param(value = "starttime") String starttime, @Param(value = "endtime") String endtime);
	
	@InsertProvider(type = MoneyProvider.class,method="insertAllDrawMoneyRecord")
	public void insertAllDrawMoneyRecord(@Param(value = "mid") int mid, @Param(value = "money") Double money);
	
	@SelectProvider(type = MoneyProvider.class,method="getCheckList")
	public List<CheckDrawMoney> getCheckList();
	
	@InsertProvider(type = MoneyProvider.class,method="saveEarnings")
	public void saveEarnings(@Param(value = "mid") int mid, @Param(value = "wid") int wid, @Param(value = "earnings") BigDecimal earnings, @Param(value = "unit") int unit);
}
