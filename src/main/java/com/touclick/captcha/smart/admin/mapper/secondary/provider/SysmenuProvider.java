package com.touclick.captcha.smart.admin.mapper.secondary.provider;

import java.util.Map;

public class SysmenuProvider {
	public String getOneMenu(Map<String,Object> paras){
		return "select * from sys_menu t where t.sid in (select distinct t.pid from sys_menu t left join sys_menu_role t1 on t.sid=t1.sid where t.useflag=1 and t1.role=#{role}) and t.useflag=1 order by t.priority";
	}
	public String getTwoMenu(Map<String,Object> paras){
		return "select t.* from sys_menu t left join sys_menu_role t1 on t.sid=t1.sid where t.useflag=1 and t.pid=#{oneid} and t1.role=#{role} order by t.priority";
	}
}
