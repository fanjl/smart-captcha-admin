package com.touclick.captcha.smart.admin.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 日期工具类
 * 
 * @author wzj
 * @version v1.0
 */
public class DateUtil {
    /**
     * 日期格式
     */
    public static final String DATE = "yyyyMMdd";
    public static final String YEAR_MONTH = "yyyyMM";
    public static final String DATETIME = "yyyyMMddHHmmss";
    public static final String DATETIMEMILLI = "yyyyMMddHHmmssSSS";
    public static final String HYPHEN_DATE = "yyyy-MM-dd";
    public static final String HC_DATETIME = "yyyy-MM-dd HH:mm:ss";
    public static final DateFormat FORMATTIME = getDateFormat(HC_DATETIME);

    /**
     * 获取指定日期格式的日期格式化对象 若指定的日期格式为非法格式，则返回NULL
     * 
     * @param pattern
     * @return 日期格式化对象，若指定的日期格式为非法格式，则返回NULL
     */
    public static DateFormat getDateFormat(String pattern) {
        if (StringUtil.nullOrEmpty(pattern)) {
            throw new IllegalArgumentException("arg 'pattern' can't be empty");
        }
        DateFormat dateFormat = null;
        try {
            dateFormat = new SimpleDateFormat(pattern);
        } catch (IllegalArgumentException e) {
            // do nothing
        }
        return dateFormat;
    }
    public static String Nowformat() {
        Date date = new Date();
        return FORMATTIME.format(date);
    }
    public static Date longToDate(long time) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        return c.getTime();
    }
    public static String longToString(long time, String pattern) {
        Date date = new Date();
        date.setTime(time);
        return getDateFormat(pattern).format(date);
    }
    
    public static String next(String start, int day)  {
        Date date = new Date();
        long time = 0L;
        try {
            time = FORMATTIME.parse(start).getTime() + day * 60 * 60 * 24 * 1000L;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        date.setTime(time);
        return FORMATTIME.format(date);
    }
    
    public static String next(String start, int day, String pattern)  {
        Date date = new Date();
        long time = 0L;
        try {
            time = getDateFormat(pattern).parse(start).getTime() + day * 60 * 60 * 24 * 1000L;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        date.setTime(time);
        return getDateFormat(pattern).format(date);
    }

    /**
     * 将目标日期对象按指定的日期格式转换为日期字符串
     * 
     * @param target
     *            目标对象
     * @param pattern
     *            日期格式
     * @return 日期字符串
     */
    public static String formatDate(Date target, String pattern) {
        String formattedDate = null;
        DateFormat dateFormat = getDateFormat(pattern);
        if (dateFormat != null) {
            formattedDate = dateFormat.format(target);
        } else {
            throw new IllegalArgumentException("not support pattern: " + pattern);
        }
        return formattedDate;
    }

    /**
     * 将目标日期字符串按指定的日期格式解析为日期对象
     * 
     * @param dateStr
     *            日期字符串
     * @param pattern
     *            日期格式
     * @return 日期对象
     */
    public static Date parseDate(String dateStr, String pattern) {
        Date parsedDate = null;
        DateFormat dateFormat = getDateFormat(pattern);
        if (dateFormat != null) {
            try {
                parsedDate = dateFormat.parse(dateStr);
            } catch (ParseException e) {
                throw new IllegalArgumentException(e);
            }
        } else {
            throw new IllegalArgumentException("not support pattern: " + pattern);
        }
        return parsedDate;
    }

    public static int getLastDay(String year, String month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.valueOf(year));
        cal.set(Calendar.MONTH, Integer.valueOf(month) - 1);
        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static List<String> getYearList(int leftPlusX, int rightPlusX, boolean desc) {
        List<String> yearList = new ArrayList<String>();
        Calendar cal = Calendar.getInstance();
        int curYear = cal.get(Calendar.YEAR);
        if (desc) { // 倒序
            for (int year = (curYear + rightPlusX); year >= (curYear - leftPlusX); year--) {
                yearList.add(String.valueOf(year));
            }
        } else { // 正序
            for (int year = (curYear - leftPlusX); year <= (curYear + rightPlusX); year++) {
                yearList.add(String.valueOf(year));
            }
        }
        return yearList;
    }

    public static Date getFirstDayOfLastXMonth(int x) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - x);
        return new Date(cal.getTimeInMillis());
    }
    public static String getFirstDayStrOfLastXMonth(int x) {
        Date firstDayDate = getFirstDayOfLastXMonth(x);
        return formatDate(firstDayDate, HYPHEN_DATE);
    }

    public static Date getLastDayOfLastXMonth(int x) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - x);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return new Date(cal.getTimeInMillis());
    }

    public static String getLastDayStrOfLastXMonth(int x) {
        Date lastDayDate = getLastDayOfLastXMonth(x);
        return formatDate(lastDayDate, HYPHEN_DATE);
    }

    public static Date getCurrDayOfLastXMonth(int x) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - x);
        return new Date(cal.getTimeInMillis());
    }

    public static String getCurrDayStrOfLastXMonth(int x) {
        Date currDayOfLastXMonth = getCurrDayOfLastXMonth(x);
        return formatDate(currDayOfLastXMonth, HYPHEN_DATE);
    }
    
    public static long getNoMillLongTime(long time) {
        String noMillTime = formatDate(longToDate(time), HYPHEN_DATE);
        time = parseDate(noMillTime, HYPHEN_DATE).getTime();
        return time;
    }
    
    public static void main(String[] args) {
        Date datestart = com.touclick.captcha.smart.admin.utils.DateUtil.parseDate("2016-03-01 00:00:00", com.touclick.captcha.smart.admin.utils.DateUtil.HC_DATETIME);
        Date dateend = com.touclick.captcha.smart.admin.utils.DateUtil.parseDate("2016-03-02 00:00:00", com.touclick.captcha.smart.admin.utils.DateUtil.HC_DATETIME);
        Date date = parseDate("2016-03-01 15:15:30", HC_DATETIME);
        if (date.after(datestart) && date.before(dateend)) {
            System.out.println("old problem png create in");
        }
        System.out.println(getLastDay("2012", "2"));
    }
}
