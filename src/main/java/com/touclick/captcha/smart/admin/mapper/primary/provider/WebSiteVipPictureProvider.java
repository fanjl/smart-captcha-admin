package com.touclick.captcha.smart.admin.mapper.primary.provider;

public class WebSiteVipPictureProvider {
	public String save(){
		return "insert into website_vippic(wid,vid,servicetime,endtime) values(#{wv.wid},#{wv.vid},#{wv.servicetime},#{wv.endtime})";
	}
	
	public String delByEndtime(){
		return "delete from website_ads where type=#{type} and wid in (select wid from website_vippic where endtime = #{endtime})";
	}
	
	public String getObjectByWidVid(){
		return "select * from website_vippic t where t.wid=#{wid} and t.vid=#{vid}";
	}
	
	public String update(){
		return "update website_vippic t set t.servicetime = #{wv.servicetime},t.endtime = #{wv.endtime} where t.wid = #{wv.wid} and t.vid = #{wv.vid}";
	}
}
