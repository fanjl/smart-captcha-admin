package com.touclick.captcha.smart.admin.model;

public class MemberInfo {
	private int id;
	private int mid;
	private int type;
	private String value;
	private String accounttype;
	private int useflag;
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getAccounttype() {
		accounttype = MemberInfoType.getComment(type);
		return accounttype;
	}
	public void setAccounttype(String accounttype) {
		this.accounttype = accounttype;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUseflag() {
		return useflag;
	}
	public void setUseflag(int useflag) {
		this.useflag = useflag;
	}
	
}
