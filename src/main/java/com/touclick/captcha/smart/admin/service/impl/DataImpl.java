package com.touclick.captcha.smart.admin.service.impl;

import com.touclick.captcha.smart.admin.mapper.primary.WebSiteMapper;
import com.touclick.captcha.smart.admin.mapper.secondary.DataMapper;
import com.touclick.captcha.smart.admin.model.*;
import com.touclick.captcha.smart.admin.service.inter.IData;
import com.touclick.captcha.smart.admin.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Service("IData")
public class DataImpl  implements IData {
	@Autowired
	private WebSiteMapper websiteMapper;
	@Autowired
	private DataMapper dataMapper;
	@Override
    public int getTodaySv(int mid) {
		List<WebSite> weblist = websiteMapper.getWebSiteList(mid);
		if(weblist.size()>0){
			Integer sv = dataMapper.getTodaySv(weblist);
			if(sv==null){
				return 0;
			}
			return sv;
		}
		return 0;
	}
	@Override
    public long getTotalGByWids(List<Integer> wids) {

        if(wids.size()>0){
            Long sv = dataMapper.getTotalGByWids(wids);
            return sv;
        }
        return 0;
    }
	@Override
    public List<LtypeData> getDataByDateRange(int wid, long begin, long end) {

		List<Tlast> sv = dataMapper.getDataByDateRange(wid,begin,end);
		List<LtypeData> ltypedata = new ArrayList<LtypeData>();
		LtypeData ld = new LtypeData();
		for(Tlast t : sv){
			if(t.getDate()!=ld.getDate()){
				if(ld.getDate()!=0){
					ltypedata.add(ld);
					ld = new LtypeData();
				}
			}
			ld.setDate(t.getDate());
			if(t.getLtype()== LTypeEnum.G.getIndex()){
				ld.setG(t.getCount());
			}else if(t.getLtype()==LTypeEnum.VY.getIndex()){
				ld.setVy(t.getCount());
			}else if(t.getLtype()==LTypeEnum.VN.getIndex()){
				ld.setVn(t.getCount());
			}else if(t.getLtype()==LTypeEnum.SV.getIndex()){
				ld.setSv(t.getCount());
			}else if(t.getLtype()==LTypeEnum.AD.getIndex()){
				ld.setAd(t.getCount());
			}
		}
		if(ld.getDate()!=0){
			ltypedata.add(ld);
		}
		for(LtypeData ltd : ltypedata){
			if(ltd.getSv()==0){
				ltd.setAdr("0.00%");
			}else{
				ltd.setAdr(new DecimalFormat("#.000").format(ltd.getAd()*100.0/ltd.getSv())+"%");
			}
		}
		return ltypedata;
	}
	@Override
    public List<LtypeData> getDataByDateRangeMonth(int wid, long begin, long end) {
		List<Tall> sv = dataMapper.getDataByDateRangeMonth(wid,begin,end);
		List<LtypeData> ltypedata = new ArrayList<LtypeData>();
		LtypeData ld = new LtypeData();
		for(Tall t : sv){
			if(t.getDate()!=ld.getDate()){
				if(ld.getDate()!=0){
					ltypedata.add(ld);
					ld = new LtypeData();
				}
			}
			ld.setDate(t.getDate());
			if(t.getLtype()==LTypeEnum.G.getIndex()){
				ld.setG(t.getCount());
			}else if(t.getLtype()==LTypeEnum.VY.getIndex()){
				ld.setVy(t.getCount());
			}else if(t.getLtype()==LTypeEnum.VN.getIndex()){
				ld.setVn(t.getCount());
			}else if(t.getLtype()==LTypeEnum.SV.getIndex()){
				ld.setSv(t.getCount());
			}else if(t.getLtype()==LTypeEnum.AD.getIndex()){
				ld.setAd(t.getCount());
			}
		}
		if(ld.getDate()!=0){
			ltypedata.add(ld);
		}
		for(LtypeData ltd : ltypedata){
			if(ltd.getSv()==0){
				ltd.setAdr("0.00%");
			}else{
				ltd.setAdr(new DecimalFormat("#.000").format(ltd.getAd()*100.0/ltd.getSv())+"%");
			}
		}
		return ltypedata;
	}
	@Override
    public List<LtypeData> getDataByDateRangeWeek(int wid, long begin, long end) {
        List<Tlast> sv = dataMapper.getDataByDateRangeWeek(wid,begin,end);
        List<LtypeData> ltypedata = new ArrayList<LtypeData>();
        LtypeData ld = new LtypeData();
        int count = 0;
        for(Tlast t : sv){
            t.setDate(DateUtil.getNoMillLongTime(t.getDate()));
            if(t.getDate()!=ld.getDate()){
                if(ld.getDate()!=0){
                    ltypedata.add(ld);
                    ld = new LtypeData();
                    count = 0;
                }
            }
            ld.setDate(t.getDate());
            if(t.getLtype()==LTypeEnum.G.getIndex()){
                count = count + t.getCount() ;
                ld.setG(count);
            }else if(t.getLtype()==LTypeEnum.VY.getIndex()){
                ld.setVy(t.getCount());
            }else if(t.getLtype()==LTypeEnum.VN.getIndex()){
                ld.setVn(t.getCount());
            }else if(t.getLtype()==LTypeEnum.SV.getIndex()){
                ld.setSv(t.getCount());
            }else if(t.getLtype()==LTypeEnum.AD.getIndex()){
                ld.setAd(t.getCount());
            }
        }
        if(ld.getDate()!=0){
            ltypedata.add(ld);
        }
        for(LtypeData ltd : ltypedata){
            if(ltd.getSv()==0){
                ltd.setAdr("0.00%");
            }else{
                ltd.setAdr(new DecimalFormat("#.000").format(ltd.getAd()*100.0/ltd.getSv())+"%");
            }
        }
        return ltypedata;
    }
	@Override
    public List<LtypeData> getDataByDateRangeYear(int wid, long begin, long end) {
		List<Tall> sv = dataMapper.getDataByDateRangeYear(wid,begin,end);
		List<LtypeData> ltypedata = new ArrayList<LtypeData>();
		LtypeData ld = new LtypeData();
		for(Tall t : sv){
			if(t.getDate()!=ld.getDate()){
				if(ld.getDate()!=0){
					ltypedata.add(ld);
					ld = new LtypeData();
				}
			}
			ld.setDate(t.getDate());
			if(t.getLtype()==LTypeEnum.G.getIndex()){
				ld.setG(t.getCount());
			}else if(t.getLtype()==LTypeEnum.VY.getIndex()){
				ld.setVy(t.getCount());
			}else if(t.getLtype()==LTypeEnum.VN.getIndex()){
				ld.setVn(t.getCount());
			}else if(t.getLtype()==LTypeEnum.SV.getIndex()){
				ld.setSv(t.getCount());
			}else if(t.getLtype()==LTypeEnum.AD.getIndex()){
				ld.setAd(t.getCount());
			}
		}
		if(ld.getDate()!=0){
			ltypedata.add(ld);
		}
		for(LtypeData ltd : ltypedata){
			if(ltd.getSv()==0){
				ltd.setAdr("0.00%");
			}else{
				ltd.setAdr(new DecimalFormat("#.000").format(ltd.getAd()*100.0/ltd.getSv())+"%");
			}
		}
		return ltypedata;
	}
	@Override
    public Integer getYestodayAsv(String ads){
		return dataMapper.getYestodayAsv(ads);
	}
	@Override
    public List<Tall> getAllData(int ltype){
		List<Tall> sv = dataMapper.getAllData(ltype);
		return sv;
	}
	@Override
    public int getAvgHourDataByLtype(int wid,int ltype) {
		List<Tlast> list = dataMapper.getAvgHourDataByLtype(wid,ltype);
		int sum = 0;
		for(Tlast t : list){
			sum += t.getCount();
		}
		int avgCount = sum/list.size();
		return avgCount;
	}
	@Override
    public int getAvgDayDataByLtype(int wid,int ltype) {
		List<Tall> list = dataMapper.getAvgDayDataByLtype(wid,ltype);
		int sum = 0;
		for(Tall t : list){
			sum += t.getCount();
		}
		int avgCount = sum/list.size();
		return avgCount;
	}
}
