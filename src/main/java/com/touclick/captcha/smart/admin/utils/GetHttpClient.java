package com.touclick.captcha.smart.admin.utils;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.Charset;

public class GetHttpClient {
	private static final PoolingHttpClientConnectionManager CONN_MGR;
	private static final CloseableHttpClient HTTP_CLIENT;
	private static final SocketConfig DEFAULT_SOCKET_CONFIG;
	private static final ConnectionConfig DEFAULT_CONN_CONFIG;
	private static final RequestConfig DEFAULT_REQ_CONFIG;
	private static final int DEFAULT_BUFFER_SIZE = 1024 * 1024;
	private static final Charset DEFAULT_CHARSET = Consts.UTF_8;
	private static final int DEFAULT_CONNECTION_REQUEST_TIMEOUT = 5 * 1000;
	private static final int DEFAULT_CONNECT_TIMEOUT = 5 * 1000;
	private static final int DEFAULT_SOCKET_TIMEOUT = 10 * 1000;
	private static final int DEFAULT_MAX_TOTAL = 512;
	private static final int DEFAULT_MAX_PER_ROUTE = 2;

	static {
		DEFAULT_SOCKET_CONFIG = SocketConfig.DEFAULT;
		DEFAULT_CONN_CONFIG = ConnectionConfig.custom().setBufferSize(DEFAULT_BUFFER_SIZE).setCharset(DEFAULT_CHARSET)
				.build();
		DEFAULT_REQ_CONFIG = RequestConfig.custom().setConnectionRequestTimeout(DEFAULT_CONNECTION_REQUEST_TIMEOUT)
				.setConnectTimeout(DEFAULT_CONNECT_TIMEOUT).setSocketTimeout(DEFAULT_SOCKET_TIMEOUT).build();
		CONN_MGR = new PoolingHttpClientConnectionManager();
		CONN_MGR.setMaxTotal(DEFAULT_MAX_TOTAL);
		CONN_MGR.setDefaultMaxPerRoute(DEFAULT_MAX_PER_ROUTE);
		CONN_MGR.setDefaultConnectionConfig(DEFAULT_CONN_CONFIG);
		CONN_MGR.setDefaultSocketConfig(DEFAULT_SOCKET_CONFIG);
		HTTP_CLIENT = HttpClients.custom().setConnectionManager(CONN_MGR).setDefaultRequestConfig(DEFAULT_REQ_CONFIG)
				.build();
	}

	public static String getHttpClient(String url) {
		Logger logger = Logger.getLogger(GetHttpClient.class);
		if (url.indexOf("http") == -1) {
			url = "http://" + url;
		}
		HttpGet httpGet = new HttpGet(url);
		httpGet.addHeader("Accept", "text/html");
		httpGet.addHeader("Accept-Charset", "utf-8");
		httpGet.addHeader("Accept-Encoding", "gzip");
		httpGet.addHeader("Accept-Language", "en-US,en");
		httpGet.addHeader("User-Agent",
				"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.160 Safari/537.22");
		logger.info("Executing request" + httpGet.getRequestLine());
		try {
			CloseableHttpResponse response = HTTP_CLIENT.execute(httpGet);
			logger.info(response.getStatusLine());
			HttpEntity entity = response.getEntity();
			return EntityUtils.toString(entity);
		} catch (ClientProtocolException e) {
			logger.info(url);
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.info(url);
			logger.error(e.getMessage());
		}
		return "";
	}
	public static String getHttpPost(String url, HttpEntity httpentity) {
		Logger logger = Logger.getLogger(GetHttpClient.class);
		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(httpentity);
		//logger.info("Executing request" + httpPost.getRequestLine());
		try {
			CloseableHttpResponse response = HTTP_CLIENT.execute(httpPost);
			//logger.info(response.getStatusLine());
			HttpEntity entity = response.getEntity();
			return EntityUtils.toString(entity);
		} catch (ClientProtocolException e) {
			logger.info(url);
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.info(url);
			logger.error(e.getMessage());
		}
		return "";
	}
//	public static void main(String[] args) {
//		System.out.println(VerifyWebsiteUtils.verifyByWaddress("http://www.52zll.top"));
//		String response = GetHttpClient.getHttpClient("http://napi.9eban.com/day/DayInfo");
//		System.out.println(response.charAt(0));
//		System.out.println(response);
//	}
}
