package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.PayOrderProvider;
import com.touclick.captcha.smart.admin.model.PayOrder;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

public interface PayOrderMapper {
	@InsertProvider(type=PayOrderProvider.class,method="save")
	public void save(@Param(value = "p") PayOrder po);
	
	@UpdateProvider(type=PayOrderProvider.class,method="updateStatus")
	public int updateStatus(@Param(value = "oid") long oid, @Param(value = "status") int status, @Param(value = "tradeno") String tradeno);
	
	@SelectProvider(type=PayOrderProvider.class,method="getObjectByOid")
	public PayOrder getObjectByOid(@Param(value = "oid") long oid);
}
