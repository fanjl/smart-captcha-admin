package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.MemberGroup;

import java.util.List;

public interface IGroup {
    /**
     * @param @param  mid
     * @param @return 设定文件
     * @return List<MemberGroup>    返回类型
     * 广告组列表
     */
    List<MemberGroup> getGroupByMid(int mid);

    /**
     * @param @param mg    设定文件
     * @return boolean    返回类型
     * 保存广告组
     */
    boolean save(MemberGroup mg);

    /**
     * @param @return 设定文件
     * @return List<MemberGroup>    返回类型
     * 获取所有广告组
     */
    List<MemberGroup> getAllGroup();


}
