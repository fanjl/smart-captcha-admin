package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.WebSiteVipPictureProvider;
import com.touclick.captcha.smart.admin.model.WebSiteVipPicture;
import org.apache.ibatis.annotations.*;

public interface WebSiteVipPictureMapper {
	@InsertProvider(type=WebSiteVipPictureProvider.class,method="save")
	public void save(@Param(value = "wv") WebSiteVipPicture wv);
	
	@DeleteProvider(type=WebSiteVipPictureProvider.class,method="delByEndtime")
	public int delByEndtime(@Param(value = "endtime") String endtime, @Param(value = "type") int type);
	
	@SelectProvider(type=WebSiteVipPictureProvider.class,method="getObjectByWidVid")
	public WebSiteVipPicture getObjectByWidVid(@Param(value = "wid") int wid, @Param(value = "vid") int vid);
	
	@UpdateProvider(type=WebSiteVipPictureProvider.class,method="update")
	public void update(@Param(value = "wv") WebSiteVipPicture wv);
}
