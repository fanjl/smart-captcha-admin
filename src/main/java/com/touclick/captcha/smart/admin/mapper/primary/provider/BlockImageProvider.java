package com.touclick.captcha.smart.admin.mapper.primary.provider;

import com.touclick.captcha.smart.admin.model.BlockImage;

import java.util.Map;

/**
 * BlockImage provider
 *
 * @author bing.liu
 * @date 2015-09-21
 */
public class BlockImageProvider {
    public String findAll(){
        return "SELECT * FROM block_image";
    }

    public String findByNameId(Map<String,Integer> params){
        return "SELECT * FROM block_image where nameid = #{nameId}";
    }

    public String insert(Map<String,BlockImage> params){
        return "INSERT INTO block_image(nameid,image) values(#{blockImage.nameId},#{blockImage.image,jdbcType=BLOB})";
    }
}
