package com.touclick.captcha.smart.admin.model;

import java.util.Date;

public class Balance {
	private double prices;
	private int level;
	private long date = new Date().getTime();
	public double getPrices() {
		return prices;
	}
	public void setPrices(double prices) {
		this.prices = prices;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public Balance(double prices, int level) {
		super();
		this.prices = prices;
		this.level = level;
	}
	
}
