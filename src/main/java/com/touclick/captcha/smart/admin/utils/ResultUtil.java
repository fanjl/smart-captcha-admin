package com.touclick.captcha.smart.admin.utils;


import com.touclick.captcha.smart.admin.model.result.ErrorInfo;
import com.touclick.captcha.smart.admin.model.result.Meta;
import com.touclick.captcha.smart.admin.model.result.Result;

/**
 * Created by yuanshichao on 2016/11/17.
 */
public class ResultUtil {

    public static Result success(Object data) {
        Meta meta = new Meta(200, "success");
        return new Result(meta, data);
    }

    public static Result success() {
        return success(null);
    }

    public static Result buildErrorResult(ErrorInfo info) {
        Meta meta = new Meta(info.getErrorCode(), info.getErrorMessage());
        return new Result(meta);
    }

}
