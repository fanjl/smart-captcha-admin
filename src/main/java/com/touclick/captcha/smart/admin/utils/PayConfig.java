package com.touclick.captcha.smart.admin.utils;

public class PayConfig {
	protected static final String ALIPAY_COMPANY_ACCOUNT="touclick@163.com";
	protected static final String ALIPAY_COMPANY_NAME="杭州微触科技有限公司";
	/**
	 * 向网站主支付广告费用的通知地址
	 */
	protected static final String NOTIFY_URL="121.40.85.118/admin/pay/ali_notify";
	/**
	 * 购买图片支付的通知地址
	 */
	protected static final String PAY_NOTIFY_URL="http://121.40.85.118/admin/pay/pic_ali_notify";
	
	
//	protected static final String PAY_USER_NOTIFY_URL="http://121.40.85.118:8888/buy/user_ali_notify";
	
//	protected static final String PAY_USER_RETURN_URL="http://121.40.85.118:8888/member/index";
	
	protected static final String PAY_USER_NOTIFY_URL="http://admin.touclick.com/buy/user_ali_notify";
	    
	protected static final String PAY_USER_RETURN_URL="http://admin.touclick.com/member/index";
	
	
	protected static final String RETURN_URL="http://121.40.85.118/admin/pay/return_url";
	
}
