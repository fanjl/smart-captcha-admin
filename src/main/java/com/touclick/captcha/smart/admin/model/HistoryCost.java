package com.touclick.captcha.smart.admin.model;

import java.math.BigDecimal;
import java.util.Date;

public class HistoryCost {
	private int mid;
	private int aid;
	private int wid;
	private BigDecimal cost;
	private Date time;
	private int unit;
	private int drawflag=1;//提现标志1、未提现0、已提现
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	public BigDecimal getCost() {
		return cost;
	}
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public int getUnit() {
		return unit;
	}
	public void setUnit(int unit) {
		this.unit = unit;
	}
	public int getDrawflag() {
		return drawflag;
	}
	public void setDrawflag(int drawflag) {
		this.drawflag = drawflag;
	}
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	
}
