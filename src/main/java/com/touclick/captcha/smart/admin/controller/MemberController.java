package com.touclick.captcha.smart.admin.controller;

import com.google.gson.Gson;
import com.touclick.captcha.exception.TouclickException;
import com.touclick.captcha.model.Status;
import com.touclick.captcha.service.TouClick;
import com.touclick.captcha.smart.admin.auth.UserContext;
import com.touclick.captcha.smart.admin.config.JwtConfig;
import com.touclick.captcha.smart.admin.model.*;
import com.touclick.captcha.smart.admin.model.result.ErrorInfo;
import com.touclick.captcha.smart.admin.model.result.Result;
import com.touclick.captcha.smart.admin.service.inter.IData;
import com.touclick.captcha.smart.admin.service.inter.IMember;
import com.touclick.captcha.smart.admin.service.inter.IWebSite;
import com.touclick.captcha.smart.admin.utils.DateUtils;
import com.touclick.captcha.smart.admin.utils.ResultUtil;
import com.touclick.captcha.smart.admin.utils.VerifyWebsiteUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/member")
public class MemberController {
    private static final String PUBKEY = "a2b659ad-75e4-4e94-9979-c84ee2b3b30a";// 公钥(从点触官网获取)
    private static final String PRIKEY = "1d72ab4f-6dfc-4f11-82c7-a5ea3058a93b";// 私钥(从点触官网获取)
    @Autowired
    private IData iData;
    @Autowired
    private IMember iMember;
    @Autowired
    private IWebSite iWebsite;

    @Autowired
    private JwtConfig jwtConfig;
//    @Autowired
//    private MemberLevelCacheService iMemberLevelService;

    private TouClick touclick = new TouClick();

//    private IWebSiteInfo iWebSiteInfo = new WebSiteInfoRedis();

    private static final String VERSION = "5.2.0";// 不可修改

    Logger logger = Logger.getLogger(MemberController.class);

    Gson gson = new Gson();
    
    @RequestMapping(value = "websitelogin")
    @ResponseBody
    public int websitelogin(int wid, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Member m = iMember.getMemberByWid(wid);
        session.setAttribute("member", m);
        if (m != null) {
            return 1;
        } else {
            return 2;
        }
    }

    @ResponseBody
    @RequestMapping(value = "login")
    public Result login(Member member, HttpServletRequest request) {

        HttpSession session = request.getSession();
        String token = request.getParameter("token");
        String checkAddress = request.getParameter("checkAddress");
        String sid = request.getParameter("sid");
        try {
            Status status = touclick.check(checkAddress, sid, token, PUBKEY, PRIKEY);
            if (status != null && status.getCode() == 0) {
                Member m = iMember.checkLogin(member);
                // 账号存在
                if (m != null) {
                    // 校验密码
                    if (m.getPassword().equals(member.getPassword())) {
                        session.setAttribute("member", m);
                        Claims claims = Jwts.claims().setSubject(m.getEmail());
                        List<GrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority(UserRoleEnum.fromValue(m.getRole()).name()));
                        claims.put("scopes", authorities.stream().map(s -> s.toString()).collect(Collectors.toList()));
                        claims.put("id", m.getMid());
                        LocalDateTime currentTime = LocalDateTime.now();
                        String tokenJwt = Jwts.builder()
                                    .setClaims(claims)
                                    .setIssuer(jwtConfig.getTokenIssuer())
                                    .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant()))
                                    .setExpiration(Date.from(currentTime.plusMinutes(jwtConfig.getTokenExpirationTime()).atZone(ZoneId.systemDefault()).toInstant()))
                                    .signWith(SignatureAlgorithm.HS512, jwtConfig.getTokenSigningKey())
                                    .compact();
                        logger.info("check success");
                        m.setToken(tokenJwt);
                        return ResultUtil.success(m);
                    }else{
                        logger.info(m.getMid()+":login failed");
                    }
                } else {
                    return ResultUtil.buildErrorResult(ErrorInfo.PASSWORD_ERROR);
                }
            }
        } catch (TouclickException e1) {
            logger.error(e1.getMessage(), e1);
        }
        return ResultUtil.buildErrorResult(ErrorInfo.SERVER_ERROR);
    }

    @ResponseBody
    @RequestMapping("loginBySiteKey")
    public int loginBySiteKey(String key, String value, HttpSession session) {
        Member m = iMember.checkLoginBySiteKey(key, value);
        if (m != null) {
            session.setAttribute("member", m);
            return 1;
        } else {
            return 2;
        }
    }

    @RequestMapping(value = "logout")
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("member");
        return "redirect:/login.html";
    }

    @RequestMapping(value = "index")
    public String index(HttpServletRequest request, HttpServletResponse response, Model model,@AuthenticationPrincipal UserContext userContext) {
        HttpSession session = request.getSession();
        Member m = iMember.getMember(userContext.getUserId());
        if (m != null) {
            boolean oldversion = false;
            String pubkey = "";
            String token = "";
            List<WebSite> websiteList = iWebsite.getWebSiteList(m.getMid());
            for (WebSite ws : websiteList) {
                String version = ws.getCversion().substring(0, 1);
                if (Character.isDigit(version.charAt(0))) {
                    if (Integer.parseInt(version) < 5) {
                        pubkey = ws.getPubkey();
                        token = ws.getToken();
                        oldversion = true;
                        break;
                    }
                } else {
                    pubkey = ws.getPubkey();
                    token = ws.getToken();
                    oldversion = true;
                    break;
                }
            }
            if (oldversion) {
                StringBuilder sb = new StringBuilder();
                sb.append("http://admin-old.touclick.com/login.html?tem=91&way=sitekey&key=");
                sb.append(pubkey);
                sb.append("&value=");
                sb.append(token);
                return "redirect:" + sb.toString();
            } else {
                logger.info("go member index");
                return "member/index";
            }
        } else {
            return "redirect:/login.html";
        }
    }

    @RequestMapping(value = "home")
    public String home(HttpServletRequest request, Model model,@AuthenticationPrincipal UserContext userContext) {
        Member m = iMember.getMember(userContext.getUserId());
        Gson gson = new Gson();
        if (m != null) {
            if (m.getRole() == 1) {
                List<WebSite> websiteList = iWebsite.getWebSiteList(m.getMid());
                model.addAttribute("sv", 0);
                if (websiteList != null && websiteList.size() > 0) {
                    model.addAttribute("websiteList", websiteList);
                    model.addAttribute("jsondaydata",
                            gson.toJson(iData.getDataByDateRange(websiteList.get(0).getWid(), 0, 0)));
                    model.addAttribute(
                            "jsonmonthdata",
                            gson.toJson(iData.getDataByDateRangeMonth(websiteList.get(0).getWid(),
                                    DateUtils.getFirstDayOfMonth(), DateUtils.getLastDayOfMonth())));
                    model.addAttribute(
                            "jsonyeardata",
                            gson.toJson(iData.getDataByDateRangeYear(websiteList.get(0).getWid(),
                                    DateUtils.getFirstMonth(), 0)));
                }
                return "member/websiter";
            } else if (m.getRole() == 2) {
                // model.addAttribute("daydata", this.getAdDateData("day"));
                // model.addAttribute("monthdata", this.getAdDateData("month"));
                // model.addAttribute("yeardata", this.getAdDateData("year"));
                return "member/advertiser";
            } else {
                // model.addAttribute("list", iMoney.getCheckList());
                return "redirect:/admin/alldata";
            }
        } else {
            return "redirect:/login.html";
        }
    }

    // @RequestMapping(value = "active")
    // public String activeMember( HttpServletRequest request, Model model) {
    // String token = request.getParameter("token");
    // Member member = iMember.getMemberByToken(token);
    // if(member == null){
    // return "member/error";
    // }else{
    // Date updateTime = member.getUpdatetime();
    // if(DateUtils.caculaterDay(updateTime.getTime(),new Date().getTime()) >
    // 2){
    // //ToDo 重发激活邮件，更新token
    // member.setToken(UUID.randomUUID().toString());
    // iMember.updateToken(member);
    // String result = SendCloudUtils.sendActiveMail(member.getToken(),
    // member.getEmail(), member.getRealname());
    // logger.info(result);
    // return "redirect:/linkexpiration.html";
    // }else{
    // member.setStatus(1);
    // iMember.updateStatus(member);
    // HttpSession session = request.getSession();
    // session.setAttribute("member", member);
    // return "member/index";
    // }
    // }
    // }
    // @ResponseBody
    // @RequestMapping(value = "resendEmail")
    // public String reactiveMember( HttpServletRequest request, Model model) {
    // String email = request.getParameter("email");
    // String token = request.getParameter("token");
    // Member member = iMember.getMemberByToken(token);
    // if(member == null){
    // return "fail";
    // }else{
    // member.setToken(UUID.randomUUID().toString());
    // iMember.updateToken(member);
    // SendCloudUtils.sendActiveMail(member.getToken(), member.getEmail() ,
    // member.getRealname());
    // }
    // return "success";
    // }
    @ResponseBody
    @RequestMapping(value = "regist")
    @Transactional(rollbackFor = RuntimeException.class)
    public String regist(HttpServletRequest request, HttpServletResponse response) {
        // touclick 验证
        String token = request.getParameter("token");
        String checkAddress = request.getParameter("checkAddress");
        String sid = request.getParameter("sid");
        Map<String, Object> message = new HashMap<String, Object>();
        try {
            Status status = touclick.check(checkAddress, sid, token, PUBKEY, PRIKEY);
            if (status != null && status.getCode() == 0) {
                // --------新增用户信息------------
                Member member = new Member();
                member.setEmail(request.getParameter("email"));
                member.setPassword(request.getParameter("password"));
                member.setQq(request.getParameter("qq"));
                member.setTel(request.getParameter("tel"));
                member.setRealname(request.getParameter("realname"));
                member.setRole(Integer.parseInt(request.getParameter("role")));
                member.setCreatetime(new Date());
                member.setLevel(0);
                // member.setToken(UUID.randomUUID().toString());
                // member.setStatus(0);
                iMember.add(member);

                // 激活邮箱
                // String result =
                // SendCloudUtils.sendActiveMail(member.getToken(),
                // member.getEmail(), member.getRealname());
                // logger.info(result);
                // Member m =
                // iMember.getMemberByWaddress(request.getParameter("waddress"));
                Member m = iMember.getMemberByEmail(request.getParameter("email"));
//                iMemberLevelService.putMemberLevel(m.getMid(), 0);
                // --------新增网站---------------
                WebSite website = new WebSite();
                website.setWname(request.getParameter("wname"));
                website.setWaddress(request.getParameter("waddress"));
                website.setWtype(request.getParameter("wtype"));
                website.setPlatform(request.getParameter("platform"));
                website.setVerifyflag(1);
                website.setCtype(13);
                website.setCtypeplus("1");
                website.setSkin("white");
                // if("api".equals(website.getPlatform())){
                // website.setCversion("v2-2");
                // }else if("discuz".equals(website.getPlatform())){
                // website.setCversion("v3-0");
                // }
                website.setCversion(VERSION);
                website.setPubkey(UUID.randomUUID().toString());
                website.setToken(UUID.randomUUID().toString());
                website.setCreatetime(new Date());
                website.setMid(member.getMid());
                iWebsite.addWebSite(website);
                // 缓存网站和验证码类型的关系
//                iWebSiteInfo.setCTypeWithWid(website.getCtype() + "", website.getWid() + "");
//                iWebSiteInfo.setDefaultCTypeByWid(website.getWid() + "", website.getCtype() + "");
//                iWebSiteInfo.setWidWithPubkey(website.getWid() + "", website.getPubkey());
//                iWebSiteInfo.setPrivateKeyByWid(website.getWid() + "", website.getToken());
                // --------网站其他信息----------
                WebSiteInfo wsi = new WebSiteInfo();
                wsi.setWid(website.getWid());
                wsi.setType(WebSiteInfoEnum.oldCtype.getIndex());
                wsi.setValue("1,3");
                iWebsite.addWebSiteInfo(wsi);
                // --------用户免费试用添加historypay过期期限----------
//                iBan.saveTrial(member);
                
                message.put("isok", true);
                message.put("website_key", website.getPubkey());
                message.put("private_key", website.getToken());
                String json = gson.toJson(message);
                return json;
                // return
                // "redirect:/active.html?token="+member.getToken()+"&email="+member.getEmail();
            }
        } catch (TouclickException e1) {
            logger.error(e1.getMessage(), e1);
        }
        message.put("isok", false);
        String json = gson.toJson(message);
        return json;
    }

    @ResponseBody
    @RequestMapping(value = "regist_special")
    public String regist_special(HttpServletRequest request, HttpServletResponse response) {
        Member member = iMember.getMember(1);
        WebSite website = new WebSite();
        website.setWname(request.getParameter("wname"));
        website.setWaddress(request.getParameter("waddress"));
        website.setWtype(request.getParameter("wtype"));
        website.setPlatform(request.getParameter("platform"));
        website.setVerifyflag(Integer.parseInt(request.getParameter("verifyflag")));
        website.setPubkey(UUID.randomUUID().toString());
        website.setToken(UUID.randomUUID().toString());
        website.setCreatetime(new Date());
        website.setMid(member.getMid());
        iWebsite.addWebSite(website);
        Map<String, Object> message = new HashMap<String, Object>();
        message.put("isok", true);
        message.put("website_key", website.getPubkey());
        message.put("private_key", website.getToken());
        Gson gson = new Gson();
        String json = gson.toJson(message);
        return json;
    }

    class MysqlDataModel {
        public int mid;
        public String email;
        public String password;
        public String QQ;
        public String tel;
        public String realname;
        public int role = 1;
        public int wid;
        public String wname;
        public String waddress;
        public String wtype;
        public String platform;
        public String pubkey;
        public String token;
        public String ctype = "1";
        public String cversion;

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("mid=").append(mid);
            sb.append('&');
            sb.append("email=").append(email);
            sb.append('&');
            sb.append("password=").append(password);
            sb.append('&');
            sb.append("QQ=").append(QQ);
            sb.append('&');
            sb.append("tel=").append(tel);
            sb.append('&');
            sb.append("realname=").append(realname);
            sb.append('&');
            sb.append("role=").append(role);
            sb.append('&');
            sb.append("wid=").append(wid);
            sb.append('&');
            sb.append("wname=").append(wname);
            sb.append('&');
            sb.append("waddress=").append(waddress);
            sb.append('&');
            sb.append("wtype=").append(wtype);
            sb.append('&');
            sb.append("platform=").append(platform);
            sb.append('&');
            sb.append("pubkey=").append(pubkey);
            sb.append('&');
            sb.append("token=").append(token);
            sb.append('&');
            sb.append("ctype=").append(ctype);
            sb.append('&');
            sb.append("cversion=").append(cversion);
            return sb.toString();
        }
    }

    @ResponseBody
    @RequestMapping(value = "registtemp")
    public boolean registtemp(HttpServletRequest request, HttpServletResponse response) {
        logger.info("-----------begin--------------------");
        BufferedReader br;
        String s = "";
        try {
            br = new BufferedReader(new InputStreamReader(request.getInputStream(), "utf8"));
            s = br.readLine();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        String[] parar = s.split("\\&");
        MysqlDataModel model = new MysqlDataModel();

        // MysqlDataModel.class.getf
        for (String item : parar) {
            String[] tmp = item.split("\\=");
            if (tmp.length != 2) {
                System.out.println("tmp.length != 2");
                continue;
            }
            java.lang.reflect.Field field = null;
            try {
                field = model.getClass().getField(tmp[0]);
            } catch (NoSuchFieldException e1) {
                e1.printStackTrace();
            } catch (SecurityException e1) {
                e1.printStackTrace();
            }
            try {
                if (field.getType().equals(String.class)) {
                    field.set(model, tmp[1]);
                } else if (field.getType().equals(int.class)) {
                    field.setInt(model, Integer.parseInt(tmp[1]));
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        Member member = new Member();
        member.setMid(model.mid);
        member.setEmail(model.email);
        member.setPassword(model.password);
        member.setQq(model.QQ);
        member.setTel(model.tel);
        member.setRealname(model.realname);
        member.setRole(model.role);
        member.setCreatetime(new Date());

        WebSite website = new WebSite();
        website.setWid(model.wid);
        website.setWname(model.wname);
        website.setWaddress(model.waddress);
        website.setVerifyflag(1);
        website.setWtype(model.wtype);
        website.setPlatform(model.platform);
        String old_ctype = model.ctype;
        int o_ctype = 0;
        if (!StringUtils.isEmpty(old_ctype)) {
            List<String> ctypelist = Arrays.asList(old_ctype.split(","));
            logger.info(Arrays.toString(old_ctype.split(",")));
            if (ctypelist.contains("1") || ctypelist.contains("3")) {
                o_ctype = CTypeEnum.Click.getId();
            } else if (ctypelist.contains("4")) {
                o_ctype = CTypeEnum.Block.getId();
            } else if (ctypelist.contains("7")) {
                o_ctype = CTypeEnum.Drag.getId();
            }
            website.setCtype(o_ctype);
            website.setCtypeplus(o_ctype + "");
        } else {
            logger.info("-----------ctype is empty------------");
            return false;
        }
        website.setCversion(model.cversion);
        website.setPubkey(model.pubkey);
        website.setToken(model.token);
        website.setCreatetime(new Date());
        website.setMid(model.mid);

        WebSiteInfo wsi = new WebSiteInfo();
        wsi.setWid(website.getWid());
        wsi.setType(WebSiteInfoEnum.oldCtype.getIndex());
        wsi.setValue(old_ctype);
        try {
            iMember.addTemp(member);
            iWebsite.addWebSiteTemp(website);
            iWebsite.addWebSiteInfo(wsi);
            return false;
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        // CacheParam.WEBSITEMAP.put(website.getPubkey(), website);
        // ConcurrentHashMap<Integer, String> infomap = new
        // ConcurrentHashMap<Integer,String>();
        // infomap.put(Integer.valueOf(WebSiteInfoEnum.oldCtype.getIndex()),
        // old_ctype);
        // CacheParam.WEBSITEINFOMAP.put(Integer.valueOf(website.getWid()),
        // infomap);

        // updateWebsiteZKCache(website.getPubkey(), s);
        logger.info("-----------end--------------------");
        return true;
    }

    @RequestMapping(value = "updateMember")
    public String updateMember(HttpSession session, Model model) {
        model.addAttribute("member", session.getAttribute("member"));
        return "member/updateinfo";
    }

    @ResponseBody
    @RequestMapping(value = "updatepwd")
    public String updatePwd(HttpServletRequest request) {
        Member m = (Member) request.getSession().getAttribute("member");
        String oldpwd = request.getParameter("oldpwd");
        String newpwd = request.getParameter("newpwd");
        if (iMember.checkOldpwd(oldpwd, m.getMid())) {
            iMember.updateNewpwd(newpwd, m.getMid());
            return "true";
        } else {
            return "false";
        }
    }

    @ResponseBody
    @RequestMapping(value = "updateinfo")
    public String updateinfo(HttpServletRequest request) {
        Member m = (Member) request.getSession().getAttribute("member");
        m.setRealname(request.getParameter("realname"));
        m.setQq(request.getParameter("qq"));
        m.setTel(request.getParameter("tel"));
        iMember.updateInfo(m);
        return "true";
    }

    @ResponseBody
    @RequestMapping(value = "existEmail")
    public boolean existEmail(String email) {
        boolean b = iMember.existEmail(email);
        return b;
    }

    @ResponseBody
    @RequestMapping(value = "existWebSite")
    public boolean existWebSite(String waddress, HttpServletRequest request) {
        Member m = (Member) request.getSession().getAttribute("member");
        if (m != null) {
            // 如果自己拥有的网站中不存在，则去查看全部网站中是否存在
            boolean b = iWebsite.existWebSiteByMid(waddress, m.getMid());
            if (b) {
                return !b;
            } else {
                boolean a = iWebsite.existWebSite(waddress);
                return a;
            }
        } else {
            return iWebsite.existWebSite(waddress);
        }
    }

    @ResponseBody
    @RequestMapping(value = "existInviteCode")
    public String existInviteCode(String invite_code) {
        boolean b = iMember.existInviteCode(invite_code);
        return "{\"isexist\":" + b + "}";
    }

    @ResponseBody
    @RequestMapping(value = "existEmailByAddress")
    public boolean existEmailByAddress(String email, String waddress) {
        boolean b = iMember.existEmailByAddress(email, waddress);
        return b;
    }

    @RequestMapping(value = "downloadVerify")
    public ResponseEntity<String> download(String waddress, HttpServletResponse res) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", "verify.html");
        return new ResponseEntity<String>(DigestUtils.md5Hex(waddress + DateUtils.today()), headers, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "verify")
    public boolean verify(String waddress) {
        return VerifyWebsiteUtils.verifyByWaddress(waddress);
    }

    @ResponseBody
    @RequestMapping(value = "getEmailByWaddress")
    public String getEmailByWaddress(String waddress) {
        Member m = iMember.getMemberByWaddress(waddress);
        // 无此网站
        if (m == null) {
            return "false";
        }
        String body = "";
        if (m.getEmail().indexOf("@") != -1) {
            body = m.getEmail().substring(0, m.getEmail().indexOf("@"));
        }
        if (body.length() > 4) {
            return m.getEmail().substring(0, body.length() - 4) + "****"
                    + m.getEmail().substring(m.getEmail().indexOf("@"), m.getEmail().length());
        }
        return m.getEmail();
    }

    @ResponseBody
    @RequestMapping(value = "resetPassEmailByWaddress")
    public boolean resetPassEmailByWaddress(String email, String password, String waddress) {
        boolean verifyResult = VerifyWebsiteUtils.verifyByWaddress(waddress);
        if (verifyResult) {
            int result = iMember.resetPassEmailByWaddress(email, password, waddress);
            if (result > 0) {
                return true;
            }
        }
        return false;
    }

    public static String replaceIndex(int index, String res) {
        return res.substring(index + 1) + ":00";
    }
}
