package com.touclick.captcha.smart.admin.mapper.secondary.provider;

import java.util.Map;

public class MoneyProvider {
	public String getEarnings(Map<String,Object> paras){
		return "select SUM(t.cost) from history_cost t where t.drawflag=1 and t.mid=#{mid} group by t.mid";
	}
	public String getHistoryCost(Map<String,Object> paras){
		return "select * from history_cost t where t.mid=#{mid} and t.drawflag=1 order by t.time";
	}
	public String getHsitoryBill(Map<String,Object> paras){
		return "select * from history_cost t where t.mid=#{mid} order by t.time";
	}
	public String getDrawMoneyRecord(Map<String,Object> paras){
		return "select * from draw_money t where t.mid=#{mid} and t.drawflag=1 order by t.createtime";
	}
	public String getCountMoneyByDateRange(Map<String,Object> paras){
		return "select sum(t.cost) from history_cost t where t.drawflag=1 and t.mid=#{mid} and t.time BETWEEN #{starttime} and #{endtime} GROUP BY t.mid";
	}
	public String updateDrawflag(Map<String,Object> paras){
		return "update history_cost t set t.drawflag=0 where t.mid=#{mid} and t.drawflag=1 and t.time BETWEEN #{starttime} and #{endtime}";
	}
	public String insertDrawMoneyRecord(Map<String,Object> paras){
		return "insert into draw_money(mid,createtime,money,drawflag,starttime,endtime) values(#{mid},CURRENT_TIMESTAMP,#{money},2,#{starttime},#{endtime})";
	}
	public String updateAllDrawflag(Map<String,Object> paras){
		return "update history_cost t set t.drawflag=0 where t.mid=#{mid} and t.drawflag=1";
	}
	public String insertAllDrawMoneyRecord(Map<String,Object> paras){
		return "insert into draw_money(mid,createtime,money,drawflag,starttime,endtime) values(#{mid},CURRENT_TIMESTAMP,#{money},2,(select min(t.time) from history_cost t where t.mid=#{mid} and t.drawflag=1),(select max(t.time) from history_cost t where t.mid=#{mid} and t.drawflag=1))";
	}
	public String getCheckList(){
		return "select t.drawid,t.mid,t1.realname,t.money,t.createtime,t.drawflag from draw_money t left join member t1 on t.mid=t1.mid";
	}
	public String saveEarnings(Map<String,Object> paras){
		return "insert into history_cost(mid,wid,cost,time,unit,drawflag) values(#{mid},#{wid},#{earnings},CURRENT_TIMESTAMP,#{unit},1)";
	}
}
