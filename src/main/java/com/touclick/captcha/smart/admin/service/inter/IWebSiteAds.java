package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.WebSiteAds;

import java.util.List;

public interface IWebSiteAds {
	public List<Integer> getWebSiteAdsByWid(int wid, int type);
	
	public void save(WebSiteAds wsa);
}
