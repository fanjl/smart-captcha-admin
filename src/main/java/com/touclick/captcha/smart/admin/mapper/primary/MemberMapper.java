package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.MemberProvider;
import com.touclick.captcha.smart.admin.model.Member;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;

import java.util.List;
@Mapper
public interface MemberMapper {
    @InsertProvider(type = MemberProvider.class, method = "addMember")
    @Options(flushCache = Options.FlushCachePolicy.TRUE, statementType = StatementType.PREPARED, useGeneratedKeys = true, keyProperty = "member.mid", keyColumn = "member.mid")
    public int add(@Param(value = "member") Member member);

    @InsertProvider(type = MemberProvider.class, method = "addMemberTemp")
    @Options(flushCache = Options.FlushCachePolicy.TRUE, statementType = StatementType.PREPARED, useGeneratedKeys = true, keyProperty = "member.mid", keyColumn = "member.mid")
    public int addTemp(@Param(value = "member") Member member);

    @InsertProvider(type = MemberProvider.class, method = "addMemberInfo")
    public void addMemberInfo(@Param(value = "mid") int mid, @Param(value = "type") int type,
                              @Param(value = "value") String value);

    @SelectProvider(type = MemberProvider.class, method = "checkLogin")
    public Member checkLogin(@Param(value = "member") Member member);

    @SelectProvider(type = MemberProvider.class, method = "existEmail")
    public Member existEmail(@Param(value = "email") String email);

    @SelectProvider(type = MemberProvider.class, method = "existEmailByAddress")
    public Member existEmailByAddress(@Param(value = "email") String email, @Param(value = "waddress") String waddress);

    @SelectProvider(type = MemberProvider.class, method = "getWidByMid")
    public int getWidByMid(@Param(value = "mid") int mid);

    @SelectProvider(type = MemberProvider.class, method = "getPwdByMid")
    public String getPwdByMid(@Param(value = "mid") int mid);

    @UpdateProvider(type = MemberProvider.class, method = "updateNewpwd")
    public void updateNewpwd(@Param(value = "newpwd") String newpwd, @Param(value = "mid") int mid);

    @UpdateProvider(type = MemberProvider.class, method = "updateNewpwdByEmail")
    public int updateNewpwdByEmail(@Param(value = "newpwd") String newpwd, @Param(value = "email") String email);

    @UpdateProvider(type = MemberProvider.class, method = "updateLevel")
    public Integer updateLevel(@Param(value = "level") int level, @Param(value = "mid") int mid);

    @UpdateProvider(type = MemberProvider.class, method = "updateInfo")
    public void updateInfo(@Param(value = "member") Member member);
    
    @UpdateProvider(type = MemberProvider.class, method = "updateStatus")
    public void updateStatus(@Param(value = "member") Member member);
    
    @UpdateProvider(type = MemberProvider.class, method = "updateToken")
    public void updateToken(@Param(value = "member") Member member);
    
    @SelectProvider(type = MemberProvider.class, method = "getAllMember")
    public List<Member> getAllMember();
    
    @SelectProvider(type = MemberProvider.class, method = "getVipMember")
    public List<Member> getVipMember();
    
    @SelectProvider(type = MemberProvider.class, method = "getMember")
    public Member getMember(@Param(value = "mid") int mid);

    @SelectProvider(type = MemberProvider.class, method = "checkLoginBySiteKey")
    public Member checkLoginBySiteKey(@Param(value = "pubkey") String pubkey, @Param("token") String token);

    @SelectProvider(type = MemberProvider.class, method = "getMemberByWaddress")
    public Member getMemberByWaddress(@Param(value = "waddress") String waddress);

    @SelectProvider(type = MemberProvider.class, method = "getMemberByToken")
    public Member getMemberByToken(@Param(value = "token") String token);

    @SelectProvider(type = MemberProvider.class, method = "getMemberByEmail")
    public Member getMemberByEmail(@Param(value = "email") String email);

    @UpdateProvider(type = MemberProvider.class, method = "resetPassEmailByWaddress")
    public int resetPassEmailByWaddress(@Param(value = "email") String email,
                                        @Param(value = "password") String password, @Param(value = "waddress") String address);

    @SelectProvider(type = MemberProvider.class, method = "getMemberByWid")
    public Member getMemberByWid(@Param(value = "wid") int wid);
    
    @SelectProvider(type = MemberProvider.class, method = "getMemberListByCreateTime")
    public List<Member> getMemberListByCreateTime(@Param(value = "start") String start, @Param(value = "end") String end);
    
}
