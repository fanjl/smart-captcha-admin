package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.VipPictureProvider;
import com.touclick.captcha.smart.admin.model.VipPicture;
import com.touclick.captcha.smart.admin.model.VipPictureClass;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface VipPictureMapper {
	@SelectProvider(type=VipPictureProvider.class,method="getPictureClassByType")
	public List<VipPictureClass> getPictureClassByType(@Param(value = "pid") int pid);
	
	@SelectProvider(type=VipPictureProvider.class,method="getVipPictureByClass")
	public List<VipPicture> getVipPictureByClass(@Param(value = "vid") int vid);
	
	@SelectProvider(type=VipPictureProvider.class,method="getObjecgByVid")
	public VipPictureClass getObjecgByVid(@Param(value = "vid") int vid);
	
	@SelectProvider(type=VipPictureProvider.class,method="getBypicReady")
	public List<VipPictureClass> getBypicReady(@Param(value = "pid") int pid, @Param(value = "mid") int mid);
}
