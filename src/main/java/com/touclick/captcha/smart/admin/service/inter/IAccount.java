package com.touclick.captcha.smart.admin.service.inter;


import com.touclick.captcha.smart.admin.model.MemberInfo;

import java.util.List;

public interface IAccount {
	/**
	 * 获取拥有账号列表
	 * @param mid
	 * @return
	 */
	public List<MemberInfo> getAccountList(int mid);
	/**
	 * 检查是否有重复的收款账号
	 * @param accounttype
	 * @param account
	 * @return
	 */
	public boolean checkAccount(int accounttype, String account);
	/**
	 * 新增收款账号
	 * @param mid
	 * @param accounttype
	 * @param account
	 * @return
	 */
	public int saveAccount(int mid, int accounttype, String account);
	/**
	 * 删除账号
	 * @param id
	 * @return
	 */
	public int delAccount(int id);
	/**
	 * 默认使用账号
	 * @param mid
	 * @param accountid
	 */
	public void defaultAccount(int mid, int accountid);
}
