package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.WebSiteGroupProvider;
import com.touclick.captcha.smart.admin.model.WebSiteGroup;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface WebSiteGroupMapper {
	@SelectProvider(type = WebSiteGroupProvider.class,method = "getWebSiteGroupListByWid")
	public List<WebSiteGroup> getWebSiteGroupListByWid(@Param(value = "wid") int wid);
	
	@SelectProvider(type = WebSiteGroupProvider.class,method = "getGidByWidAndType")
	public List<Integer> getGidByWidAndType(@Param(value = "wid") int wid, @Param(value = "type") int type);
	
	@InsertProvider(type=WebSiteGroupProvider.class,method="save")
	public int save(@Param("wsg") WebSiteGroup wsg);
	
	@DeleteProvider(type=WebSiteGroupProvider.class,method="delete")
	public int delete(@Param("wsg") WebSiteGroup wsg);
}
