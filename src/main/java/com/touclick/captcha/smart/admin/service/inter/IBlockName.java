package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.BlockName;

import java.util.List;

/**
 * BlockName service interface
 *
 * @author bing.liu
 * @date 2015-09-21
 */
public interface IBlockName {

    /**
     * Insert a blockname into the database
     * @param blockName
     */
    public void insert(BlockName blockName);

    /**
     * Select all the blockname
     * @return
     */
    public List<BlockName> findAll();

    /**
     * Find by name
     * @param name
     * @return
     */
    public BlockName findByName(String name);
}
