package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.TestProvider;
import com.touclick.captcha.smart.admin.model.VipPicture;
import com.touclick.captcha.smart.admin.model.VipPictureClass;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;

public interface TestMapper {
	@InsertProvider(type = TestProvider.class,method = "insertVipPictureClass")
	public void insertVipPictureClass(@Param("vpt") VipPictureClass vpt);
	
	@InsertProvider(type = TestProvider.class,method = "insertVipPicture")
	public void insertVipPicture(@Param("vp") VipPicture vp);
}
