package com.touclick.captcha.smart.admin.model;

/**
 * Created by foxer on 2016/10/17.
 */
public enum GroupTypeEnum {
    ADMIN(1),//表示管理员上传

    WEBSITE(2);//表示网站自定义

    private int id;

    GroupTypeEnum(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }
}
