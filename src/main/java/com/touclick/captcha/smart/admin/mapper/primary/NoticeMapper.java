package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.NoticeProvider;
import com.touclick.captcha.smart.admin.model.Notice;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import java.util.List;

public interface NoticeMapper {
	@SelectProvider(type=NoticeProvider.class,method="getLastNotice")
	public Notice getLastNotice();
	
	@SelectProvider(type=NoticeProvider.class,method="noticeList")
	public List<Notice> noticeList();
	
	@InsertProvider(type=NoticeProvider.class,method="save")
	public void save(@Param(value = "notice") Notice notice);
	
	@UpdateProvider(type=NoticeProvider.class,method="del")
	public void del(@Param(value = "nid") int nid);
}
