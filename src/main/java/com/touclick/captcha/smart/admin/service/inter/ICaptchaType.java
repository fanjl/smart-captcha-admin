package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.CaptchaType;

import java.util.List;

/**
 * @author bing.liu
 */
public interface ICaptchaType {
    /**
     * Insert
     * @param type
     */
    public void insert(CaptchaType type);

    /**
     * Select a type by id
     * @param id
     * @return
     */
    public CaptchaType selectById(int id);

    /**
     * Select all captcha type
     * @return
     */
    public List<CaptchaType> selectAll();

    /**
     * Select a captcha type by name
     * @param name
     * @return
     */
    public CaptchaType selectByName(String name);
}
