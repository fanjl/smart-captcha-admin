package com.touclick.captcha.smart.admin.model;

import org.apache.commons.codec.binary.Base64;

public class VipPictureClass {
	private int vid;
	private String name;
	private byte[] backgroud;
	private String backgroudbase64;//背景图的base64编码
	private int count_picture;
	private float unit_price;
	private int count_used;
	private int pid;
	private String endtime;
	public int getVid() {
		return vid;
	}
	public void setVid(int vid) {
		this.vid = vid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getUnit_price() {
		return unit_price;
	}
	public void setUnit_price(float unit_price) {
		this.unit_price = unit_price;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public byte[] getBackgroud() {
		return backgroud;
	}
	public void setBackgroud(byte[] backgroud) {
		this.backgroud = backgroud;
	}
	public String getBackgroudbase64() {
		this.backgroudbase64 = new String(Base64.encodeBase64(backgroud));
		return backgroudbase64;
	}

	public void setBackgroudbase64(String backgroudbase64) {
		this.backgroudbase64 = backgroudbase64;
	}
	public int getCount_picture() {
		return count_picture;
	}
	public void setCount_picture(int count_picture) {
		this.count_picture = count_picture;
	}
	public int getCount_used() {
		return count_used;
	}
	public void setCount_used(int count_used) {
		this.count_used = count_used;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	
}
 