package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.GroupAds;

import java.util.List;

public interface IGroupAds {
	public List<GroupAds> findAll();
	
	public List<Integer> findByGid(int gid);
	
	public void save(GroupAds ga);
}
