package com.touclick.captcha.smart.admin.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * Created by fanjl on 2017/2/15.
 */
@Configuration
@MapperScan(basePackages = AdminDataConfig.PACKAGE, sqlSessionFactoryRef = "adminDataSqlSessionFactory")
public class AdminDataConfig {

    static final String PACKAGE = "com.touclick.captcha.smart.admin.mapper.secondary";


    @Bean(name = "adminDataDataSource")
    @ConfigurationProperties(prefix = "datasource.secondary")
    public DataSource DataSourceSlave() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "adminDataTransactionManager")
    public DataSourceTransactionManager adminDataTransactionManager(@Qualifier("adminDataDataSource") DataSource adsDataSource) {
        return new DataSourceTransactionManager(adsDataSource);
    }

    @Bean(name = "adminDataSqlSessionFactory")
    public SqlSessionFactory adminDataSqlSessionFactory(@Qualifier("adminDataDataSource") DataSource adsDataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(adsDataSource);
        return sessionFactory.getObject();
    }
}
