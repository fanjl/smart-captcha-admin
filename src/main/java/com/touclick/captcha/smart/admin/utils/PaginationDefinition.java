package com.touclick.captcha.smart.admin.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;


/**
 * 分页器定义信息，如每页默认大小、转换为JSON时的JSON键名
 * 
 * @author wzj
 * @version v1.0
 */
public class PaginationDefinition implements InitializingBean {
	private static final Logger LOGGER = LoggerFactory.getLogger(com.touclick.captcha.smart.admin.utils.PaginationDefinition.class);
	// 每页默认记录条数
	private static int defaultPageSize;
	// 对应 "总记录数"的JSON键名
	private static String totalRecordJsonLable;
	// 对应 "记录"的JSON键名
	private static String recordsJsonLable;

	public static int getDefaultPageSize() {
		return defaultPageSize;
	}

	public void setDefaultPageSize(int defaultPageSize) {
		com.touclick.captcha.smart.admin.utils.PaginationDefinition.defaultPageSize = defaultPageSize;
	}

	public static String getTotalRecordJsonLable() {
		return totalRecordJsonLable;
	}

	public void setTotalRecordJsonLable(String totalRecordJsonLable) {
		com.touclick.captcha.smart.admin.utils.PaginationDefinition.totalRecordJsonLable = totalRecordJsonLable;
	}

	public static String getRecordsJsonLable() {
		return recordsJsonLable;
	}

	public void setRecordsJsonLable(String recordsJsonLable) {
		com.touclick.captcha.smart.admin.utils.PaginationDefinition.recordsJsonLable = recordsJsonLable;
	}
	
	@Override
    public void afterPropertiesSet() throws Exception {
		if(defaultPageSize <= 0){
			defaultPageSize = 20;
			LOGGER.warn("Property 'defaultPageSize' is not setted or setted wrong, " +
					"use default value 20.");
		}
		if(StringUtil.nullOrEmpty(totalRecordJsonLable)){
			totalRecordJsonLable = "Total";
			LOGGER.warn("Property 'totalRecordJsonLable' is not setted, use default value 'Total'.");
		}
		if(StringUtil.nullOrEmpty(recordsJsonLable)){
			recordsJsonLable = "Rows";
			LOGGER.warn("Property 'recordsJsonLable' is not setted, use default value 'Rows'.");
		}
	}
}
