package com.touclick.captcha.smart.admin.mapper.secondary.provider;

import com.touclick.captcha.smart.admin.model.WebSite;

import java.util.List;
import java.util.Map;

public class DataProvider {
	public String getTodaySv(Map<String,Object> paras){
		List<WebSite> list = (List<WebSite>)paras.get("list");
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT sum(t.count) FROM tall t where t.date/1000<UNIX_TIMESTAMP(curdate()+1)-1 and t.date/1000>UNIX_TIMESTAMP(curdate()) ");
		if(list!=null&&list.size()>0){
			String websites = "";
			for(int i=0;i<list.size();i++){
				if(i==list.size()-1){
					websites += list.get(i).getWid();
				}else{
					websites += list.get(i).getWid()+",";
				}
			}
			sb.append(" and t.wid in ("+websites+")");
			sb.append(" group by t. aid");
		}
		return sb.toString();
	}
	
	   public String getTotalGByWids(Map<String,Object> paras){
	        List<Integer> list = (List<Integer>)paras.get("wids");
	        StringBuilder sb = new StringBuilder();
	        sb.append("SELECT sum(t.count) FROM tall t where  ");
	        if(list!=null&&list.size()>0){
	            String websites = "";
	            for(int i=0;i<list.size();i++){
	                if(i==list.size()-1){
	                    websites += list.get(i);
	                }else{
	                    websites += list.get(i)+",";
	                }
	            }
	            sb.append("  t.wid in ("+websites+")");
	        }
	        return sb.toString();
	    }
	
	public String getDataSumByWidLtypeAndTime(Map<String,Object> paras){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT t.wid,t.date,t.ctype,t.ltype,t.module,sum(t.count) as count FROM tlast t where t.wid=#{wid}  ");
        sb.append(" and t.date>=#{begin} and t.date<=#{end}");
        sb.append(" and t.ltype=#{ltype}");
        sb.append(" group by t.date order by t.date");
        return sb.toString();
    }
	public String getDataByDateRange(Map<String,Object> paras){
		Long begin = (Long)paras.get("begin");
		Long end = (Long)paras.get("end");
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT t.aid,t.wid,t.date,t.ctype,t.ltype,t.module,sum(t.count) as count FROM tlast t where t.wid=#{wid}  ");
		if(begin!=null&&end!=null&&begin.longValue()>0&&end.longValue()>0){
			sb.append(" and t.date>#{begin} and t.date<#{end}");
		}else{
			sb.append(" and t.date>unix_timestamp(curdate())*1000");
		}
		sb.append(" group by t.ltype,t.date order by t.date");
		return sb.toString();
	}
	
	public String getDataByDateRangeWeek(Map<String,Object> paras){
        return "SELECT t.aid,t.wid,t.date,t.ctype,t.ltype,t.module,sum(t.count) as count FROM tlast t where t.wid=#{wid} and t.date>=#{begin} and t.date<=#{end} group by t.ltype,t.date order by t.date";
    }
	public String getDataByDateRangeMonth(Map<String,Object> paras){
		return "SELECT t.aid,t.wid,t.date,t.ctype,t.ltype,t.module,sum(t.count) as count FROM tall t where t.wid=#{wid} and t.date>=#{begin} and t.date<=#{end} group by t.ltype,t.date order by t.date";
	}
	public String getDataByDateRangeYear(Map<String,Object> paras){
		Long begin = (Long)paras.get("begin");
		Long end = (Long)paras.get("end");
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT t.aid,t.wid,t.date,t.ctype,t.ltype,t.module,sum(t.count) as count FROM tall t where t.wid=#{wid} ");
		if(begin!=null&&end!=null&&begin.longValue()>0&&end.longValue()>0){
			sb.append(" and t.date>#{begin} and t.date<#{end}");
		}else{
			sb.append(" and t.date>#{begin}");
		}
		sb.append(" group by t.ltype,FROM_UNIXTIME(t.date/1000,'%Y-%m') order by t.date");
		return sb.toString();
	}
	public String getYestodayAsv(Map<String,String> paras){
		return "SELECT sum(t.count) FROM tall t where t.date/1000<UNIX_TIMESTAMP(curdate())-1 and t.date/1000>UNIX_TIMESTAMP(curdate()-1) and t.aid in (#{ads}) and t.ltype=4";
	}
	
	public String getAllData(){
		return "SELECT t.wid,sum(t.count) as count,round(avg(t.count), 0) as daycount,round(avg(t.count)/24, 0) as hourcount FROM (SELECT t1.wid,sum(t1.count) AS count FROM tall t1 where t1.ltype = #{ltype} GROUP BY t1.wid,t1.date) AS t group by t.wid order by daycount desc";
	}
	
	public String getAvgHourDataByLtype(){
		return "select t.*,sum(t.count) as count from tlast t where t.wid=#{wid} and t.ltype=#{ltype} group by t.date";
	}
	public String getAvgDayDataByLtype(){
		return "select t.*,sum(t.count) as count from tall t where t.wid=#{wid} and t.ltype=#{ltype} group by t.date";
	}
}
