package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.AdsProvider;
import com.touclick.captcha.smart.admin.model.Ads;
import com.touclick.captcha.smart.admin.model.MemberGroup;
import com.touclick.captcha.smart.admin.utils.Pagination;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;

import java.util.List;

/**
 * Ads mapper
 *
 * @author bing.liu
 */
@Mapper
public interface AdsMapper {
    @InsertProvider(type = AdsProvider.class,method = "insert")
    @Options(flushCache=Options.FlushCachePolicy.TRUE,statementType= StatementType.PREPARED,useGeneratedKeys=true,keyProperty="ads.aid",keyColumn="ads.aid")
    public void insert(@Param(value = "ads") Ads ads);

    @SelectProvider(type = AdsProvider.class,method = "selectByAid")
    public Ads selectByAid(@Param(value = "aid") int aid);

    @SelectProvider(type = AdsProvider.class,method = "selectByAids")
    public List<Ads> selectByAids(@Param(value = "aids") String aids);

    @SelectProvider(type = AdsProvider.class,method = "selectByAtype")
    public List<Ads> selectByAtype(@Param(value = "atype") int atype);

    @SelectProvider(type = AdsProvider.class,method = "selectByCtype")
    public List<Ads> selectByCtype(@Param(value = "ctype") int ctype);

    @SelectProvider(type = AdsProvider.class,method = "selectByMid")
    public List<Ads> selectByMid(@Param(value = "mid") int mid);
    
    @SelectProvider(type = AdsProvider.class,method = "selectAll")
    public List<Ads> selectAll();
    
    @SelectProvider(type = AdsProvider.class,method = "getAidByMid")
    public List<Integer> getAidByMid(@Param(value = "mid") int mid);
    
    @SelectProvider(type = AdsProvider.class,method = "getAdsByMid")
    public List<Ads> getAdsByMid(@Param(value = "mid") int mid);
    
    @SelectProvider(type = AdsProvider.class,method = "getMemberGroupConfByAds")
    public List<MemberGroup> getMemberGroupConfByAds(@Param(value = "ads") String ads);
    
    @SelectProvider(type = AdsProvider.class,method = "getAidByGid")
    public List<Integer> getAidByGid(@Param(value = "gid") int gid);
    
    @SelectProvider(type = AdsProvider.class,method = "selectAllAid")
    public List<Integer> selectAllAid();
    
    @SelectProvider(type = AdsProvider.class,method = "getNotAds")
    public List<Ads> getNotAds();
    
    @SelectProvider(type = AdsProvider.class,method = "delAds")
    public void delAds(@Param(value = "mid") int mid, @Param(value = "aid") int aid);
    
    @SelectProvider(type = AdsProvider.class,method = "getAdsByWid")
    public List<Integer> getAdsByWid(@Param(value = "wid") int wid);
    
    @SelectProvider(type = AdsProvider.class,method = "getDefaultAdsAndPrivateAds")
    public List<Integer> getDefaultAdsAndPrivateAds(@Param(value = "wid") int wid);
    
    @SelectProvider(type = AdsProvider.class,method = "getAidByVid")
    public List<Integer> getAidByVid(@Param(value = "vid") int vid);
    
    @SelectProvider(type = AdsProvider.class,method = "getAdsByGid")
    public List<Ads> getAdsByGid(@Param(value = "gid") int gid);
    
    @SelectProvider(type = AdsProvider.class,method = "getAdsListByWid")
    public List<Ads> getAdsListByWid(@Param(value = "wid") int wid);
    
    @SelectProvider(type = AdsProvider.class,method = "getAdsByPagination")
    public List<Ads> getAdsByPagination(@Param(value = "ads") Pagination<Ads> ads);
    
    @SelectProvider(type = AdsProvider.class,method = "countTotal")
    public Integer countTotal();
    
    @SelectProvider(type = AdsProvider.class,method = "getDistinctWidWithTime")
    public List<Ads> getDistinctWidWithTime(@Param(value = "start") String start, @Param(value = "end") String end);
    
    
}
