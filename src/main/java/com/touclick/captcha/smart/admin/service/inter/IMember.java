package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.Member;

import java.util.List;

public interface IMember {
    /**
     * 新增会员
     * 
     * @param member
     * @return
     */
    public int add(Member member);

    public int addTemp(Member member);

    /**
     * 登录校验
     * 
     * @param member
     * @return
     */
    public Member checkLogin(Member member);

    /**
     * 检查email是否重复
     * 
     * @param email
     * @return
     */
    public boolean existEmail(String email);

    /**
     * 
     * @author wangpeng
     * @Title: existEmail
     * @Description: TODO(检查email是否和除了自己的以外的重复)
     * @param @param email
     * @param @return 设定文件
     * @return boolean 返回类型
     * @throws
     */
    public boolean existEmailByAddress(String email, String waddress);

    /**
     * 检查邀请码是否存在
     * 
     * @param invitecode
     * @return
     */
    public boolean existInviteCode(String invitecode);

    /**
     * 获取会员拥有的网站
     * 
     * @param mid
     * @return
     */
    public int getWidByMid(int mid);

    /**
     * 校验旧密码
     * 
     * @param oldpwd
     * @param mid
     * @return
     */
    public boolean checkOldpwd(String oldpwd, int mid);

    /**
     * 保存新密码
     * 
     * @param newpwd
     * @param mid
     */
    public void updateNewpwd(String newpwd, int mid);

    /**
     * 保存个人信息
     * 
     * @param m
     */
    public void updateInfo(Member m);
    
    public void updateStatus(Member m);
    
    public void updateToken(Member m);

    /**
     * 
     * @author wangpeng
     * @Title: getAllMember
     * @Description: TODO(获取所有用户)
     * @param @return 设定文件
     * @return List<Member> 返回类型
     * @throws
     */
    public List<Member> getAllMember();

    /**
     * 
     * @author wangpeng
     * @Title: getMember
     * @Description: TODO(根据用户id获取用户)
     * @param @param mid
     * @param @return 设定文件
     * @return Member 返回类型
     * @throws
     */
    public Member getMember(int mid);

    /**
     * 
     * @author wangpeng
     * @Title: checkLoginBySiteKey
     * @Description: TODO(根据网站公钥私钥获取用户)
     * @param @param pubkey
     * @param @param token
     * @param @return 设定文件
     * @return Member 返回类型
     * @throws
     */
    public Member checkLoginBySiteKey(String pubkey, String token);

    /**
     * 
     * @author wangpeng
     * @Title: updateNewpwdByEmail
     * @Description: TODO(根据email更新密码)
     * @param @return 设定文件
     * @return int 返回类型
     * @throws
     */
    public int updateNewpwdByEmail(String password, String email);

    /**
     * 
     * @author wangpeng
     * @Title: getMemberByWaddress
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param @return 设定文件
     * @return Member 返回类型
     * @throws
     */
    public Member getMemberByWaddress(String address);
    
    public Member getMemberByToken(String token);
    
    public Member getMemberByEmail(String email);

    /**
     * 
     * @author wangpeng
     * @Title: resetPassEmailByWaddress
     * @Description: TODO(根据网站修改email和密码)
     * @param @return 设定文件
     * @return int 返回类型
     * @throws
     */
    public int resetPassEmailByWaddress(String email, String password, String waddress);

    public Member getMemberByWid(int wid);

    /**
     * 
     * @author fanjl
     * @Title: updateMemberLevelByMid
     * @param 修改level的同时需要更新缓存  iMemberLevelService.putMemberLevel(mid, level);
     * @param @return 设定文件
     * @return int 返回类型
     * @throws
     */
    public int updateMemberLevelByMid(int level, int mid);
    
    public List<Member> getVipMember();
    
    public List<Member> getMemberListByCreateTime(String start, String end);
}
