package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.WebSiteVipPicture;

public interface IWebSiteVipPicture {
	
	public void save(WebSiteVipPicture wsvp);
	
	public int delByEndtime(String endtime, int type);
	
	public WebSiteVipPicture getObjectByWidVid(int wid, int vid);
	
	public void update(WebSiteVipPicture wsvp);
}
