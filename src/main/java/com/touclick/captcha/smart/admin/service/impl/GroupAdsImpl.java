package com.touclick.captcha.smart.admin.service.impl;

import com.touclick.captcha.smart.admin.mapper.primary.GroupAdsMapper;
import com.touclick.captcha.smart.admin.model.GroupAds;
import com.touclick.captcha.smart.admin.service.inter.IGroupAds;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GroupAdsImpl  implements IGroupAds {

	private static Logger logger = Logger.getLogger(GroupAdsImpl.class);
	private GroupAdsMapper groupAdsMapper;
	private GroupAdsMapper write;
	
	@Override
    public List<GroupAds> findAll() {
		List<GroupAds> list = groupAdsMapper.findAll();
		return list;
	}
	
	@Override
    public List<Integer> findByGid(int gid) {
		List<Integer> list = groupAdsMapper.findByGid(gid);
		return list;
	}
	@Override
    public void save(GroupAds ga){
		write.save(ga);
	}
}
