package com.touclick.captcha.smart.admin.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Ads {

	/** ad id */
	private int aid;
	/** type */
	private int ctype;
	/** use flag */
	private int useflag=1;
	/** Create time */
	private Date createtime;
	/** Delete time */
	private Date deletetime;
	/** member id */
	private int mid;
	private int wid;
	private int vid;//代表是购买的图片的类型id
	private int ispub;

	/** Backgroud image data */
	private  byte[] backgroud;
	
	private String backgroudbase64;//背景图的base64编码
	private String transparent;//透明度，使用","分隔
	private int imgtexttype;//印刷图片类型1和印刷文字类型0
	private String printfont;//印刷文字
	private String clickfont;//点击文字
	private List<String> spicbase64 = new ArrayList<String>();//印刷小图的base64编码
	private List<String> notetextstr = new ArrayList<String>();//提示文字集合

	/** small picture list */
	private  byte[] spic;

	/** Note text */
	private  byte[] notetext;

	/** Whether is ad or not */
	private int atype; //0表示默认图片 1表示网站自定义图片和购买的图片，购买的图片通过vid是不是有值或者不等于0来区分

	/** Protect area points */
	private  byte[] protects;

	/** Success jump url */
	private String succurl;

	/** Logo url */
	private String logourl;
	
	/** Json mapper */
	private transient ObjectMapper mapper = new ObjectMapper();

	/** Small pic or text list */
	private List<byte[]> smallPicOrTextList = new ArrayList<byte[]>();

	/** Note text list */
	private List<byte[]> noteTextList = new ArrayList<byte[]>();

	private List<List<List<Integer>>> protectPointList = new ArrayList<List<List<Integer>>>();

	public List<byte[]> getSmallPicOrTextList() {
		return smallPicOrTextList;
	}

	public void setSmallPicOrTextList(List<byte[]> smallPicOrTextList) {
		this.smallPicOrTextList = smallPicOrTextList;
	}

	public List<byte[]> getNoteTextList() {
		return noteTextList;
	}

	public void setNoteTextList(List<byte[]> noteTextList) {
		this.noteTextList = noteTextList;
	}

	public List<List<List<Integer>>> getProtectPointList() {
		return protectPointList;
	}

	public void setProtectPointList(List<List<List<Integer>>> protectPointList) {
		this.protectPointList = protectPointList;
	}

	/**
	 * Add pic or text byte data to the map
	 * 
	 * @param d
	 */
	public void addSmallPicOrText(byte d[]) {

		smallPicOrTextList.add(d);
	}

	/**
	 * Add note text data to the map
	 * 
	 * @param notespic
	 */
	public void addNoteText(String note) {

		noteTextList.add(note.getBytes());
	}

	/**
	 * Add note text byte to the map
	 * 
	 * @param note
	 */
	public void addNoteText(byte[] note) {

		noteTextList.add(note);
	}

	/**
	 * Add pic or text data and note text to the map
	 * 
	 * @param d
	 * @param note
	 */
	public void addSmallPicOrTextAndNoteText(byte d[], String note) {
		addSmallPicOrText(d);
		addNoteText(note);
	}

	/**
	 * Add pic or text data and note text to the list
	 * 
	 * @param d
	 * @param note
	 */
	public void addSmallPicOrTextAndNoteText(byte d[], byte[] note) {
		addSmallPicOrText(d);
		addNoteText(note);
	}

	/**
	 * Add a point to the protect list
	 * 
	 * @param point
	 */
	public void addProtectPoint(List<List<Integer>> point) {
		if (point != null && point.size() >0 ) {
			protectPointList.add(point);
		}
	}

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}

	public int getCtype() {
		return ctype;
	}

	public void setCtype(int ctype) {
		this.ctype = ctype;
	}

	public int getUseflag() {
		return useflag;
	}

	public void setUseflag(int useflag) {
		this.useflag = useflag;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Date getDeletetime() {
		return deletetime;
	}

	public void setDeletetime(Date deletetime) {
		this.deletetime = deletetime;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public int getIspub() {
		return ispub;
	}

	public void setIspub(int ispub) {
		this.ispub = ispub;
	}

	public byte[] getBackgroud() {
		return backgroud;
	}

	public void setBackgroud(byte[] backgroud) {
		this.backgroud = backgroud;
	}

	public byte[] getSpic() {
		List<String> base64List = new ArrayList<String>();
		for (byte[] el : smallPicOrTextList) {
			base64List.add(Base64.encodeBase64String(el));
		}
		try {
			this.spic = mapper.writeValueAsBytes(base64List);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return spic;
	}

	public void setSpic(byte[] spic) {
		this.spic = spic;
		try {
			List<String> base64List = mapper.readValue(spic, List.class);

			if (base64List != null) {
				smallPicOrTextList.clear();
				for (String el : base64List) {
					smallPicOrTextList.add(Base64.decodeBase64(el));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public byte[] getNotetext() {

		List<String> base64List = new ArrayList<String>();
		for (byte[] el : noteTextList) {
			base64List.add(Base64.encodeBase64String(el));
		}

		try {
			this.notetext = mapper.writeValueAsBytes(base64List);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return notetext;
	}

	public void setNotetext(byte[] notetext) {
		this.notetext = notetext;
		try {
			List<String> base64List = mapper.readValue(notetext, List.class);

			if (base64List != null) {
				noteTextList.clear();
				for (String el : base64List) {
					noteTextList.add(Base64.decodeBase64(el));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getAtype() {
		return atype;
	}

	public void setAtype(int atype) {
		this.atype = atype;
	}

	public byte[] getProtects() {
		try {
			this.protects = mapper.writeValueAsBytes(protectPointList);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return protects;
	}

	public void setProtects(byte[] protects) {
		this.protects = protects;
		try {
			protectPointList = mapper.readValue(protects, List.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getSuccurl() {
		return succurl;
	}

	public void setSuccurl(String succurl) {
		this.succurl = succurl;
	}

	public String getLogourl() {
		return logourl;
	}

	public void setLogourl(String logourl) {
		this.logourl = logourl;
	}

	public String getBackgroudbase64() {
		this.backgroudbase64 = new String(Base64.encodeBase64(backgroud));
		return backgroudbase64;
	}

	public void setBackgroudbase64(String backgroudbase64) {
		this.backgroudbase64 = backgroudbase64;
	}

	public String getTransparent() {
		return transparent;
	}

	public void setTransparent(String transparent) {
		this.transparent = transparent;
	}

	public int getImgtexttype() {
		return imgtexttype;
	}

	public void setImgtexttype(int imgtexttype) {
		this.imgtexttype = imgtexttype;
	}

	public String getPrintfont() {
		return printfont;
	}

	public void setPrintfont(String printfont) {
		this.printfont = printfont;
	}

	public String getClickfont() {
		try {
			if(notetext!=null){
				List<String> base64List = mapper.readValue(notetext, List.class);
				clickfont = "";
				for(String str : base64List){
					clickfont = clickfont+new String(Base64.decodeBase64(str),"UTF-8");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return clickfont;
	}

	public void setClickfont(String clickfont) {
		this.clickfont = clickfont;
	}

	public List<String> getSpicbase64() {
		if(imgtexttype==1){
			try {
				if(spic!=null){
					List<String> base64List = mapper.readValue(spic, List.class);
					spicbase64.clear();
					for(String str : base64List){
						spicbase64.add(str);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return spicbase64;
	}
	
	public void setSpicbase64(List<String> spicbase64) {
		this.spicbase64 = spicbase64;
	}
	
	public List<String> getNotetextstr() {
		if(imgtexttype==1){
			try {
				if(notetext!=null){
					List<String> base64List = mapper.readValue(notetext, List.class);
					notetextstr.clear();
					for(String str : base64List){
						notetextstr.add(new String(Base64.decodeBase64(str),"UTF-8"));
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return notetextstr;
	}

	public void setNotetextstr(List<String> notetextstr) {
		this.notetextstr = notetextstr;
	}

	public int getVid() {
		return vid;
	}

	public void setVid(int vid) {
		this.vid = vid;
	}

	public int getWid() {
		return wid;
	}

	public void setWid(int wid) {
		this.wid = wid;
	}

	
}
