package com.touclick.captcha.smart.admin.model;

import java.util.List;

public class MemberGroup {
	private int mid;
	private String gname;
	private int gid;
	private int count ;
	private String formula;
	private List<Ads> adsList;
	
	public String getFormula() {
		return formula;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getGid() {
		return gid;
	}
	public void setGid(int gid) {
		this.gid = gid;
	}
	public String getGname() {
		return gname;
	}
	public void setGname(String gname) {
		this.gname = gname;
	}
	public List<Ads> getAdsList() {
		return adsList;
	}
	public void setAdsList(List<Ads> adsList) {
		this.adsList = adsList;
	}
	
}
