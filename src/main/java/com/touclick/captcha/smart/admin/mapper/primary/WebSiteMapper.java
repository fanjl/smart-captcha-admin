package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.WebSiteProvider;
import com.touclick.captcha.smart.admin.model.WebSite;
import com.touclick.captcha.smart.admin.model.WebSiteInfo;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;

import java.util.List;

public interface WebSiteMapper {
	@InsertProvider(type=WebSiteProvider.class,method="addWebSite")
	@Options(flushCache=Options.FlushCachePolicy.TRUE,statementType= StatementType.PREPARED,useGeneratedKeys=true,keyProperty="website.wid",keyColumn="website.wid")
	public void addWebSite(@Param(value = "website") WebSite website);
	
	@InsertProvider(type=WebSiteProvider.class,method="addWebSiteTemp")
	@Options(flushCache=Options.FlushCachePolicy.TRUE,statementType= StatementType.PREPARED,useGeneratedKeys=true,keyProperty="website.wid",keyColumn="website.wid")
	public int addWebSiteTemp(@Param(value = "website") WebSite website);
	
	@InsertProvider(type=WebSiteProvider.class,method="addWebSiteInfo")
	public int addWebSiteInfo(@Param(value = "websiteinfo") WebSiteInfo websiteinfo);
	
	@SelectProvider(type=WebSiteProvider.class,method="existWebSite")
	public List<WebSite> existWebSite(@Param(value = "waddress") String waddress);

	@SelectProvider(type=WebSiteProvider.class,method="existWebSiteByMid")
	public List<WebSite> existWebSiteByMid(@Param(value = "waddress") String waddress, @Param(value = "mid") int mid);
	
	@SelectProvider(type = WebSiteProvider.class,method = "getWebsiteByPubkey")
	public WebSite getWebsiteByPubkey(@Param(value = "pubkey") String pubkey);
	
	@SelectProvider(type = WebSiteProvider.class,method = "getWebsiteCountByMid")
	public Integer getWebsiteCountByMid(@Param(value = "mid") int mid);
	
	@SelectProvider(type = WebSiteProvider.class,method = "getWebSiteList")
	public List<WebSite> getWebSiteList(@Param(value = "mid") int mid);
	
	
	@SelectProvider(type = WebSiteProvider.class,method = "findAll")
	public List<WebSite> findAll();
	
	@SelectProvider(type = WebSiteProvider.class,method = "findWebSiteInfo")
	public List<WebSiteInfo> findWebSiteInfo(@Param(value = "wid") int wid, @Param(value = "type") int type);
	
	@SelectProvider(type = WebSiteProvider.class,method = "findAllWebSiteInfo")
	public List<WebSiteInfo> findAllWebSiteInfo();
	
	@SelectProvider(type = WebSiteProvider.class,method = "isOnlyOwnerPic")
    public Integer isOnlyOwnerPic(@Param(value = "wid") int wid, @Param(value = "type") int type);
	
	
	@InsertProvider(type = WebSiteProvider.class,method = "insertWebSiteInfo")
	public void insertWebSiteInfo(@Param(value = "wid") int wid, @Param(value = "type") int type, @Param(value = "value") int value);
	
	@DeleteProvider(type = WebSiteProvider.class,method = "cancelByWidAndType")
	public void cancelByWidAndType(@Param(value = "wid") int wid, @Param(value = "type") int type);
	
	@UpdateProvider(type = WebSiteProvider.class,method = "updateCtypeAndSkin")
	public void updateCtypeAndSkin(@Param(value = "wid") int wid, @Param(value = "ctype") int ctype, @Param(value = "skin") String skin);
	
	@SelectProvider(type = WebSiteProvider.class,method = "getWebSiteByWid")
	public WebSite getWebSiteByWid(@Param(value = "wid") int wid);
	
	@UpdateProvider(type = WebSiteProvider.class,method = "delWebSite")
	public void delWebSite(@Param(value = "wid") int wid);
	
	@UpdateProvider(type = WebSiteProvider.class,method = "resetToken")
	public void resetToken(@Param(value = "wid") int wid, @Param(value = "token") String token);
	
	@UpdateProvider(type = WebSiteProvider.class,method = "updateVerify")
	public void updateVerify(@Param(value = "wid") int wid, @Param(value = "verifyflag") int verifyflag);
	
	@SelectProvider(type = WebSiteProvider.class,method = "getWebsiteInfo")
	public List<WebSiteInfo> getWebsiteInfo(@Param(value = "wid") int wid);
	
	@UpdateProvider(type = WebSiteProvider.class,method = "updateWebSiteInfo")
	public void updateWebSiteInfo(@Param(value = "wid") int wid, @Param(value = "type") int type, @Param(value = "value") String value);
	
	@DeleteProvider(type = WebSiteProvider.class,method = "delCustom")
	public void delCustom(@Param(value = "wid") int wid, @Param(value = "type") int type);
	
	@UpdateProvider(type = WebSiteProvider.class,method = "udpateCversionByWid")
	public void udpateCversionByWid(@Param(value = "wid") int wid, @Param("cversion") String cversion);
	
	@UpdateProvider(type = WebSiteProvider.class,method = "udpateCversionByMid")
	public void udpateCversionByMid(@Param(value = "mid") int mid, @Param("cversion") String cversion);
	
	@UpdateProvider(type = WebSiteProvider.class,method = "udpateCversionByPubkey")
	public void udpateCversionByPubkey(@Param(value = "pubkey") String pubkey, @Param("cversion") String cversion);
	
	@UpdateProvider(type = WebSiteProvider.class,method = "updateCversionCTypeByPubkey")
	public void updateCversionCTypeByPubkey(@Param(value = "pubkey") String pubkey, @Param("cversion") String cversion, @Param("ctype") int ctype);
	
	
	@UpdateProvider(type = WebSiteProvider.class,method = "udpateCversionByPlatformCversion")
	public Integer udpateCversionByPlatformCversion(@Param(value = "platform") String platform, @Param(value = "srcVersion") String srcVersion, @Param(value = "desVersion") String desVersion);
	
	@SelectProvider(type = WebSiteProvider.class,method = "getWebSiteByPlatformCversion")
	public List<WebSite> getWebSiteByPlatformCversion(@Param(value = "platform") String platform, @Param(value = "cversion") String cversion);
	
	@SelectProvider(type = WebSiteProvider.class,method = "getAllWebSite")
	public List<WebSite> getAllWebSite();
	
	@SelectProvider(type = WebSiteProvider.class,method = "getOldVersionWebSite")
    public List<String> getOldVersionWebSite();
	
	@SelectProvider(type = WebSiteProvider.class,method = "findWebSiteInfoByWid")
	public List<WebSiteInfo> findWebSiteInfoByWid(@Param(value = "wid") int wid);
    
}
