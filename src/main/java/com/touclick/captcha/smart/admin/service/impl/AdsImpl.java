package com.touclick.captcha.smart.admin.service.impl;

import com.touclick.captcha.smart.admin.mapper.primary.AdsMapper;
import com.touclick.captcha.smart.admin.model.Ads;
import com.touclick.captcha.smart.admin.model.MemberGroup;
import com.touclick.captcha.smart.admin.service.inter.IAds;
import com.touclick.captcha.smart.admin.utils.Pagination;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author bing.liu
 */

@Service("IAds")
public class AdsImpl  implements IAds {
    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(AdsImpl.class);
    @Autowired
    private AdsMapper adsMapper;
    /**
     * Insert a ads into the database;
     * @param ads
     */
    @Override
    public void insert(Ads ads){
        
        adsMapper.insert(ads);
    }

    /**
     * Select a ads by given ads id
     * @param aid
     * @return
     */
    @Override
    public Ads selectByAid(int aid){
        
        return adsMapper.selectByAid(aid);
    }

	@Override
	public List<Ads> selectByAids(List<Integer> aids) {
		if(aids== null||aids.size() ==0){
			return new ArrayList<Ads>(0);
		}
		StringBuilder sb = new StringBuilder();
		for(int i= 0;i<aids.size();i++){
			if(i>0){
				sb.append(',');
			}
			sb.append(aids.get(i));
		}
		
		return adsMapper.selectByAids(sb.toString());
	}

	/**
     * Select ads by given atype
     * @param atype
     * @return
     */
    @Override
    public List<Ads> selectByAtype(int atype){
        
        return adsMapper.selectByAtype(atype);
    }

    /**
     * Select ads list by given ctype
     * @param ctype
     * @return
     */
    @Override
    public List<Ads> selectByCtype(int ctype){
        
        return adsMapper.selectByCtype(ctype);
    }

    /**
     * Select ads list by given member id
     * @param mid
     * @return
     */
    @Override
    public List<Ads> selectByMid(int mid){
        return adsMapper.selectByMid(mid);
    }

    /**
     * Select ads list 
     * @return
     */
	@Override
    public List<Ads> selectAll() {
        return adsMapper.selectAll();
	}
	/**
     * 根据会员id获得广告图片id
     * @return
     */
	@Override
    public List<Integer> getAidByMid(int mid){
		
        return adsMapper.getAidByMid(mid);
	}
	/**
     * 根据广告图片id获取所在图片组的配置信息
     * @param ads
     * @return
     */
	@Override
    public List<MemberGroup> getMemberGroupConfByAds(String ads){
        return adsMapper.getMemberGroupConfByAds(ads);
	}
	/**
     * 根据图片组id获取图片组中的图片集合
     * @param gid
     * @return
     */
	@Override
    public List<Integer> getAidByGid(int gid){
        return adsMapper.getAidByGid(gid);
	}
	
	/**
	 * 获取所有的AidList
	 */
	@Override
    public List<Integer> selectAllAid() {
        return adsMapper.selectAllAid();
	}
	/**
	 * 获取非广告图文点击验证码集合
	 */
	@Override
    public List<Ads> getNotAds(){
        return adsMapper.getNotAds();
	}
	@Override
    public void delAds(int mid,int aid){
        adsMapper.delAds(mid,aid);
	}
	
	/**
	 * 获取网站专属图片（atype=1 && wid = #wid）
	 */
	public List<Integer> getAdsByWid(int wid){
        return adsMapper.getAdsByWid(wid);
	}
	
	/**
	 * 
	* @Title: getAdsByWid
	* @Description: 获取默认图和网站专属图
	* @param @param wid
	* @param @return    设定文件
	* @return List<Integer>    返回类型
	* @throws
	 */
	public List<Integer> getDefaultAdsAndPrivateAds(int wid){
        return adsMapper.getDefaultAdsAndPrivateAds(wid);
	}
	
	@Override
    public List<Integer> getAidByVid(int vid){
        return adsMapper.getAidByVid(vid);
	}
	
	@Override
    public List<Ads> getAdsByGid(int gid){
        return adsMapper.getAdsByGid(gid);
	}
	@Override
    public List<Ads> getAdsByMid(int mid){
        return adsMapper.getAdsByMid(mid);
	}

    @Override
    public List<Ads> getAdsListByWid(int wid) {
        return adsMapper.getAdsListByWid(wid);
    }
    public List<Ads> getAdsByPagination(Pagination<Ads> ads) {
        return adsMapper.getAdsByPagination(ads);
    }

    @Override
    public int countTotal() {
        return adsMapper.countTotal();
    }


    @Override
    public List<Ads> getDistinctWidWithTime(String start, String end) {
        return adsMapper.getDistinctWidWithTime(start,end);
    }
}
