package com.touclick.captcha.smart.admin.mapper.secondary.provider;

import com.touclick.captcha.smart.admin.model.BlockName;

import java.util.Map;

/**
 * BlockName provider
 *
 * @author bing.liu
 * @date 2015-09-21
 */
public class BlockNameProvider {
    public String findAll(){
        return "SELECT * FROM block_name";
    }

    public String insert(Map<String,BlockName> params){
        return "INSERT INTO block_name(name) values(#{blockName.name})";
    }

    public String findByName(Map<String,String> params){
        return "SELECT * FROM block_name where name = #{name}";
    }
}
