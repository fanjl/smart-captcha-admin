package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.BlockNameProvider;
import com.touclick.captcha.smart.admin.model.BlockName;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * BlockName mapper
 *
 * @author bing.liu
 * @date 2015-09-21
 */
public interface BlockNameMapper {
    @InsertProvider(type = BlockNameProvider.class,method = "insert")
    public void insert(@Param(value = "blockName") BlockName blockName);

    @SelectProvider(type = BlockNameProvider.class,method = "findAll")
    public List<BlockName> findAll();

    @SelectProvider(type = BlockNameProvider.class,method = "findByName")
    public BlockName findByName(@Param(value = "name") String name);
}
