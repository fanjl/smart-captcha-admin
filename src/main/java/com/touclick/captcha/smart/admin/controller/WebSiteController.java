package com.touclick.captcha.smart.admin.controller;

import com.touclick.captcha.smart.admin.auth.UserContext;
import com.touclick.captcha.smart.admin.model.Member;
import com.touclick.captcha.smart.admin.model.WebSite;
import com.touclick.captcha.smart.admin.model.WebSiteInfo;
import com.touclick.captcha.smart.admin.model.WebSiteInfoEnum;
import com.touclick.captcha.smart.admin.service.inter.IMember;
import com.touclick.captcha.smart.admin.service.inter.IWebSite;
import com.touclick.captcha.smart.admin.utils.LevelConfigUtils;
import com.touclick.captcha.smart.admin.utils.VerifyWebsiteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping(value = "/website")
public class WebSiteController {
    private static Logger logger = LoggerFactory.getLogger(WebSiteController.class);
    @Autowired
    private IWebSite iWebSite;
//    private IWebSiteInfo iWebSiteInfo = new WebSiteInfoRedis();
////    @Autowired
////    private WebSiteCacheService webSiteCacheService;
//    @Autowired
//    private WebSiteCache webSiteCache;
    @Autowired
    private IMember iMember;
    
    private static final String VERSION = "5.2.0";

    @RequestMapping(value = "websiteList")
    public String websiteList(HttpSession session, Model model, @AuthenticationPrincipal UserContext userContext) {
        List<WebSite> list = iWebSite.getWebSiteList(userContext.getUserId());
        model.addAttribute("list", list);
        return "website/websitelist";
    }

    @ResponseBody
    @RequestMapping(value = "addWebsite")
    public int addWebsite(HttpServletRequest request, WebSite website,@AuthenticationPrincipal UserContext userContext) {
        website.setPubkey(UUID.randomUUID().toString());
        website.setToken(UUID.randomUUID().toString());
        website.setCreatetime(new Date());
        website.setUpdatetime(new Date());
        website.setCtype(13);
        website.setCtypeplus("1");
        website.setSkin("white");
        // if("api".equals(website.getPlatform())){
        // website.setCversion("v2-2");
        // }else if("discuz".equals(website.getPlatform())){
        // website.setCversion("v3-0");
        // }else{
        // website.setCversion("v2-2");
        // }
        website.setCversion(VERSION);
        website.setMid(userContext.getUserId());
        // 判断权限是否能够添加网站
        int idCount = iWebSite.getWebSiteCountByMid(userContext.getUserId());
        Member m = iMember.getMember(userContext.getUserId());
        int idLimits = LevelConfigUtils.getIdCountLimits(m.getLevel());
        if (idCount >= idLimits) { // 权限不足
            return 3;
        }
        // 如果自己账号中已经存在该网站，则不进行网站校验
//        boolean existFlag = iWebSite.existWebSiteByMid(website.getWaddress(), m.getMid());
//        if (existFlag) {
//            website.setVerifyflag(1);
//        } else {
//            boolean verify = VerifyWebsiteUtils.verifyByWaddress(website.getWaddress());
//            if (!verify) {// 验证没通过
//                return 2;
//            }
//        }
        try {
            iWebSite.addWebSite(website);
//            iWebSiteInfo.setCTypeWithWid(website.getCtype() + "", website.getWid() + "");
//            iWebSiteInfo.setDefaultCTypeByWid(website.getWid() + "", website.getCtype() + "");
//            iWebSiteInfo.setWidWithPubkey(website.getWid() + "", website.getPubkey());
//            iWebSiteInfo.setPrivateKeyByWid(website.getWid() + "", website.getToken());
            WebSiteInfo wsi = new WebSiteInfo();
            wsi.setWid(website.getWid());
            wsi.setType(WebSiteInfoEnum.oldCtype.getIndex());
            wsi.setValue("1,3");
            iWebSite.addWebSiteInfo(wsi);
            List<WebSiteInfo> list = new ArrayList<WebSiteInfo>();
            list.add(wsi);
//            webSiteCacheService.putWebSite(website, list);
        } catch (Exception e) {

            e.printStackTrace();
            logger.error("addWebsite error", e.getMessage());
            return 0;// 保存失败
        }
        return 1;// 保存成功
    }

    @ResponseBody
    @RequestMapping(value = "verifyWebsite")
    public boolean verifyWebsite(HttpSession session) {
        Member m = (Member) session.getAttribute("member");
        List<WebSite> list = iWebSite.getWebSiteList(m.getMid());
        Boolean flag = false;
        for (WebSite ws : list) {
            if (ws.getVerifyflag() == 1) {
                flag = true;
                continue;
            }
            flag = VerifyWebsiteUtils.verifyByWaddress(ws.getWaddress());
        }
        return flag;
    }

    @ResponseBody
    @RequestMapping(value = "delWebsite")
    public boolean delWebSite(HttpServletRequest request, int wid,@AuthenticationPrincipal UserContext userContext) {
        Member m = iMember.getMember(userContext.getUserId());
        List<WebSite> list = iWebSite.getWebSiteList(m.getMid());
        if (list.size() == 1) {
            return false;
        }
        try {
            iWebSite.delWebSite(wid);
//            webSiteCache.saveWebSiteCache(wid);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ResponseBody
    @RequestMapping(value = "resetToken")
    public String resetToken(int wid) {
        String newtoken = UUID.randomUUID().toString();
        try {
            iWebSite.resetToken(wid, newtoken);
//            webSiteCache.saveWebSiteCache(wid);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return newtoken;
    }

    @ResponseBody
    @RequestMapping(value = "verify")
    public boolean verify(int wid, String waddress) {
//        boolean flag = VerifyWebsiteUtils.verifyByWaddress(waddress);
//        if (flag) {
//            iWebSite.updateVerify(wid, 1);
//        }
//        webSiteCache.saveWebSiteCache(wid);
        return true;
    }

    @ResponseBody
    @RequestMapping("updateCversion")
    public boolean updateCversion(int wid, String cversion, String caller) {
        if ("touclick".equals(caller)) {
            try {
                iWebSite.udpateCversionByWid(wid, cversion);
//                webSiteCache.saveWebSiteCache(wid);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    @ResponseBody
    @RequestMapping("updateCversionByMember")
    public boolean updateCversionByMember(Member member) {
        Member m = iMember.checkLogin(member);
        if (m != null) {
            iWebSite.udpateCversionByMid(m.getMid(), "5.0.1");
            List<WebSite> websiteList = iWebSite.getWebSiteList(m.getMid());
//            webSiteCache.saveWebSiteCacheList(websiteList);
            return true;
        }
        return false;
    }

    @RequestMapping("updateCversionByPubkey")
    @ResponseBody
    public boolean updateCversionByPubkey(String pubkey) {
        if (!StringUtils.isEmpty(pubkey)) {
            iWebSite.udpateCversionByPubkey(pubkey, "5.0.1");
            WebSite ws = iWebSite.getWebSiteByPubkey(pubkey);
//            webSiteCache.saveWebSiteCache(ws.getWid());
            return true;
        }
        return false;
    }

    @RequestMapping("updateOldCversionByPubkey")
    @ResponseBody
    public boolean updateOldCversionByPubkey() {
        List<String> pubkeys = iWebSite.getOldVersionWebSite();
        for (String pubkey : pubkeys) {
            if (!StringUtils.isEmpty(pubkey)) {
                iWebSite.udpateCversionByPubkey(pubkey, "5.0.1");
                WebSite ws = iWebSite.getWebSiteByPubkey(pubkey);
//                webSiteCache.saveWebSiteCache(ws.getWid());
            }
        }
        return true;
    }

    @RequestMapping("updateRedisCversionByPubkey")
    @ResponseBody
    public boolean updateRedisCversionByPubkey() {
        List<WebSite> pubkeys = iWebSite.getAllWebSite();
        for (int i = 0; i < pubkeys.size(); i++) {
            try {
                if (i % 100 == 0) {
                    logger.info("已经更新了" + i + ";");
                }
                if (pubkeys.get(i) != null) {
//                    webSiteCache.saveWebSiteCache(pubkeys.get(i).getWid());
                }

            } catch (NullPointerException e) {
                logger.error("null", e);
            }
        }

        return true;
    }

    @RequestMapping("updateCversionCTypeByPubkey")
    @ResponseBody
    public boolean updateCversionCTypeByPubkey(String pubkey, String cversion, int ctype) {
        if (!StringUtils.isEmpty(pubkey)) {
            iWebSite.updateCversionCTypeByPubkey(pubkey, cversion, ctype);
            WebSite ws = iWebSite.getWebSiteByPubkey(pubkey);
//            webSiteCache.saveWebSiteCache(ws.getWid());
            return true;
        }
        return false;
    }

    @RequestMapping("updateCversionByPlatformCversion")
    @ResponseBody
    public String updateCversionByPlatformCversion(String platform, String srcVersion, String desVersion) {
        System.out.println("platform=" + platform + "srcVersion=" + srcVersion + "desVersion=" + desVersion);
        List<WebSite> list = iWebSite.getWebSiteByPlatformCversion(platform, srcVersion);
        System.out.println("数量：" + list.size());
        int result = iWebSite.udpateCversionByPlatformCversion(platform, srcVersion, desVersion);
        System.out.println("更新数量：" + result);
        for (WebSite ws : list) {
            ws.setCversion(desVersion);
        }
//        webSiteCache.saveWebSiteCacheList(list);
        return "更新完成";
    }
}
