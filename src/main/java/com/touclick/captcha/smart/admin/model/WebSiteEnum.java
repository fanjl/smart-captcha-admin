package com.touclick.captcha.smart.admin.model;

public enum WebSiteEnum {
	widNeedGid("网站需要广告组",3),widNoNeedGid("网站不需要此广告组",4),
	gidNeedWid("广告组需要放入此网站",5),gidNoNeedWid("广告组不能放入此网站上",6),
	
	widNeedAid("网站需要Aid",7),widNoNeedAid("网站不需要Aid",8);
	
	private String name;
	private int index;
	private WebSiteEnum(String name, int index){
		this.name = name;  
        this.index = index;
	}
	/**
     * 返回索引值
     * @param name
     * @return
     */
    public static int getIndex(String name){
    	for (com.touclick.captcha.smart.admin.model.WebSiteEnum c : com.touclick.captcha.smart.admin.model.WebSiteEnum.values()) {
            if (c.name().equals(name)) {  
                return c.index;
            }
    	}
        return 0;
    }
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
    
}
