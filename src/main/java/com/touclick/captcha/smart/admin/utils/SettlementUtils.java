package com.touclick.captcha.smart.admin.utils;

import com.touclick.captcha.smart.admin.model.HistoryPay;
import com.touclick.captcha.smart.admin.model.Member;

import java.math.BigDecimal;

public class SettlementUtils {
	private static final double V1PRICE_PER_MONTH = 200.00;//专业版每个月价格
	private static final double V2PRICE_PER_MONTH = 1000.00;//企业版每个月价格
	private static final double V3PRICE_PER_MONTH = 2000.00;//旗舰版每个月价格
	private static final double V4PRICE_PER_MONTH = 3000.00;//专业版每个月价格
    private static final double V5PRICE_PER_MONTH = 5000.00;//企业版每个月价格
    private static final double V6PRICE_PER_MONTH = 8000.00;//旗舰版每个月价格
    private static final double V7PRICE_PER_MONTH = 10000.00;//专业版每个月价格
	private static final double SALE_ONE_YEAR = 0.85; //一年折扣
	
	private SettlementUtils(){
		
	}
	/**
	 * 
	* @author wangpeng
	* @Title: upgradeLevel 
	* @Description: TODO(无剩余时间结算) 
	* @param @param srcLevel
	* @param @param targetLevel
	* @param @param months
	* @param @return    设定文件 
	* @return double    返回类型 
	* @throws
	 */
	public static double upgradeLevel(int srcLevel,int targetLevel,int months){
		BigDecimal srcPrice = new BigDecimal(getLevelPrice(srcLevel));
		BigDecimal targetPrice = new BigDecimal(getLevelPrice(targetLevel));
		BigDecimal diffPrice = targetPrice.subtract(srcPrice);
		BigDecimal finalPrice = diffPrice.multiply(new BigDecimal(months));
//		if(months>11){
//			return finalPrice.multiply(new BigDecimal(SALE_ONE_YEAR)).doubleValue();
//		}
		return finalPrice.doubleValue();
	}
	/**
	 * 
	* @author wangpeng
	* @Title: reNewLevel 
	* @Description: TODO(续费) 
	* @param @param srcLevel
	* @param @param months
	* @param @return    设定文件 
	* @return double    返回类型 
	* @throws
	 */
	public static double reNewLevel(int srcLevel,int months){
		BigDecimal srcPrice = new BigDecimal(getLevelPrice(srcLevel));
		BigDecimal finalPrice = srcPrice.multiply(new BigDecimal(months));
//		if(months>11){
//			return finalPrice.multiply(new BigDecimal(SALE_ONE_YEAR)).doubleValue();
//		}
		return finalPrice.doubleValue();
	}
	/**
	 * 
	* @author wangpeng
	* @Title: upgradeLevel 
	* @Description: TODO(有剩余天数时间结算) 
	* @param @param srcLevel
	* @param @param targetLevel
	* @param @param months
	* @param @param surplusDays
	* @param @return    设定文件 
	* @return double    返回类型 
	* @throws
	 */
	public static double upgradeLevel(int srcLevel,int targetLevel,int months,int surplusDays){
		BigDecimal surplusPrice = new BigDecimal(getLevelPricePerDay(getLevelPrice(srcLevel))).multiply(new BigDecimal(surplusDays));//剩余天数核算的钱
		BigDecimal targetPrice = new BigDecimal(getLevelPrice(targetLevel));
		//BigDecimal diffPrice = targetPrice.subtract(srcPrice);
		BigDecimal finalPrice = targetPrice.multiply(new BigDecimal(months));
//		if(months>11){
//			return finalPrice.multiply(new BigDecimal(SALE_ONE_YEAR)).subtract(surplusPrice).doubleValue();
//		}
		return finalPrice.subtract(surplusPrice).doubleValue();
	}
	/**
	 * 
	* @author wangpeng
	* @Title: getLevelPrice 
	* @Description: TODO(获取每个级别的价钱) 
	* @param @param level
	* @param @return    设定文件 
	* @return double    返回类型 
	* @throws
	 */
	private static double getLevelPrice(int level){
		switch(level){
		case 1:
			return V1PRICE_PER_MONTH;
		case 2:
			return V2PRICE_PER_MONTH;
		case 3:
			return V3PRICE_PER_MONTH;
		case 4:
            return V4PRICE_PER_MONTH;
        case 5:
            return V5PRICE_PER_MONTH;
        case 6:
            return V6PRICE_PER_MONTH;
        case 7:
            return V7PRICE_PER_MONTH;
		}
		return 0;
	}
	/**
	 * 
	* @author wangpeng
	* @Title: getLevelPricePerDay 
	* @Description: TODO(获取各级别每天的价钱) 
	* @param @param monthPrice
	* @param @return    设定文件 
	* @return double    返回类型 
	* @throws
	 */
	private static double getLevelPricePerDay(double monthPrice){
		BigDecimal monthPri = new BigDecimal(monthPrice);
		return monthPri.divide(new BigDecimal(30),2, BigDecimal.ROUND_HALF_EVEN).doubleValue();
	}
	
	public static double caculateFinalPrice(Member m , HistoryPay hp ,int level, int month){
        double prices = 0.0;
        if (hp != null) {
            int day = DateUtils.caculaterDay(hp.getEndTime());
            if (m.getLevel() == level) {
                // 续费业务
                if (day > 0) {
                    prices = SettlementUtils.reNewLevel(m.getLevel(), month);
                }
            } else {
                // 升级业务
                if (day > 0) {
                    prices = SettlementUtils.upgradeLevel(m.getLevel(), level, month, day);
                } else {
                    prices = SettlementUtils.upgradeLevel(m.getLevel(), level, month);
                }
            }
        } else {
            prices = SettlementUtils.upgradeLevel(m.getLevel(), level, month);
        }
        if (prices < 0) {
            prices = 0;
        }
        return prices;
    }
}
