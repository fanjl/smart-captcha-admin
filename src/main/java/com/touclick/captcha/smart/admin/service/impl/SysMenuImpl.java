package com.touclick.captcha.smart.admin.service.impl;

import com.touclick.captcha.smart.admin.mapper.primary.SysMenuMapper;
import com.touclick.captcha.smart.admin.model.SysMenu;
import com.touclick.captcha.smart.admin.service.inter.ISysMenu;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ISysMenu")
public class SysMenuImpl   implements ISysMenu {
	private static Logger logger = Logger.getLogger(SysMenuImpl.class);
	@Autowired
	private SysMenuMapper sysMenu;
	@Override
    public List<SysMenu> getOneMenu(int role) {
		List<SysMenu> list = sysMenu.getOneMenu(role);
		return list;
	}

	@Override
    public List<SysMenu> getTwoMenu(int oneid, int role) {
		//sysMenu = bs.getWritableSQLSession().getMapper(SysMenuMapper.class);
		List<SysMenu> list = sysMenu.getTwoMenu(oneid,role);
		return list;
	}

}
