package com.touclick.captcha.smart.admin.model;

public enum LModuleEnum {
    Regist(1), Login(2), Other(0);
    int id;

    private LModuleEnum(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
