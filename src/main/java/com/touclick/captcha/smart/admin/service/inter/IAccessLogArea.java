package com.touclick.captcha.smart.admin.service.inter;


import com.touclick.captcha.smart.admin.model.AccessLogArea;
import com.touclick.captcha.smart.admin.model.AccessLogDeviceType;
import com.touclick.captcha.smart.admin.model.AccessLogSystem;

import java.util.List;

public interface IAccessLogArea {
    int insert(AccessLogArea accesslogarea);
    
    int insertDevice(AccessLogDeviceType accesslogdevicetype);
    
    int insertSystem(AccessLogSystem accesslogsystem);
    
    List<AccessLogArea> getAreaListByPubkeyTime(String Pubkey, String start, String end);
    
    List<AccessLogSystem> getSystemListByPubkeyTime(String Pubkey, String start, String end);

}
