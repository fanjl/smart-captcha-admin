package com.touclick.captcha.smart.admin.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 基于jackson框架封装的Json处理器
 * 实现由Java对象转换为Json串，或由Json串转换为Java对象
 * 
 * @author wzj
 * @version v1.0
 */
public class JsonProcessor implements InitializingBean {
	private static final Logger LOGGER = LoggerFactory.getLogger(com.touclick.captcha.smart.admin.utils.JsonProcessor.class);
	private static final Map<String, ObjectMapper> OBJECT_MAPPER =
			new HashMap<String, ObjectMapper>();
	private static final String DEFAULT_DATE_PATTERN = DateUtil.HC_DATETIME;
	
	/**
	 * 本处理器支持的日期格式，
	 * 作为本处理类的属性，由Spring负责注入
	 */
	private static String[] datePatterns;
	
	public void setDatePatterns(String[] datePatterns){
		com.touclick.captcha.smart.admin.utils.JsonProcessor.datePatterns = datePatterns;
	}
	
	/**
	 * 获取处理器支持的日期格式
	 * @return
	 */
	public static String[] getSupportedDatePatterns(){
		String[] datePatternsCopy = null;
		if(datePatterns != null){
			datePatternsCopy = Arrays.copyOf(datePatterns, datePatterns.length);
		}
		return datePatternsCopy;
	}
	
	/**
	 * 将java对象转换成Json串
	 * 若java对象中含有Date类型的属性，则统一转换为默认日期格式{yyyy-MM-dd HH:mm:ss}串，
	 * 若实现不同Date类型属性以不同格式转换，请使用注解方式。
	 * 若转换失败，将抛出运行时转换异常
	 * @param object 目标java对象
	 * @return
	 */
	public static String toJson(Object object){
		return toJson(object, DEFAULT_DATE_PATTERN);
	}
	
	/**
	 * 将java对象转换成Json串
	 * 若java对象中含有Date类型的属性，则统一转换为指定的日期格式串，
	 * 若实现不同Date类型属性以不同格式转换，请使用注解方式。
	 * 若转换失败，将抛出运行时转换异常
	 * @param object 目标java对象
	 * @param datePattern 日期格式，指定的日期格式需在本处理器支持的日期格式范围内，
	 * 					      否则，将抛出运行时异常
	 * @return
	 */
	public static String toJson(Object object, String datePattern){
		String jsonStr = null;
		ObjectMapper objectMapper = lookupObjectMapper(datePattern);
		if(objectMapper == null){
			throw new IllegalArgumentException("JsonProcessor do not support date pattern: "
					+ datePattern);
		}
		try {
			jsonStr = objectMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new IllegalArgumentException(e);
		}
		return jsonStr;
	}
	
	/**
	 * 将pagination对象转换成Json串
	 * 若pagination对象中含有Date类型的属性，则统一转换为默认日期格式{yyyy-MM-dd HH:mm:ss}串，
	 * 若实现不同Date类型属性以不同格式转换，请使用注解方式。
	 * 若转换失败，将抛出运行时转换异常
	 * @param page pagination对象
	 * @return
	 */
	public static String toJson(Pagination<?> page){
		return toJson(page, null);
	}
	
	/**
	 * 将Pagination对象转换成Json串
	 * 若Pagination对象中含有Date类型的属性，则统一转换为指定的日期格式串，
	 * 若实现不同Date类型属性以不同格式转换，请使用注解方式。
	 * 若转换失败，将抛出运行时转换异常
	 * @param page Pagination对象
	 * @param datePattern 日期格式，指定的日期格式需在本处理器支持的日期格式范围内，
	 * 					      否则，将抛出运行时异常
	 * @return
	 */
	public static String toJson(Pagination<?> page, String datePattern){
		return page.toJson(datePattern);
	}
	
	
	
	/**
	 * object转化成liger grid json数据
	 * @param object List集合
	 * @param total 总条数
	 * @param datePattern 日期类型
	 * @return
	 */
	public static String toLigerGridJson(Object object , Long total , String datePattern){
		String rows = toJson(object, datePattern);
		StringBuilder sb = new StringBuilder();
		sb.append("{\"Rows\":");
		sb.append(rows);
		sb.append(",\"Total\":");
		sb.append(total);
		sb.append("}");
		return sb.toString();
	}
	
	/**
	 * 转换对象为JSON串，并指定键名。
	 * 本方法同时支持转换的对象为基本类型的包装类、String及Date类型，如key="result", value=true，
	 * 则返回 {"result":true}，Date类型将采用默认日期格式{yyyy-MM-dd HH:mm:ss}转换为相应日期字符串
	 * @param key 键名
	 * @param value 目标对象
	 * @return
	 */
	public static String toJson(String key, Object value){
		return toJson(key, value, null);
	}
	
	/**
	 * 转换对象为JSON串，并指定键名。
	 * 本方法同时支持转换的对象为基本类型的包装类、String及Date类型，如key="result", value=true，则返回 {"result":true}，Date类型
	 * 将根据datePattern参数转换为相应日期字符串，若datePattern为Null，将采用默认日期格式{yyyy-MM-dd HH:mm:ss}
	 * @param key 键名
	 * @param value 目标对象
	 * @param datePattern 日期格式，指定的日期格式需在本处理器支持的日期格式范围内，
	 * 					      否则，将抛出运行时异常。
	 * @return
	 */
	public static String toJson(String key, Object value, String datePattern){
		StringBuilder resultBuilder = new StringBuilder();
		resultBuilder.append("{\"");
		resultBuilder.append(key);
		resultBuilder.append("\":");
		if(value instanceof String || value instanceof Character){
			resultBuilder.append("\"");
			resultBuilder.append(value);
			resultBuilder.append("\"");
		}else if(value instanceof Boolean || value instanceof Number){
			resultBuilder.append(value);
		}else if(value instanceof Date){
			resultBuilder.append("\"");
			String dateStr = DateUtil
					.formatDate((Date)value, datePattern != null ? datePattern : DEFAULT_DATE_PATTERN);
			resultBuilder.append(dateStr);
			resultBuilder.append("\"");
		}else{
			resultBuilder.append(toJson(value, datePattern));
		}
		resultBuilder.append("}");
		return resultBuilder.toString();
	}
	
	/**
	 * 将Json串转换成Java对象
	 * 若Json串中含有Date类型的值，则统一以默认的日期格式{yyyy-MM-dd HH:mm:ss} 进行解析
	 * 若Json串中含有不同格式的日期值，请使用注解方式。
	 * @param json 欲转换的Json串
	 * @param targetClazz 目标对象对应的Class对象
	 * @return
	 */
	public static <T> T toObject(String json, Class<T> targetClazz){
		return toObject(json, targetClazz, DEFAULT_DATE_PATTERN);
	}
	
	/**
	 * 将Json串转换成Java对象
	 * 若Json串中含有Date类型的值，则统一以指定的日期格式进行解析
	 * 若Json串中含有不同格式的日期值，请使用注解方式。
	 * 若读取Json发生IO异常，将抛出IO运行时异常
	 * @param json 欲转换的Json串
	 * @param targetClazz 目标对象对应的Class对象
	 * @param datePattern 日期格式，指定的日期格式需在本处理器支持的日期格式范围内，
	 * 					      否则，将抛出运行时异常
	 * @return
	 */
	public static <T> T toObject(String json, Class<T> targetClazz, String datePattern){
		T targetObject = null;
		ObjectMapper objectMapper = lookupObjectMapper(datePattern);
		if(objectMapper == null){
			throw new IllegalArgumentException("JsonProcessor do not support date pattern: "
					+ datePattern);
		}
		try {
			targetObject = objectMapper.readValue(json, targetClazz);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
		return targetObject;
	}
	
	/**
	 * 查询对应日期格式的objectMapper对象
	 * @param datePattern 日期格式
	 * @return
	 */
	private static ObjectMapper lookupObjectMapper(String datePattern){
		if(datePattern == null){
			datePattern = DEFAULT_DATE_PATTERN;
		}
		ObjectMapper objectMapper = OBJECT_MAPPER.get(datePattern);
		return objectMapper;
	}
	
	@Override
    public void afterPropertiesSet() throws Exception {
		if(datePatterns == null || datePatterns.length == 0){
			datePatterns = new String[]{DEFAULT_DATE_PATTERN};
			LOGGER.warn("Property 'datePatterns' is not setted, " +
					"use default '" + DEFAULT_DATE_PATTERN + "' instead.");
		}
		
		for(String datePattern : datePatterns){
			ObjectMapper objectMapper = newObjectMapper(datePattern);
			OBJECT_MAPPER.put(datePattern, objectMapper);
		}
		
		// 若配置列表中不包含默认日期格式
		if(OBJECT_MAPPER.get(DEFAULT_DATE_PATTERN) == null){
			ObjectMapper defaultObjectMapper = newObjectMapper(DEFAULT_DATE_PATTERN);
			OBJECT_MAPPER.put(DEFAULT_DATE_PATTERN, defaultObjectMapper);
		}
	}
	
	/**
	 * 实例化ObjectMapper
	 * 因实例化ObjectMapper较耗资源，不建议在系统运行时再实例化该类，故本方法是私有的。
	 * @param datePattern 日期格式
	 * @return
	 */
	private ObjectMapper newObjectMapper(String datePattern){
		DateFormat dateFormat = DateUtil.getDateFormat(datePattern);
		if(dateFormat == null){
			throw new IllegalArgumentException("not support date pattern: " + datePattern);
		}
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setDateFormat(dateFormat);
		// 禁用 空bean(no getter) 检查
		objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		// 只输出非空属性到Json字符串
		//objectMapper.setSerializationInclusion(Include.NON_NULL);
		return objectMapper;
	}
	
}
