package com.touclick.captcha.smart.admin.service.impl;

import com.touclick.captcha.smart.admin.mapper.primary.MemberMapper;
import com.touclick.captcha.smart.admin.model.Member;
import com.touclick.captcha.smart.admin.service.inter.IMember;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("IMember")
@Component
public class MemberImpl implements IMember {
    private static Logger logger = Logger.getLogger(SysMenuImpl.class);
    @Autowired
    private MemberMapper writeMemberMapper;
    @Override
    public int add(Member member) {
        String password = member.getPassword();
        member.setPassword(DigestUtils.md5Hex(DigestUtils.sha256Hex(DigestUtils.md5Hex(password))));
        return writeMemberMapper.add(member);
    }

    @Override
    public int addTemp(Member member) {
        String password = member.getPassword();
        member.setPassword(DigestUtils.md5Hex(DigestUtils.sha256Hex(DigestUtils.md5Hex(password))));
        return writeMemberMapper.addTemp(member);
    }

    @Override
    public Member checkLogin(Member member) {
        String password = member.getPassword();
        member.setPassword(DigestUtils.md5Hex(DigestUtils.sha256Hex(DigestUtils.md5Hex(password))));
        Member m = writeMemberMapper.checkLogin(member);
        return m;
    }

    @Override
    public boolean existEmail(String email) {
        Member m = writeMemberMapper.existEmail(email);
        if (m != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean existEmailByAddress(String email, String waddress) {
        Member m = writeMemberMapper.existEmailByAddress(email, waddress);
        if (m != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean existInviteCode(String invitecode) {
        // memberMapper = writableSQLSession.getMapper(MemberMapper.class);
        // boolean flag = memberMapper.existInviteCode(invitecode);
        return false;
    }

    @Override
    public int getWidByMid(int mid) {
        int wid = writeMemberMapper.getWidByMid(mid);
        return wid;
    }

    @Override
    public boolean checkOldpwd(String oldpwd, int mid) {
        String pwd = writeMemberMapper.getPwdByMid(mid);
        if (pwd.equals(DigestUtils.md5Hex(DigestUtils.sha256Hex(DigestUtils.md5Hex(oldpwd))))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void updateNewpwd(String newpwd, int mid) {
        writeMemberMapper.updateNewpwd(DigestUtils.md5Hex(DigestUtils.sha256Hex(DigestUtils.md5Hex(newpwd))), mid);
    }

    @Override
    public void updateInfo(Member m) {
        writeMemberMapper.updateInfo(m);
    }

    @Override
    public void updateStatus(Member m) {
        writeMemberMapper.updateStatus(m);
    }

    @Override
    public void updateToken(Member m) {
        writeMemberMapper.updateToken(m);
    }

    @Override
    public List<Member> getAllMember() {
        List<Member> list = writeMemberMapper.getAllMember();
        return list;
    }

    @Override
    public Member getMember(int mid) {
        return writeMemberMapper.getMember(mid);
    }

    @Override
    public Member checkLoginBySiteKey(String pubkey, String token) {
        return writeMemberMapper.checkLoginBySiteKey(pubkey, token);
    }

    @Override
    public int updateNewpwdByEmail(String newpwd, String email) {
        return writeMemberMapper.updateNewpwdByEmail(
                DigestUtils.md5Hex(DigestUtils.sha256Hex(DigestUtils.md5Hex(newpwd))), email);
    }

    @Override
    public Member getMemberByWaddress(String address) {
        return writeMemberMapper.getMemberByWaddress(address);
    }

    @Override
    public Member getMemberByToken(String token) {
        return writeMemberMapper.getMemberByToken(token);
    }

    @Override
    public int resetPassEmailByWaddress(String email, String password, String waddress) {
        return writeMemberMapper.resetPassEmailByWaddress(email,
                DigestUtils.md5Hex(DigestUtils.sha256Hex(DigestUtils.md5Hex(password))), waddress);
    }

    @Override
    public Member getMemberByWid(int wid) {
        return writeMemberMapper.getMemberByWid(wid);
    }

    @Override
    public int updateMemberLevelByMid(int level, int mid) {
        return writeMemberMapper.updateLevel(level, mid);
    }

    @Override
    public Member getMemberByEmail(String email) {
        return writeMemberMapper.getMemberByEmail(email);
    }

    @Override
    public List<Member> getVipMember() {
        List<Member> list = writeMemberMapper.getVipMember();
        return list;
    }

    @Override
    public List<Member> getMemberListByCreateTime(String start, String end) {
        List<Member> list = writeMemberMapper.getMemberListByCreateTime(start,end);
        return list;
    }
}
