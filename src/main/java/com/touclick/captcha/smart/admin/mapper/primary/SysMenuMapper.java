package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.SysmenuProvider;
import com.touclick.captcha.smart.admin.model.SysMenu;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface SysMenuMapper {
	@SelectProvider(type=SysmenuProvider.class,method="getOneMenu")
	public List<SysMenu> getOneMenu(@Param(value = "role") int role);
	@SelectProvider(type=SysmenuProvider.class,method="getTwoMenu")
	public List<SysMenu> getTwoMenu(@Param(value = "oneid") int oneid, @Param(value = "role") int role);
}
