package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.BlockImageProvider;
import com.touclick.captcha.smart.admin.model.BlockImage;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * BlockBlock mapper
 *
 * @author bing.liu
 * @date 2015-09-21
 */
public interface BlockImageMapper {

    @SelectProvider(type = BlockImageProvider.class,method = "findAll")
    public List<BlockImage> findAll();

    @SelectProvider(type = BlockImageProvider.class,method = "findByNameId")
    public List<BlockImage> findByNameId(@Param(value = "nameId") int nameId);

    @InsertProvider(type = BlockImageProvider.class,method = "insert")
    public void insert(@Param(value = "blockImage") BlockImage blockImage);
}
