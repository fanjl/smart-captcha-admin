package com.touclick.captcha.smart.admin.mapper.primary.provider;

public class PayOrderProvider {
	public String save(){
		return "insert into payorder(oid,mid,wid,vid,money,servicetime,createtime,status) values(#{p.oid},#{p.mid},#{p.wid},#{p.vid},#{p.money},#{p.servicetime},#{p.createtime},#{p.status})";
	}
	public String updateStatus(){
		return "update payorder t set t.status=#{status},t.tradeno=#{tradeno} where t.oid=#{oid}";
	}
	public String getObjectByOid(){
		return "select * from payorder where oid = #{oid}";
	}
}
