package com.touclick.captcha.smart.admin.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by fanjl on 2017/2/13.
 */
@Configuration
@ConfigurationProperties(prefix = "redis")
public class RedisConfig {
    private String REDIS_NAME;
    private String REDIS_ZK_ADDR;
    private String REDIS_MAX_ACTIVE;
    private String REDIS_MAX_IDLE;
    private String REDIS_MAX_WAIT;

    public String getREDIS_NAME() {
        return this.REDIS_NAME;
    }

    public void setREDIS_NAME(String REDIS_NAME) {
        this.REDIS_NAME = REDIS_NAME;
    }

    public String getREDIS_ZK_ADDR() {
        return this.REDIS_ZK_ADDR;
    }

    public void setREDIS_ZK_ADDR(String REDIS_ZK_ADDR) {
        this.REDIS_ZK_ADDR = REDIS_ZK_ADDR;
    }

    public String getREDIS_MAX_ACTIVE() {
        return this.REDIS_MAX_ACTIVE;
    }

    public void setREDIS_MAX_ACTIVE(String REDIS_MAX_ACTIVE) {
        this.REDIS_MAX_ACTIVE = REDIS_MAX_ACTIVE;
    }

    public String getREDIS_MAX_IDLE() {
        return this.REDIS_MAX_IDLE;
    }

    public void setREDIS_MAX_IDLE(String REDIS_MAX_IDLE) {
        this.REDIS_MAX_IDLE = REDIS_MAX_IDLE;
    }

    public String getREDIS_MAX_WAIT() {
        return this.REDIS_MAX_WAIT;
    }

    public void setREDIS_MAX_WAIT(String REDIS_MAX_WAIT) {
        this.REDIS_MAX_WAIT = REDIS_MAX_WAIT;
    }
}
