package com.touclick.captcha.smart.admin.model;

public class CversionMapper {
	private String cversion;
	private String ctype;
	private String noctype;
	public String getCversion() {
		return cversion;
	}
	public void setCversion(String cversion) {
		this.cversion = cversion;
	}
	public String getNoctype() {
		return noctype;
	}
	public void setNoctype(String noctype) {
		this.noctype = noctype;
	}
	public String getCtype() {
		return ctype;
	}
	public void setCtype(String ctype) {
		this.ctype = ctype;
	}
	
}
