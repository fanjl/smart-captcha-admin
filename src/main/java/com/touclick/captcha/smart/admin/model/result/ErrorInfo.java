package com.touclick.captcha.smart.admin.model.result;

public enum ErrorInfo {

    AUTHENTICATION(4001, "认证失败"),
    USER_PERMISSION_DENIED(4006, "没有权限"),
    USER_NOT_FOUND(6001,"用户不存在"),
    USER_NAME_EXITS(6002,"用户名已被注册"),
    PASSWORD_ERROR(6003,"密码错误"),
    IDENTITY_EXISTS(6004,"身份证已被注册"),
    CORP_KEYPAIR_NOT_FOUND(6008,"找不到公钥私钥"),
    USER_ROLE_NOT_MATCH(6010,"用户角色不匹配"),
    USER_STATUS_ERROR(6011,"用户状态异常"),
    TELEPHONE_EXISTS(6012,"手机号已经注册"),
    TELEPHONE_ILLEGAL(6013,"手机号不合法"),
    DOWNLOAD_ERROR(8003,"下载失败"),
    SERVER_ERROR(9001,"服务器错误");

    private int errorCode;
    private String errorMessage;

    private ErrorInfo(int code, String message) {
        this.errorCode = code;
        this.errorMessage = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
