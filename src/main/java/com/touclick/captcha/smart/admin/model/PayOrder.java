package com.touclick.captcha.smart.admin.model;

import java.util.Date;

public class PayOrder {
	private long oid;
	private int mid;
	private int wid;
	private int vid;
	private int money;
	private int servicetime;//使用时间单位月
	private Date createtime;
	private int status;//订单状态
	private String tradeno; //支付宝交易号 用于查询交易状态
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getOid() {
		return oid;
	}
	public void setOid(long oid) {
		this.oid = oid;
	}
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	public int getServicetime() {
		return servicetime;
	}
	public void setServicetime(int servicetime) {
		this.servicetime = servicetime;
	}
	public String getTradeno() {
		return tradeno;
	}
	public void setTradeno(String tradeno) {
		this.tradeno = tradeno;
	}
	public int getVid() {
		return vid;
	}
	public void setVid(int vid) {
		this.vid = vid;
	}
	
}
