package com.touclick.captcha.smart.admin.service.inter;


import com.touclick.captcha.smart.admin.model.HistoryPay;
import com.touclick.captcha.smart.admin.model.Member;

import java.util.List;

public interface IBalance {
	public void save(HistoryPay po);
	
	public void saveSimple(HistoryPay po);
	
	public void saveTrial(Member m);
	
	public void updateStatus(int hid, int status);
	
	public void updateByAdmin(HistoryPay po);
	
	public void update(HistoryPay hp);
	
	public HistoryPay getObjectByMid(int mid, int status);
	
	public List<HistoryPay> getHistoryPaysByMid(int mid);
	
	public HistoryPay getLastPayByMid(int mid);
	
	public HistoryPay getOrderByHid(int hid);
	
	public HistoryPay getObjectByaliCount(String aliCount);
	
	public int deleteHistoryPaysByhid(int mid) ;
	
	public List<HistoryPay> getHistoryPaysByStatus(int status);
	
	public List<HistoryPay> getHistoryPaysByEndTime(long endTime);
}
