package com.touclick.captcha.smart.admin.mapper.secondary;

import com.touclick.captcha.smart.admin.mapper.secondary.provider.DataProvider;
import com.touclick.captcha.smart.admin.model.Tall;
import com.touclick.captcha.smart.admin.model.Tlast;
import com.touclick.captcha.smart.admin.model.WebSite;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface DataMapper {
	@SelectProvider(type=DataProvider.class,method="getTodaySv")
	public Integer getTodaySv(List<WebSite> weblist);
	
	@SelectProvider(type=DataProvider.class,method="getTotalGByWids")
    public Long getTotalGByWids(@Param(value = "wids") List<Integer> wids);
	
	@SelectProvider(type=DataProvider.class,method="getYestodayAsv")
	public Integer getYestodayAsv(@Param(value = "ads") String ads);
	
	@SelectProvider(type=DataProvider.class,method="getDataByDateRange")
	public List<Tlast> getDataByDateRange(@Param(value = "wid") int wid, @Param(value = "begin") long begin, @Param(value = "end") long end);
	
	@SelectProvider(type=DataProvider.class,method="getDataByDateRangeMonth")
	public List<Tall> getDataByDateRangeMonth(@Param(value = "wid") int wid, @Param(value = "begin") long begin, @Param(value = "end") long end);
	
	@SelectProvider(type=DataProvider.class,method="getDataByDateRangeWeek")
    public List<Tlast> getDataByDateRangeWeek(@Param(value = "wid") int wid, @Param(value = "begin") long begin, @Param(value = "end") long end);
	
	@SelectProvider(type=DataProvider.class,method="getDataByDateRangeYear")
	public List<Tall> getDataByDateRangeYear(@Param(value = "wid") int wid, @Param(value = "begin") long begin, @Param(value = "end") long end);
	
	@SelectProvider(type=DataProvider.class,method="getAllData")
	public List<Tall> getAllData(@Param("ltype") int ltype);
	
	@SelectProvider(type=DataProvider.class,method="getAvgHourDataByLtype")
	public List<Tlast> getAvgHourDataByLtype(@Param("wid") int wid, @Param("ltype") int ltype);
	
	@SelectProvider(type=DataProvider.class,method="getAvgDayDataByLtype")
	public List<Tall> getAvgDayDataByLtype(@Param("wid") int wid, @Param("ltype") int ltype);
	
	@SelectProvider(type=DataProvider.class,method="getDataSumByWidLtypeAndTime")
    public List<Tall> getDataSumByWidLtypeAndTime(@Param("wid") int wid, @Param("ltype") int ltype, @Param("begin") long begin, @Param(value = "end") long end);
}
