package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.AccountProvider;
import com.touclick.captcha.smart.admin.model.MemberInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AccountMapper {
	@SelectProvider(type=AccountProvider.class,method="getAccountList")
	public List<MemberInfo> getAccountList(@Param(value = "mid") int mid);
	
	@SelectProvider(type=AccountProvider.class,method="checkAccount")
	public MemberInfo checkAccount(@Param(value = "accounttype") int accounttype, @Param(value = "account") String account);
	
	@InsertProvider(type=AccountProvider.class,method="saveAccount")
	public int saveAccount(@Param(value = "mid") int mid, @Param(value = "accounttype") int accounttype, @Param(value = "account") String account, @Param(value = "useflag") int useflag);
	
	@DeleteProvider(type=AccountProvider.class,method="delAccount")
	public int delAccount(@Param(value = "id") int id);
	
	@UpdateProvider(type=AccountProvider.class,method="updateAccount")
	public void updateAccount(@Param(value = "mid") int mid);
	
	@UpdateProvider(type=AccountProvider.class,method="defaultAccount")
	public void defaultAccount(@Param(value = "id") int id);
}
