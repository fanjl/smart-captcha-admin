package com.touclick.captcha.smart.admin.model;

import java.util.Date;

/**
 * 图标选择型验证码图片名称model
 *
 * @author bing.liu
 * @date 2015-09-21
 */

public class BlockName {
    /** id */
    private int id;

    /** 创建时间 */
    private Date createTime;

    /** name */
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString(){
        return name;
    }

    @Override
    public boolean equals(Object o){
        if(o == null || !(o instanceof com.touclick.captcha.smart.admin.model.BlockName)){
            return false;
        }

        com.touclick.captcha.smart.admin.model.BlockName other = (com.touclick.captcha.smart.admin.model.BlockName)o;
        return this.name.equals(other.name);
    }

    @Override
    public int hashCode(){
        return name.hashCode();
    }
}
