package com.touclick.captcha.smart.admin.mapper.primary.provider;

import com.touclick.captcha.smart.admin.model.WebSite;

import java.util.Map;

public class WebSiteProvider {
	public String addWebSite(Map<String, WebSite> paras) {
		return "insert into website(wname,pubkey,token,useflag,createtime,mid,wtype,platform,waddress,ctype,ctypeplus,cversion,skin,verifyflag) values"
				+ "(#{website.wname},#{website.pubkey},#{website.token},#{website.useflag},#{website.createtime},#{website.mid},#{website.wtype},#{website.platform},#{website.waddress},#{website.ctype},#{website.ctypeplus},#{website.cversion},#{website.skin},#{website.verifyflag})";
	}

	public String addWebSiteTemp(Map<String, WebSite> paras) {
		return "insert into website(wid,wname,pubkey,token,useflag,createtime,mid,wtype,platform,waddress,ctype,ctypeplus,cversion,skin,verifyflag) values"
				+ "(#{website.wid},#{website.wname},#{website.pubkey},#{website.token},#{website.useflag},#{website.createtime},#{website.mid},#{website.wtype},#{website.platform},#{website.waddress},#{website.ctype},#{website.ctypeplus},#{website.cversion},#{website.skin},#{website.verifyflag})";
	}

	public String addWebSiteInfo() {
		return "insert into website_info(wid,type,value) values(#{websiteinfo.wid},#{websiteinfo.type},#{websiteinfo.value})";
	}

	public String existWebSite(Map<String, WebSite> paras) {
		return "select * from website t where t.waddress=#{waddress} and t.useflag=1";
	}
	
	public String existWebSiteByMid(){
		return "select * from website t where t.waddress=#{waddress} and t.useflag=1 and t.mid=#{mid}";
	}
	
	public String getWebsiteByPubkey(Map<String, WebSite> paras) {
		return "select * from website t where t.pubkey=#{pubkey}";
	}
	
	public String getWebsiteCountByMid(Map<String, WebSite> paras) {
		return "select count(*) from website t where t.mid=#{mid} and useflag = 1";
	}
	
	public String getWebSiteList(Map<String, Object> paras) {
		return "select * from website t where t.useflag=1 and t.mid=#{mid}";
	}
	
	

	public String isOnlyOwnerPic(Map<String, Integer> params) {
		return "select value from website_info where wid=#{wid} and type=#{type}";
	}

	public String findAll() {
		return "select * from website t where t.useflag = 1";
	}

	public String findWebSiteInfo(Map<String, Integer> params) {
		if(params.get("type") != null){
			return "select * from website_info where type = #{type}";
		}
		return "select * from website_info where wid = #{wid}";
	}
	
	public String findWebSiteInfoByWid() {
        return "select * from website_info where wid = #{wid}";
    }
	
	
	public String findAllWebSiteInfo() {
		return "select * from website_info";
	}

	public String insertWebSiteInfo() {
		return "insert into website_info(wid,type,value) values(#{wid},#{type},#{value})";
	}
	
	public String cancelByWidAndType() {
		return "delete from website_info where wid=#{wid} and type=#{type}";
	}

	public String getUsingWebsiteByMid() {
		return "select * from website t where t.mid=#{mid} and t.useflag=1";
	}

	public String updateCtypeAndSkin() {
		return "update website t set t.ctype=#{ctype},t.skin=#{skin} where t.wid=#{wid}";
	}

	public String getWebSiteByWid() {
		return "select * from website t where t.wid=#{wid}";
	}

	public String delWebSite() {
		return "update website t set t.useflag = 0,t.deletetime = now() where t.wid = #{wid}";
	}
	
	public String resetToken(){
		return "update website t set t.token = #{token} where t.wid = #{wid}";
	}
	public String updateVerify(){
		return "update website t set t.verifyflag = #{verifyflag} where t.wid = #{wid}";
	}
	public String getWebsiteInfo(){
		return "select * from website_info t where t.wid = #{wid}";
	}
	public String updateWebSiteInfo(){
		return "update website_info t set t.value = #{value} where t.wid = #{wid} and t.type = #{type}";
	}
	public String delCustom(){
		return "delete from website_info where wid = #{wid} and type = #{type}";
	}
	public String udpateCversionByWid(){
		return "update website t set t.cversion = #{cversion} where t.wid = #{wid}";
	}
	public String udpateCversionByMid(){
		return "update website t set t.cversion = #{cversion} where t.mid = #{mid}";
	}
	public String udpateCversionByPubkey(){
		return "update website t set t.cversion = #{cversion} where t.pubkey = #{pubkey}";
	}
	public String updateCversionCTypeByPubkey(){
		return "update website t set t.cversion = #{cversion} and t.ctype=#{ctype} where t.pubkey = #{pubkey}";
	}
	public String udpateCversionByPlatformCversion(){
		return "update website t set t.cversion = #{desVersion} where t.platform = #{platform} and t.cversion = #{srcVersion}";
	}
	public String getWebSiteByPlatformCversion(){
		return "select * from website t where t.platform = #{platform} and t.cversion = #{cversion}";
	}
	public String getAllWebSite(){
		return "select * from website t where t.useflag = 1";
	}
    public String getOldVersionWebSite(){
        return "select pubkey from website t where t.cversion not in ('5.0.1','5.2.0','5.1.0') ";
    }
	
	
}
