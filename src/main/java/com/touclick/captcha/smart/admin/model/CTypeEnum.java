package com.touclick.captcha.smart.admin.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/** 
 * @ClassName: CTypeEnum 
 * @Description: TODO
 *  
 */
public enum CTypeEnum {
    Click("图文点击型", 1 , 1), Block("图标选择型", 2 , 2), Rotate("拖动旋转型", 4 , 4), Pic("图点击型", 6,6), Block2("图标选择型(包含文字印刷)", 11 , 11), Rotate2(
            "新版旋转型", 12, 4), Click2("新版图文点击型", 13, 1), Block3("新版图标选择型", 14 , 2),

    Drag2("拖动型2", 5,5), Pic4("4图点击型", 7,7), Pic6("6图点击型", 8,8), Pic8("8图点击型", 9,9), Pic18("18图点击型", 10,10), Drag("拖动型", 3 ,3);

    private final static Logger LOOGER = LoggerFactory.getLogger(com.touclick.captcha.smart.admin.model.CTypeEnum.class);

    /**
     * 默认类型
     */
    public final static com.touclick.captcha.smart.admin.model.CTypeEnum DEFAULT_CTYPE = com.touclick.captcha.smart.admin.model.CTypeEnum.Block;

    final String displayName;
    final int id;
    final int customId;

    private CTypeEnum(String displayName, int id, int customId) {
        this.displayName = displayName;
        this.id = id;
        this.customId = customId;
        MapHolder.map.put(id, this);
        MapHolder.map.put(customId, this);
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getId() {
        return id;
    }
    
    public int getCustomId() {
        return customId;
    }

    private static class MapHolder {
        private final static Map<Integer, com.touclick.captcha.smart.admin.model.CTypeEnum> map = new HashMap<Integer, com.touclick.captcha.smart.admin.model.CTypeEnum>();
    }

    /**
     * 5.2.0以前使用
     * 
     * @param id
     * @return
     */
    @Deprecated
    public static com.touclick.captcha.smart.admin.model.CTypeEnum getCTypeEnumById(int id) {
        com.touclick.captcha.smart.admin.model.CTypeEnum ctype = MapHolder.map.get(id);
        if (ctype == null) {
            LOOGER.error("id is error ,can not find ");
            ctype = com.touclick.captcha.smart.admin.model.CTypeEnum.DEFAULT_CTYPE;
        }
        return ctype;
    }

    /**
     * 可能返回 null
     * @author foxer 2016.08.26
     * @param id
     * @return
     */
    public static com.touclick.captcha.smart.admin.model.CTypeEnum getCTypeById(int id) {
        return MapHolder.map.get(id);
    }
    
}
