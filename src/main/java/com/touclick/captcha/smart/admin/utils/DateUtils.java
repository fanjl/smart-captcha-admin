package com.touclick.captcha.smart.admin.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	public static long getFirstDayOfMonth(){
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.getFirstDayOfWeek();
		calendar.set(year, month, day);
		return calendar.getTimeInMillis();
	}
	public static long getLastDayOfMonth(){
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH)+1;
		int day = calendar.getFirstDayOfWeek();
		calendar.set(year, month, day);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		return calendar.getTimeInMillis();
	}
	public static long getFirstMonth(){
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		calendar.set(year, 0, 1);
		return calendar.getTimeInMillis();
	}
	public static Date afterMonth(int n){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, n);
		return calendar.getTime();
	}
	public static Date dateAfterMonth(Date begin, int n){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(begin);
		calendar.add(Calendar.MONTH, n);
		return calendar.getTime();
	}
	public static Date longToDate(long time){
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		return c.getTime();
	}
	public static int caculaterDay(long endTime){
		try{
			long nowTime = new Date().getTime();
			if(endTime < nowTime){
				return 0;
			}
			long daylong = endTime - nowTime;
			long day = daylong/86400000;
			String daystr = day + "";
			return Integer.valueOf(daystr);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	public static int caculaterDay(long startTime, long endTime){
        try{
            if(endTime < startTime){
                return 0;
            }
            long daylong = endTime - startTime;
            long day = daylong/86400000;
            String daystr = day + "";
            return Integer.valueOf(daystr);
        }catch(Exception e){
            e.printStackTrace();
        }
        return 0;
    }
	public static int caculaterMonth(long startTime,long endTime){
        try{
            long daylong = endTime - startTime;
            long day = daylong/86400000L;
            long month = day / 30;
            String monthstr = month + "";
            return Integer.valueOf(monthstr);
        }catch(Exception e){
            e.printStackTrace();
        }
        return 0;
    }
	public static String today(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}
	public static long addMonthTime(long startTime,int month){
	    long monthTime = month * 30 * 86400000L;
	    long endTime = startTime + monthTime;
        return endTime;
    }
	public static void main(String[] args) {
		Date da = new Date();
		long newl = da.getTime() + 86400000L*114;
		System.out.println(da.getTime());
		System.out.println(longToDate(newl));
		System.out.println(newl);
		System.out.println(longToDate(addMonthTime(da.getTime(), 1)));
	}
}
