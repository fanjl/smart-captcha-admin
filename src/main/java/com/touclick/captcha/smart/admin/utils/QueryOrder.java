package com.touclick.captcha.smart.admin.utils;

public enum QueryOrder {
	ASC("ASC"), DESC("DESC");
	
	private String type;
	
	private QueryOrder(String type){
		this.type = type;
	}
	
	public String transformToQueryString(String columnName){
		StringBuilder builder = new StringBuilder(StringUtil.SIGN_SINGLE_BLANK);
		builder.append(columnName);
		builder.append(StringUtil.SIGN_SINGLE_BLANK);
		builder.append(type);
		return builder.toString();
	}
}
