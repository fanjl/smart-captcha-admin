package com.touclick.captcha.smart.admin.mapper.secondary.provider;


import com.touclick.captcha.smart.admin.model.CaptchaType;

import java.util.Map;

/**
 * @author bing.liu
 */
public class CaptchaTypeProvider {
    public String insert(Map<String,CaptchaType> params){
        return "INSERT INTO ctype (name) values(#{ctype.name})";
    }

    public String selectById(Map<String,Integer> params){
        return "SELECT * FROM ctype where id = ${id}";
    }

    public String selectAll(){
        return "SELECT * FROM ctype";
    }

    public String selectByName(Map<String,String> params){
        return "SELECT * FROM ctype where name = '#{name}'";
    }
}
