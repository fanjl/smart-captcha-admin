package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.BlockImage;

import java.util.List;

/**
 * BlockImage service interface
 *
 * @author bing.liu
 * @date 2015-09-21
 */
public interface IBlockImage {

    /**
     * Select all the block iamge
     * @return
     */
    public List<BlockImage> findAll();

    /**
     * Select the block image by nameid
     * @param nameId
     * @return
     */
    public List<BlockImage> findByNameId(int nameId);

    /**
     * Insert a blockimage into the database
     * @param blockImage
     */
    public void insert(BlockImage blockImage);
}
