package com.touclick.captcha.smart.admin.model;

import org.springframework.web.multipart.MultipartFile;

public class CustomAds {
	private String tempurl;
	private String printfont;//原顺序印刷文字
	private String newprintfont;//点击文字放前边，其他放后边的印刷文字
	private String clickfont;
	private int imgtexttype;
	private String transparent1;
	private String transparent2;
	private String transparent3;
	private String remind1;
	private String remind2;
	private String remind3;
	private MultipartFile smallpic1;
	private MultipartFile smallpic2;
	private MultipartFile smallpic3;
	private String background;
	private int adgroups;
	private int wid;
	public String getTempurl() {
		return tempurl;
	}
	public void setTempurl(String tempurl) {
		this.tempurl = tempurl;
	}
	public String getPrintfont() {
		return printfont;
	}
	public void setPrintfont(String printfont) {
		this.printfont = printfont;
	}
	public String getClickfont() {
		return clickfont;
	}
	public void setClickfont(String clickfont) {
		this.clickfont = clickfont;
	}
	public String getRemind1() {
		return remind1;
	}
	public void setRemind1(String remind1) {
		this.remind1 = remind1;
	}
	public String getRemind2() {
		return remind2;
	}
	public void setRemind2(String remind2) {
		this.remind2 = remind2;
	}
	public String getRemind3() {
		return remind3;
	}
	public void setRemind3(String remind3) {
		this.remind3 = remind3;
	}
	public int getImgtexttype() {
		return imgtexttype;
	}
	public void setImgtexttype(int imgtexttype) {
		this.imgtexttype = imgtexttype;
	}
	public MultipartFile getSmallpic1() {
		return smallpic1;
	}
	public void setSmallpic1(MultipartFile smallpic1) {
		this.smallpic1 = smallpic1;
	}
	public MultipartFile getSmallpic2() {
		return smallpic2;
	}
	public void setSmallpic2(MultipartFile smallpic2) {
		this.smallpic2 = smallpic2;
	}
	public MultipartFile getSmallpic3() {
		return smallpic3;
	}
	public void setSmallpic3(MultipartFile smallpic3) {
		this.smallpic3 = smallpic3;
	}
	public String getTransparent1() {
		return transparent1;
	}
	public void setTransparent1(String transparent1) {
		this.transparent1 = transparent1;
	}
	public String getTransparent2() {
		return transparent2;
	}
	public void setTransparent2(String transparent2) {
		this.transparent2 = transparent2;
	}
	public String getTransparent3() {
		return transparent3;
	}
	public void setTransparent3(String transparent3) {
		this.transparent3 = transparent3;
	}
	public String getNewprintfont() {
		return newprintfont;
	}
	public void setNewprintfont(String newprintfont) {
		this.newprintfont = newprintfont;
	}
	public int getAdgroups() {
		return adgroups;
	}
	public void setAdgroups(int adgroups) {
		this.adgroups = adgroups;
	}
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	
}
