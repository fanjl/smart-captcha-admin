package com.touclick.captcha.smart.admin.model;

public class AccessLogSystem {
    private int id;
    private String pubkey;
    private String ip;
    private String date;
    private int count;
    private String operationsystem;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getPubkey() {
        return pubkey;
    }
    public void setPubkey(String pubkey) {
        this.pubkey = pubkey;
    }
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
    public String getOperationsystem() {
        return operationsystem;
    }
    public void setOperationsystem(String operationsystem) {
        this.operationsystem = operationsystem;
    }
}
