package com.touclick.captcha.smart.admin.model;

public enum	LTypeEnum {
	G("展示量",1),VY("正确量",2),VN("错误量",3),SV("二次验证量",4),AD("广告跳转量",5),SE("二次验证错误",6);
	private String name;
	private int index;
	private LTypeEnum(String name, int index){
		this.name = name;  
        this.index = index;
	}
	/**
     * 返回索引值
     * @param name
     * @return
     */
    public static int getIndex(String name){
    	for (com.touclick.captcha.smart.admin.model.LTypeEnum c : com.touclick.captcha.smart.admin.model.LTypeEnum.values()) {
            if (c.name().equals(name)) {  
                return c.index;
            }
    	}
        return 0;
    }
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	
}
