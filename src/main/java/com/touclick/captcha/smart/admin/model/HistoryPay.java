package com.touclick.captcha.smart.admin.model;

import java.util.Date;

public class HistoryPay {
    private int hid; // 主键
    private int mid; // 用户id
    private String aliCount; // 交易流水号
    private long payTime;// 交易时间
    private long beginTime;// 开始时间
    private long endTime; // 到期时间
    private int product;// 购买产品
    private double money;// 金额
    private int status;// 交易状态
    private int contract;// 1，需要合同；0，不需要合同
    private int receipt;// 1,需要发票；0，不需要发票
    private Date payTimeDate;
    private Date beginTimeDate;
    private Date endTimeDate;
    private String payTimeStr;
    private String beginTimeStr;
    private String endTimeStr;

    private int leftDay;

    public HistoryPay() {
    }

    public HistoryPay(int hid, int mid, String aliCount, long payTime, long beginTime, long endTime, int product,
                      double money, int status) {
        this.hid = hid;
        this.mid = mid;
        this.aliCount = aliCount;
        this.payTime = payTime;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.product = product;
        this.money = money;
        this.status = status;
    }

    public int getHid() {
        return hid;
    }

    public void setHid(int hid) {
        this.hid = hid;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public String getAliCount() {
        return aliCount;
    }

    public void setAliCount(String trade_no) {
        this.aliCount = trade_no;
    }

    public long getPayTime() {
        return payTime;
    }

    public void setPayTime(long payTime) {
        this.payTime = payTime;
    }

    public long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(long beginTime) {
        this.beginTime = beginTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getPayTimeDate() {
        return payTimeDate;
    }

    public void setPayTimeDate(Date payTimeDate) {
        this.payTimeDate = payTimeDate;
    }

    public Date getBeginTimeDate() {
        return beginTimeDate;
    }

    public void setBeginTimeDate(Date beginTimeDate) {
        this.beginTimeDate = beginTimeDate;
    }

    public Date getEndTimeDate() {
        return endTimeDate;
    }

    public void setEndTimeDate(Date endTimeDate) {
        this.endTimeDate = endTimeDate;
    }

    public int getLeftDay() {
        return leftDay;
    }

    public void setLeftDay(int leftDay) {
        this.leftDay = leftDay;
    }

    public int getReceipt() {
        return receipt;
    }

    public void setReceipt(int receipt) {
        this.receipt = receipt;
    }

    public int getContract() {
        return contract;
    }

    public void setContract(int contract) {
        this.contract = contract;
    }

    public String getPayTimeStr() {
        return payTimeStr;
    }

    public void setPayTimeStr(String payTimeStr) {
        this.payTimeStr = payTimeStr;
    }

    public String getBeginTimeStr() {
        return beginTimeStr;
    }

    public void setBeginTimeStr(String beginTimeStr) {
        this.beginTimeStr = beginTimeStr;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

        
    
}
