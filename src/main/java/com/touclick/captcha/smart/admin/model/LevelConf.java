package com.touclick.captcha.smart.admin.model;

import com.touclick.level_config.LevelConfig.Conf;
import com.touclick.level_config.TimeTypeEnum;

public class LevelConf {
    public int idCountLimit;
    public String validateFreq;
    public String isEnableRemoveLogo;
    public String isEnableRemoveHelp;
    public String isCustomText;
    public String isSupportBehavior;
    public String isSupportDeviceFp;
    public String isSupportMachineLearning;
    public String isSupportSecurityListerner;
    public String isSupportRiskManagerModel;
    public String isSupportPasswordSecurity;
    public String isSupportSecurityProfessor;
    public String isSupportSsl;
    public String isSupportOverseasDeploy;
    public String isSupportExclusiveLine;
    public int maxConcurrent;
    public int maxRequest;
    public String isSupportValidateAds;
    public String isSupportSmartNoVerifyExperience;
    public String isSupportVerifyTypeChange;
    public String isSupportRemoveTouclickHelp;
    public String isSupportHdPicture;
    public String isSupportCustomVerifyPicture;
    public String isSupportCustomPromptPicture;
    public String isSupportCustomClickPicture;
    public String isSupportMultiLanguage;
    public String isSupportEmergencyResponse;
    public String isSupportMonthSecurityReport;
    public String isSupportYearSecurityReport;
    public String isSupportUserIdentify;
    public String isSupportAccurateBadBehavior;
    public String isSupportMillCooperativeDefense;
    public String isSupportMobileMultiTerminal;

    public LevelConf(){

    }
    public LevelConf(Conf conf) {
        this.idCountLimit = conf.getIdCountLimit();
        if (conf.getSsl()) {
            this.isSupportSsl = "支持";
        } else {
            this.isSupportSsl = "不支持";
        }
        if (conf.getMobileMultiTerminal()) {
            this.isSupportMobileMultiTerminal = "支持";
        } else {
            this.isSupportMobileMultiTerminal = "不支持";
        }
        this.maxConcurrent = conf.getMaxConcurrent();
        this.maxRequest = conf.getMaxRequest();
        for (TimeTypeEnum t : TimeTypeEnum.values()) {
            if (t.getName().equals(conf.getValidateFeatureUpdateFrequency().getName())) {
                this.validateFreq = t.getDescribe(t.name());
            }
        }
        if (conf.getPasswordSecurity()) {
            this.isSupportPasswordSecurity = "支持";
        } else {
            this.isSupportPasswordSecurity = "不支持";
        }
        if (conf.getSecurityProfessor()) {
            this.isSupportSecurityProfessor = "支持";
        } else {
            this.isSupportSecurityProfessor = "不支持";
        }
        if (conf.getRemoveTouclickHelp()) {
            this.isSupportRemoveTouclickHelp = "支持";
        } else {
            this.isSupportRemoveTouclickHelp = "不支持";
        }
        if (conf.getIsEnableRemoveLogo()) {
            this.isEnableRemoveLogo = "支持";
        } else {
            this.isEnableRemoveLogo = "不支持";
        }

    }

    public String getIsCustomText() {
        return isCustomText;
    }

    public void setIsCustomText(String isCustomText) {
        this.isCustomText = isCustomText;
    }

    public int getIdCountLimit() {
        return idCountLimit;
    }

    public void setIdCountLimit(int idCountLimit) {
        this.idCountLimit = idCountLimit;
    }

    public String getIsEnableRemoveLogo() {
        return isEnableRemoveLogo;
    }

    public void setIsEnableRemoveLogo(String isEnableRemoveLogo) {
        this.isEnableRemoveLogo = isEnableRemoveLogo;
    }

    public String getIsSupportBehavior() {
        return isSupportBehavior;
    }

    public void setIsSupportBehavior(String isSupportBehavior) {
        this.isSupportBehavior = isSupportBehavior;
    }

    public String getIsSupportDeviceFp() {
        return isSupportDeviceFp;
    }

    public void setIsSupportDeviceFp(String isSupportDeviceFp) {
        this.isSupportDeviceFp = isSupportDeviceFp;
    }

    public String getIsSupportMachineLearning() {
        return isSupportMachineLearning;
    }

    public void setIsSupportMachineLearning(String isSupportMachineLearning) {
        this.isSupportMachineLearning = isSupportMachineLearning;
    }

    public String getIsSupportSecurityListerner() {
        return isSupportSecurityListerner;
    }

    public void setIsSupportSecurityListerner(String isSupportSecurityListerner) {
        this.isSupportSecurityListerner = isSupportSecurityListerner;
    }

    public String getIsSupportRiskManagerModel() {
        return isSupportRiskManagerModel;
    }

    public void setIsSupportRiskManagerModel(String isSupportRiskManagerModel) {
        this.isSupportRiskManagerModel = isSupportRiskManagerModel;
    }

    public String getIsSupportPasswordSecurity() {
        return isSupportPasswordSecurity;
    }

    public void setIsSupportPasswordSecurity(String isSupportPasswordSecurity) {
        this.isSupportPasswordSecurity = isSupportPasswordSecurity;
    }

    public String getIsSupportSecurityProfessor() {
        return isSupportSecurityProfessor;
    }

    public void setIsSupportSecurityProfessor(String isSupportSecurityProfessor) {
        this.isSupportSecurityProfessor = isSupportSecurityProfessor;
    }

    public String getIsSupportSsl() {
        return isSupportSsl;
    }

    public void setIsSupportSsl(String isSupportSsl) {
        this.isSupportSsl = isSupportSsl;
    }

    public String getIsSupportOverseasDeploy() {
        return isSupportOverseasDeploy;
    }

    public void setIsSupportOverseasDeploy(String isSupportOverseasDeploy) {
        this.isSupportOverseasDeploy = isSupportOverseasDeploy;
    }

    public String getIsSupportExclusiveLine() {
        return isSupportExclusiveLine;
    }

    public void setIsSupportExclusiveLine(String isSupportExclusiveLine) {
        this.isSupportExclusiveLine = isSupportExclusiveLine;
    }

    public int getMaxConcurrent() {
        return maxConcurrent;
    }

    public void setMaxConcurrent(int maxConcurrent) {
        this.maxConcurrent = maxConcurrent;
    }

    public int getMaxRequest() {
        return maxRequest;
    }

    public void setMaxRequest(int maxRequest) {
        this.maxRequest = maxRequest;
    }

    public String getIsSupportValidateAds() {
        return isSupportValidateAds;
    }

    public void setIsSupportValidateAds(String isSupportValidateAds) {
        this.isSupportValidateAds = isSupportValidateAds;
    }

    public String getIsSupportSmartNoVerifyExperience() {
        return isSupportSmartNoVerifyExperience;
    }

    public void setIsSupportSmartNoVerifyExperience(String isSupportSmartNoVerifyExperience) {
        this.isSupportSmartNoVerifyExperience = isSupportSmartNoVerifyExperience;
    }

    public String getIsSupportVerifyTypeChange() {
        return isSupportVerifyTypeChange;
    }

    public void setIsSupportVerifyTypeChange(String isSupportVerifyTypeChange) {
        this.isSupportVerifyTypeChange = isSupportVerifyTypeChange;
    }

    public String getIsSupportRemoveTouclickHelp() {
        return isSupportRemoveTouclickHelp;
    }

    public void setIsSupportRemoveTouclickHelp(String isSupportRemoveTouclickHelp) {
        this.isSupportRemoveTouclickHelp = isSupportRemoveTouclickHelp;
    }

    public String getIsSupportHdPicture() {
        return isSupportHdPicture;
    }

    public void setIsSupportHdPicture(String isSupportHdPicture) {
        this.isSupportHdPicture = isSupportHdPicture;
    }

    public String getIsSupportCustomVerifyPicture() {
        return isSupportCustomVerifyPicture;
    }

    public void setIsSupportCustomVerifyPicture(String isSupportCustomVerifyPicture) {
        this.isSupportCustomVerifyPicture = isSupportCustomVerifyPicture;
    }

    public String getIsSupportCustomPromptPicture() {
        return isSupportCustomPromptPicture;
    }

    public void setIsSupportCustomPromptPicture(String isSupportCustomPromptPicture) {
        this.isSupportCustomPromptPicture = isSupportCustomPromptPicture;
    }

    public String getIsSupportCustomClickPicture() {
        return isSupportCustomClickPicture;
    }

    public void setIsSupportCustomClickPicture(String isSupportCustomClickPicture) {
        this.isSupportCustomClickPicture = isSupportCustomClickPicture;
    }

    public String getIsSupportMultiLanguage() {
        return isSupportMultiLanguage;
    }

    public void setIsSupportMultiLanguage(String isSupportMultiLanguage) {
        this.isSupportMultiLanguage = isSupportMultiLanguage;
    }

    public String getIsSupportEmergencyResponse() {
        return isSupportEmergencyResponse;
    }

    public void setIsSupportEmergencyResponse(String isSupportEmergencyResponse) {
        this.isSupportEmergencyResponse = isSupportEmergencyResponse;
    }

    public String getIsSupportMonthSecurityReport() {
        return isSupportMonthSecurityReport;
    }

    public void setIsSupportMonthSecurityReport(String isSupportMonthSecurityReport) {
        this.isSupportMonthSecurityReport = isSupportMonthSecurityReport;
    }

    public String getIsSupportYearSecurityReport() {
        return isSupportYearSecurityReport;
    }

    public void setIsSupportYearSecurityReport(String isSupportYearSecurityReport) {
        this.isSupportYearSecurityReport = isSupportYearSecurityReport;
    }

    public String getIsSupportUserIdentify() {
        return isSupportUserIdentify;
    }

    public void setIsSupportUserIdentify(String isSupportUserIdentify) {
        this.isSupportUserIdentify = isSupportUserIdentify;
    }

    public String getIsSupportAccurateBadBehavior() {
        return isSupportAccurateBadBehavior;
    }

    public void setIsSupportAccurateBadBehavior(String isSupportAccurateBadBehavior) {
        this.isSupportAccurateBadBehavior = isSupportAccurateBadBehavior;
    }

    public String getIsSupportMillCooperativeDefense() {
        return isSupportMillCooperativeDefense;
    }

    public void setIsSupportMillCooperativeDefense(String isSupportMillCooperativeDefense) {
        this.isSupportMillCooperativeDefense = isSupportMillCooperativeDefense;
    }

    public String getIsSupportMobileMultiTerminal() {
        return isSupportMobileMultiTerminal;
    }

    public void setIsSupportMobileMultiTerminal(String isSupportMobileMultiTerminal) {
        this.isSupportMobileMultiTerminal = isSupportMobileMultiTerminal;
    }

    public String getValidateFreq() {
        return validateFreq;
    }

    public void setValidateFreq(String validateFreq) {
        this.validateFreq = validateFreq;
    }

    public String getIsEnableRemoveHelp() {
        return isEnableRemoveHelp;
    }

    public void setIsEnableRemoveHelp(String isEnableRemoveHelp) {
        this.isEnableRemoveHelp = isEnableRemoveHelp;
    }
}
