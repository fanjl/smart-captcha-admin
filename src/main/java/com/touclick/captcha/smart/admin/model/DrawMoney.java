package com.touclick.captcha.smart.admin.model;

import java.math.BigDecimal;
import java.util.Date;

public class DrawMoney {
	private int mid;
	private Date createtime;
	private BigDecimal money;
	private int drawflag;
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public BigDecimal getMoney() {
		return money;
	}
	public void setMoney(BigDecimal money) {
		this.money = money;
	}
	public int getDrawflag() {
		return drawflag;
	}
	public void setDrawflag(int drawflag) {
		this.drawflag = drawflag;
	}
}
