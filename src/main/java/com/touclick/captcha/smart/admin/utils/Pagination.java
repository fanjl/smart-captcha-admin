package com.touclick.captcha.smart.admin.utils;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 分页器
 * @author wzj
 * @version v1.0
 * 
 * @param <T> 查询结果的数据类型
 */
public class Pagination<T> {
	// 查询总数时用于指定查询所有列
	private static final String ALL = "*";
	public static final Pattern ORDER_BY_PATTERN = Pattern.compile("(?i)order\\s+by\\s+");
	private static final String ORDER_BY_KEY_WORD = " ORDER BY ";
	// 当前页码
	private int pageNo;
	// 每页记录数，默认值取自分页定义
	private int pageSize = PaginationDefinition.getDefaultPageSize();
	// 总页数
	private int totalPage;
	// 数据记录偏移量，从 0 开始
	private long offset;
	// 本页内的所有记录
	private List<T> records;
	// 总记录数
	private long totalRecords;
	// 是否开启总数自动查询功能
	private boolean autoQueryTotal = true;
	// 查询HQL
	private String hql;
	// 查询总数的查询列
	private String countColumn = ALL;
	
	private Map<String, QueryOrder> queryOrderMap;
	// 查询条件参数
	private Object queryParam;
	// 转换为json时，“总记录” 对应的键名
	private String jsonTotalRecordsLabel = PaginationDefinition.getTotalRecordJsonLable();
	// 转换为json时，“记录” 对应的键名
	private String jsonRecordsLabel = PaginationDefinition.getRecordsJsonLable();
	
	/**
	 * 关闭自动查询总数功能
	 */
	public com.touclick.captcha.smart.admin.utils.Pagination<T> disableAutoQueryTotal(){
		autoQueryTotal = false;
		return this;
	}
	
	/**
	 * 开启自动查询总数功能，默认已开启
	 */
	public com.touclick.captcha.smart.admin.utils.Pagination<T> enableAutoQueryTotal(){
		autoQueryTotal = true;
		return this;
	}
	
	/**
	 * 是否自动查询总数
	 * @return
	 */
	public boolean autoQueryTotal(){
		return autoQueryTotal;
	}
	
	public int getPageNo() {
		return pageNo;
	}

	public com.touclick.captcha.smart.admin.utils.Pagination<T> setPageNo(Integer pageNo) {
		if(pageNo == null || pageNo <= 0){
			pageNo = 1;
		}
		this.pageNo = pageNo;
		updateOffset();
		return this;
	}

	public int getPageSize() {
		return pageSize;
	}

	public com.touclick.captcha.smart.admin.utils.Pagination<T> setPageSize(Integer pageSize) {
		if(pageSize == null || pageSize < 0){
			this.pageSize = PaginationDefinition.getDefaultPageSize();
		} else {
			this.pageSize = pageSize;
		}
		updateOffset();
		return this;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public com.touclick.captcha.smart.admin.utils.Pagination<T> setTotalPage(int totalPage) {
		this.totalPage = totalPage;
		return this;
	}

	public long getOffset() {
		return offset;
	}
	
	private void updateOffset(){
		long offset = (pageNo - 1) * pageSize;
		this.offset = offset;
	}
	
	public List<T> getRecords() {
		return records;
	}

	public com.touclick.captcha.smart.admin.utils.Pagination<T> setRecords(List<T> records) {
		this.records = records;
		return this;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public com.touclick.captcha.smart.admin.utils.Pagination<T> setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
		totalPage = (int)(totalRecords%pageSize != 0 ? totalRecords/pageSize+1 : totalRecords/pageSize);
		return this;
	}
	
	public String getHql() {
		return hql;
	}

	public com.touclick.captcha.smart.admin.utils.Pagination<T> setHql(String hql) {
		this.hql = hql;
		return this;
	}
	
	public String getCountColumn() {
		return countColumn;
	}

	public com.touclick.captcha.smart.admin.utils.Pagination<T> setCountColumn(String countColumn) {
		this.countColumn = countColumn;
		return this;
	}
	
	public com.touclick.captcha.smart.admin.utils.Pagination<T> addQueryOrder(String columnName, QueryOrder order){
		if(queryOrderMap == null){
			queryOrderMap = new LinkedHashMap<String, QueryOrder>();
		}
		queryOrderMap.put(columnName, order);
		return this;
	}
	
	public com.touclick.captcha.smart.admin.utils.Pagination<T> completeQueryStringWithOrderClause(){
		if(queryOrderMap != null){
			StringBuilder hqlBuilder = new StringBuilder(hql);
			int orderByEndIndex = -1;
			Matcher orderByMatcher = ORDER_BY_PATTERN.matcher(hql);
			if(orderByMatcher.find()){
				orderByEndIndex = orderByMatcher.end() - 1;
			}else{
				hqlBuilder.append(ORDER_BY_KEY_WORD);
				orderByEndIndex = hqlBuilder.length() - 1;
			}
			for(Entry<String, QueryOrder> queryOrder : queryOrderMap.entrySet()){
				String columnName = queryOrder.getKey();
				if(hqlBuilder.indexOf(columnName, orderByEndIndex) > 0){ // 若HQL中已有该排序字段，将忽略
					continue;
				}else{
					String orderByClausePart = queryOrder.getValue().transformToQueryString(columnName);
					if(hqlBuilder.length() - 1 > orderByEndIndex){
						hqlBuilder.append(StringUtil.SIGN_COMMA);
					}
					hqlBuilder.append(orderByClausePart);
				}
			}
			hql = hqlBuilder.toString();
		}
		return this;
	}

	public Object getQueryParam() {
		return queryParam;
	}

	/**
	 * 设置本分页器的查询条件参数
	 * @param queryParam
	 */
	public void setQueryParam(Object queryParam) {
		this.queryParam = queryParam;
	}

	/**
	 * 使用默认Json标签，默认日期格式{yyyy-MM-dd HH:mm:ss}将分页器内容转换为客户端UI可识别的Json字符串
	 * @return
	 */
	public String toJson(){
		return toJson(jsonTotalRecordsLabel, jsonRecordsLabel);
	}

	/**
	 * 使用默认Json标签，指定日期格式将分页器内容转换为客户端UI可识别的Json字符串
	 * @param datePattern 日期格式
	 * @return
	 */
	public String toJson(String datePattern){
		return toJson(jsonTotalRecordsLabel, jsonRecordsLabel, datePattern);
	}

	/**
	 * 使用指定Json标签，默认日期格式{yyyy-MM-dd HH:mm:ss}将分页器内容转换为客户端UI可识别的Json字符串
	 * @param totalRecordsLabel 总记录数标签
	 * @param recordsLabel 记录标签
	 * @return
	 */
	public String toJson(String totalRecordsLabel, String recordsLabel){
		return toJson(totalRecordsLabel, recordsLabel, null);
	}

	/**
	 * 使用指定Json标签，指定日期格式将分页器内容转换为客户端UI可识别的Json字符串
	 * @param totalRecordsLabel 总记录数标签
	 * @param recordsLabel 记录标签
	 * @param datePattern 日期格式
	 * @return
	 */
	public String toJson(String totalRecordsLabel, String recordsLabel, String datePattern){
		StringBuilder jsonBuilder = new StringBuilder("{\"");
		jsonBuilder.append(totalRecordsLabel);
		jsonBuilder.append("\":");
		jsonBuilder.append(totalRecords);
		jsonBuilder.append(",\"");
		jsonBuilder.append(recordsLabel);
		jsonBuilder.append("\":");
		if(datePattern == null){
			jsonBuilder.append(JsonProcessor.toJson(records));
		}else{
			jsonBuilder.append(JsonProcessor.toJson(records, datePattern));
		}
		jsonBuilder.append("}");
		return jsonBuilder.toString();
	}
}
