package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.Notice;

import java.util.List;

public interface INotice {
	/**
	 * @author wangpeng
	* @Title: getLastNotice 
	* @Description: TODO(获取最新公告) 
	* @param @return    设定文件 
	* @return Notice    返回类型 
	* @throws
	 */
	public Notice getLastNotice();
	
	/**
	 * @author wangpeng
	* @Title: noticeList 
	* @Description: TODO(获取公告列表) 
	* @param @return    设定文件 
	* @return List<Notice>    返回类型 
	* @throws
	 */
	public List<Notice> noticeList();
	
	/**
	 * @author wangpeng
	* @Title: save 
	* @Description: TODO(保存公告) 
	* @param @param notice
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws
	 */
	public boolean save(Notice notice);
	
	/**
	 * 
	* @author wangpeng
	* @Title: del 
	* @Description: TODO(删除公告) 
	* @param @param nid
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws
	 */
	public boolean del(int nid);
}
