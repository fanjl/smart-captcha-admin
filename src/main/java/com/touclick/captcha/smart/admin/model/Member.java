package com.touclick.captcha.smart.admin.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Member {
	private int mid;
	private String email;
	@JsonIgnore
	private String password;
	private String realname;
	private int level = 0;
	private int role = 1;
	private BigDecimal balance=new BigDecimal(0);
	private String qq;
	private String tel;
	private int useflag = 1;
	private Date createtime;
	private Date updatetime;
	private Date deletetime;
	private String createTimeStr;
	private int createDayNum;
	
	private String token;
	private int status;
	
	private List<com.touclick.captcha.smart.admin.model.WebSite> websites;
	private List<HistoryPay> historyPays;
	private List<Byte[]> imgs;
	private long allTotalG;
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getRole() {
		return role;
	}
	public void setRole(int role) {
		this.role = role;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public int getUseflag() {
		return useflag;
	}
	public void setUseflag(int useflag) {
		this.useflag = useflag;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public Date getDeletetime() {
		return deletetime;
	}
	public void setDeletetime(Date deletetime) {
		this.deletetime = deletetime;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreateTimeStr() {
        return createTimeStr;
    }
    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }
    public List<com.touclick.captcha.smart.admin.model.WebSite> getWebsites() {
        return websites;
    }
    public void setWebsites(List<WebSite> websites) {
        this.websites = websites;
    }
    public List<HistoryPay> getHistoryPays() {
        return historyPays;
    }
    public void setHistoryPays(List<HistoryPay> historyPays) {
        this.historyPays = historyPays;
    }
    public List<Byte[]> getImgs() {
        return imgs;
    }
    public void setImgs(List<Byte[]> imgs) {
        this.imgs = imgs;
    }
    public long getAllTotalG() {
        return allTotalG;
    }
    public void setAllTotalG(long allTotalG) {
        this.allTotalG = allTotalG;
    }
    public int getCreateDayNum() {
        return createDayNum;
    }
    public void setCreateDayNum(int createDayNum) {
        this.createDayNum = createDayNum;
    }
	
	
}
