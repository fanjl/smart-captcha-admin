package com.touclick.captcha.smart.admin.mapper.secondary.provider;

import com.touclick.captcha.smart.admin.model.Member;

import java.util.Map;

public class MemberProvider {
    public String addMember(Map<String, Member> paras) {
        return "insert into member(email,password,realname,qq,tel,role,balance,useflag,createtime,level) values(#{member.email},#{member.password},#{member.realname},#{member.qq},#{member.tel},#{member.role},#{member.balance},#{member.useflag},#{member.createtime},#{member.level})";
    }

    public String addMemberTemp(Map<String, Member> paras) {
        return "insert into member(mid,email,password,realname,qq,tel,role,balance,useflag,createtime) values(#{member.mid},#{member.email},#{member.password},#{member.realname},#{member.qq},#{member.tel},#{member.role},#{member.balance},#{member.useflag},#{member.createtime})";
    }

    public String checkLogin(Map<String, Member> paras) {
        return "select * from member t where t.email=#{member.email} and t.useflag=1";
    }

    public String addMemberInfo(Map<String, Object> paras) {
        return "insert into member_info values(#{mid},#{type},#{value})";
    }

    public String existEmail(Map<String, Object> paras) {
        return "select * from member t where t.email=#{email} and t.useflag=1";
    }

    public String existEmailByAddress() {
        return "select * from member t where t.email=#{email} and t.useflag=1 and t.mid not in(select mid from website where waddress=#{waddress})";
    }

    public String getWidByMid(Map<String, Object> paras) {
        return "select t.wid from website t where t.useflag=1 and t.mid=#{mid}";
    }

    public String getPwdByMid(Map<String, Object> paras) {
        return "select t.password from member t where t.useflag=1 and t.mid=#{mid}";
    }

    public String updateNewpwd(Map<String, Object> paras) {
        return "update member t set t.password=#{newpwd} where t.mid=#{mid}";
    }

    public String updateLevel(Map<String, Object> paras) {
        return "update member t set t.level=#{level} where t.mid=#{mid}";
    }

    public String updateNewpwdByEmail() {
        return "update member t set t.password=#{newpwd} where t.email=#{email}";
    }

    public String updateInfo(Map<String, Member> paras) {
        return "update member t set t.realname=#{member.realname},t.qq=#{member.qq},t.tel=#{member.tel} where t.mid=#{member.mid}";
    }

    public String updateStatus(Map<String, Member> paras) {
        return "update member t set t.status=#{member.status} where t.mid=#{member.mid}";
    }

    public String updateToken(Map<String, Member> paras) {
        return "update member t set t.token=#{member.token} where t.mid=#{member.mid}";
    }

    public String getAllMember() {
        return "select * from member t where t.useflag=1";
    }

    public String getMember() {
        return "select * from member t where t.mid=#{mid}";
    }

    public String getVipMember() {
        return "select * from member t where t.useflag=1 and t.level > 0";
    }

    public String checkLoginBySiteKey() {
        return "select * from member t where t.mid=(select mid from website where pubkey=#{pubkey} and token=#{token})";
    }

    public String getMemberByWaddress() {
        return "select * from member t where t.mid=(select mid from website where waddress=#{waddress})";
    }

    public String getMemberByToken() {
        return "select * from member t where t.token=#{token}";
    }

    public String getMemberByEmail() {
        return "select * from member t where t.email=#{email}";
    }

    public String resetPassEmailByWaddress() {
        return "update member t set t.email=#{email},t.password=#{password} where t.mid=(select mid from website where waddress=#{waddress})";
    }

    public String getMemberByWid() {
        return "select * from member t where t.mid = (select mid from website t1 where t1.wid=#{wid})";
    }
    
    public String getMemberListByCreateTime() {
        return "select * from member t where t.createtime between #{start} and #{end} order by t.createtime desc";
    }
    
    
}
