package com.touclick.captcha.smart.admin.service.impl;

import com.touclick.captcha.smart.admin.mapper.primary.GroupMapper;
import com.touclick.captcha.smart.admin.model.MemberGroup;
import com.touclick.captcha.smart.admin.service.inter.IGroup;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GroupImpl  implements IGroup {
    private static Logger logger = Logger.getLogger(GroupImpl.class);
    @Autowired
    private GroupMapper groupMapper;

    @Override
    public List<MemberGroup> getGroupByMid(int mid) {
        List<MemberGroup> list = groupMapper.getGroupByMid(mid);
        return list;
    }

    @Override
    public boolean save(MemberGroup mg) {

        try {
            groupMapper.save(mg);
        } catch (Exception e) {
            logger.info(e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public List<MemberGroup> getAllGroup() {

        List<MemberGroup> list = groupMapper.getAllGroup();
        return list;
    }
}
