package com.touclick.captcha.smart.admin.service.impl;


import com.touclick.captcha.smart.admin.mapper.primary.WebSiteMapper;
import com.touclick.captcha.smart.admin.model.WebSite;
import com.touclick.captcha.smart.admin.model.WebSiteInfo;
import com.touclick.captcha.smart.admin.model.WebSiteInfoEnum;
import com.touclick.captcha.smart.admin.service.inter.IWebSite;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WebSiteImpl  implements IWebSite {
    private static Logger logger = Logger.getLogger(WebSiteImpl.class);

    @Autowired
    private WebSiteMapper websiteMapper;
    @Override
    public void addWebSite(WebSite website) {
        websiteMapper.addWebSite(website);
    }

    @Override
    public int addWebSiteTemp(WebSite website) {
        return websiteMapper.addWebSiteTemp(website);
    }

    @Override
    public int addWebSiteInfo(WebSiteInfo wsi) {

        return websiteMapper.addWebSiteInfo(wsi);
    }

    @Override
    public boolean existWebSite(String waddress) {

        List<WebSite> list = websiteMapper.existWebSite(waddress);
        if (list != null && list.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean existWebSiteByMid(String waddress, int mid) {

        List<WebSite> list = websiteMapper.existWebSiteByMid(waddress, mid);
        if (list != null && list.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据pubkey 查询相应的website
     * 
     * @param pubkey
     * @return
     */
    public WebSite getWebsiteByPubkey(String pubkey) {

        WebSite w = websiteMapper.getWebsiteByPubkey(pubkey);
        return w;
    }

    @Override
    public List<WebSite> getWebSiteList(int mid) {

        List<WebSite> list = websiteMapper.getWebSiteList(mid);
        return list;
    }
    @Override
    public WebSite getWebSiteByPubkey(String pubkey) {

        WebSite webSite = websiteMapper.getWebsiteByPubkey(pubkey);
        return webSite;
    }

    @Override
    public boolean isOnlyOwnerPic(int wid) {

        Integer value = websiteMapper.isOnlyOwnerPic(wid, WebSiteInfoEnum.isOwnerPic.getIndex());
        if (value != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isNeedAds(int wid) {

        int type = WebSiteInfoEnum.getIndex("isNeedAds");
        Integer value = websiteMapper.isOnlyOwnerPic(wid, type);
        if (value != null && value.intValue() == 1) {
            return true;
        }
        return false;
    }

    @Override
    public List<WebSite> findAll() {

        List<WebSite> list = websiteMapper.findAll();
        return list;
    }

    @Override
    public List<WebSiteInfo> findAllWebSiteInfo(int wid, int type) {

        List<WebSiteInfo> list = websiteMapper.findWebSiteInfo(wid, type);
        return list;
    }
    
    @Override
    public List<WebSiteInfo> findWebSiteInfoByWid(int wid) {

        List<WebSiteInfo> list = websiteMapper.findWebSiteInfoByWid(wid);
        return list;
    }

    @Override
    public List<WebSiteInfo> findAllWebSiteInfo() {

        List<WebSiteInfo> list = websiteMapper.findAllWebSiteInfo();
        return list;
    }

    @Override
    public void updateCustomPic(int wid, boolean custompic) {

        int custompicflag = 0;
        if (custompic) {
            custompicflag = 1;
            websiteMapper.insertWebSiteInfo(wid, WebSiteInfoEnum.getIndex(WebSiteInfoEnum.isOwnerPic.toString()),
                    custompicflag);
        } else {
            custompicflag = 0;
            websiteMapper.cancelByWidAndType(wid, WebSiteInfoEnum.getIndex(WebSiteInfoEnum.isOwnerPic.toString()));
        }

    }

    @Override
    public void updateCtypeAndSkin(int wid, int ctype, String skin) {

        websiteMapper.updateCtypeAndSkin(wid, ctype, skin);
    }

    @Override
    public WebSite getWebSiteByWid(int wid) {

        return websiteMapper.getWebSiteByWid(wid);
    }

    @Override
    public void delWebSite(int wid) {

        websiteMapper.delWebSite(wid);
    }

    @Override
    public void resetToken(int wid, String token) {

        websiteMapper.resetToken(wid, token);
    }

    @Override
    public void updateVerify(int wid, int verifyflag) {

        websiteMapper.updateVerify(wid, verifyflag);
    }

    @Override
    public List<WebSiteInfo> getWebsiteInfo(int wid) {

        return websiteMapper.getWebsiteInfo(wid);
    }

    @Override
    public void updateWebSiteInfo(int wid, int type, String value) {

        websiteMapper.updateWebSiteInfo(wid, type, value);
    }

    @Override
    public void delCustom(int wid, int type) {

        websiteMapper.delCustom(wid, type);
    }

    @Override
    public void udpateCversionByWid(int wid, String cversion) {

        websiteMapper.udpateCversionByWid(wid, cversion);
    }

    @Override
    public void udpateCversionByMid(int mid, String cversion) {

        websiteMapper.udpateCversionByMid(mid, cversion);
    }

    @Override
    public int udpateCversionByPlatformCversion(String platform, String srcVersion, String desVersion) {

        return websiteMapper.udpateCversionByPlatformCversion(platform, srcVersion, desVersion);
    }

    @Override
    public List<WebSite> getAllWebSite() {

        return websiteMapper.getAllWebSite();
    }

    @Override
    public void udpateCversionByPubkey(String pubkey, String cversion) {

        websiteMapper.udpateCversionByPubkey(pubkey, cversion);
    }

    @Override
    public List<WebSite> getWebSiteByPlatformCversion(String platform, String cversion) {

        return websiteMapper.getWebSiteByPlatformCversion(platform, cversion);
    }

    @Override
    public void updateCversionCTypeByPubkey(String pubkey, String cversion, int ctype) {

        websiteMapper.updateCversionCTypeByPubkey(pubkey, cversion, ctype);
    }

    @Override
    public int getWebSiteCountByMid(int mid) {

        return websiteMapper.getWebsiteCountByMid(mid);
    }

    @Override
    public void updateRemoveStatus(int wid, boolean isRemoveLogo, int value) {

        int isRemoveLogoFlag = 0;
        if (isRemoveLogo) {
            isRemoveLogoFlag = 1;
            websiteMapper.insertWebSiteInfo(wid, value, isRemoveLogoFlag);
        } else {
            isRemoveLogoFlag = 0;
            websiteMapper.cancelByWidAndType(wid, value);
        }
    }

    @Override
    public List<String> getOldVersionWebSite() {

        return websiteMapper.getOldVersionWebSite();
    }
}
