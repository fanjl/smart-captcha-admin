package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.PayProvider;
import com.touclick.captcha.smart.admin.model.AlipayDetail;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

public interface PayMapper {
	@SelectProvider(type=PayProvider.class,method="getAlipayDetail")
	public AlipayDetail getAlipayDetail(@Param(value = "drawid") int drawid);
	
	@UpdateProvider(type=PayProvider.class,method="updateDrawMoney")
	public void updateDrawMoney(@Param(value = "drawid") int drawid);
}
