package com.touclick.captcha.smart.admin.utils;

import com.touclick.captcha.smart.admin.model.CversionMapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CversionUtils {
	public static List<CversionMapper> getCversionMapperCtype() throws DocumentException {
		List<CversionMapper> list = new ArrayList<CversionMapper>();
		SAXReader reader = new SAXReader();
		File file = new File(CversionUtils.class.getResource("/cversion.xml").getFile());
		Document document = reader.read(file);
		Element root = document.getRootElement();
		List<Element> childElements = root.elements();
		for (Element child : childElements) {
			CversionMapper cm = new CversionMapper();
			cm.setCtype(child.elementText("ctype"));
			cm.setNoctype(child.elementText("noctype"));
			cm.setCversion(child.attributeValue("id"));
			list.add(cm);
		}
		return list;
	}
}
