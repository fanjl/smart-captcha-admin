package com.touclick.captcha.smart.admin.model;

import java.util.Date;

/**
 * 图标选择型验证码图片表model
 *
 * @author bing.liu
 *
 */
public class BlockImage {

    /** id */
    private int id;

    /** 名称表ID */
    private int nameId;

    /** 创建时间 */
    private Date createTime;

    /** 图片数据 */
    private byte[] image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNameId() {
        return nameId;
    }

    public void setNameId(int nameId) {
        this.nameId = nameId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
