package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.WebSiteGroup;

import java.util.List;

public interface IWebSiteGroup {
	public List<WebSiteGroup> getWebSiteGroupByWid(String wid);
	
	public List<Integer> getGidByWidAndType(int wid, int type);
	
	public int save(WebSiteGroup wsg);
	
	public int delete(WebSiteGroup wsg);
}
