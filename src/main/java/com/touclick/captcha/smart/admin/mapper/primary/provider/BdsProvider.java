package com.touclick.captcha.smart.admin.mapper.primary.provider;

import java.util.Map;

public class BdsProvider {
	public String save(){
		return "insert into bds(mid,wid,background) values(#{bds.mid},#{bds.wid},#{bds.background,jdbcType=BLOB})";
	}
	public String getBdsByWid(){
		return "select * from bds t where t.wid = #{wid}";
	}
	public String findAll(){
		return "select * from bds ";
	}
	public String delBds(){
		return "delete from bds where bid=#{bid}";
	}
	public String selectAllBid(){
		return "select bid from bds";
	}
	public String selectByBid(Map<String,String> params){
		return "select * from bds where bid = #{bid}";
	}
	public String selectByBids(Map<String,String> params){
		String value = params.get("bids").toString();
		return "SELECT * FROM bds where bid in ("+value+")";
	}
	public String getDistinctWid(){
        return "select distinct wid from bds ";
    }
	


}
