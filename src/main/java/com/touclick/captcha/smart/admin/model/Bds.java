package com.touclick.captcha.smart.admin.model;

import org.apache.commons.codec.binary.Base64;

public class Bds {
	private int bid;
	private int mid;
	private int wid;
	private byte[] background;
	private String backgroundbase64;
	public int getBid() {
		return bid;
	}
	public void setBid(int bid) {
		this.bid = bid;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	public byte[] getBackground() {
		return background;
	}
	public void setBackground(byte[] background) {
		this.background = background;
	}
	public String getBackgroundbase64() {
		this.backgroundbase64 = new String(Base64.encodeBase64(background));
		return backgroundbase64;
	}
	public void setBackgroundbase64(String backgroundbase64) {
		this.backgroundbase64 = backgroundbase64;
	}
	
}
