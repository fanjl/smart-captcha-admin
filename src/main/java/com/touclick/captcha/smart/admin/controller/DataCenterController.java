package com.touclick.captcha.smart.admin.controller;

import com.google.gson.Gson;
import com.touclick.captcha.smart.admin.auth.UserContext;
import com.touclick.captcha.smart.admin.model.LtypeData;
import com.touclick.captcha.smart.admin.model.WebSite;
import com.touclick.captcha.smart.admin.service.inter.IData;
import com.touclick.captcha.smart.admin.service.inter.IWebSite;
import com.touclick.captcha.smart.admin.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/datacenter")
public class DataCenterController {
	@Autowired
	private IData iData;
	@Autowired
	private IWebSite iWebsite;
	Gson gson = new Gson();
	@RequestMapping(value="viewValidateData")
	public String viewValidateData(HttpSession session, Model model, @AuthenticationPrincipal UserContext userContext){
		List<WebSite> websiteList = iWebsite.getWebSiteList(userContext.getUserId());
		if(websiteList!=null&&websiteList.size()>0){
			model.addAttribute("websiteList", websiteList);
			model.addAttribute("daydata", gson.toJson(iData.getDataByDateRange(websiteList.get(0).getWid(), 0, 0)));
			model.addAttribute("monthdata", gson.toJson(iData.getDataByDateRangeMonth(websiteList.get(0).getWid(), DateUtils.getFirstDayOfMonth(), DateUtils.getLastDayOfMonth())));
			model.addAttribute("yeardata", gson.toJson(iData.getDataByDateRangeYear(websiteList.get(0).getWid(), DateUtils.getFirstMonth(), 0)));
		}
		return "datacenter/viewValidateData";
	}
	@ResponseBody
	@RequestMapping(value="getDataByDateRange")
	public String getDataByDateRange(int wid, String dateRange, String datetype){
		long begin = 0;
		long end = 0;
		if(dateRange!=null&&!"".equals(dateRange)){
			String[] range = dateRange.split(" - ");
			begin = java.sql.Date.valueOf(range[0]).getTime();
			end = java.sql.Date.valueOf(range[1]).getTime();
		}
		List<LtypeData> list = new ArrayList<LtypeData>();
		if("day".equals(datetype)){
			list = iData.getDataByDateRange(wid, begin, end);
		}else if("month".equals(datetype)){
			if("".equals(dateRange)){
				begin = DateUtils.getFirstDayOfMonth();
				end = DateUtils.getLastDayOfMonth();
			}
			list = iData.getDataByDateRangeMonth(wid, begin, end);
		}else if("year".equals(datetype)){
			if("".equals(dateRange)){
				begin = DateUtils.getFirstMonth();
			}
			list = iData.getDataByDateRangeYear(wid, begin, end);
		}
		return gson.toJson(list);
	}
	@RequestMapping(value="viewAdsData")
	public String viewAdsData(HttpSession session, Model model,@AuthenticationPrincipal UserContext userContext){
		List<WebSite> websiteList = iWebsite.getWebSiteList(userContext.getUserId());
		if(websiteList!=null&&websiteList.size()>0){
			model.addAttribute("websiteList", websiteList);
			model.addAttribute("daydata", gson.toJson(iData.getDataByDateRange(websiteList.get(0).getWid(), 0, 0)));
			model.addAttribute("monthdata", gson.toJson(iData.getDataByDateRangeMonth(websiteList.get(0).getWid(), DateUtils.getFirstDayOfMonth(), DateUtils.getLastDayOfMonth())));
			model.addAttribute("yeardata", gson.toJson(iData.getDataByDateRangeYear(websiteList.get(0).getWid(), DateUtils.getFirstMonth(), 0)));
		}
		return "datacenter/viewAdsData";
	}
	@ResponseBody
	@RequestMapping(value="getCtypesDataByDateRange")
	public String getCtypesDataByDateRange(int wid, String dateRange, String datetype){
		long begin = 0;
		long end = 0;
		if(dateRange!=null&&!"".equals(dateRange)){
			String[] range = dateRange.split(" - ");
			begin = java.sql.Date.valueOf(range[0]).getTime();
			end = java.sql.Date.valueOf(range[1]).getTime();
		}
		List<LtypeData> list = new ArrayList<LtypeData>();
		if("day".equals(datetype)){
			list = iData.getDataByDateRange(wid, begin, end);
		}else if("month".equals(datetype)){
			if("".equals(dateRange)){
				begin = DateUtils.getFirstDayOfMonth();
				end = DateUtils.getLastDayOfMonth();
			}
			list = iData.getDataByDateRangeMonth(wid, begin, end);
		}else if("year".equals(datetype)){
			if("".equals(dateRange)){
				begin = DateUtils.getFirstMonth();
			}
			list = iData.getDataByDateRangeYear(wid, begin, end);
		}
		return gson.toJson(list);
	}
	@ResponseBody
	@RequestMapping(value="changeDateType")
	public String changeDateType(String datetype, int wid){
		if("day".equals(datetype)){
			return gson.toJson(iData.getDataByDateRange(wid, 0, 0));
		}else if("month".equals(datetype)){
			return gson.toJson(iData.getDataByDateRangeMonth(wid, DateUtils.getFirstDayOfMonth(), DateUtils.getLastDayOfMonth()));
		}else if("year".equals(datetype)){
			return gson.toJson(iData.getDataByDateRangeYear(wid, DateUtils.getFirstMonth(), 0));
		}else{
			return "";
		}
	}
	@ResponseBody
	@RequestMapping(value="getCurrentDateData")
	public String getCurrentDateData(int wid){
		Map<String,List<LtypeData>> map = new HashMap<String,List<LtypeData>>();
		map.put("day", iData.getDataByDateRange(wid, 0, 0));
		map.put("month", iData.getDataByDateRangeMonth(wid, DateUtils.getFirstDayOfMonth(), DateUtils.getLastDayOfMonth()));
		map.put("year", iData.getDataByDateRangeYear(wid, DateUtils.getFirstMonth(), 0));
		return gson.toJson(map);
	}
}
