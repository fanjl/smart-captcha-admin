package com.touclick.captcha.smart.admin.utils;


import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 * @author wzj
 * @version v1.0
 */
public class StringUtil {
	public final static String EMPTY = "";
	public final static String SIGN_SINGLE_BLANK = " ";
	public final static String SIGN_ELLIPSIS = "...";
	public final static String SIGN_HYPHEN = "-";
	public final static String SIGN_COMMA = ",";
	public final static String SING_BAR = "|";
	public final static String SING_SEMICOLON = ";";
	public final static String SING_DOT = ".";
	public final static String SING_EQUAL = "=";
	// 简易HTML标签正则
	private final static Pattern REGEXP_HTML_TAG = Pattern.compile("<[^>]+>");
	// 双引号正则
	private final static Pattern REGEXP_DOUBLE_QUOTE = Pattern.compile("\"");
	
	private final static Pattern REGEXP_LINE_BREAK = Pattern.compile("[\\\r\\\n]");
	
	private final static Pattern REGEXP_HTML_SPACE = Pattern.compile("&nbsp;");
	/**
	 * 清除字符串所有的HTML标签
	 * @param target 目标字符串
	 * @return 去除HTML标签后的纯文本文件
	 */
	public static String removeHtmlTag(String target){
		if(target == null){
			return null;
		}
		Matcher tagMatcher = REGEXP_HTML_TAG.matcher(target);
		return tagMatcher.replaceAll(EMPTY);
	}
	
	/**
	 * 清除字符串中所有的HTML转义空格(&nbsp;)
	 * @param target 目标字符串
	 * @return 去除HTML转义空格 (&nbsp;)后的文本串
	 */
	public static String removeHtmlSpace(String target){
		if(target == null){
			return null;
		}
		Matcher tagMatcher = REGEXP_HTML_SPACE.matcher(target);
		return tagMatcher.replaceAll(EMPTY);
	}
	
	/**
	 * 规整HTML串：
	 * 将串中所有双引号转换成单引号，以利于解析成JSON格式；
	 * 将串中所有换行符转换成单空格。
	 * @param target
	 * @return
	 */
	public static String uniformHTMLStr(String target){
		String singleQuoteForm = doubleQuoteToSingleQuote(target);
		String noLineBreak = removeLineBreak(singleQuoteForm);
		return noLineBreak;
	}
	
	/**
	 * 双引号转单引号
	 * @param target 目标字符串
	 * @return
	 */
	public static String doubleQuoteToSingleQuote(String target){
		if(target == null){
			return null;
		}
		Matcher quoteMatcher = REGEXP_DOUBLE_QUOTE.matcher(target);
		return quoteMatcher.replaceAll("'");
	}
	/**
	 * 清除字符串中所有换行符
	 * @param target 目标字符串
	 * @return
	 */
	public static String removeLineBreak(String target){
		if(target == null){
			return null;
		}
		Matcher breakMatcher = REGEXP_LINE_BREAK.matcher(target);
		return breakMatcher.replaceAll(SIGN_SINGLE_BLANK);
	}
	
	/**
	 * 连接字符串
	 * @param targets
	 * @return
	 */
	public static String catenate(String... targets){
		if(targets == null){
			throw new IllegalArgumentException("args 'targets' can not be null");
		}
		StringBuilder builder = new StringBuilder();
		for(String target : targets){
			builder.append(target);
		}
		return builder.toString();
	}
	
	/**
	 * 连接字符串
	 * @param targets
	 * @return
	 */
	public static String catenate(Object... targets){
		if(targets == null){
			throw new IllegalArgumentException("args 'targets' can not be null");
		}
		StringBuilder builder = new StringBuilder();
		for(Object target : targets){
			builder.append(target.toString());
		}
		return builder.toString();
	}
	
	/**
	 * 拼接数组或集合元素成字符串，并以指定分隔符分隔
	 * 本方法只处理数组或集合，否则，返回target.toString()
	 * 若数组或集合大小为0，则返回空字符串
	 * @param target 数组或集合
	 * @param token 分隔符
	 * @return
	 */
	public static String catenate(Object target, String token){
		Collection<?> targets = null;
		if(target == null || token == null){
			throw new IllegalArgumentException("args 'targets' can not be null");
		}
		StringBuilder builder = new StringBuilder();
		if(target.getClass().isArray()){
			targets = Arrays.asList(target);
		}else if(target instanceof Collection<?>){
			targets = (Collection<?>)target;
		}else{
			return target.toString();
		}
		if(targets.size() == 0){
			return com.touclick.captcha.smart.admin.utils.StringUtil.EMPTY;
		}else{
			for(Object element : targets){
				builder.append(element);
				builder.append(token);
			}
			return builder.substring(0, builder.length() - token.length());
		}
	}
	
	/**
	 * 判断是否为Null或空
	 * @param target 目标字符串
	 * @return
	 */
	public static boolean nullOrEmpty(String target){
		return target==null || target.isEmpty();
	}
	
	/**
	 * 若目标字符串为空字符串（""），则转换为NULL，否则，返回原值
	 * @param target 目标字符串
	 * @return
	 */
	public static String emptyToNull(String target){
		return nullOrEmpty(target) ? null : target;
	}
	
	/**
	 * 以指定分隔符分隔字符串
	 * @param target 目标字符串
	 * @param token 分隔符
	 * @return 分隔后的子字符串集合
	 */
	public static List<String> splitByToken(String target, String token){
		if(target == null || token == null){
			throw new IllegalArgumentException("args can not be null");
		}
		String[] splitted = target.split(token);
		return Arrays.asList(splitted);
	}
	
	/**
	 * 获取指定分隔符前的字符串，不包含分隔符
	 * @param target 目标字符串
	 * @param token 分隔符
	 * @return 指定分隔符前的字符串
	 */
	public static String formerPartByToken(String target, String token){
		if(target == null || token == null){
			throw new IllegalArgumentException("args can not be null");
		}
		int tokenIndex = target.indexOf(token);
		return target.substring(0, tokenIndex);
	}
	
	/**
	 * 获取指定分隔符后的字符串，不包含分隔符
	 * @param target 目标字符串
	 * @param token 分隔符
	 * @return 指定分隔符后的字符串
	 */
	public static String latterPartByToken(String target, String token){
		if(target == null || token == null){
			throw new IllegalArgumentException("args can not be null");
		}
		int tokenIndex = target.indexOf(token);
		return target.substring(tokenIndex + 1);
	}
	/**
	 * 截断字符串
	 * @param target 目标字符串
	 * @param maxLength 最大长度
	 * @param appendEllipsis 追加省略号
	 * @return
	 */
	public static String truncate(String target, int maxLength, boolean appendEllipsis){
		if(target == null){
			return null;
		}
		if(target.length() > maxLength){
			if(appendEllipsis){
				return target.substring(0, maxLength) + SIGN_ELLIPSIS;
			}else{
				return target.substring(0, maxLength);
			}
		}else{
			return target;
		}
	}
	
	/**
	 * 判断字符串是否为空，不为空返回true
	 * @param string
	 * @return
	 */
	public static boolean isNotEmpty(String string) {
		return string != null && string.length() > 0;
	}
	
	/**
	 * 判断字符串是否为空，为空返回true
	 * @param string
	 * @return
	 */
	public static boolean isEmpty(String string) {
		return string == null || string.length() == 0;
	}
	
	/**
	 * 获取子串，子串匹配顺序由后至前。若目标串中无对应子串起始字符串，则返回null
	 * @param target 目标字符串
	 * @param subStrBeginWith 子串的起始字符串
	 * @return
	 */
	public static String lastSubstr(String target, String subStrBeginWith){
		if(target != null){
			int lastIdx = target.lastIndexOf(subStrBeginWith);
			if(lastIdx >= 0){
				return target.substring(lastIdx);
			}
		}
		return null;
	}
}
