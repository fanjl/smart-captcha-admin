package com.touclick.captcha.smart.admin.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.touclick.captcha.smart.admin.model.CropPicture;
import org.apache.commons.codec.binary.Base64;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ImageUtils {
    /**
     * 图片裁剪
     * 
     * @param src
     * @param dest
     * @param x
     * @param y
     * @param w
     * @param h
     * @throws IOException
     */
    public static byte[] cutImage(byte[] bimg, CropPicture cp, String destsrc) throws IOException {
        int ImgW = (int) cp.getImgW();
        int ImgH = new BigDecimal(cp.getImgH()).setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
        BufferedImage resizedImage = new BufferedImage(ImgW, ImgH, BufferedImage.TYPE_INT_RGB);
        BufferedImage simg = ImageIO.read(new ByteArrayInputStream(bimg));
        Graphics2D gp = resizedImage.createGraphics();
        gp.drawImage(simg, 0, 0, ImgW, ImgH, 0, 0, cp.getImgInitW(), cp.getImgInitH(), null);
        gp.dispose();
        BufferedImage rotated_image = rotateImage(resizedImage, cp.getRotation());
        int rotated_width = rotated_image.getWidth();
        int rotated_height = rotated_image.getHeight();
        int dx = rotated_width - ImgW;
        int dy = rotated_height - ImgH;
        BufferedImage cropped_rotated_image = new BufferedImage(ImgW, ImgH, BufferedImage.TYPE_INT_RGB);
        Graphics2D gp1 = cropped_rotated_image.createGraphics();
        gp1.drawImage(rotated_image, 0, 0, ImgW, ImgH, dx / 2, dy / 2, dx / 2 + ImgW, dy / 2 + ImgH, null);
        gp1.dispose();
        BufferedImage final_image = new BufferedImage(cp.getCropW(), cp.getCropH(), BufferedImage.TYPE_INT_RGB);
        Graphics2D gp2 = final_image.createGraphics();
        gp2.drawImage(cropped_rotated_image, 0, 0, cp.getCropW(), cp.getCropH(), cp.getImgX1(), cp.getImgY1(),
                cp.getImgX1() + cp.getCropW(), cp.getImgY1() + cp.getCropH(), null);
        gp2.dispose();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ImageIO.write(final_image, "jpg", out);
        return out.toByteArray();

    }

    public static BufferedImage rotateImage(final BufferedImage bufferedimage, final int degree) {
        int w = bufferedimage.getWidth();
        int h = bufferedimage.getHeight();
        int type = bufferedimage.getColorModel().getTransparency();
        BufferedImage img;
        Graphics2D graphics2d;
        (graphics2d = (img = new BufferedImage(w, h, type)).createGraphics()).setRenderingHint(
                RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2d.rotate(Math.toRadians(degree), w / 2, h / 2);
        graphics2d.drawImage(bufferedimage, 0, 0, null);
        graphics2d.dispose();
        return img;
    }

    public static byte[] installByteImg(String source) {
        ObjectMapper mapper = new ObjectMapper();
        char[] sourceChar = source.toCharArray();
        List<byte[]> spic = new ArrayList<byte[]>();
        byte[] img1 = null;
        for (char printf : sourceChar) {
            List<String> base64List = new ArrayList<String>();
            try {
                spic.add(String.valueOf(printf).getBytes("UTF-8"));
                for (byte[] el : spic) {
                    base64List.add(Base64.encodeBase64String(el));
                }
                img1 = mapper.writeValueAsBytes(base64List);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return img1;
    }
    public static List<String> splitString(String source) {
        List<String> base64List = new ArrayList<String>();
        if(source!=null){
            char[] sourceChar = source.toCharArray();
            for (char printf : sourceChar) {
                base64List.add(String.valueOf(printf));
            }
        }else{
            return null;
        }
        return base64List;
    }
    public static List<String> saveListByIndex(List<String> source, int index) {
        if(source!=null && source.size()>0){
            for (int i = source.size(); i > index; i--) {  
                source.remove(i-1);  
            }  
        }else{
            return null;
        }
        return source;
    }

    public static byte[] installByteImgFromList(List<byte[]> bytes) {
        ObjectMapper mapper = new ObjectMapper();
        byte[] img1 = null;
        List<String> base64List = new ArrayList<String>();
        try {
            for (byte[] el : bytes) {
                base64List.add(Base64.encodeBase64String(el));
            }
            img1 = mapper.writeValueAsBytes(base64List);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return img1;
    }
    
    public static String formatTxt(String printfont, String transparent) {
        StringBuilder txt = new StringBuilder();
        if(printfont!=null){
            if(printfont.contains("'")){
                printfont.replace("'", "");
            }
        }
        if(transparent!=null){
            if(transparent.contains("'")){
                transparent.replace("'", "");
            }
        }
        txt.append("{printfont:'").append(printfont).
        append("',transparent:'").append(transparent).append("'}");
        return txt.toString();
    }
    public static void main(String[] args) {
        List<String> source  = new ArrayList<String>();
        source.add("月");source.add("明");source.add("星");source.add("月");
        List<String> strList = saveListByIndex(source, 2);
        System.out.println(strList);
    }
}
