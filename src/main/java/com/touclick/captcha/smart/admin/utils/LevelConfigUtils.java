package com.touclick.captcha.smart.admin.utils;

import com.renren.zookeeper.ZkConfig;
import com.touclick.level_config.LevelConfig;
import com.touclick.level_config.LevelConfig.Conf;
import org.apache.log4j.Logger;

public class LevelConfigUtils {
	private static final Logger LOGGER = Logger.getLogger(LevelConfigUtils.class);
	private static  LevelConfig level = LevelConfig.getInstance();
	static {
		String zkaddr = GlobalConfig.getString("REDIS_ZK_ADDR");
		ZkConfig zk = new ZkConfig();
		zk.setHost(zkaddr);
		zk.setRoot("/");
		LevelConfig.init(zk);
	}
	public static int getIdCountLimits(int levelInt){
		int result = level.getIdCountLimit(levelInt);
		return result;
	}
	public static boolean getIsEnableRemoveLogo(int levelInt){
		boolean result =  level.getIsEnableRemoveLogo(levelInt);
		return result;
	}
	public static boolean getIsEnableCustomPromptText(int levelInt){
		boolean result =  level.getCustomPromptText(levelInt);
		return result;
	}
	public static boolean getIsEnableRemoveHelp(int levelInt){
        boolean result =  level.getRemoveTouclickHelp(levelInt);
        return result;
    }
	public static boolean getIsEnableBehaviorFeature(int levelInt){
        boolean result =  level.getBehaviorFeature(levelInt);
        return result;
    }
	public static Conf getConf(int levelInt){
		return level.getConf(levelInt);
	}
}
