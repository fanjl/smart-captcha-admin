package com.touclick.captcha.smart.admin.auth.token;

public interface JwtToken {
    String getToken();
}
