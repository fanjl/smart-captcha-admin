package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.BdsProvider;
import com.touclick.captcha.smart.admin.model.Bds;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface BdsMapper {
	@InsertProvider(type=BdsProvider.class,method="save")
	public int save(@Param(value = "bds") Bds bds);
	
	@SelectProvider(type=BdsProvider.class,method="getBdsByWid")
	public List<Bds> getBdsByWid(@Param(value = "wid") int wid);
	
	@SelectProvider(type=BdsProvider.class,method="findAll")
	public List<Bds> findAll();
	
	@DeleteProvider(type=BdsProvider.class,method="delBds")
	public int delBds(@Param(value = "bid") int bid);

	@SelectProvider(type=BdsProvider.class,method="selectAllBid")
	public List<Integer> selectAllBid();

	@SelectProvider(type = BdsProvider.class,method = "selectByBid")
	public Bds selectByBid(@Param(value = "bid") int bid);

	@SelectProvider(type = BdsProvider.class,method = "selectByBids")
	public List<Bds> selectByBids(@Param(value = "bids") String bids);
	
	@SelectProvider(type = BdsProvider.class,method = "getDistinctWid")
    public List<Bds> getDistinctWid();
	
}
