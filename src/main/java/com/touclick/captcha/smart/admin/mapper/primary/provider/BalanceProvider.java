package com.touclick.captcha.smart.admin.mapper.primary.provider;

public class BalanceProvider {
    public String save() {
        return "insert into history_pay(pay_hid,mid,ali_count,pay_time,begin_time,end_time,product,money,status,contract,receipt) values(#{p.hid},#{p.mid},#{p.aliCount},#{p.payTime},#{p.beginTime},#{p.endTime},#{p.product},#{p.money},#{p.status},#{p.contract},#{p.receipt})";
    }

    public String saveSimple() {
        return "insert into history_pay(mid,ali_count,pay_time,product,money,status,contract,receipt) values(#{p.mid},#{p.aliCount},#{p.payTime},#{p.product},#{p.money},#{p.status},#{p.contract},#{p.receipt})";
    }

    public String update() {
        return "update history_pay t set t.status=#{p.status}, t.ali_count=#{p.aliCount}, t.pay_time=#{p.payTime}"
                + " , t.begin_time=#{p.beginTime}, t.end_time=#{p.endTime},t.contract=#{p.contract},t.receipt=#{p.receipt}  where  t.pay_hid=#{p.hid}";
    }

    public String updateByAdmin() {
        return "update history_pay t set t.status=#{p.status}, "
                + " t.end_time=#{p.endTime},t.product=#{p.product},t.money=#{p.money}   where  t.pay_hid=#{p.hid}";
    }

    public String updateStatus() {
        return "update history_pay t set t.status=#{status} where t.pay_hid=#{hid}";
    }

    public String getObjectByMid() {
        return "select * from history_pay where mid = #{mid} and status = #{status} order by pay_time desc limit 1";
    }

    public String getObjectByaliCount() {
        return "select * from history_pay where ali_count = #{ali_count}";
    }

    public String getHistoryPaysByMid() {
        return "select * from history_pay where mid = #{mid} ";
    }

    public String deleteHistoryPaysByhid() {
        return "delete  from history_pay where pay_hid = #{hid} ";
    }

    public String getLastPayByMid() {
        return "select * from history_pay where mid = #{mid} order by pay_hid desc limit 1";
    }

    public String getOrderByHid() {
        return "select * from history_pay where pay_hid = #{hid}";
    }

    public String getHistoryPaysByStatus() {
        return "select * from history_pay where status = #{status}";
    }
    
    public String getHistoryPaysByEndTime() {
        return "select * from history_pay where end_time<#{endTime} and status=1";
    }
    

}
