package com.touclick.captcha.smart.admin.mapper.primary.provider;

public class NoticeProvider {
	public String getLastNotice(){
		return "select * from notice t where t.pubtime=(select max(pubtime) from notice where useflag=1 )";
	}
	
	public String noticeList(){
		return "select * from notice t where t.useflag=1 order by t.pubtime desc";
	}
	
	public String save(){
		return "insert into notice(mid,title,content,useflag) values(#{notice.mid},#{notice.title},#{notice.content},1)";
	}
	
	public String del(){
		return "update notice t set t.useflag=0 where t.nid=#{nid}";
	}
}
