package com.touclick.captcha.smart.admin.model;

import org.apache.commons.codec.binary.Base64;

public class VipPicture {
	private int vpid;
	private int ctype;
	private byte[] backgroud;
	private String backgroudbase64;//背景图的base64编码
	private byte[] spic;
	private byte[] notetext;
	private int vid;
	public int getVpid() {
		return vpid;
	}
	public void setVpid(int vpid) {
		this.vpid = vpid;
	}
	public int getCtype() {
		return ctype;
	}
	public void setCtype(int ctype) {
		this.ctype = ctype;
	}
	public byte[] getBackgroud() {
		return backgroud;
	}
	public void setBackgroud(byte[] backgroud) {
		this.backgroud = backgroud;
	}
	public byte[] getSpic() {
		return spic;
	}
	public void setSpic(byte[] spic) {
		this.spic = spic;
	}
	public byte[] getNotetext() {
		return notetext;
	}
	public void setNotetext(byte[] notetext) {
		this.notetext = notetext;
	}
	public int getVid() {
		return vid;
	}
	public void setVid(int vid) {
		this.vid = vid;
	}
	public String getBackgroudbase64() {
		this.backgroudbase64 = new String(Base64.encodeBase64(backgroud));
		return backgroudbase64;
	}

	public void setBackgroudbase64(String backgroudbase64) {
		this.backgroudbase64 = backgroudbase64;
	}
}
