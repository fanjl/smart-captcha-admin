package com.touclick.captcha.smart.admin.controller;

import com.google.gson.Gson;
import com.touclick.captcha.smart.admin.auth.UserContext;
import com.touclick.captcha.smart.admin.model.MemberGroup;
import com.touclick.captcha.smart.admin.service.inter.IGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("group")
public class GroupController {
	@Autowired
	private IGroup iGroup;
	Gson gson = new Gson();
	@RequestMapping("groupList")
	public String groupList(HttpServletRequest request, Model model, @AuthenticationPrincipal UserContext userContext){
		List<MemberGroup> groupList = iGroup.getGroupByMid(userContext.getUserId());
		model.addAttribute("groupList", groupList);
		return "group/grouplist";
	}
	@ResponseBody
	@RequestMapping("save")
	public boolean save(HttpServletRequest request, MemberGroup mg, @AuthenticationPrincipal UserContext userContext){
		mg.setMid(userContext.getUserId());
		return iGroup.save(mg);
	}
	@ResponseBody
	@RequestMapping(value="getAdGroups")
	public String getAdGroups(HttpServletRequest request, @AuthenticationPrincipal UserContext userContext){
		List<MemberGroup> groupList = iGroup.getGroupByMid(userContext.getUserId());
		return gson.toJson(groupList);
	}
}
