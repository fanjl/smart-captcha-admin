package com.touclick.captcha.smart.admin.model;

public enum WebSiteInfoEnum {
	isOwnerPic("是否仅使用私有图库",1,"自定义上传图库"),isNeedAds("是否需要广告",2,""),oldCtype("旧系统的ctype",3,""),defaultCaptchaTypeProb("默认验证码类型出现概率",4,""),
	extraCaptchaTypeConfig("扩展验证码配置",5,""),visitCount("访问频次",6,""),checkCode("checkCode",7,""),IS_REMOVE_LOGO("是否去除logo",8,""),
	IS_REMOVE_HELP("是否启用去除点触帮助超链接",9,""),IS_BEHAVIOR_FEATURE("是否启用行为监控",10,""),IS_CUSTOM_TEXT("是否自定义提示文字",11,"");
	
	
	private String name;
	private int index;
	private String describe;
	private WebSiteInfoEnum(String name, int index, String describe){
		this.name = name;  
        this.index = index;
        this.describe = describe;
	}
	/**
     * 返回索引值
     * @param name
     * @return
     */
    public static int getIndex(String name){
    	for (com.touclick.captcha.smart.admin.model.WebSiteInfoEnum c : com.touclick.captcha.smart.admin.model.WebSiteInfoEnum.values()) {
            if (c.name().equals(name)) {  
                return c.index;
            }
    	}
        return 0;
    }
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
    
}
