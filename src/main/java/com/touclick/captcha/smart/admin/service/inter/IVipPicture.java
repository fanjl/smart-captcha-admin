package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.VipPicture;
import com.touclick.captcha.smart.admin.model.VipPictureClass;

import java.util.List;

public interface IVipPicture {
	/**
	 * 
	* @author wangpeng
	* @Title: getPictureClassByType 
	* @Description: TODO(获取所有vip图片的类别) 
	* @param @return    设定文件 
	* @return List<VipPictureType>    返回类型 
	* @throws
	 */
	public List<VipPictureClass> getPictureClassByType(int pid);
	
	/**
	 * 
	* @author wangpeng
	* @Title: getVipPictureByClass 
	* @Description: TODO(根据vip图片的类型获取图片) 
	* @param @param vid
	* @param @return    设定文件 
	* @return List<VipPicture>    返回类型 
	* @throws
	 */
	public List<VipPicture> getVipPictureByClass(int vid);
	/**
	 * 
	* @author wangpeng
	* @Title: getVipPictureClassByVid 
	* @Description: TODO(根据vid获得对象) 
	* @param @param vid
	* @param @return    设定文件 
	* @return VipPictureClass    返回类型 
	* @throws
	 */
	public VipPictureClass getObjecgByVid(int vid);
	/**
	 * 
	* @author wangpeng
	* @Title: getBypicReady 
	* @Description: TODO(获取已经购买的图片信息) 
	* @param @param pid mid
	* @param @return    设定文件 
	* @return List<VipPictureClass>    返回类型 
	* @throws
	 */
	public List<VipPictureClass> getBypicReady(int pid, int mid);
}
