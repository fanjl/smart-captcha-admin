package com.touclick.captcha.smart.admin.mapper.primary.provider;

import java.util.Map;

public class PayProvider {
	public String getAlipayDetail(Map<String,Object> paras){
		return "select t.drawid as liushui,t2.value as ali_person_account,t1.realname,t.money from draw_money t,member t1,member_info t2 where t.mid=t1.mid and t1.mid=t2.mid and t.drawid=#{drawid} and t.drawflag=2 and t2.type=1 and t2.useflag=1";
	}
	public String updateDrawMoney(Map<String,Object> paras){
		return "update draw_money t set t.drawflag=1 where t.drawflag=2 and t.drawid=#{drawid}";
	}
}
