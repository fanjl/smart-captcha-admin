package com.touclick.captcha.smart.admin.model;

import java.math.BigDecimal;
import java.util.Date;

public class CheckDrawMoney {
	private int drawid;
	private int mid;
	private String realname;
	private BigDecimal money;
	private Date createtime;
	private int drawflag;
	public int getDrawid() {
		return drawid;
	}
	public void setDrawid(int drawid) {
		this.drawid = drawid;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public BigDecimal getMoney() {
		return money;
	}
	public void setMoney(BigDecimal money) {
		this.money = money;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public int getDrawflag() {
		return drawflag;
	}
	public void setDrawflag(int drawflag) {
		this.drawflag = drawflag;
	}
	
}
