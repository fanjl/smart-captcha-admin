package com.touclick.captcha.smart.admin.model;

public class Tall {
	private int aid;
	private int wid;
	private long date;
	private int ctype;
	private int ltype;
	private int module;
	private int count;
	private int daycount;
	private int hourcount;
	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public int getCtype() {
		return ctype;
	}
	public void setCtype(int ctype) {
		this.ctype = ctype;
	}
	public int getLtype() {
		return ltype;
	}
	public void setLtype(int ltype) {
		this.ltype = ltype;
	}
	public int getModule() {
		return module;
	}
	public void setModule(int module) {
		this.module = module;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getDaycount() {
		return daycount;
	}
	public void setDaycount(int daycount) {
		this.daycount = daycount;
	}
	public int getHourcount() {
		return hourcount;
	}
	public void setHourcount(int hourcount) {
		this.hourcount = hourcount;
	}
	
}
