package com.touclick.captcha.smart.admin.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by yuanshichao on 16/4/20.
 */
public class GlobalConfig {

    private static final Logger LOGGER = Logger.getLogger(GlobalConfig.class);

    private static Properties p = new Properties();

    static {
        try {
            p.load(GlobalConfig.class.getClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            LOGGER.error("load config.properties error", e);
        }

    }

    private GlobalConfig() {
    }

    public static String getString(String key) {
        return p.getProperty(key);
    }

    public static Integer getInteger(String key) {
        try {
            return Integer.valueOf(Integer.parseInt(p.getProperty(key)));
        } catch (Exception e) {
            LOGGER.error("transfer String to Integer error", e);
            return null;
        }
    }
}
