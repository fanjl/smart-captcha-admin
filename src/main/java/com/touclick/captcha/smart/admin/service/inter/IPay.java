package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.AlipayDetail;

public interface IPay {
	public AlipayDetail getAlipayDetail(int drawid);
	public void updateDrawMoney(int drawid);
}
