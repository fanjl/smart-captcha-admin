package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.SysMenu;

import java.util.List;

public interface ISysMenu {
	/**
	 * 获得一级菜单
	 * @return
	 */
	public List<SysMenu> getOneMenu(int role);
	/**
	 * 根据一级菜单获得二级菜单
	 * @param sid
	 * @return
	 */
	public List<SysMenu> getTwoMenu(int oneid, int role);
}
