package com.touclick.captcha.smart.admin.controller;

import com.touclick.captcha.smart.admin.auth.UserContext;
import com.touclick.captcha.smart.admin.model.SysMenu;
import com.touclick.captcha.smart.admin.model.UserRoleEnum;
import com.touclick.captcha.smart.admin.service.inter.ISysMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/left")
public class LeftController {
	@Autowired
	private ISysMenu isysmenu;
//	@ResponseBody
//	@RequestMapping(value="leftmenu")
//    public String leftmenu(HttpServletRequest request,HttpSession session){
//		Member m = (Member)session.getAttribute("member");
//		StringBuffer sb = new StringBuffer();
//		List<SysMenu> onemenu = isysmenu.getOneMenu(m.getRole());
//		for(int i=0;i<onemenu.size();i++){
//			List<SysMenu> twomenu = isysmenu.getTwoMenu(onemenu.get(i).getSid(),m.getRole());
//			if(i==0){
//				sb.append("<li class=\"active treeview\">");
//			}else{
//				sb.append("<li class=\"treeview\">");
//			}
//			if(twomenu.size()==1){
//				sb.append("<a target=\"mainFrame\" id=\"activeMenu\" href=\""+request.getContextPath()+twomenu.get(0).getUrl()+"\">");
//			}else{
//				sb.append("<a href=\"#\">");
//			}
//			sb.append("<i class=\"fa "+onemenu.get(i).getIcon()+"\"></i>");
//			sb.append("<span>"+onemenu.get(i).getName()+"</span> ");
//			sb.append("<i class=\"fa fa-angle-left pull-right\"></i>");
//			sb.append("</a>");
//			if(twomenu.size()>1){
//				sb.append("<ul class=\"treeview-menu\">");
//				for(int j=0;j<twomenu.size();j++){
//					if(j==0){
//						sb.append("<li class=\"active\"><a target=\"mainFrame\" href=\""+request.getContextPath()+twomenu.get(j).getUrl()+"\">");
//					}else{
//						sb.append("<li><a target=\"mainFrame\" href=\""+request.getContextPath()+twomenu.get(j).getUrl()+"\">");
//					}
//					sb.append("<i class=\"fa fa-circle-o\"></i> "+twomenu.get(j).getName()+"</a></li>");
//				}
//				sb.append("</ul>");
//			}
//			sb.append("</li>");
//		}
//        return sb.toString();
//    }
	@ResponseBody
	@RequestMapping(value="leftmenu")
    public String leftmenu(HttpServletRequest request, HttpSession session, @AuthenticationPrincipal UserContext userContext){
//		Member m = (Member)session.getAttribute("member");
		StringBuffer sb = new StringBuffer();
		int role = UserRoleEnum.fromString(userContext.getAuthorities().get(0).toString()).getId();
		List<SysMenu> onemenu = isysmenu.getOneMenu(role);
		for(int i=0;i<onemenu.size();i++){
			List<SysMenu> twomenu = isysmenu.getTwoMenu(onemenu.get(i).getSid(),role);
			if(onemenu.get(i).getSid()==7){
			}else{
				sb.append("<section class=\"sidebar\">");
				sb.append("<div class=\"user-panel-menu\" style=\"height: 90px;\">");
				sb.append("<div class=\"center-block\" style=\"margin-top:10px;\">");
//				if(twomenu.size()==1){
//					sb.append("<a  class=\"dropdown-toggle\" target=\"mainFrame\" id=\"sid_"+onemenu.get(i).getSid()+"\" onclick=\"a_click(this)\" href=\""+request.getContextPath()+twomenu.get(0).getUrl()+"\">");
//				}else{
//					sb.append("<a href=\"#\">");
//				}
				sb.append("<a  class=\"dropdown-toggle\" target=\"mainFrame\" id=\"sid_"+onemenu.get(i).getSid()+"\" onclick=\"a_click(this)\" href=\""+request.getContextPath()+twomenu.get(0).getUrl()+"\">");
				sb.append("<img src="+onemenu.get(i).getIcon()+" />");
				sb.append("<div class=\"info\" style=\"margin-top:10px;\">");
				sb.append("<span>"+onemenu.get(i).getName()+"</span>");
				sb.append("</div>").append("</a>");
				sb.append("</div>").append("</div>").append("</section>");
			}
		}
        return sb.toString();
    }
}
