package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.BalanceProvider;
import com.touclick.captcha.smart.admin.model.HistoryPay;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface BalanceMapper {
    @InsertProvider(type = BalanceProvider.class, method = "save")
    public void save(@Param(value = "p") HistoryPay p);

    @InsertProvider(type = BalanceProvider.class, method = "saveSimple")
    public void saveSimple(@Param(value = "p") HistoryPay p);

    @UpdateProvider(type = BalanceProvider.class, method = "updateStatus")
    public void updateStatus(@Param(value = "hid") long hid, @Param(value = "status") int status);

    @UpdateProvider(type = BalanceProvider.class, method = "update")
    public void update(@Param(value = "p") HistoryPay p);

    @SelectProvider(type = BalanceProvider.class, method = "getObjectByMid")
    @Results({ @Result(property = "hid", column = "pay_hid"), @Result(property = "aliCount", column = "ali_count"),
            @Result(property = "payTime", column = "pay_time"), @Result(property = "beginTime", column = "begin_time"),
            @Result(property = "endTime", column = "end_time") })
    public HistoryPay getObjectByMid(@Param(value = "mid") int mid, @Param(value = "status") int status);

    @SelectProvider(type = BalanceProvider.class, method = "getObjectByaliCount")
    @Results({ @Result(property = "hid", column = "pay_hid"), @Result(property = "aliCount", column = "ali_count"),
            @Result(property = "payTime", column = "pay_time"), @Result(property = "beginTime", column = "begin_time"),
            @Result(property = "endTime", column = "end_time") })
    public HistoryPay getObjectByaliCount(@Param(value = "ali_count") String ali_count);

    @SelectProvider(type = BalanceProvider.class, method = "getHistoryPaysByMid")
    @Results({ @Result(property = "hid", column = "pay_hid"), @Result(property = "aliCount", column = "ali_count"),
            @Result(property = "payTime", column = "pay_time"), @Result(property = "beginTime", column = "begin_time"),
            @Result(property = "endTime", column = "end_time"), @Result(property = "contract", column = "contract"),
            @Result(property = "receipt", column = "receipt") })
    public List<HistoryPay> getHistoryPaysByMid(@Param(value = "mid") int mid);

    @DeleteProvider(type = BalanceProvider.class, method = "deleteHistoryPaysByhid")
    public Integer deleteHistoryPaysByhid(@Param(value = "hid") int hid);

    @SelectProvider(type = BalanceProvider.class, method = "getLastPayByMid")
    @Results({ @Result(property = "hid", column = "pay_hid"), @Result(property = "aliCount", column = "ali_count"),
            @Result(property = "payTime", column = "pay_time"), @Result(property = "beginTime", column = "begin_time"),
            @Result(property = "endTime", column = "end_time"), @Result(property = "contract", column = "contract"),
            @Result(property = "receipt", column = "receipt") })
    public HistoryPay getLastPayByMid(@Param(value = "mid") int mid);

    @SelectProvider(type = BalanceProvider.class, method = "getOrderByHid")
    @Results({ @Result(property = "hid", column = "pay_hid"), @Result(property = "aliCount", column = "ali_count"),
            @Result(property = "payTime", column = "pay_time"), @Result(property = "beginTime", column = "begin_time"),
            @Result(property = "endTime", column = "end_time"), @Result(property = "contract", column = "contract"),
            @Result(property = "receipt", column = "receipt") })
    public HistoryPay getOrderByHid(@Param(value = "hid") int hid);

    @UpdateProvider(type = BalanceProvider.class, method = "updateByAdmin")
    public void updateByAdmin(@Param(value = "p") HistoryPay p);

    @SelectProvider(type = BalanceProvider.class, method = "getHistoryPaysByStatus")
    @Results({ @Result(property = "hid", column = "pay_hid"), @Result(property = "aliCount", column = "ali_count"),
            @Result(property = "payTime", column = "pay_time"), @Result(property = "beginTime", column = "begin_time"),
            @Result(property = "endTime", column = "end_time"), @Result(property = "contract", column = "contract"),
            @Result(property = "receipt", column = "receipt") })
    public List<HistoryPay> getHistoryPaysByStatus(@Param(value = "status") int status);
    
    @SelectProvider(type = BalanceProvider.class, method = "getHistoryPaysByEndTime")
    @Results({ @Result(property = "hid", column = "pay_hid"), @Result(property = "aliCount", column = "ali_count"),
            @Result(property = "payTime", column = "pay_time"), @Result(property = "beginTime", column = "begin_time"),
            @Result(property = "endTime", column = "end_time"), @Result(property = "contract", column = "contract"),
            @Result(property = "receipt", column = "receipt") })
    public List<HistoryPay> getHistoryPaysByEndTime(@Param(value = "endTime") long endTime);
    

}
