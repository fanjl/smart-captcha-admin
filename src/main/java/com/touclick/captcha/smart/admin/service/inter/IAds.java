package com.touclick.captcha.smart.admin.service.inter;


import com.touclick.captcha.smart.admin.model.Ads;
import com.touclick.captcha.smart.admin.model.MemberGroup;
import com.touclick.captcha.smart.admin.utils.Pagination;

import java.util.List;

/**
 * @author bing.liu
 */
public interface IAds {
    /**
     * Insert a ads into the database;
     * @param ads
     */
    public void insert(Ads ads);

    /**
     * Select a ads by given ads id
     * @param aid
     * @return
     */
    public Ads selectByAid(int aid);

    /**
     * select a ads list by aid list
     * @param aids list of aid
     * @return List<Ads>
     */
    public List<Ads> selectByAids(List<Integer> aids);
    /**
     * Select ads by given atype
     * @param atype
     * @return
     */
    public List<Ads> selectByAtype(int atype);

    /**
     * Select ads list by given ctype
     * @param ctype
     * @return
     */
    public List<Ads> selectByCtype(int ctype);

    /**
     * Select ads list by given member id
     * @param mid
     * @return
     */
    public List<Ads> selectByMid(int mid);
    
    /**
     * Select ads list
     * @return
     */
    public List<Ads> selectAll();
    
    /**
     * 根据会员id获得广告图片id
     * @return
     */
    public List<Integer> getAidByMid(int mid);
    /**
     * 根据广告图片id获取所在图片组的配置信息
     * @param ads
     * @return
     */
    public List<MemberGroup> getMemberGroupConfByAds(String ads);
    /**
     * 根据图片组id获取图片组中的图片集合
     * @param gid
     * @return
     */
    public List<Integer> getAidByGid(int gid);
    
    /**
     * 获取所有的Aid集合
     * @return
     */
    public List<Integer> selectAllAid();
    /**
     * 获取非广告集合
     * @return
     */
    public List<Ads> getNotAds();
    /**
     * 删除该人自定义的广告
     * @param mid
     * @param aid
     */
    public void delAds(int mid, int aid);
    
    /**
     * 
    * @author wangpeng
    * @Title: getAidByVid 
    * @Description: TODO(获取收费图片) 
    * @param @param vid
    * @param @return    设定文件 
    * @return List<Integer>    返回类型 
    * @throws
     */
    public List<Integer> getAidByVid(int vid);
    
    /**
     * 
    * @author wangpeng
    * @Title: getAdsByGid 
    * @Description: TODO(获取广告组中的广告图片) 
    * @param @return    设定文件 
    * @return List<Ads>    返回类型 
    * @throws
     */
    public List<Ads> getAdsByGid(int gid);
    
    /**
     * 
    * @author wangpeng
    * @Title: getAdsByMid 
    * @Description: TODO(根据mid获得自定义图片) 
    * @param @param mid
    * @param @return    设定文件 
    * @return List<Ads>    返回类型 
    * @throws
     */
    public List<Ads> getAdsByMid(int mid);
    /**
     * @param wid 
     * 根据wid和atype=1查询网站自定义图片
     * @return
     */
    public List<Ads> getAdsListByWid(int wid);
    
    /**
     * @param start end 
     * 传入createtime不包括的时间，返回distinct的wid以及createtime
     * @return
     */
    public List<Ads> getDistinctWidWithTime(String start, String end);
    
    public List<Ads> getAdsByPagination(Pagination<Ads> ads);
    
    public int countTotal();
    
}
