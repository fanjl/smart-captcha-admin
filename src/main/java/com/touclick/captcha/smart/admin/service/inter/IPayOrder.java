package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.PayOrder;

public interface IPayOrder {
	public void save(PayOrder po);
	
	public int updateStatus(long oid, int status, String tradeno);
	
	public PayOrder getObjectByOid(long oid);
}
