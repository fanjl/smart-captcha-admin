package com.touclick.captcha.smart.admin.model;

public enum MemberLevelEnum {
	FREE(0,"免费版"),VIP1(1,"专业版"),VIP2(2,"企业版"),VIP3(3,"旗舰版");
	private String name;
	private int index;
	private MemberLevelEnum(int index,String name){
		this.name = name;
		this.index = index;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	
}
