package com.touclick.captcha.smart.admin.model;

public enum UnitEnum {
	year("年",1),month("月",2),day("天",3);
	private String name;
	private int index;
	private UnitEnum(String name, int index){
		this.name = name;  
        this.index = index;
	}
	/**
     * 返回索引值
     * @param name
     * @return
     */
    public static int getIndex(String name){
    	for (com.touclick.captcha.smart.admin.model.UnitEnum c : com.touclick.captcha.smart.admin.model.UnitEnum.values()) {
            if (c.name().equals(name)) {  
                return c.index;
            }
    	}
        return 0;
    }
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
}
