package com.touclick.captcha.smart.admin.mapper.primary.provider;

import com.touclick.captcha.smart.admin.model.Group;

import java.util.Map;

public class ResourceGroupProvider {
    public String insert(Map<String, Group> params) {
        return "INSERT INTO resource_group (mid,ctype,createtime,wid,type) VALUES("
                + "#{group.mid},#{group.ctype},#{group.createtime},#{group.wid},#{group.type})";
    }
    
    public String getGroupByWid(Map<String,Object> paras) {
        return "select * from resource_group where wid=#{wid}";
    }
    
    public String getGidsByWid(Map<String,Object> paras) {
        return "select * from resource_group where wid=#{wid}";
    }
    
    public String getGidsByWidAndCtype(Map<String,Object> paras) {
        return "select * from resource_group where wid=#{wid} and ctype=#{ctype}";
    }
    
    public String getGroupById(Map<String,Object> paras) {
        return "select * from resource_group where gid=#{gid}";
    }
    

}
