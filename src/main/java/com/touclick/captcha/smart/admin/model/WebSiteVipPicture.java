package com.touclick.captcha.smart.admin.model;

import java.util.Date;

public class WebSiteVipPicture {
	private int wid;
	private int vid;
	private int servicetime;//有效期 单位 月
	private Date endtime;//到期时间
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	public int getVid() {
		return vid;
	}
	public void setVid(int vid) {
		this.vid = vid;
	}
	public int getServicetime() {
		return servicetime;
	}
	public void setServicetime(int servicetime) {
		this.servicetime = servicetime;
	}
	public Date getEndtime() {
		return endtime;
	}
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	
}
