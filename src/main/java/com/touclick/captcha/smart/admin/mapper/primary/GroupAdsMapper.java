package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.GroupAdsProvider;
import com.touclick.captcha.smart.admin.model.GroupAds;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface GroupAdsMapper {
	@SelectProvider(type = GroupAdsProvider.class,method = "findAll")
	public List<GroupAds> findAll();
	
	@SelectProvider(type = GroupAdsProvider.class,method = "findByGid")
	public List<Integer> findByGid(@Param(value = "gid") int gid);
	
	@InsertProvider(type = GroupAdsProvider.class,method = "save")
	public void save(@Param(value = "ga") GroupAds ga);
}
