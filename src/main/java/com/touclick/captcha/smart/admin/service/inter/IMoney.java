package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.CheckDrawMoney;
import com.touclick.captcha.smart.admin.model.DrawMoney;
import com.touclick.captcha.smart.admin.model.HistoryCost;

import java.math.BigDecimal;
import java.util.List;

public interface IMoney {
	/**
	 * 获取网站主的收益
	 * @param mid
	 * @return
	 */
	public BigDecimal getEarnings(int mid);
	/**
	 * 获取网站主历史收益
	 * @param mid
	 * @return
	 */
	public List<HistoryCost> getHistoryCost(int mid);
	
	/**
	 * 获取网站主历史账单
	 * @param mid
	 * @return
	 */
	public List<HistoryCost> getHsitoryBill(int mid);
	/**
	 * 获取网站主提现记录
	 * @param mid
	 * @return
	 */
	public List<DrawMoney> getDrawMoneyRecord(int mid);
	/**
	 * 获取网站主某时间段的收益
	 * @param mid
	 * @param dateRange
	 * @return
	 */
	public BigDecimal getCountMoneyByDateRange(int mid, String[] dateRange);
	/**
	 * 获取网站主全部收益
	 * @param mid
	 * @return
	 */
	public BigDecimal getCountAllMoney(int mid);
	/**
	 * 获取审核记录
	 * @return
	 */
	public List<CheckDrawMoney> getCheckList();
	/**
	 * 保存收益
	 * @param mid
	 * @param wid
	 * @param earnings
	 * @param unit
	 */
	public void saveEarnings(int mid, int wid, BigDecimal earnings, int unit);
}
