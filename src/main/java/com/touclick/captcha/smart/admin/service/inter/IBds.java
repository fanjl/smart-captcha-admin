package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.Bds;

import java.util.List;

public interface IBds {
	public int save(Bds bds);
	
	public List<Bds> getBdsByWid(int wid);
	
	public int delBds(int bid);

	public List<Integer> selectAllBid();

	public Bds selectByBid(int bid);

	public List<Bds> selectByBids(List<Integer> bids);
	
	public List<Bds> getDistinctWid();
}
