package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.WebSite;

public interface IZookeeperService {
	/**
	 * 
	 * @param wsm
	 */
	public void addWebSite(WebSite wsm);
	/**
	 * 
	 * @param wsm
	 */
	public void updateWebSite(WebSite wsm);
	/**
	 * 
	 * @param wid
	 */
	public void deleteWebSite(int wid);
}
