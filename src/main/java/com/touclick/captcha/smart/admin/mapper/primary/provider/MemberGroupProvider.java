package com.touclick.captcha.smart.admin.mapper.primary.provider;

import java.util.Map;

public class MemberGroupProvider {
	 
	public String findAll(Map<String,Object> paras){
		return "select * from member_group ";
	}
	
	public String findByGid(Map<String,Object> paras){
		return "select * from member_group where gid = #{gid}";
	}
	
	public String findAllGid(Map<String,Object> paras){
		return "select gid from member_group ";
	}
}
