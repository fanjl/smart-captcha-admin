package com.touclick.captcha.smart.admin.controller;

import com.google.gson.Gson;
import com.touclick.captcha.smart.admin.auth.UserContext;
import com.touclick.captcha.smart.admin.model.*;
import com.touclick.captcha.smart.admin.service.inter.*;
import com.touclick.captcha.smart.admin.utils.CversionUtils;
import com.touclick.captcha.smart.admin.utils.ImageUtils;
import com.touclick.captcha.smart.admin.utils.LevelConfigUtils;
import org.apache.commons.codec.binary.Base64;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping(value = "captcha")
public class CaptchaController {
    @Autowired
    private IAds iAds;
    @Autowired
    private IBds iBds;
    @Autowired
    private IWebSite iWebSite;
    @Autowired
    private IGroupAds iGroupAds;

    Gson gson = new Gson();

    @RequestMapping(value = "home")
    public String home(HttpServletRequest request, Model model,@AuthenticationPrincipal UserContext userContext) {

        List<WebSite> list = iWebSite.getWebSiteList(userContext.getUserId());
        List<CversionMapper> clist = new ArrayList<CversionMapper>();
        try {
            clist = CversionUtils.getCversionMapperCtype();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        model.addAttribute("website", list);
        model.addAttribute("cversionMapper", gson.toJson(clist));
        return "captcha/tactics";
    }
    @ResponseBody
    @RequestMapping(value = "WebSiteTastic")
    public String WebSiteTastic(HttpServletRequest request, int wid, Model model) {
        LevelConf levelConf = new LevelConf();
        return gson.toJson(levelConf);
    }

    @RequestMapping(value = "captchaManage")
    public String captchaManage(HttpServletRequest request, Model model, @AuthenticationPrincipal UserContext userContext) {
        List<WebSite> list = iWebSite.getWebSiteList(userContext.getUserId());
        List<CversionMapper> clist = new ArrayList<CversionMapper>();
        try {
            clist = CversionUtils.getCversionMapperCtype();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        model.addAttribute("website", list);
        model.addAttribute("cversionMapper", gson.toJson(clist));
        return "captcha/captchamanage";
    }

    @RequestMapping(value = "imgText")
    public String imgText(int currentwebsite, HttpServletRequest request, Model model) {
        int wid = Integer.parseInt(request.getParameter("currentwebsite"));
        List<Ads> notads = iAds.getNotAds();
        // List<Ads> custom = iAds.getAdsByMid(m.getMid());
        List<Ads> custom = iAds.getAdsListByWid(wid);
        boolean custompic = iWebSite.isOnlyOwnerPic(currentwebsite);
        model.addAttribute("notads", notads);
        model.addAttribute("custom", custom);
        model.addAttribute("custompic", custompic);
        model.addAttribute("wid", currentwebsite);
        return "captcha/imgtext";
    }

    @RequestMapping(value = "rotateManage")
    public String rotateManage(int currentwebsite, HttpServletRequest request, Model model) {
        List<Bds> list = iBds.getBdsByWid(currentwebsite);
        boolean custompic = iWebSite.isOnlyOwnerPic(currentwebsite);
        model.addAttribute("custompic", custompic);
        model.addAttribute("bds", list);
        model.addAttribute("wid", currentwebsite);
        return "captcha/rotatemanage";
    }

    @ResponseBody
    @RequestMapping(value = "cropimg")
    public String cropimg(CropPicture cp, HttpServletRequest request) {
        CropMessage gm = new CropMessage();
        byte[] bimage = Base64.decodeBase64(cp.getImgUrl().substring(cp.getImgUrl().indexOf(",") + 1,
                cp.getImgUrl().length()));
        try {
            String tempSrc = request.getSession().getServletContext().getRealPath("/temp");
            String tempImgName = UUID.randomUUID() + ".jpg";
            byte[] nimage = ImageUtils.cutImage(bimage, cp, tempSrc + "/" + tempImgName);
            gm.setStatus("success");
            gm.setUrl("data:image/jpg;base64," + new String(Base64.encodeBase64(nimage)));
            // gm.setUrl(request.getContextPath()+"/temp/"+tempImgName);
            gm.setWidth(cp.getCropW());
            gm.setHeight(cp.getCropH());
        } catch (Exception e) {
            gm.setStatus("error");
            gm.setMessage("裁剪失败，请重试");
            e.printStackTrace();
        }
        Gson gson = new Gson();
        return gson.toJson(gm);
    }

    @ResponseBody
    @RequestMapping(value = "saveCustomPic")
    public boolean saveCustomPic(CustomAds ca, HttpServletRequest request,@AuthenticationPrincipal UserContext userContext) {
        
        // String imgsrc =
        // request.getSession().getServletContext().getRealPath("/temp/")+ca.getTempurl().substring(ca.getTempurl().indexOf("temp")+4,ca.getTempurl().length());
        // File file = new File(imgsrc);
        Ads ads = new Ads();
        ads.setCtype(1);
        ads.setMid(userContext.getUserId());
        ads.setIspub(0);
        ads.setLogourl("");
        ads.setAtype(1);
        ads.setWid(ca.getWid());
        ads.setImgtexttype(ca.getImgtexttype());
        try {
            // ads.setBackgroud(FileUtils.readFileToByteArray(file));
            ads.setBackgroud(Base64.decodeBase64(ca.getBackground()));
            if (ca.getImgtexttype() == 0) {
                char[] clickfont = ca.getClickfont().toCharArray();
                char[] printfont = ca.getNewprintfont().toCharArray();
                for (char printf : printfont) {
                    ads.addSmallPicOrText(String.valueOf(printf).getBytes("UTF-8"));
                }
                for (char clickf : clickfont) {
                    ads.addNoteText(String.valueOf(clickf).getBytes("UTF-8"));
                }
                ads.setPrintfont(ca.getPrintfont());
            } else if (ca.getImgtexttype() == 1) {
                String transparent = "";
                if (ca.getSmallpic1() != null) {
                    ads.addSmallPicOrTextAndNoteText(ca.getSmallpic1().getBytes(), ca.getRemind1().getBytes("UTF-8"));
                    transparent = transparent + "," + ca.getTransparent1();
                }
                if (ca.getSmallpic2() != null) {
                    ads.addSmallPicOrTextAndNoteText(ca.getSmallpic2().getBytes(), ca.getRemind2().getBytes("UTF-8"));
                    transparent = transparent + "," + ca.getTransparent2();
                }
                if (ca.getSmallpic3() != null) {
                    ads.addSmallPicOrTextAndNoteText(ca.getSmallpic3().getBytes(), ca.getRemind3().getBytes("UTF-8"));
                    transparent = transparent + "," + ca.getTransparent3();
                }
                ads.setTransparent(transparent.substring(1, transparent.length()));
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        iAds.insert(ads);
        if (ca.getAdgroups() != 0) {
            GroupAds ga = new GroupAds();
            ga.setAid(ads.getAid());
            ga.setGid(ca.getAdgroups());
            ga.setMid(userContext.getUserId());
            ga.setCtype(ads.getCtype());
            iGroupAds.save(ga);
        }
        return true;
    }





    @ResponseBody
    @RequestMapping(value = "saveDragPic")
    public boolean saveDragPic(HttpServletRequest request, String dragPic, int wid) {
        Member m = (Member) request.getSession().getAttribute("member");
        Ads ads = new Ads();
        ads.setCtype(3);
        ads.setMid(m.getMid());
        ads.setIspub(0);
        ads.setLogourl("");
        ads.setAtype(1);
        ads.setWid(wid);
        ads.setBackgroud(Base64.decodeBase64(dragPic));
        iAds.insert(ads);
        return true;
    }

    @ResponseBody
    @RequestMapping(value = "updateCustomPic")
    public boolean updateCustomPic(HttpServletRequest request, boolean custompic, int wid) {
        try {
            iWebSite.updateCustomPic(wid, custompic);
//            iWebSiteInfo.setIsOwnPicByWid(custompic == true ? 1 : 0, wid);
//            webSiteCache.saveWebSiteCache(wid);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ResponseBody
    @RequestMapping(value = "updateRemoveLogoStatus")
    public int updateRemoveLogoStatus(HttpServletRequest request, boolean isRemoveLogo, int wid) {
        try {
            Member m = (Member) request.getSession().getAttribute("member");
            if (isRemoveLogo) {
                if (!LevelConfigUtils.getIsEnableRemoveLogo(m.getLevel())) {
                    return 3;
                }
                ;
            }
            iWebSite.updateRemoveStatus(wid, isRemoveLogo,
                    WebSiteInfoEnum.getIndex(WebSiteInfoEnum.IS_REMOVE_LOGO.toString()));
//            iWebSiteInfo.setRemoveStatusByWid(isRemoveLogo == true ? 1 : 0, wid, Constant.LOGO_W_KEY);
//            webSiteCache.saveWebSiteCache(wid);
        } catch (Exception e) {
            e.printStackTrace();
            return 2;
        }
        return 1;
    }

    @ResponseBody
    @RequestMapping(value = "updateRemoveHelpStatus")
    public int updateRemoveHelpStatus(HttpServletRequest request, boolean isRemoveHelp, int wid) {
        try {
            Member m = (Member) request.getSession().getAttribute("member");
            if (isRemoveHelp) {
                if (!LevelConfigUtils.getIsEnableRemoveHelp(m.getLevel())) {
                    return 3;
                }
                ;
            }
            iWebSite.updateRemoveStatus(wid, isRemoveHelp,
                    WebSiteInfoEnum.getIndex(WebSiteInfoEnum.IS_REMOVE_HELP.toString()));
//            iWebSiteInfo.setRemoveStatusByWid(isRemoveHelp == true ? 1 : 0, wid, Constant.HELP_W_KEY);
//            webSiteCache.saveWebSiteCache(wid);
        } catch (Exception e) {
            e.printStackTrace();
            return 2;
        }
        return 1;
    }

    @ResponseBody
    @RequestMapping(value = "updateCustomTxtStatus")
    public int updateIsSupportCustomTxt(HttpServletRequest request, boolean isCustomTxt, int wid) {
        try {
            Member m = (Member) request.getSession().getAttribute("member");
            if (isCustomTxt) {
                if (!LevelConfigUtils.getIsEnableCustomPromptText(m.getLevel())) {
                    return 3;
                }
                ;
            }
            iWebSite.updateRemoveStatus(wid, isCustomTxt,
                    WebSiteInfoEnum.getIndex(WebSiteInfoEnum.IS_CUSTOM_TEXT.toString()));
//            iWebSiteInfo.setRemoveStatusByWid(isCustomTxt == true ? 1 : 0, wid, Constant.PROMOPTTEXT_W_KEY);
//            webSiteCache.saveWebSiteCache(wid);
        } catch (Exception e) {
            e.printStackTrace();
            return 2;
        }
        return 1;
    }

    @ResponseBody
    @RequestMapping(value = "updateBehaviorFeature")
    public int updateBehaviorFeature(HttpServletRequest request, boolean isBehaviorFeature, int wid) {
        try {
            Member m = (Member) request.getSession().getAttribute("member");
            if (isBehaviorFeature) {
                if (!LevelConfigUtils.getIsEnableBehaviorFeature(m.getLevel())) {
                    return 3;
                }
                ;
            }
            iWebSite.updateRemoveStatus(wid, isBehaviorFeature,
                    WebSiteInfoEnum.getIndex(WebSiteInfoEnum.IS_BEHAVIOR_FEATURE.toString()));
//            iWebSiteInfo.setRemoveStatusByWid(isBehaviorFeature == true ? 1 : 0, wid, Constant.BEHAVIOR_W_KEY);
//            webSiteCache.saveWebSiteCache(wid);
        } catch (Exception e) {
            e.printStackTrace();
            return 2;
        }
        return 1;
    }

    @ResponseBody
    @RequestMapping(value = "delAds")
    public boolean delAds(HttpServletRequest request, int aid) {
        Member m = (Member) request.getSession().getAttribute("member");
        try {
            iAds.delAds(m.getMid(), aid);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ResponseBody
    @RequestMapping(value = "updateCtypeAndSkin")
    public boolean updateCtypeAndSkin(int wid, int ctype, String skin) {
        try {
            WebSite website = iWebSite.getWebSiteByWid(wid);
            if (website.getCtype() != ctype) {
                iWebSite.delCustom(wid, WebSiteInfoEnum.isOwnerPic.getIndex());
            }
            iWebSite.updateCtypeAndSkin(wid, ctype, skin);
            if (ctype == 1) {
                iWebSite.updateWebSiteInfo(wid, WebSiteInfoEnum.oldCtype.getIndex(), "1,3");
            } else if (ctype == 2) {
                iWebSite.updateWebSiteInfo(wid, WebSiteInfoEnum.oldCtype.getIndex(), "4");
            }
//            iWebSiteInfo.setCTypeWithWid(ctype + "", wid + "");
//            iWebSiteInfo.updateDefaultCType(wid + "", ctype + "", 5);
//            webSiteCache.saveWebSiteCache(wid);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ResponseBody
    @RequestMapping(value = "getWebsiteCtypeSkin",method = RequestMethod.POST)
    public String getWebsiteCtypeSkin(int wid) {
        WebSite ws = iWebSite.getWebSiteByWid(wid);
        return ws.getCtype() + "," + ws.getSkin();
    }

    @ResponseBody
    @RequestMapping(value = "saveRotateCustom")
    public boolean saveRotateCustom(HttpServletRequest request, String rotatePic, int wid) {
        Member m = (Member) request.getSession().getAttribute("member");
        Bds bds = new Bds();
        bds.setMid(m.getMid());
        bds.setWid(wid);
        bds.setBackground(Base64.decodeBase64(rotatePic));
        int result = iBds.save(bds);
        if (result > 0) {
            return true;
        }
        return false;
    }

    @ResponseBody
    @RequestMapping(value = "delBds")
    public boolean delBds(int bid) {
        int result = iBds.delBds(bid);
        if (result > 0) {
            return true;
        }
        return false;
    }
}
