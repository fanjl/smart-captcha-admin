package com.touclick.captcha.smart.admin.service.impl;

import com.touclick.captcha.smart.admin.mapper.primary.BdsMapper;
import com.touclick.captcha.smart.admin.model.Bds;
import com.touclick.captcha.smart.admin.service.inter.IBds;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("iBds")
public class BdsImpl  implements IBds {
	private BdsMapper write;
	private BdsMapper read;
	@Override
    public int save(Bds bds) {
		
		return write.save(bds);
	}
	@Override
    public List<Bds> getBdsByWid(int wid) {
		
		return read.getBdsByWid(wid);
	}
	public List<Bds> findAll() {
		
		return read.findAll();
	}
	@Override
    public int delBds(int bid){

		return write.delBds(bid);
	}
	public List<Integer> selectAllBid(){
		
		return read.selectAllBid();
	}

	@Override
	public Bds selectByBid(int bid) {
		
		return read.selectByBid(bid);
	}

	@Override
	public List<Bds> selectByBids(List<Integer> bids) {
		if(bids== null||bids.size() ==0){
			return new ArrayList<Bds>(0);
		}
		StringBuilder sb = new StringBuilder();
		for(int i= 0;i<bids.size();i++){
			if(i>0){
				sb.append(',');
			}
			sb.append(bids.get(i));
		}
		
		return read.selectByBids(sb.toString());
	}
    @Override
    public List<Bds> getDistinctWid() {
        
        return read.getDistinctWid();
    }

}
