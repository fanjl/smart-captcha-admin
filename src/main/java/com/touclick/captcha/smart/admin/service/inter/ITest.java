package com.touclick.captcha.smart.admin.service.inter;

import com.touclick.captcha.smart.admin.model.VipPicture;
import com.touclick.captcha.smart.admin.model.VipPictureClass;

/**
 * 
* @ClassName: ITest 
* @Description: TODO(测试使用) 
* @author wangpeng
* @date 2016年1月5日 下午4:29:04 
*
 */
public interface ITest {
	public void insertVipPictureClass(VipPictureClass vpt);
	
	public void insertVipPicture(VipPicture vp);
}
