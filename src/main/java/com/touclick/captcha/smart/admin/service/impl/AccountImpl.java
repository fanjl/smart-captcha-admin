package com.touclick.captcha.smart.admin.service.impl;

import com.touclick.captcha.smart.admin.mapper.primary.AccountMapper;
import com.touclick.captcha.smart.admin.model.MemberInfo;
import com.touclick.captcha.smart.admin.service.inter.IAccount;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class AccountImpl   implements IAccount {
	private static Logger logger = Logger.getLogger(AccountImpl.class);
	private AccountMapper readAccountMapper;
	private AccountMapper writeAccountMapper;
	@Override
    public List<MemberInfo> getAccountList(int mid) {
		List<MemberInfo> list = readAccountMapper.getAccountList(mid);
		return list;
	}
	@Override
    public boolean checkAccount(int accounttype,String account) {

		MemberInfo m = readAccountMapper.checkAccount(accounttype,account);
		if(m!=null){
			return false;
		}else{
			return true;
		}
	}
	@Override
    @Transactional(rollbackFor=Exception.class)
	public int saveAccount(int mid,int accounttype, String account) {


		List<MemberInfo> list = readAccountMapper.getAccountList(mid);
		int useflag=0;
		if(list.size()==0){
			useflag=1;
		}
		return writeAccountMapper.saveAccount(mid,accounttype,account,useflag);
	}
	@Override
    public int delAccount(int id){

		return writeAccountMapper.delAccount(id);
	}
	@Override
    @Transactional(rollbackFor=Exception.class)
	public void defaultAccount(int mid, int accountid) {

		writeAccountMapper.updateAccount(mid);
		writeAccountMapper.defaultAccount(accountid);
	}
}
