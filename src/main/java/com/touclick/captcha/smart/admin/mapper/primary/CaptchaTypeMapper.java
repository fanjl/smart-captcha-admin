package com.touclick.captcha.smart.admin.mapper.primary;

import com.touclick.captcha.smart.admin.mapper.primary.provider.CaptchaTypeProvider;
import com.touclick.captcha.smart.admin.model.CaptchaType;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * @author bing.liu
 */
public interface CaptchaTypeMapper {
    @InsertProvider(type = CaptchaTypeProvider.class,method = "insert")
    public void insert(@Param(value = "ctype") CaptchaType ctype);

    @SelectProvider(type = CaptchaTypeProvider.class,method = "selectById")
    public CaptchaType selectById(@Param(value = "id") int id);

    @SelectProvider(type = CaptchaTypeProvider.class,method = "selectAll")
    public List<CaptchaType> selectAll();

    @SelectProvider(type = CaptchaTypeProvider.class,method = "selectByName")
    public CaptchaType selectByName(@Param(value = "name") String name);
}
