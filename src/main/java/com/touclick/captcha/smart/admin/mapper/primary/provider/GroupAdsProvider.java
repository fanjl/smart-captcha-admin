package com.touclick.captcha.smart.admin.mapper.primary.provider;

import java.util.Map;

public class GroupAdsProvider {
	 
	public String findAll(Map<String,Object> paras){
		return "select * from group_ads ";
	}
	
	public String findByGid(Map<String,Object> paras){
		return "select aid from group_ads where gid = #{gid}";
	}
	
	public String save(){
		return "insert into group_ads(gid,aid,ctype,mid) values(#{ga.gid},#{ga.aid},#{ga.ctype},#{ga.mid})";
	}
}
