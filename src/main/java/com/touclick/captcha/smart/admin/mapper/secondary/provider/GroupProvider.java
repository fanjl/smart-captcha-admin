package com.touclick.captcha.smart.admin.mapper.secondary.provider;

public class GroupProvider {
	public String getGroupByMid(){
		return "select * from member_group t where t.mid=#{mid}";
	}
	public String save(){
		return "insert into member_group(gname,mid,count,formula) values(#{mg.gname},#{mg.mid},#{mg.count},#{mg.formula})";
	}
	public String getAllGroup(){
		return "select * from member_group t";
	}
}
