<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<head>
<#include "../common/base.ftl">
<link rel="stylesheet"
	href="${ctx }/plugins/ladda/ladda-themeless.min.css">
<link rel="stylesheet" href="${ctx }/css/timeManage.css">
<link rel="stylesheet" href="${ctx }/css/titatoggle-dist.css">
</head>
<body>
	<div class="wrapper">
		<div class="payPage container-fluid">
			<!-- 头部模块 -->
			<div class="payPage-head row">
				<!-- 头像 -->
				<div class="payPage-user col-sm-6">
					<div class="row">
						<div class="payPage-userimg col-sm-2"></div>
						<div class="col-sm-10">
							<div class="payPage-username">${sessionScope.member.realname}<span></span>
							</div>
							<div class="row">
								<div class="payPage-version col-md-6">
									您当前的版本： <span> <c:choose>
											<c:when test="${sessionScope.member.level==0}">
												<td>免费版</td>
											</c:when>
											<c:when test="${sessionScope.member.level==1}">
												<td>站长版</td>
											</c:when>
											<c:when test="${sessionScope.member.level==2}">
												<td>企业标准版A</td>
											</c:when>
											<c:when test="${sessionScope.member.level==3}">
												<td>企业标准版B</td>
											</c:when>
											<c:when test="${sessionScope.member.level==4}">
												<td>企业标准版C</td>
											</c:when>
											<c:when test="${sessionScope.member.level==5}">
												<td>企业旗舰版A</td>
											</c:when>
											<c:when test="${sessionScope.member.level==6}">
												<td>企业旗舰版B</td>
											</c:when>
											<c:when test="${sessionScope.member.level==7}">
												<td>企业旗舰版C</td>
											</c:when>
										</c:choose>
									</span>
								</div>
								<div class="payPage-day col-md-6">
									当前剩余时间：
									<c:if test="${historypay.leftDay == -1}">
										<span>免费版不限制天数</span>
									</c:if>
									<c:if test="${historypay.leftDay != -1}">
										<span>${historypay.leftDay}天</span>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<a href="/buy/history"
					class="col-sm-4 col-sm-offset-2 payPage-history">查看购买历史>></a>
			</div>
			<!-- 内容模块 -->
			<div class="row payPage-content">
				<!-- 左边模块 -->
				<div class="payPage-left col-sm-3">
					<h3>当前套餐内容</h3>
					<div class="payPage-combo">
						<p>
							<strong>更新频率：</strong><span> ${conf.validateFreq }</span>
						</p>
						<p>
							<strong>HTTP/HTTPS:</strong><span> ${conf.isSupportSsl }</span>
						</p>
						<p>
							<strong>移动端多平台：</strong><span>
								${conf.isSupportMobileMultiTerminal }</span>
						</p>
						<p>
							<strong>并发峰值：</strong><span>${conf.maxConcurrent } Q/s</span>
						</p>
						<p>
							<strong>请求量上限：</strong><span> ${conf.maxRequest } 次/小时</span>
						</p>
						<p>
							<strong>部署ID数：</strong><span>${conf.idCountLimit }次</span>
						</p>

						<div id="toggleDiv" style="display: none;">
							<p>
								<strong>密码安全检测：</strong><span>${conf.isSupportPasswordSecurity }
								</span>
							</p>
							<p>
								<strong>是否支持去除点触帮助超链接：</strong><span>${conf.isSupportRemoveTouclickHelp }</span>
							</p>
							<p>
								<strong>是否支持去除点触logo：</strong><span>${conf.isEnableRemoveLogo }</span>
							</p>
						</div>
						<p>
							<a href="javascript:showAll();">查看完整功能清单>></a>
						</p>
						<div id="contactqq" style="display: none;">
							<p>
								<span>请加QQ: 800161394 开通有优惠</span>
							</p>
						</div>
						<button class="btn btn-info" onclick="contactservice()">联系客服开通/升级</button>

					</div>
				</div>
				<!-- 右边模块 -->
				<div class="payPage-right col-sm-9">
					<div class="payPage-r-box">
						<h3>
							开通服务/充值服务<sub>(联系在线客服，根据在线客服的引导进行开通付费)</sub>
						</h3>
						<div class="payPage-v-option">
							<p>选择版本：</p>
							<form name="balance" action="" method="post" target="_blank">
								<div class="row payPage-pdleft payPage-btn" id="levelDiv">
									<button class="btn btn-default" value="1">站长版</button>
									<button class="btn btn-default" value="2">企业标准版A</button>
									<button class="btn btn-default btn-info" value="3">企业标准版B</button>
									<button class="btn btn-default" value="4">企业标准版C</button>
									<button class="btn btn-default" value="5">企业旗舰版A</button>
									<button class="btn btn-default " value="6">企业旗舰版B</button>
									<button class="btn btn-default" value="7">企业旗舰版C</button>

								</div>
								<p>
									开通时长：<span class="payPage-time" id="month">1</span>月
								</p>
								<!-- 目前等级为V<span>0</span><br>
									升级到等级V<span id = 'levelSpan'>1</span><br> -->
								<div class="slider-date" id="slider-date-1">

									<!--底层-->
									<ul class="slider-bg clearfix">
										<li>1</li>
										<li>2</li>
										<li>3</li>
										<li>4</li>
										<li>5</li>
										<li>6</li>
										<li>7</li>
										<li>8</li>
										<li>9</li>
										<li>10</li>
										<li>11</li>
										<li>1年</li>
									</ul>

									<!--互动层-->
									<div class="slider-bar">
										<ul class="slider-bg clearfix">
											<li>1<span>1个月</span></li>
											<li>2<span>2个月</span></li>
											<li>3<span>3个月</span></li>
											<li>4<span>4个月</span></li>
											<li>5<span>5个月</span></li>
											<li>6<span>6个月</span></li>
											<li>7<span>7个月</span></li>
											<li>8<span>8个月</span></li>
											<li>9<span>9个月</span></li>
											<li>10<span>10</span></li>
											<li>11<span>11</span></li>
											<li>1年<span>1年</span></li>
										</ul>
										<!--滑块按钮-->
										<a href="javascript:;" class="slider-bar-btn"><i></i><i></i></a>
									</div>

								</div>
								<br>
								<p>
									应付金额：<span id='priceSpan' class="payPage-total">2000.00</span>元
								</p>
								<p>付款方式:</p>
								<div class="row payPage-pdleft payPagr-pay">
									<div class="col-xs-3 col-xs-offset-1 select"></div>
									<div class="col-xs-3 col-xs-offset-1"></div>
									<div class="col-xs-3 col-xs-offset-1"></div>
								</div>
								<p>服务合同：</p>
								<div class="row payPage-pdleft payPage-serve">
									<p>是否需要服务合同：</p>
									<div class="checkbox-slider--b-flat payPage-checkbox">
										<label><input type="checkbox" checked="" id="contract"><span>是</span></label>
									</div>
								</div>
								<p>发票：</p>
								<div class="row payPage-pdleft payPage-serve">
									<p>是否需要发票：</p>
									<div class="checkbox-slider--b-flat payPage-checkbox">
										<label><input type="checkbox" checked="checked"
											id="receipt"><span>是</span></label>
									</div>
								</div>
							</form>
							<button class="btn btn-info" onclick="orderPage()">确定购买</button>

						</div>

					</div>
				</div>
			</div>

		</div>
		<!-- /.content -->

		<div class="modal fade" id="ErrorAlert" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div>
							<div style="text-align: center;">
								<span class="lead" id="errorText"></span>
							</div>
						</div>
						<div></div>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
		</div>
	</div>


</body>
<script type="text/javascript" src="${ctx}/js/buy/buyManage.js?v=1.000"></script>
<script src="${ctx}/plugins/ladda/spin.min.js"></script>
<script src="${ctx}/plugins/ladda/ladda.min.js"></script>
<script>
	$(".payPage-btn").on(
			'click',
			'button',
			function(event) {
				event.preventDefault();
				/* Act on the event */
				$(this).removeClass('btn-default').addClass('btn-info')
						.siblings().removeClass('btn-info').addClass(
								'btn-default');
			});
	payModeBg();
	$(".payPagr-pay").on('click', 'div', function(event) {
		$("#errorText").text("目前只支持支付宝付款");
		$("#ErrorAlert").modal("show");
		/* event.preventDefault();
		$(this).index();
		console.log($(this).index());
		$(this).addClass('select').siblings().removeClass('select');
		payModeBg(); */
	});
	$(".payPage-checkbox").on("change", "input", function(event) {
		/* Act on the event */
		if ($(this).attr("checked") === "checked") {
			$(this).siblings().html("否");
			$(this).removeAttr("checked");
		} else {
			$(this).siblings().html("是");
			$(this).attr("checked", "checked");
		}
	});
	function payModeBg() {
		for (var i = 0; i <= $(".payPagr-pay div").length; i++) {
			if ($(".payPagr-pay div").eq(i).hasClass('select')) {
				console.log("select")
				$(".payPagr-pay div").eq(i).css(
						{
							"background" : "url(/image/pay/p" + (i + 1)
									+ ".png) no-repeat",
							"backgroundSize" : "100% auto"
						});
			} else {
				$(".payPagr-pay div").eq(i).css(
						{
							"background" : "url(/image/pay/p" + (i + 1)
									+ "1.png) no-repeat",
							"backgroundSize" : "100% auto"
						});
			}

		}
		;
	}
	if ($(window).width() > 768) {
		if ($(".payPage-right").height() > $(".payPage-left").height()) {
			$(".payPage-content").height($(".payPage-right").height());
		} else {
			$(".payPage-content").height($(".payPage-left").height());
		}
	} else {

	}
</script>
</html>
