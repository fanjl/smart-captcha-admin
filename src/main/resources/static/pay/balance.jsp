<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<head>
<#include "../common/base.ftl">
<link rel="stylesheet" href="${ctx }/plugins/ladda/ladda-themeless.min.css">
<link rel="stylesheet" href="${ctx }/css/timeManage.css">
<style>
body {
	overflow: scroll;
}
</style>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>结算</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">结算</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">结算</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
						<form name="balance" action="payOnline"  target= "_blank">
						目前等级为V<span>${member.level }</span><br>
							升级到等级V<span id = 'levelSpan'>${level }</span><br>
							<input type = "hidden" value = "${level }" name = 'level' id = 'level'>
							<!-- <select   name = "month" id="month">
								<option value="1">1个月</option>
								<option value="2">2个月</option>
								<option value="3">3个月</option>
								
							</select><br> -->
							<div class="slider-date" id="slider-date-1">

								<!--底层-->
								<ul class="slider-bg clearfix">
									<li>1</li>
									<li>2</li>
									<li>3</li>
									<li>4</li>
									<li>5</li>
									<li>6</li>
									<li>7</li>
									<li>8</li>
									<li>9</li>
									<li>10</li>
									<li>11</li>
									<li>1年</li>
								</ul>
							
								<!--互动层-->
								<div class="slider-bar">
									<ul class="slider-bg clearfix">
										<li>1<span>1个月</span></li>
										<li>2<span>2个月</span></li>
										<li>3<span>3个月</span></li>
										<li>4<span>4个月</span></li>
										<li>5<span>5个月</span></li>
										<li>6<span>6个月</span></li>
										<li>7<span>7个月</span></li>
										<li>8<span>8个月</span></li>
										<li>9<span>9个月</span></li>
										<li>10<span>10</span></li>
										<li>11<span>11</span></li>
										<li>1年<span>1年</span></li>
									</ul>
									<!--滑块按钮-->
									<a href="javascript:;" class="slider-bar-btn"><i></i><i></i></a>
								</div>
							
							</div> <br>
							<input type = "hidden" value = "${balance.prices }" name = 'price' id='price'>
							应付<span id = 'priceSpan'>${balance.prices }</span>元
						</form>
						</div>
						<!-- /.box-body -->
						<div class="box-footer btn-group">
							<button class="btn btn-default" onclick="history.go(-1)">返回上一页</button>
						</div>
						<div class="box-footer btn-group">
							<button class="btn btn-default" onclick="goAliPay(this)" value="${level }">去支付</button>
						</div>
					</div>
					<!-- /.box -->
				</section>
				<!-- ./col -->
			</div>
			<!-- /.row -->
			<!-- 模态框（Modal） -->
		</section>
		<!-- /.content -->
	</div>
</body>
<script src="${ctx }/plugins/ladda/spin.min.js"></script>
<script src="${ctx }/plugins/ladda/ladda.min.js"></script>
<script type="text/javascript" src="${ctx }/js/buy/buyManage.js?v=1.000"></script>
<script type="text/javascript" src="${ctx }/js/buy/timeManage.js?v=1.000"></script>
</html>
