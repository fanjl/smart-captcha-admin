<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<head>
<#include "../common/base.ftl">
<link rel="stylesheet" href="${ctx }/plugins/ladda/ladda-themeless.min.css">
<style>
body {
	overflow: scroll;
}
</style>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>付费历史</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">付费历史</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">付费历史</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="account">
								<thead>
									<tr>
										<th>产品</th>
										<th>支付时间</th>
										<th>开始时间</th>
										<th>到期时间</th>
										<th>价格</th>
										<th>状态</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
										<c:forEach var="hp" items="${hplist}">
										<tr>
											<td>
											<a href="javascript:goOrderPageFromHistory(${hp.hid})" >
											<c:choose>
											<c:when test="${hp.product==1}">
												站长版
											</c:when>
											<c:when test="${hp.product==2}">
												企业标准版A
											</c:when>
											<c:when test="${hp.product==3}">
												企业标准版B
											</c:when>
											<c:when test="${hp.product==4}">
												企业标准版C
											</c:when>
											<c:when test="${hp.product==5}">
												企业旗舰版A
											</c:when>
											<c:when test="${hp.product==6}">
												企业旗舰版B
											</c:when>
											<c:when test="${hp.product==7}">
												企业旗舰版C
											</c:when>
										</c:choose>
											</a>
											</td>
											<td><fmt:formatDate pattern="yyyy年MM月dd日  HH:mm:ss"
													value="${hp.payTimeDate}" /></td>
											<td><fmt:formatDate pattern="yyyy年MM月dd日  HH:mm:ss"
													value="${hp.beginTimeDate}" /></td>
											<td><fmt:formatDate pattern="yyyy年MM月dd日  HH:mm:ss"
													value="${hp.endTimeDate}" /></td>
											<td>${hp.money}</td>
											<td>${hp.status==1?'已支付':(hp.status==2?'已过期':'未支付')}</td>
											<td><c:if test="${hp.status==0 }"><input type="hidden" value="${hp.hid}" id="hid_history">
												<button class="btn btn-danger btn-sm" onclick="deleteOrder()">删除</button>
											</c:if></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
						<div class="box-footer btn-group">
							<button class="btn btn-default" onclick="history.go(-1)">返回上一页</button>
							
						</div>
					</div>
					<!-- /.box -->
				</section>
				<!-- ./col -->
			</div>
			<!-- /.row -->
			<!-- 模态框（Modal） -->
		</section>
		<!-- /.content -->
	</div>
</body>
<script src="${ctx }/plugins/ladda/spin.min.js"></script>
<script src="${ctx }/plugins/ladda/ladda.min.js"></script>
<script type="text/javascript" src="${ctx}/js/buy/buyManage.js?v=1.000"></script>
<script type="text/javascript" >
$(function() {
	$('#account').dataTable({
		"bPaginate" : true,
		"bLengthChange" : true,
		"bFilter" : true,
		"bSort" : false,
		"bInfo" : true,
		"bAutoWidth" : false,
		"oLanguage" : {
			"sProcessing" : "正在加载中......",
			"sLengthMenu" : "每页显示 _MENU_ 条记录",
			"sZeroRecords" : "对不起，查询不到相关数据！",
			"sEmptyTable" : "表中无数据存在！",
			"sInfo" : "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录",
			"sInfoFiltered" : "数据表中共为 _MAX_ 条记录",
			"sSearch" : "搜索",
			"oPaginate" : {
				"sFirst" : "首页",
				"sPrevious" : "上一页",
				"sNext" : "下一页",
				"sLast" : "末页"
			}
		}
	});
});

</script>
</html>
