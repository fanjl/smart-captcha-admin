<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<head>
<#include "../common/base.ftl">
<link rel="stylesheet" href="${ctx }/plugins/ladda/ladda-themeless.min.css">
<link rel="stylesheet" href="${ctx }/css/timeManage.css">
<link rel="stylesheet" href="${ctx }/css/titatoggle-dist.css">
</head>
<body>
<div class="wrapper">
				<h3>订单信息</h3>
				<div class="payPage-head row">
					<div class="payPage-user col-sm-6">
							<p>版本：</p>
							<form name="balance" action="/buy/goAliPay" method="post" target="_blank">
								<div class="row payPage-pdleft payPage-btn" id="levelDiv">
									<button class="btn btn-info" value="${historyPay.product }">
									<c:choose>
											<c:when test="${historyPay.product==1}">
												<td>站长版</td>
											</c:when>
											<c:when test="${historyPay.product==2}">
												<td>企业标准版A</td>
											</c:when>
											<c:when test="${historyPay.product==3}">
												<td>企业标准版B</td>
											</c:when>
											<c:when test="${historyPay.product==4}">
												<td>企业标准版C</td>
											</c:when>
											<c:when test="${historyPay.product==5}">
												<td>企业旗舰版A</td>
											</c:when>
											<c:when test="${historyPay.product==6}">
												<td>企业旗舰版B</td>
											</c:when>
											<c:when test="${historyPay.product==7}">
												<td>企业旗舰版C</td>
											</c:when>
										</c:choose>
										</button>
								</div>
								<p>
									开通时长：<span class="payPage-time" id="month">${month}</span>月
								</p>
								<input type="hidden" value="${historyPay.hid }" id="hid">
								<br>
								<!-- 目前等级为V<span>0</span><br>
									升级到等级V<span id = 'levelSpan'>1</span><br> -->
								<p>
									应付金额：<span id='priceSpan' class="payPage-total">${historyPay.money}</span>元
								</p>
								<p>付款方式:</p>
								<div class="row payPage-pdleft payPagr-pay">
									<div class="col-xs-3 col-xs-offset-1 select"></div>
								</div>
								<p>服务合同：</p>
								<div class="row payPage-pdleft payPage-serve">
									<p>是否需要服务合同：</p> <span>${historyPay.contract==1?'是':'否'}</span>
									<input type="hidden" value="${historyPay.contract }"
											id="contract">
								</div>
								<p>发票：</p>
								<div class="row payPage-pdleft payPage-serve">
									<p>是否需要发票：</p><span>${historyPay.receipt==1?'是':'否'}</span>
									<input type="hidden" value="${historyPay.receipt }"
											id="receipt">
								</div>
								<input type="hidden" name="level" value="${historyPay.product }">
								<input type="hidden" name="month" value="${month}">
								<input type="hidden" name="receipt" value="${historyPay.receipt }">
								<input type="hidden" name="contract" value="${historyPay.contract }">
								<input type="hidden" name="price" value="${historyPay.money }">
								<input type="hidden" name="hid" value="${historyPay.hid }">
							</form>
							<button class="btn btn-info"  onclick="goAliPay()">前往支付</button>
					</div>
				</div>

			</div>
		<div class="modal fade" id="ErrorAlert" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div>
							<div style="text-align: center;">
								<span class="lead" id="errorText"></span>
							</div>
						</div>
						<div></div>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
		</div>
	</div>
<script type="text/javascript" src="${ctx}/js/buy/buyManage.js?v=1.000"></script>
<script src="${ctx}/plugins/ladda/spin.min.js"></script>
<script src="${ctx}/plugins/ladda/ladda.min.js"></script>
<script>
	$(".payPage-btn").on('click', 'button', function(event) {
		event.preventDefault();
		/* Act on the event */
		$(this).removeClass('btn-default').addClass('btn-info').siblings().removeClass('btn-info').addClass('btn-default');
	});
	payModeBg();
	$(".payPagr-pay").on('click', 'div', function(event) {
		$("#errorText").text("目前只支持支付宝付款");
		$("#ErrorAlert").modal("show");
		/* event.preventDefault();
		$(this).index();
		console.log($(this).index());
		$(this).addClass('select').siblings().removeClass('select');
		payModeBg(); */
	});
	$(".payPage-checkbox").on("change","input",function(event) {
		/* Act on the event */
		if ($(this).attr("checked")==="checked") {
			$(this).siblings().html("否");
			$(this).removeAttr("checked");
		}else{
			$(this).siblings().html("是");
			$(this).attr("checked","checked");
		}
	});
	function payModeBg(){
		for (var i = 0; i <=$(".payPagr-pay div").length; i++) {
			if($(".payPagr-pay div").eq(i).hasClass('select')){
				console.log("select")
				$(".payPagr-pay div").eq(i).css({
					"background":"url(/image/pay/p"+(i+1)+".png) no-repeat",
					"backgroundSize": "100% auto"
				});
			}else{
				$(".payPagr-pay div").eq(i).css({
					"background":"url(/image/pay/p"+(i+1)+"1.png) no-repeat",
					"backgroundSize": "100% auto"
				});
			}
			
		};
	}
	if($(window).width() > 768){
		if($(".payPage-right").height()>$(".payPage-left").height()){
			$(".payPage-content").height($(".payPage-right").height());
		}else{
			$(".payPage-content").height($(".payPage-left").height());
		}
	}else{
		
	}
	
</script>
</html>
