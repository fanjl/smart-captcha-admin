<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<#include "../common/base.ftl">
<style>
body {
	overflow: scroll;
}
.tablecell{
	vertical-align: middle!important;
}
.captcha_manage_smallpic_div {
    width: 60px;
    height: 60px;
    border: 1px dashed #cdcdcd;
    margin: 3px;
    line-height: 60px;
    text-align: center;
}
.captcha_manage_trans_div {
    width: 60px;
    height: 60px;
    text-align: center;
    line-height: 60px;
    font-size: 20px;
    border: 1px dashed #cdcdcd;
    margin: 3px;
    color: #666;
    font-family: 'Microsoft YaHei';
}
.captcha_manage_tword_div {
    width: 60px;
    height: 60px;
    text-align: center;
    font-size: 14px;
    border: 1px dashed #cdcdcd;
    margin: 3px;
    word-break: break-all;
    white-space: normal;
    overflow: hidden;
    color: #666;
}
</style>
<script type="text/javascript">
	var options = {
		"bPaginate" : true,
		"bLengthChange" : false,
		"bFilter" : false,
		"bSort" : true,
		"bInfo" : true,
		"bAutoWidth" : false,
		"oLanguage" : {
			"sProcessing" : "正在加载中......",
			"sLengthMenu" : "每页显示 _MENU_ 条记录",
			"sZeroRecords" : "对不起，查询不到相关数据！",
			"sEmptyTable" : "表中无数据存在！",
			"sInfo" : "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录",
			"sInfoFiltered" : "数据表中共为 _MAX_ 条记录",
			"sSearch" : "搜索",
			"oPaginate" : {
				"sFirst" : "首页",
				"sPrevious" : "上一页",
				"sNext" : "下一页",
				"sLast" : "末页"
			}
		}
	};
	$(function() {
		$('#custom').dataTable(options);
	});
	function del(aid){
		if(window.confirm("是否删除该图片组")){
			$.ajax({
				type : "post",
				url : "${ctx}/captcha/delAds",
				data : {
					aid : aid
				},
				success : function(data){
					if(data){
						alert("删除成功");
						location.reload();
					}
				}
			});
		}
	}
</script>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>图片管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">图片管理</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">广告图片</h3>
							<!-- tools box -->
							<div class="pull-right box-tools">
								<button class="btn btn-info btn-sm" data-widget='collapse'
									data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i>
								</button>
								<button class="btn btn-info btn-sm" data-widget='remove'
									data-toggle="tooltip" title="Remove">
									<i class="fa fa-times"></i>
								</button>
							</div>
							<!-- /. tools -->
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="custom">
								<thead>
									<tr>
										<th>验证图类型</th>
										<th>验证码底图</th>
										<th>印刷文字</th>
										<th>点击文字</th>
										<th>印刷小图</th>
										<th>小图透明度</th>
										<th>提示文字</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="custom" items="${custom}">
										<tr>
											<td class="tablecell">网站专属</td>
											<td><img width="250px" height="125px" alt="未加载" src="data:image/jpg;base64,${custom.backgroudbase64 }"></td>
											<td class="tablecell">${empty custom.printfont?"-":custom.printfont}</td>
											<td class="tablecell">
												<c:if test="${custom.imgtexttype==0 }">
													${custom.clickfont}
												</c:if>
												<c:if test="${custom.imgtexttype==1 }">
													-
												</c:if>
											</td>
											<td class="tablecell" align="center">
												<c:if test="${custom.imgtexttype==1 }">
													<c:forEach var="spiclist" items="${custom.spicbase64}">
														<div class="captcha_manage_smallpic_div">
															<img alt="图片未加载" src="data:image/jpg;base64,${spiclist }"/>
														</div>
													</c:forEach>
												</c:if>
												<c:if test="${custom.imgtexttype==0 }">
													-
												</c:if>
											</td>
											<td class="tablecell" align="center">
												<c:if test="${custom.imgtexttype==1 }">
													<c:set var="transparentarray" value="${fn:split(custom.transparent, ',')}" />
													<c:forEach var="trans" items="${transparentarray}">
														<div class="captcha_manage_trans_div">${trans}</div>
													</c:forEach>
												</c:if>
												<c:if test="${custom.imgtexttype==0 }">
													-
												</c:if>
											</td>
											<td class="tablecell" align="center">
												<c:if test="${custom.imgtexttype==1 }">
													<c:forEach var="notetextstr" items="${custom.notetextstr}">
														<div class="captcha_manage_tword_div">${empty notetextstr?"-":notetextstr }</div>
													</c:forEach>
												</c:if>
												<c:if test="${custom.imgtexttype==0 }">
													-
												</c:if>
											</td>
											<td>
												<button class="btn btn-danger btn-sm" onclick="del('${custom.aid}')" type="button">删除</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
			</div>
		</section>
	</div>
</body>
</html>