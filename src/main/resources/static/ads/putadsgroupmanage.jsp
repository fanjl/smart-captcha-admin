<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<head>
<#include "../common/base.ftl">
<style>
body {
	overflow: scroll;
}
</style>
<script type="text/javascript">
	function putAdsGroup(gid){
		var wid = $("#currentwebsite").val();
		$.post("putAdsGroup",{gid:gid,wid:wid},function(data){
			if(data){
				alert("投放成功");
			}else{
				alert("投放失败，请重试");
			}
		});
	}
	function cancelAdsGroup(gid){
		var wid = $("#currentwebsite").val();
		$.post("cancelAdsGroup",{gid:gid,wid:wid},function(data){
			if(data){
				alert("取消投放成功");
			}else{
				alert("取消投放失败，请重试");
			}
		});
	}
	function viewAdsGroup(gid){
		document.location.href = "viewAdsGroup?gid="+gid;
	}
</script>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>可投放广告组列表</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">可投放广告组列表</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">可投放广告组列表</h3>
							<strong>投放网站:</strong> <select id="currentwebsite"
								onchange="changewebsite();">
								<c:forEach var="list" items="${website}">
									<option value="${list.wid }">${list.wname }</option>
								</c:forEach>
							</select>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="account">
								<thead>
									<tr>
										<th width="30%">广告组名称</th>
										<th width="30%">投放状态</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="list" items="${adgroups}">
										<tr>
											<td>${list.gname}</td>
											<td>${'未投放'}</td>
											<td>
												<button class="btn btn-primary btn-shadow"
													onclick="viewAdsGroup('${list.gid}')" type="button">查看图片</button>
												<button class="btn btn-primary btn-shadow"
													onclick="putAdsGroup('${list.gid}')" type="button">投放</button>
												<button class="btn btn-danger "
													onclick="cancelAdsGroup('${list.gid}')" type="button">取消投放</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- ./col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
</body>
</html>
