<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ page import="com.touclick.admin.dao.model.CTypeEnum"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<#include "../common/base.ftl">
<link href="${ctx }/plugins/croppic/croppic.css" rel="stylesheet">
<link href="${ctx }/plugins/croppic/main.css?v=1.006" rel="stylesheet">
<link href="${ctx }/css/captcha/captchamanage.css" rel="stylesheet">
<link href="${ctx }/plugins/bootstrap-slider/bootstrap-slider.css"
	rel="stylesheet">
<script src="${ctx }/plugins/croppic/croppic.js"></script>
<script src="${ctx }/plugins/bootstrap-slider/bootstrap-slider.js"></script>
<style type="text/css">
body {
	overflow: scroll;
}

button {
	margin-top: 10px;
}

.pro {
	background-image: url("${ctx}/image/pro.png");
	height: 64px;
	margin: 0 auto;
	width: 548px;
}

.typeandskin_left {
	overflow: hidden;
	width: 200px;
}

.typeandskin_border {
	border-right: 1px solid #8f8f8f;
	margin-right: 3px;
}

.typeandskin_title {
	border-left: 6px solid #0099cc;
	font-size: 15px;
	color: #0099cc;
	font-family: 'Microsoft YaHei';
	padding-left: 4px;
	cursor: default;
}
.typeandskin_title div {
  width: 8px;
  height: 8px;
  border-radius: 4px;
  background: #8f8f8f;
  margin-right: -1px;
}
</style>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>验证码管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">验证码管理</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">验证码管理</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<div class="row">
								<div class="col-md-2" style="text-align: left;">
									<div class="typeandskin_left" style="">
										<div class="typeandskin_border" style="height: 100px;"></div>
										<div class="typeandskin_title">
											<span>根据验证码类型上传广告</span>
											<div
												style="display: inline-block; float: right; margin-top: 5px;"></div>
										</div>
										<div class="typeandskin_border" style="height: 100px;"></div>
									</div>
								</div>
								<div class="col-md-3" style="text-align: center;margin-left: 30px;">
									<div>
										<img alt="图片未加载" src="${ctx }/image/classical_new.png"
											style="height: 200px;">
									</div>
									<div>
										<button class="btn btn-default" data-toggle="modal"
											data-target="#imgtextmodel">自定义图片</button>
										<!-- <button class="btn btn-default" onclick="groupList();">广告组管理</button> -->
										<button class="btn btn-default" onclick="imgText();">图片管理</button>
									</div>
								</div>
								<div class="col-md-3" style="text-align: center;">
									<div>
										<img alt="图片未加载" src="${ctx }/image/new/block.png"
											style="height: 200px;">
									</div>
									<div>
										<button class="btn btn-default">暂无自定义</button>
									</div>
								</div>
								<div class="col-md-3" style="text-align: center;">
									<div>
										<img alt="图片未加载" src="${ctx }/image/rotate/rotate.png"
											style="height: 200px;">
									</div>
									<div>
										<!-- <button class="btn btn-default">暂无自定义</button> -->
										<button class="btn btn-default" data-toggle="modal"
											data-target="#rotateModal">自定义图片</button>
										<button class="btn btn-default" onclick="rotateManage();">图片管理</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
			</div>
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="imgtextmodel" tabindex="-1" role="dialog"
				data-backdrop="false" aria-labelledby="imgtextmodel"
				aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">自定义验证码</h4>
							<h5 class="modal-title">上传到广告组
								<select id="adgroups">
									<c:forEach items="${adgroups }" var="adgroups">
										<option id="${adgroups.gid }">${adgroups.gname }</option>
									</c:forEach>
								</select>
							</h5>
						</div>
						<div class="modal-body">
							<form id="customform">
								<div id="custom-step0">
									<div class="captcha-div0-outer">
										<div class="captcha-div0-inner" onclick="printfont()">
											<span>印刷文字型</span>
										</div>
									</div>
									<div class="captcha-div0-outer" style="margin-top: 10px;">
										<div class="captcha-div0-inner" onclick="printimg()">
											<span>印刷图片型</span>
										</div>
									</div>
									<input type="hidden" id="imgtexttype">
								</div>
								<div id="custom-step1" style="display: none;">
									<div id="step1img" class="pro"
										style="background-position: 0px 0px;"></div>
									<div class="cropHeaderWrapper">
										<div id="croppic"></div>
										<span class="btn" id="cropContainerHeaderButton">请选择文件</span>
									</div>
									<input type="hidden" id="backcroppic">
								</div>
								<div id="custom-step2" style="display: none;">
									<div class="pro" style="background-position: 0px -64px;">
									</div>
									<div>
										<span style="line-height: 26px;">输入印刷文字:</span> <input
											type="text" id="captcha-div2-input"
											style="color: #606060; font-size: 16px; font-family: 'Microsoft YaHei';">
									</div>
									<div style="height: 200px;">
										<span style="line-height: 26px; float: left;">选择要点击的文字:</span>
										<div id="captcha-div2-wordpool" style="float: left;"></div>
									</div>
								</div>
								<div id="custom-step3" style="display: none; width: 100%;">
									<div class="pro" style="background-position: 0px -192px;"></div>
									<div>
										<span class="font">选择需要印刷的小图和提示文字</span> <span
											class="font-note">*请上传尺寸小于60*60像素的图片</span>
									</div>
									<div style="margin-left: auto; margin-top: 5px;">
										<div id="captcha-div4-inputpool">
											<div class="div4-block" style="display: block;">
												<div class="u-file-c u-file-btn" style="">
													<input type="file" id="smallpic1" name="smallpic1" />选择上传文件
												</div>
												<span class="sli font left">透明度:</span> <input id="slider1"
													width="80px" data-slider-id='slider1' type="text"
													data-slider-min="0" data-slider-max="100"
													data-slider-step="1" data-slider-value="100" />
												<script type="text/javascript">
													
												</script>
												<span class="pre font left">预览:</span>
												<div class="div4-img">
													<img />
												</div>
												<span class="txt font left">提醒文字:</span> <input type="text"
													class="txt-input" id="remind1" name="txt-input1" />
											</div>
											<div class="div4-block">
												<div class="u-file-c u-file-btn" style="">
													<input type="file" id="smallpic2" name="smallpic2" />选择上传文件
												</div>
												<span class="sli font left">透明度:</span> <input id="slider2"
													data-slider-id='ex1Slider' type="text" data-slider-min="0"
													data-slider-max="100" data-slider-step="1"
													data-slider-value="100" /> <span class="pre font left">预览:</span>
												<div class="div4-img">
													<img />
												</div>
												<span class="txt font left">提醒文字:</span> <input type="text"
													class="txt-input" id="remind2" name="txt-input2" />
												<div class="sub"></div>
											</div>
											<div class="div4-block">
												<div class="u-file-c u-file-btn" style="">
													<input type="file" id="smallpic3" name="smallpic3" />选择上传文件
												</div>
												<span class="sli font left">透明度:</span> <input id="slider3"
													data-slider-id='ex1Slider' type="text" data-slider-min="0"
													data-slider-max="100" data-slider-step="1"
													data-slider-value="100" /> <span class="pre font left">预览:</span>
												<div class="div4-img">
													<img />
												</div>
												<span class="txt font left">提醒文字:</span> <input type="text"
													class="txt-input" id="remind3" name="txt-input3" />
												<div class="sub"></div>
											</div>
											<div class="div4-block div4-add" style="display: block">
												<div class="hor"></div>
												<div class="ver"></div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer" id="captcha-div-bottom"
							style="display: none;">
							<div tou_id="1" id="beforestep"
								style="width: 70px; float: right; position: absolute; right: 90px;">
								<button class="btn btn-primary">上一步</button>
							</div>
							<div tou_id="2" id="nextstep" style="width: 70px; float: right;">
								<button class="btn btn-primary">下一步</button>
							</div>
							<div tou_id="3" id="over"
								style="width: 70px; float: right; display: none;">
								<button class="btn btn-primary">完成</button>
							</div>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal -->
			</div>
			<div class="modal fade" id="rotateModal" tabindex="-1" role="dialog"
				data-backdrop="false" aria-labelledby="rotateModal"
				aria-hidden="true">
				<div class="modal-dialog" style="width: 600px;">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">旋转自定义图片</h4>
						</div>
						<div class="modal-body" style="height: 400px;">
							<div class="cropHeaderWrapper" style="overflow: hidden;">
								<div id="rotateCroppic"></div>
								<span class="btn" id="rotateCropContainerHeaderButton">请选择文件</span>
							</div>
							<input type="hidden" id="backrotatecroppic">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary"
								onclick="saverotate();">提交</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal -->
			</div>
		</section>
	</div>
</body>
<script type="text/javascript"
	src="${ctx }/js/ads/adsmanage.js?v=1.264"></script>
</html>