<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
	<style>
body {
	overflow: scroll;
}
</style>
<body>
	<#include "../common/base.ftl">
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>历史账单</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">历史账单</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">历史账单</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="earnings">
								<thead>
									<tr>
										<th>时间</th>
										<th>收益</th>
										<th>是否提取</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>时间</th>
										<th>收益</th>
										<th>是否提取</th>
									</tr>
								</tfoot>
								<tbody>
									<c:forEach var="list" items="${list}">
										<tr>
											<td><fmt:formatDate pattern="yyyy年MM月dd日 hh:mm:ss"
													value="${list.time}" /></td>
											<td>${list.cost}</td>
											<td>${list.drawflag==1?'未提取':'已提取'}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
						<div class="box-footer btn-group">
							<button class="btn btn-default" onclick="history.go(-1)">返回上一页</button>
						</div>
					</div>
					<!-- /.box -->
				</section>
			</div>
		</section>
	</div>
<script type="text/javascript">
	$(function(){
		$('#reservation').daterangepicker();
		$('#earnings').dataTable({
			"bPaginate": true,
	          "bLengthChange": false,
	          "bFilter": false,
	          "bSort": true,
	          "bInfo": true,
	          "bAutoWidth": false,
	          "oLanguage": { 
                  "sProcessing": "正在加载中......", 
                  "sLengthMenu": "每页显示 _MENU_ 条记录", 
                  "sZeroRecords": "对不起，查询不到相关数据！", 
                  "sEmptyTable": "表中无数据存在！", 
                  "sInfo": "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录", 
                  "sInfoFiltered": "数据表中共为 _MAX_ 条记录", 
                  "sSearch": "搜索", 
                  "oPaginate":  
                  { 
                      "sFirst": "首页", 
                      "sPrevious": "上一页", 
                      "sNext": "下一页", 
                      "sLast": "末页" 
                  } 
              }
		});
	});
</script>
</body>
</html>