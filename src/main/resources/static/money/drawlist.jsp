<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<#include "../common/base.ftl">
<style>
body {
	overflow: scroll;
}
</style>
<script type="text/javascript">
var verifyflag = false;
var haveAccountFlag = false;
var datepickeroptions = {
	format : 'YYYY-MM-DD',
	locale : {
		applyLabel : '确定',
		cancelLabel : '取消',
		fromLabel : '起始时间',
		toLabel : '结束时间',
		daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
        monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
                '七月', '八月', '九月', '十月', '十一月', '十二月' ],
	}
};
var partDraw=function(){
	if(!verifyflag){
		verify();
		if(verifyflag){
			haveaccount();
			if(haveaccount){
				partDrawMoney();
			}
		}
	}else{
		partDrawMoney();
	}
}
var partDrawMoney = function(){
	var dateRange = $("#reservation").val();
	if(dateRange!=""){
		$.ajax({
			type : "POST",
			url : "${ctx}/draw/countMoney",
			data : {
				dateRange : dateRange
			},
			success : function(data) {
				if(data==""||data=="0.00"){
					alert("所选时间段无可提取金额");
				}else{
					alert("本次提取总计金额:"+data+"元,请等待工作人员审核");
					document.location.href="${ctx}/draw/drawMoney";
				}
			}
		});
	}else{
		alert("请选择时间段");
	}
}
var allDraw = function(){
	if(!verifyflag){
		verify();
		if(verifyflag){
			haveaccount();
			if(haveAccountFlag){
				allDrawMoney();
			}
		}
	}else{
		allDrawMoney();
	}
}
var allDrawMoney = function (){
	if(window.confirm("确定要全部提取吗")){
		$.ajax({
			type : "POST",
			url : "${ctx}/draw/countAllMoney",
			success : function(data) {
				if(data==""||data=="0.00"){
					alert("无可提取金额");
				}else{
					alert("本次提取总计金额:"+data+"元,请等待工作人员审核");
					document.location.href="${ctx}/draw/drawMoney";
				}
			}
		});
	}
}
function verify(){
	$.ajax({
		type : "post",
		url : "${ctx}/website/verifyWebsite",
		async : false,
		success : function(data){
			if(data){
				verifyflag = true;
				return true;
			}else{
				alert("您有网站未通过验证，请在网站管理中对网站进行验证");
				return false;
			}
		}
	});
}
function haveaccount(){
	$.ajax({
		type : "post",
		url : "${ctx}/account/haveAccount",
		async : false,
		success : function(data){
			if(data=="true"){
				haveAccountFlag = true;
				return true;
			}else{
				alert("您目前没有绑定收款账号，请在账户管理中添加账户");
				return false;
			}
		}
	});
}
</script>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>提现</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">提现</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">历史收益</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="earnings">
								<thead>
									<tr>
										<th>时间</th>
										<th>收益</th>
										<th>是否提取</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>时间</th>
										<th>收益</th>
										<th>是否提取</th>
									</tr>
								</tfoot>
								<tbody>
									<c:forEach var="list" items="${list}">
										<tr>
											<td><fmt:formatDate pattern="yyyy年MM月dd日 hh:mm:ss"
													value="${list.time}" /></td>
											<td>${list.cost}</td>
											<td>${list.drawflag==1?'未提取':'已提取'}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
						<div class="box-footer btn-group">
							<button class="btn btn-default" data-toggle="modal"
								data-target="#myModal">部分提取</button>
							<button class="btn btn-default" onclick="allDraw();">全部提取</button>
							<button class="btn btn-default" onclick="history.go(-1)">返回上一页</button>
						</div>
					</div>
					<!-- /.box -->
				</section>
			</div>
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">请选择提取收益的时间段</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>时间段:</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" class="form-control pull-right"
										id="reservation" name="dateRange" />
								</div>
								<!-- /.input group -->
							</div>
							<!-- /.form group -->
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary"
								onclick="partDraw();">提交审核</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal -->
			</div>
		</section>
	</div>
	<script type="text/javascript">
	$(function(){
		$('#reservation').daterangepicker(datepickeroptions);
		$('#earnings').dataTable({
			"bPaginate": true,
	          "bLengthChange": false,
	          "bFilter": false,
	          "bSort": true,
	          "bInfo": true,
	          "bAutoWidth": false,
	          "oLanguage": { 
                  "sProcessing": "正在加载中......", 
                  "sLengthMenu": "每页显示 _MENU_ 条记录", 
                  "sZeroRecords": "对不起，查询不到相关数据！", 
                  "sEmptyTable": "表中无数据存在！", 
                  "sInfo": "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录", 
                  "sInfoFiltered": "数据表中共为 _MAX_ 条记录", 
                  "sSearch": "搜索", 
                  "oPaginate":  
                  { 
                      "sFirst": "首页", 
                      "sPrevious": "上一页", 
                      "sNext": "下一页", 
                      "sLast": "末页" 
                  } 
              }
		});
	});
</script>
</body>
</html>