<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<#include "../common/base.ftl">
<link href="${ctx }/css/buypic.css" type="text/css" rel="stylesheet" />
<style>
body {
	overflow: scroll;
}
</style>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>购买图片</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">购买图片</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="nav-tabs-custom">
				<!-- Tabs within a box -->
				<ul id="activeul" class="nav nav-tabs">
					<c:forEach var="pictureType" items="${pictureType }"
						varStatus="status">
						<c:choose>
							<c:when test="${status.index==0 }">
								<li class="active"><a href="#${pictureType }"
									data-toggle="tab">${pictureType.displayName }</a></li>
							</c:when>
							<c:otherwise>
								<li>
									<a href="#${pictureType }" data-toggle="tab" class="tabclick" pid="${pictureType.id }">${pictureType.displayName }</a>
								</li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</ul>
				<div class="tab-content no-padding" id="buypic">
					<c:forEach var="pictureType" items="${pictureType }"
						varStatus="status">
						<c:choose>
							<c:when test="${status.index==0 }">
								<div class="chart tab-pane active" id="${pictureType }"
									style="position: relative; height: 600px;">
									<c:forEach var="pictureClass" items="${pictureClass }">
										<div class="buypic-banners" style="display: block; background-image: url(getClassPicture?vid=${pictureClass.vid});">
											<div class="buypic-sliders">
												<div class="sliders-left">
													<span style="font-weight: bold; display: block">${pictureClass.name }</span>
													<span style="font-size: 13px;">${pictureClass.count_picture }</span><span
														style="font-size: 12px;">张精美图片</span><br> <span
														style="font-weight: bold">￥</span> <span
														style="margin-left: 4px; font-weight: bold">${pictureClass.unit_price }</span>
													<span style="font-weight: bold:margin-left:2px;">/</span> <span
														style="margin-left: 4px; font-weight: bold">天</span>
												</div>
												<div class="sliders-right" style="width: 105px;"
													onclick="install('${pictureClass.vid}','${pictureClass.name }','${pictureClass.unit_price }','${pictureClass.count_used }');">
													<div></div>
												</div>
											</div>
										</div>
									</c:forEach>
								</div>
							</c:when>
							<c:otherwise>
								<div class="chart tab-pane" id="${pictureType }"
									style="position: relative; height: 600px;"></div>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</div>
			</div>
		</section>
		<div class="modal fade" id="vippicModal" tabindex="-1" role="dialog"
			data-backdrop="false" aria-labelledby="vippicModal"
			aria-hidden="true">
			<div class="modal-dialog" style="width: 860px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title">详情介绍</h4>
					</div>
					<div class="modal-body" style="overflow: hidden; height: 450px;">
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal -->
		</div>
	</div>
</body>
<script type="text/javascript" src="${ctx }/js/buypic1.js?v=1.257"></script>
</html>