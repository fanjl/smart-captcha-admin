if (!(supportCss3('animation-play-state'))) {
	$(".top-wrap").animate({
		"height" : "35px"
	}, 400).html("您的浏览器版本较低，请更换Chrom(谷歌)或者Firefox(火狐)最新版本");
}
$(".top-wrap").click(function(event) {
	$(this).hide('400');
});
function supportCss3(style) {
	var prefix = [ 'webkit', 'Moz', 'ms', 'o' ], i, humpString = [], htmlStyle = document.documentElement.style, _toHumb = function(
			string) {
		return string.replace(/-(\w)/g, function($0, $1) {
			return $1.toUpperCase();
		});
	};

	for (i in prefix)
		humpString.push(_toHumb(prefix[i] + '-' + style));

	humpString.push(_toHumb(style));

	for (i in humpString)
		if (humpString[i] in htmlStyle)
			return true;

	return false;
}