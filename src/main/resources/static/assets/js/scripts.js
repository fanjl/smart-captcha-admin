var next_step = true;

var formData = new FormData();
String.prototype.trim = function () { return this.replace(/(^\s*)|(\s*$)/g, ""); };

jQuery(document)
		.ready(
				function() {
					TouClick.ready(function(){
						var check_obj_tmp = {};
					    var tc = TouClick("captcha-target",{
					        onSuccess : function(check_obj){
					        	$("#regist").css('background','#19b9e7');
					        	check_obj_tmp = check_obj;
					        },
					    });
						TouClick.$('regist').onclick = function(){
							if(!tc.getStatus()){
								//点击验证码
								return;
							}else{
								var parent_fieldset = $("#regist").parents('fieldset');
								next_step = true;
								parent_fieldset.find('input[type="text"]').each(
									function() {
										var _this = $(this);
										if (_this.val() == "") {
											_this
													.addClass('input-error');
											next_step = false;
										} else {
											_this
													.removeClass('input-error');
										}
										switch (_this
												.prop("id")) {
										case "waddress":
											waddressvalidate(_this);
										}
									});
								var wtype = '';
								$('#website_realtype .selected').each(function() {
									if (typeof ($(this).attr('tou_id')) != 'undefined') {
										wtype = wtype + $(this).attr('tou_id')+ ',';
									}
								});
								var reg = /,$/gi;
								wtype = wtype.replace(reg,"");
								if(wtype === ""){
								//	alert("请选择网站类型");
									$("#errorText").text("请选择网站类型");
									$("#ErrorAlert").modal("show");
									return false;
								}
								if (next_step) {
									var l = Ladda.create( document.querySelector( '#regist' ) );
									//verify(l,wtype);
									formData.append("realname", $("#realname").val());
									formData.append("qq", $("#qq").val());
									formData.append("tel",$("#tel").val());
									formData.append("platform",$('input[name="platform"]').val());
									formData.append("wname", $("#wname").val());
									formData.append("role", 1);
									formData.append("wtype", wtype);
									formData.append("waddress", $("#waddress").val());
									formData.append("token", check_obj_tmp.token);
									formData.append("checkAddress", check_obj_tmp.checkAddress);
									formData.append("sid", check_obj_tmp.sid);
									
									$.ajax({
										type : "post",
										url : "member/regist",
										data : formData,
										dataType : "json",
										processData : false,
										contentType : false,
										success : function(data) {
											if (data && data.isok === true) {
												$('#pubkey').val(data.website_key);
												$('#prikey').val(data.private_key);
												$("#registModal").modal("show");
											}else{
												$("#errorText").text("验证失败！请刷新重试");
												$("#ErrorAlert").modal("show");
											}
											
										}
									});
								}
							}
						};
					});
					/*
					 * Fullscreen background
					 */
					$.backstretch("https://captcha-static.touclick.com/admin/regist20160217.jpg");

					$('#top-navbar-1').on('shown.bs.collapse', function() {
						$.backstretch("resize");
					});
					$('#top-navbar-1').on('hidden.bs.collapse', function() {
						$.backstretch("resize");
					});

					/*
					 * Form
					 */
					$('.registration-form fieldset:first-child').fadeIn('slow');

					$(
							'.registration-form input[type="text"], .registration-form input[type="password"], .registration-form textarea')
							.on('focus', function() {
								$(this).removeClass('input-error');
								$(this).siblings("font").remove();
								$(this).siblings("br").remove();
							});

					// next step
					$('.registration-form .btn-next').on('click', function() {
						var parent_fieldset = $(this).parents('fieldset');
						next_step = true;
						parent_fieldset.find('input').each(function() {
							var _this = $(this);
							if (_this.val() == "") {
								_this.addClass('input-error');
								next_step = false;
							} else {
								_this.removeClass('input-error');
							}
							switch (_this.prop("id")) {
							case "email":
								emailvalidate(_this);
								break;
							case "password":
								password(_this);
								break;
							case "password2":
								repeatpassword(_this);
								break;
							case "waddress":
								waddressvalidate(_this);
								break;
							}
						});
						if (next_step) {
							parent_fieldset.fadeOut(400, function() {
								$(this).next().fadeIn();
							});
						}

					});

					// previous step
					$('.registration-form .btn-previous').on('click',function() {
						$(this).parents('fieldset').fadeOut(400,
								function() {
									$(this).prev().fadeIn();
								});
					});

					// submit
					function verify(l,wtype) {
						var waddress = $("#waddress").val();
						if (waddress == "") {
						//	alert("请先填写网站地址");
							$("#errorText").text("请先填写网站地址");
							$("#ErrorAlert").modal("show");
							return false;
						} else {
							l.start();
							setTimeout(function(){
								$.ajax({
									type : "post",
									url : "member/verify",
									async : false,
									data : {
										waddress : waddress
									},
									success : function(data) {
										if (data) {
											//verifyflag = true;
											formData.append("realname", $("#realname").val());
											formData.append("qq", $("#qq").val());
											formData.append("tel",$("#tel").val());
											formData.append("platform",$('input[name="platform"]').val());
											formData.append("wname", $("#wname").val());
											formData.append("role", 1);
											formData.append("wtype", wtype);
											$.ajax({
												type : "post",
												url : "member/regist",
												data : formData,
												dataType : "json",
												processData : false,
												contentType : false,
												success : function(data) {
													if (data && data.isok === true) {
														$('#pubkey').val(data.website_key);
														$('#prikey').val(data.private_key);
														$("#registModal").modal("show");
													}
												}
											});
										} else {
											//verifyflag = false;
											$("#errorText").text("网站验证未通过,请点击\"下载验证所需文件\",并将下载的文件放在网站的根目录下，以便验证使用");
											$("#ErrorAlert").modal("show");
											//alert("网站验证未通过,请点击\"下载验证所需文件\",并将下载的文件放在网站的根目录下，以便验证使用");
										}
										l.stop();
									}
								});
							},500);
						}
					}
				});
					/*$('#regist').on('click',function(e) {
						var parent_fieldset = $(this).parents('fieldset');
						next_step = true;
						parent_fieldset.find('input[type="text"]').each(
							function() {
								var _this = $(this);
								if (_this.val() == "") {
									_this
											.addClass('input-error');
									next_step = false;
								} else {
									_this
											.removeClass('input-error');
								}
								switch (_this
										.prop("id")) {
								case "waddress":
									waddressvalidate(_this);
								}
							});
						var wtype = '';
						$('#website_realtype .selected').each(function() {
							if (typeof ($(this).attr('tou_id')) != 'undefined') {
								wtype = wtype + $(this).attr('tou_id')+ ',';
							}
						});
						var reg = /,$/gi;
						wtype = wtype.replace(reg,"");
						if(wtype === ""){
							alert("请选择网站类型");
							return false;
						}
						if (next_step) {
							var l = Ladda.create( document.querySelector( '#regist' ) );
							verify(l);
							if (verifyflag) {
								formData.append("realname", $("#realname").val());
								formData.append("qq", $("#qq").val());
								formData.append("tel",$("#tel").val());
								formData.append("platform",$('input[name="platform"]').val());
								formData.append("wname", $("#wname").val());
								formData.append("role", 1);
								
								window.TouClick.Start({
									website_key : '1d72ab4f-6dfc-4f11-82c7-a5ea3058a93b',
									position_code : 0,
									onSuccess : function(args,check_obj) {
										formData.append("wtype", wtype);
//										TouClick.Close();
										$.ajax({
											type : "post",
											url : "member/regist",
											data : formData,
											dataType : "json",
											processData : false,
											contentType : false,
											success : function(data) {
												if (data && data.isok === true) {
													$('#pubkey').val(data.website_key);
													$('#prikey').val(data.private_key);
													$("#registModal").modal("show");
												}
											}
										});
									}
								});
							}
						}
					});
				});*/
function emailvalidate(_this) {
	var email = _this.val().trim();
	var reg = /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/i;
	if (reg.test(email) === true) {
		// 发起远程
		$.ajax({
			type : "post",
			url : "member/existEmail",
			data : {
				email : email
			},
			async : false,
			success : function(data) {
				if (!data) {
					// 验证通过
					_this.removeClass('input-error');
					formData.append("email", email);
				} else {
					_this.addClass('input-error');
					_this.siblings("font").remove();
					_this.siblings("br").remove();
					_this.after('<font color="red">邮箱已存在</font>');
					next_step = false;
				}
			}
		});
	} else {
		_this.addClass('input-error');
		_this.siblings("font").remove();
		_this.siblings("br").remove();
		_this.after('<font color="red">邮箱格式错误</font>');
		next_step = false;
	}
}
function password(_this) {
	var password = _this.val();
	var length = password.split('').length;
	if (length < 6 || length >= 20) {
		_this.addClass('input-error');
		_this.siblings("font").remove();
		_this.siblings("br").remove();
		_this.after('<font color="red">密码长度在6到20位</font>');
		next_step = false;
	} else {
		_this.removeClass('input-error');
		formData.append("password", password);
	}
}
function repeatpassword(_this) {
	var password = $('#password').val();
	var password2 = _this.val();
	if (password2 == "") {
		_this.addClass('input-error');
		_this.siblings("font").remove();
		_this.siblings("br").remove();
		_this.after('<font color="red">密码确认为空</font>');
		next_step = false;
	} else if (password2 != password) {
		_this.addClass('input-error');
		_this.siblings("font").remove();
		_this.siblings("br").remove();
		_this.after('<font color="red">两次输入密码不一致</font>');
		next_step = false;
	} else {
		_this.removeClass('input-error');
	}
}
function waddressvalidate(_this) {
	var url = _this.val().trim();
	var reg = "^((http|https)\\://)?([a-zA-Z0-9\\.\\-]+(\\:[a-zA-"   
        + "Z0-9\\.&%\\$\\-]+)*@)?((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{"   
        + "2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}"   
        + "[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|"   
        + "[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-"   
        + "4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0"   
        + "-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.[a-zA-Z]{2,4})(\\:[0-9]+)?(/"   
        + "[^/][a-zA-Z0-9\\.\\,\\?\\'\\\\/\\+&%\\$\\=~_\\-@]*)*$";
	var re = new RegExp(reg);
//	alert(re.test(url));
	if (re.test(url) === true) {
		$.ajax({
			url : "member/existWebSite",
			type : "post",
			async : false,
			data : {
				waddress : url
			},
			success : function(data) {
				if (!data) {
					_this.removeClass('input-error');
					formData.append("waddress", url);
				} else {
					// 已存在
					_this.addClass('input-error');
					_this.siblings("font").remove();
					_this.siblings("br").remove();
					_this.after('<font color="red">网站地址已存在</font><br>');
					next_step = false;
				}
			}
		});
	}else{
		_this.addClass('input-error');
		_this.siblings("font").remove();
		_this.siblings("br").remove();
		_this.after('<font color="red">网站地址格式错误</font><br>');
		next_step = false;
	}
}
function downloadverify() {
	var waddress = $("#waddress").val();
	if (waddress == "") {
		//alert("请先填写网站地址");
		$("#errorText").text("请先填写网站地址");
		$("#ErrorAlert").modal("show");
	} else {
		window.open("member/downloadVerify?waddress=" + waddress);
	}
}

$('#registModal').on('hide.bs.modal', function() {
	document.location.href = "login.html";
});
function login() {
	document.location.href = "login.html";
	/*var email = $("#email").val();
	var pwd = $("#password").val();
	if (email.length == 0) {
		//alert("\"用户名\"不能为空,请输入!");
		$("#errorText").text("\"用户名\"不能为空,请输入!");
		$("#ErrorAlert").modal("show");
		document.getElementById("email").value = "";
		document.getElementById("email").focus();
		return false;
	}
	if (pwd.length == 0) {
		document.getElementById("password").value = "";
//		alert("\"密码\"不能为空,请输入!");
		$("#errorText").text("\"密码\"不能为空,请输入!");
		$("#ErrorAlert").modal("show");
		document.getElementById("password").focus();
		return false;
	}
	$.post("member/login", {
		email : email,
		password : pwd
	}, function(returnvalue) {
		switch (returnvalue) {
		case 1:
			document.location.href = "member/index";
			break;
		case 2:
		//	alert("\"用户名\"或\"密码\"错误，请重新输入!");
			$("#errorText").text("\"用户名\"或\"密码\"错误，请重新输入!");
			$("#ErrorAlert").modal("show");
			// alert("\"用户名\"不存在,请重新输入!");
			document.getElementById("password").value = "";
			document.getElementById("password").focus();
			break;
		default:
			//alert("服务器无响应，请稍候重试！");
			$("#errorText").text("服务器无响应，请稍候重试！");
			$("#ErrorAlert").modal("show");
		}
	});*/
}