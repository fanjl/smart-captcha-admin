/// <reference path="register.js" />
/// <reference path="website_type_data.js" />
/// <reference path="../script/jquery-1.8.0.min.js" />

$(document).ready(function () {
    // 申明一个变量存储 列表选项############
    var website_realtype = $('#website_realtype');
    var selectdom = document.createElement('div');
    selectdom.id = "selectlist";
    selectdom.style.display = "none";
    var $seldom = $(selectdom);
    
    var category;
    // 遍历   website_type_data数据 
    for (var i = 0; i < website_type_data.length; i++) {
        var genv = website_type_data[i];
        
        $seldom.append("<div class='selectlist' tou_id='"+genv.id+"'><span>" + genv.name + "</span></div>")
    } 
    // 声明变量存储 二级菜单列表。
    var selectdom2 = document.createElement('div');
    selectdom2.className = "selectlist2";
    var $seldom2 = $(selectdom2);
    $seldom.append(selectdom2);

    // 更新二级菜单列表内容
    var updatesel2 = function (category) {
        $seldom2.children().remove();
        if (category.length > 16) {
            $seldom2.addClass('selectlist2_large');
            $seldom2.css('top','0px')
        } else {
            var parid = parseInt(oldactive.attr('tou_id'));
            var top2 = (parid - parseInt( category.length )/ 2) * 30;
            $seldom2.css('top', top2 + 'px');
            $seldom2.removeClass('selectlist2_large');
        }
        for (var j = 0; j < category.length; j++) {
            var cat = category[j];
            $seldom2.append("<div class='selectlist' tou_id='" + cat.id + "'><span>" + cat.name + "</span></div>");
        }
    }
    $('body').append($seldom);

    // var offset = website_realtype.offset();
    // var left = offset.left;
    // var top = offset.top - $seldom.height();
    // var width = website_realtype.outerWidth() - 2;
    // $seldom.css({ left: left + 'px', top: top + 'px' });

    var oldactive;
    $('#selectlist > .selectlist').hover(function () {
        var id = parseInt($(this).attr('tou_id')) - 1;
        updatesel2(website_type_data[id].categorys);
        if (typeof (oldactive) !== 'undefined') {
            oldactive.removeClass("selectlist_active");
        }
        $(this).addClass('selectlist_active');
        oldactive = $(this);
    }, function () {

    });
    // 提醒切换
    function note_to_ok(item_input){
        var item_note = $(item_input).parents('.item').children('.item-note');
        item_note.children().css({ 'display': 'none' });
        item_note.find('.other-note-yes').css({ 'display': 'block' });
    }
    function note_to_false(item_input, id){
        var item_note = $(item_input).parents('.item').children('.item-note');
        item_note.children().css({ 'display': 'none' });
        item_note.find('span.other-note-not').css({ 'display': 'inline' });
    }
    var updateParWdith = function () {
        var width1 = 0;
        var width2 = 0;
        window.website_types = [];
        var lenght = $('.selected').each(function (index, value) {
            var $_value = $(value);
            if (index < 2) {
                width1 += $_value.outerWidth();
            } else if (index < 4) {
                width2 += $_value.outerWidth();
            }
            window.website_types.push($_value.attr('tou_id'));
        }).length;
        if (lenght === 1) {
            note_to_false(website_realtype[0]);
        } else {
            note_to_ok(website_realtype[0]);
        }
        // var width = width1 > width2 ? width1 : width2;
        // width += 20;
        // width = width < 200 ? 200 : width;
        // website_realtype.css('width', width + 'px');
    }
    window.website_types = [];
    var selected_add = $('.item .selected_add');
    $('.selectlist2').on('click',".selectlist", function () {
        var that = $(this);
        var id = that.attr('tou_id');
        for (var i = 0; i < window.website_types.length; i++) {
            if (id === window.website_types[i]) {
                return;
            }
        }
        var text = oldactive.children().text() + ',' + that.children().text();
        var template = '<div class="selected"  tou_id="' + id + '"><span style="margin-left:7px;">' + text + '</span><div class="selected_close" title="点击删除"></div></div>';
        $seldom.css('display', 'none');
        $(template).insertBefore(selected_add);
        updateParWdith();
    });
    // $(".selected_close").click(function(event) {
    //     alert(1)
    // });
    $('.item').on("click",".selected_close",function () {
        var that = $(this).parent('.selected');
        that.remove();
        updateParWdith();
    });
    $('.item').on("click",".selected_add",function () {
        $seldom.css('display', 'block');
    })
});