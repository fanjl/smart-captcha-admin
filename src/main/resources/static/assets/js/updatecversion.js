var next_step = true;
var verifyflag = false;
var formData = new FormData();
String.prototype.trim = function () { return this.replace(/(^\s*)|(\s*$)/g, ""); };
jQuery(document)
		.ready(
				function() {

					/*
					 * Fullscreen background
					 */
					$.backstretch("https://captcha-static.touclick.com/admin/regist20160217.jpg");

					$('#top-navbar-1').on('shown.bs.collapse', function() {
						$.backstretch("resize");
					});
					$('#top-navbar-1').on('hidden.bs.collapse', function() {
						$.backstretch("resize");
					});

					/*
					 * Form
					 */
					$('.registration-form fieldset:first-child').fadeIn('slow');

					$(
							'.registration-form input[type="text"], .registration-form input[type="password"]')
							.on('focus', function() {
								$(this).removeClass('input-error');
								$(this).siblings("font").remove();
								$(this).siblings("br").remove();
							});


					// submit
					$('#submit').on('click',function(e) {
						var l = Ladda.create( document.querySelector( '#submit' ) );
						l.start();
						setTimeout(function(){
							$.ajax({
								type : "post",
								url : "website/updateCversionByMember",
								data : {
									email:$("#email").val(),
									password:$("#password").val()
								},
								success : function(data) {
									l.stop();
									if (data) {
										alert("升级成功");
										document.location.href="login.html";
									}else{
										alert("升级失败，请重试");
									}
								}
							});
						},500);
					});
				});
