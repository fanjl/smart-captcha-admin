var next_step = true;
var verifyflag = false;
var formData = new FormData();
String.prototype.trim = function () { return this.replace(/(^\s*)|(\s*$)/g, ""); };
jQuery(document)
		.ready(
				function() {

					/*
					 * Fullscreen background
					 */
					$.backstretch("https://captcha-static.touclick.com/admin/regist20160217.jpg");

					$('#top-navbar-1').on('shown.bs.collapse', function() {
						$.backstretch("resize");
					});
					$('#top-navbar-1').on('hidden.bs.collapse', function() {
						$.backstretch("resize");
					});

					/*
					 * Form
					 */
					$('.registration-form fieldset:first-child').fadeIn('slow');

					$(
							'.registration-form input[type="text"], .registration-form input[type="password"], .registration-form textarea')
							.on('focus', function() {
								$(this).removeClass('input-error');
								$(this).siblings("font").remove();
								$(this).siblings("br").remove();
							});

					// next step
					$('.registration-form .btn-next').on('click', function() {
						var parent_fieldset = $(this).parents('fieldset');
						next_step = true;
						parent_fieldset.find('input').each(function() {
							var _this = $(this);
							if (_this.val() == "") {
								_this.addClass('input-error');
								next_step = false;
							} else {
								_this.removeClass('input-error');
							}
						});
						if (next_step) {
							var l = Ladda.create( document.querySelector( '.ladda-button' ) );
							verify(l,parent_fieldset);
						}

					});

					// previous step
					$('.registration-form .btn-previous').on('click',function() {
						$(this).parents('fieldset').fadeOut(400,
								function() {
									$(this).prev().fadeIn();
								});
					});
					//touclick validate
					TouClick.ready(function(){
						var check_obj_tmp = {};
					    var tc = TouClick("captcha-target",{
					    	readyCheck: function(){
					    		var val = TouClick.$('email').value;
					    		var pwd = TouClick.$('password').value;
					    		var pwd2 = TouClick.$('password2').value;
					    		if(val == ''||pwd == '' || pwd2 == ''){
					    			return {status:false};
					    		}
					    		return {status:true,ckCode:val};
					    	},
					        onSuccess : function(check_obj){
					        	$("#submit").css('background','#19b9e7');
					        	check_obj_tmp = check_obj;
					        },
					    });
					    
						TouClick.$('submit').onclick = function(){
							if(isEmptyObject(check_obj_tmp)){
								alert("请先点击进行验证");
								return;
							}
							var parent_fieldset = $(this).parents('fieldset');
							next_step = true;
							txtvalidate(parent_fieldset,next_step);
							pwdvalidate(parent_fieldset,next_step);
							if (next_step) {
					                    	$.ajax({
					                    		url:"twice/twiceVerify",
					                    		type:"post",
					                    		data:{
					                    			token:check_obj_tmp.token,
					                    			checkAddress:check_obj_tmp.checkAddress,
					                    			sid:check_obj_tmp.sid
					                    		},
					                    		success:function(data){
					                    			if(data){
					                    				var l = Ladda.create( document.querySelector( '#submit' ) );
														l.start();
														setTimeout(function(){
															$.ajax({
																type : "post",
																url : "member/resetPassEmailByWaddress",
																data : {
																	email:$("#email").val(),
																	password:$("#password").val(),
																	waddress:$("#waddress").val()
																},
																success : function(data) {
																	l.stop();
																	if (data) {
																		alert("设置成功");
																		document.location.href="login.html";
																	}else{
																		alert("设置失败，请重试");
																	}
																}
															});
														},500);
					                    			}
					                    		}
						        });
							}
						};
					});
				});
function txtvalidate(txt,next_step){
	txt.find('input[type="text"]').each(
			function() {
				var _this = $(this);
				if (_this.val() == "") {
					_this
							.addClass('input-error');
					next_step = false;
				} else {
					_this
							.removeClass('input-error');
				}
				switch (_this.prop("id")) {
				case "email":
					emailvalidate(_this);
					break;
				case "password":
					password(_this);
					break;
				case "password2":
					repeatpassword(_this);
					break;
				}
			});
}
function pwdvalidate(txt,next_step){
	txt.find('input[type="password"]').each(
			function() {
				var _this = $(this);
				if (_this.val() == "") {
					_this
							.addClass('input-error');
					next_step = false;
				} else {
					_this
							.removeClass('input-error');
				}
				switch (_this.prop("id")) {
				case "email":
					emailvalidate(_this);
					break;
				case "password":
					password(_this);
					break;
				case "password2":
					repeatpassword(_this);
					break;
				}
			});
}
function emailvalidate(_this) {
	var email = _this.val().trim();
	var reg = /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/i;
	if (reg.test(email) === true) {
		// 发起远程
		$.ajax({
			type : "post",
			url : "member/existEmailByAddress",
			data : {
				email : email,
				waddress : $("#waddress").val()
			},
			async : false,
			success : function(data) {
				if (!data) {
					// 验证通过
					_this.removeClass('input-error');
				} else {
					_this.addClass('input-error');
					_this.siblings("font").remove();
					_this.siblings("br").remove();
					_this.after('<font color="red">邮箱已存在</font>');
					next_step = false;
				}
			}
		});
	} else {
		_this.addClass('input-error');
		_this.siblings("font").remove();
		_this.siblings("br").remove();
		_this.after('<font color="red">邮箱格式错误</font>');
		next_step = false;
	}
}
function password(_this) {
	var password = _this.val();
	var length = password.split('').length;
	if (length < 6 || length >= 20) {
		_this.addClass('input-error');
		_this.siblings("font").remove();
		_this.siblings("br").remove();
		_this.after('<font color="red">密码长度在6到20位</font>');
		next_step = false;
	} else {
		_this.removeClass('input-error');
		formData.append("password", password);
	}
}
function repeatpassword(_this) {
	var password = $('#password').val();
	var password2 = _this.val();
	if (password2 == "") {
		_this.addClass('input-error');
		_this.siblings("font").remove();
		_this.siblings("br").remove();
		_this.after('<font color="red">密码确认为空</font>');
		next_step = false;
	} else if (password2 != password) {
		_this.addClass('input-error');
		_this.siblings("font").remove();
		_this.siblings("br").remove();
		_this.after('<font color="red">两次输入密码不一致</font>');
		next_step = false;
	} else {
		_this.removeClass('input-error');
	}
}
function downloadverify() {
	var waddress = $("#waddress").val();
	if (waddress == "") {
		alert("请先填写网站地址");
	} else {
		window.open("member/downloadVerify?waddress=" + waddress);
	}
}
function isEmptyObject(e) {  
    var t;  
    for (t in e)  
        return !1;  
    return !0  
}  
function verify(l,parent_fieldset) {
	var waddress = $("#waddress").val();
	if (waddress == "") {
		alert("请先填写网站地址");
		return false;
	} else {
		l.start();
		setTimeout(function(){
			$.ajax({
				type : "post",
				url : "member/verify",
				async : false,
				data : {
					waddress : waddress
				},
				success : function(data) {
					if (data) {
						$.ajax({
							url : "member/getEmailByWaddress",
							type : "post",
							data : {
								waddress:$("#waddress").val()
							},
							async:false,
							success:function(data){
								if(data=="false"){
									alert("该网站未注册用户");
									verifyflag = false;
								}else{
									$("#currentEmail").after(data);
									verifyflag = true;
								}
							}
						});
						if(verifyflag){
							parent_fieldset.fadeOut(400, function() {
								$(this).next().fadeIn();
							});
						}
					} else {
						verifyflag = false;
						alert("网站验证未通过,请点击\"下载验证所需文件\",并将下载的文件放在网站的根目录下，以便验证使用");
					}
					l.stop();
				}
			});
		},500);
	}
}
