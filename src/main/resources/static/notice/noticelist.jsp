<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<head>
<#include "../common/base.ftl">
<style>
body {
	overflow: scroll;
}
</style>
<script type="text/javascript">
	function view(title,content) {
		$("#title").val(title);
		$("#content").val(content);
		$("#saveBtn").css("display","none");
		$('#noticeModal').modal('show');
	}
	function addNotice(){
		$("#title").val("");
		$("#content").val("");
		$("#saveBtn").css("display","block");
		$('#noticeModal').modal('show');
	}
	function save(){
		var title = $("#title").val();
		var content = $("#content").val();
		$.ajax({
			type : "post",
			url : "save",
			async : false,
			data : {
				title : title,
				content : content
			},
			success : function(msg){
				if(msg){
					alert("新增成功");
					location.reload();
				}else{
					alert("新增失败，情重试");
				}
			}
		});
	}
	function del(nid){
		if(window.confirm("确认删除该公告吗？")){
			$.ajax({
				type : "post",
				url : "del",
				async : false,
				data : {
					nid : nid
				},
				success : function(msg){
					if(msg){
						alert("删除成功");
						location.reload();
					}else{
						alert("删除失败");
					}
				}
			});
		}
	}
</script>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>公告列表</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">公告列表</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">公告列表</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="account">
								<thead>
									<tr>
										<th>公告标题</th>
										<th>公告内容</th>
										<th>发布时间</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="list" items="${noticeList}">
										<tr>
											<td>
												<c:if test="${fn:length(list.title)>'13'}">  
									            	${fn:substring(list.title,0,13)}...  
									            </c:if> 
									            <c:if test="${fn:length(list.title)<='13'}">  
									                ${list.title}
									            </c:if>
									        </td>
											<td>
												<c:if test="${fn:length(list.content)>'20'}">  
									            	${fn:substring(list.content,0,20)}...  
									            </c:if> 
									            <c:if test="${fn:length(list.content)<='20'}">  
									                ${list.content}
									            </c:if>
									        </td>
											<td><fmt:formatDate value="${list.pubtime}" type="date" /></td>
											<%-- <td>${fn:substring(list.token,0,8)}******${fn:substring(list.token,24,36)} --%>
											<td>
												<button class="btn btn-primary btn-sm" onclick="view('${list.title}','${list.content }')" type="button">查看</button>
												<c:if test="${member.role==3 }">
													<button type="button" class="btn btn-danger btn-sm	" onclick="del('${list.nid}');">删除</button>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<c:if test="${member.role==3}">
							<div class="box-footer btn-group">
								<button class="btn btn-default" onclick="addNotice();" >新增</button>
						</c:if>
						</div>
					</div>
					<!-- /.box -->
				</section>
				<!-- ./col -->
			</div>
			<!-- /.row -->
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="noticeModal" tabindex="-1" role="dialog"
				data-backdrop="false" aria-labelledby="noticeModal"
				aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" style="height: 400px">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">公告</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>公告标题:</label> <input class="form-control pull-right"
									id="title" />
							</div>
							<div class="form-group">
								<label>公告内容:</label> 
								<textarea class="form-control pull-right" id="content" row="10" style="height: 200px" placeholder="公告内容"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" style="float:right;">关闭</button>	
							<button type="button" class="btn btn-primary" onclick="save();" id="saveBtn" style="float:right;">提交</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal -->
			</div>
		</section>
		<!-- /.content -->
	</div>
</body>
</html>
