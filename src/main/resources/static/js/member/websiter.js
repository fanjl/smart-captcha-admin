$('#day').click(function() {
	var wid = $("#website").val();
	$.ajax({
		type : "post",
		url : "../datacenter/changeDateType",
		dataType : "json",
		data : {
			wid : wid,
			datetype : "day"
		},
		success : function(data) {
			setdata(data);
			setoptions('%Y-%m-%d %H时', 5, 'today-chart');
			monthchart = new Highcharts.Chart(options);
		}
	});
});
$('#month').click(function() {
	var wid = $("#website").val();
	$.ajax({
		type : "post",
		url : "../datacenter/changeDateType",
		dataType : "json",
		data : {
			wid : wid,
			datetype : "month"
		},
		success : function(data) {
			setdata(data);
			setoptions('%Y-%m-%d', 5, 'month-chart');
			monthchart = new Highcharts.Chart(options);
		}
	});
});
$('#year').click(function() {
	var wid = $("#website").val();
	$.ajax({
		type : "post",
		url : "../datacenter/changeDateType",
		dataType : "json",
		data : {
			wid : wid,
			datetype : "year"
		},
		success : function(data) {
			setdata(data);
			setoptions('%Y-%m', 1, 'year-chart');
			yearchart = new Highcharts.Chart(options);
		}
	});
});
function setoptions(format, tickInter, divid) {
	options = {
		chart : {
			renderTo : divid, // 图表放置的容器，DIV
			defaultSeriesType : 'spline', // 图表类型为曲线图
			zoomType : 'x'
		},
		title : {
			text : '验证数据' // 图表标题
		},
		xAxis : {
			type : 'datetime',
			categories : time_arr,
			labels : {
				formatter : function() {
					return Highcharts.dateFormat(format, this.value);
				}
			},
			tickInterval : tickInter
		},
		yAxis : {
			title : {
				text : '数量'
			},
			min : 0
		},
		tooltip : {// 当鼠标悬置数据点时的提示框
			shared : true,
			formatter : function() { // 格式化提示信息
				if (typeof (this.points) == 'undefined') {
					return '网页异常，请刷新重试';
				} else {
					return '时间:'
							+ Highcharts.dateFormat(format, this.points[0].x)
							+ '<br/>请求量:' + this.points[0].y + '<br/>验证正确量:'
							+ this.points[1].y + '<br/>验证错误量:'
							+ this.points[2].y + '<br/>验证完成量:'
							+ this.points[3].y + '<br/>广告跳转量:'
							+ this.points[4].y;
				}
			}
		},
		legend : {
			enabled : true
		// 设置图例不可见
		},
		exporting : {
			enabled : false
		// 设置导出按钮不可用
		},
		credits : {
			enabled : false
		},
		series : [ {
			name : '请求量',
			data : g_arr
		}, {
			name : '验证正确量',
			data : vy_arr
		}, {
			name : '验证错误量',
			data : vn_arr
		}, {
			name : '验证完成量',
			data : sv_arr
		}, {
			name : '广告跳转量',
			data : ad_arr
		} ]
	}
}
function setdata(jsonarray) {
	time_arr = [];
	g_arr = [];
	vy_arr = [];
	vn_arr = [];
	sv_arr = [];
	ad_arr = [];
	for (var i = 0; i < jsonarray.length; i++) {
		var r = jsonarray[i];
		time_arr.push(r.date);
		g_arr.push(r.g);
		vy_arr.push(r.vy);
		vn_arr.push(r.vn);
		sv_arr.push(r.sv);
		ad_arr.push(r.ad);
	}
}
function changewebsite() {
	var wid = $("#website").val();
	$.ajax({
		type : "post",
		url : "../datacenter/getCurrentDateData",
		dataType : "json",
		data : {
			wid : wid
		},
		success : function(data) {
			daytable.fnClearTable();
			monthtable.fnClearTable();
			yeartable.fnClearTable();
			if (data.day.length > 0) {
				daytable.fnAddData(data.day);
				daytable.fnDraw();
				setdata(data.day);
			} else {
				setdata("");
			}
			if (data.month.length > 0) {
				monthtable.fnAddData(data.month);
				monthtable.fnDraw();
				setdata(data.month);
			} else {
				setdata("");
			}
			if (data.year.length > 0) {
				yeartable.fnAddData(data.year);
				yeartable.fnDraw();
				setdata(data.year);
			} else {
				setdata("");
			}
			setoptions('%Y-%m-%d %H时', 5, 'today-chart');
			daychart = new Highcharts.Chart(options);
			setoptions('%Y-%m-%d', 5, 'month-chart');
			monthchart = new Highcharts.Chart(options);
			setoptions('%Y-%m', 1, 'year-chart');
			yearchart = new Highcharts.Chart(options);
		}
	});
}
function settableoptions(data, format) {
	options = {
		"dom" : 'T<"clear">lfrtip',
		"tableTools" : {
			"sSwfPath" : "../plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
		},
		"data" : data,
		"columns" : [ {
			"data" : "date"
		}, {
			"data" : "g"
		}, {
			"data" : "vy"
		}, {
			"data" : "vn"
		}, {
			"data" : "sv"
		}, {
			"data" : "ad"
		}, {
			"data" : "adr"
		} ],
		"columnDefs" : [ {
			"render" : function(data, type, row) {
				return Highcharts.dateFormat(format, data);
			},
			"targets" : 0
		} ],
		"bPaginate" : false,
		"bLengthChange" : false,
		"bFilter" : false,
		"bSort" : false,
		"bInfo" : true,
		"bAutoWidth" : false,
		"oLanguage" : {
			"sProcessing" : "正在加载中......",
			"sLengthMenu" : "每页显示 _MENU_ 条记录",
			"sZeroRecords" : "对不起，查询不到相关数据！",
			"sEmptyTable" : "表中无数据存在！",
			"sInfo" : "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录",
			"sInfoFiltered" : "数据表中共为 _MAX_ 条记录",
			"sSearch" : "搜索",
			"oPaginate" : {
				"sFirst" : "首页",
				"sPrevious" : "上一页",
				"sNext" : "下一页",
				"sLast" : "末页"
			}
		}
	}
}
