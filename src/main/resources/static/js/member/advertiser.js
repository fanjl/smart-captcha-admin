var jsonarray = '';
	var time_arr = [];
	var vy_arr = [];
	var vyn_arr = [];
	var g_arr = [];
	var ad_arr = [];
	$(function() {
		Highcharts.setOptions({
			global : {
				useUTC : false
			}
		});
		$.ajax({
			url : "getAreaData",
			async : false,
			data : {
				'datetype' : 'day'
			},
			success : function(data) {
				jsonarray = eval("(" + data + ")");
				for (var i = 0; i < jsonarray.length; i++) {
					var r = jsonarray[i];
					time_arr.push(r.time);
					vy_arr.push(r.VY);
					vyn_arr.push(r.vyn);
					g_arr.push(r.G);
					ad_arr.push(r.AD);
				}
			}
		});
		var daychart;
		daychart = new Highcharts.Chart({
			chart : {
				renderTo : 'today-chart', //图表放置的容器，DIV 
				defaultSeriesType : 'spline', //图表类型为曲线图 
			},
			title : {
				text : '广告走势图' //图表标题 
			},
			xAxis : {
				type : 'datetime',
				categories : time_arr,
				dateTimeLabelFormats : {
					hour : '%H:%M'
				}
			},
			yAxis : {
				title : {
					text : '数量'
				},
				min : 0
			},
			tooltip : {//当鼠标悬置数据点时的提示框 
				shared : true,
				formatter : function() { //格式化提示信息 
					return '广告互动完成量:' + this.points[0].y + '<br/>广告互动量:'
							+ this.points[1].y + '<br/>广告展示量:'
							+ this.points[2].y + '<br/>广告跳转量:'
							+ this.points[3].y;
				}
			},
			legend : {
				enabled : false
			//设置图例不可见 
			},
			exporting : {
				enabled : false
			//设置导出按钮不可用 
			},
			credits : {
				text : 'Touclick.com', //设置LOGO区文字 
				url : 'http://www.touclick.com' //设置LOGO链接地址 
			},
			series : [ {
				name : 'VY',
				data : vy_arr
			}, {
				name : 'vyn',
				data : vyn_arr
			}, {
				name : 'G',
				data : g_arr
			}, {
				name : 'AD',
				data : ad_arr
			} ]
		});
	});
	$('#month').one('click',function(){
		jsonarray = '';
		time_arr = [];
		vy_arr = [];
		vyn_arr = [];
		g_arr = [];
		ad_arr = [];
		$.ajax({
			url : "getAreaData",
			async : false,
			data : {
				'datetype' : 'month'
			},
			success : function(data) {
				jsonarray = eval("(" + data + ")");
				for (var i = 0; i < jsonarray.length; i++) {
					var r = jsonarray[i];
					time_arr.push(r.time);
					vy_arr.push(r.VY);
					vyn_arr.push(r.vyn);
					g_arr.push(r.G);
					ad_arr.push(r.AD);
				}
			}
		});
		var monthchart;
		monthchart = new Highcharts.Chart({
			chart : {
				renderTo : 'month-chart', //图表放置的容器，DIV 
				defaultSeriesType : 'spline', //图表类型为曲线图 
			},
			title : {
				text : '广告走势图' //图表标题 
			},
			xAxis : {
				type : 'datetime',
				categories : time_arr,
				dateTimeLabelFormats : {
					hour : '%H:%M'
				}
			},
			yAxis : {
				title : {
					text : 'number'
				},
				min : 0
			},
			tooltip : {//当鼠标悬置数据点时的提示框 
				shared : true,
				formatter : function() { //格式化提示信息 
					return '广告互动完成量:' + this.points[0].y + '<br/>广告互动量:'
							+ this.points[1].y + '<br/>广告展示量:'
							+ this.points[2].y + '<br/>广告跳转量:'
							+ this.points[3].y;
				}
			},
			legend : {
				enabled : false
			//设置图例不可见 
			},
			exporting : {
				enabled : false
			//设置导出按钮不可用 
			},
			credits : {
				text : 'Touclick.com', //设置LOGO区文字 
				url : 'http://www.touclick.com' //设置LOGO链接地址 
			},
			series : [ {
				name : 'VY',
				data : vy_arr
			}, {
				name : 'vyn',
				data : vyn_arr
			}, {
				name : 'G',
				data : g_arr
			}, {
				name : 'AD',
				data : ad_arr
			} ]
		});
	});
	$('#year').one('click',function(){
		jsonarray = '';
		time_arr = [];
		vy_arr = [];
		vyn_arr = [];
		g_arr = [];
		ad_arr = [];
		$.ajax({
			url : "getAreaData",
			async : false,
			data : {
				'datetype' : 'year'
			},
			success : function(data) {
				jsonarray = eval("(" + data + ")");
				for (var i = 0; i < jsonarray.length; i++) {
					var r = jsonarray[i];
					time_arr.push(r.time);
					vy_arr.push(r.VY);
					vyn_arr.push(r.vyn);
					g_arr.push(r.G);
					ad_arr.push(r.AD);
				}
			}
		});
		var yearchart;
		yearchart = new Highcharts.Chart({
			chart : {
				renderTo : 'year-chart', //图表放置的容器，DIV 
				defaultSeriesType : 'spline', //图表类型为曲线图 
			},
			title : {
				text : '广告走势图' //图表标题 
			},
			xAxis : {
				type : 'datetime',
				categories : time_arr,
				dateTimeLabelFormats : {
					hour : '%H:%M'
				}
			},
			yAxis : {
				title : {
					text : 'number'
				},
				min : 0
			},
			tooltip : {//当鼠标悬置数据点时的提示框 
				shared : true,
				formatter : function() { //格式化提示信息 
					return '广告互动完成量:' + this.points[0].y + '<br/>广告互动量:'
							+ this.points[1].y + '<br/>广告展示量:'
							+ this.points[2].y + '<br/>广告跳转量:'
							+ this.points[3].y;
				}
			},
			legend : {
				enabled : false
			//设置图例不可见 
			},
			exporting : {
				enabled : false
			//设置导出按钮不可用 
			},
			credits : {
				text : 'Touclick.com', //设置LOGO区文字 
				url : 'http://www.touclick.com' //设置LOGO链接地址 
			},
			series : [ {
				name : 'VY',
				data : vy_arr
			}, {
				name : 'vyn',
				data : vyn_arr
			}, {
				name : 'G',
				data : g_arr
			}, {
				name : 'AD',
				data : ad_arr
			} ]
		});
	});