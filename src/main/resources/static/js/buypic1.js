$(function(){
	$(".buypic-banners").mouseenter(function(){
		var _this = $(this);
		_this.find("div.sliders-left").addClass("sliders-left-cover");
		enter(_this);
	});
	$(".buypic-banners").mouseleave(function(){
		var _this = $(this);
		_this.find("div.sliders-left").addClass("sliders-left-cover");
		leave(_this);
	});
});
function install(vid,name,unit_price,count_used){
	var html = '<div id="carousel-pic" class="carousel slide" style="width: 490px;height: 400px;float:left;" data-ride="carousel">';
	html += '<ol class="carousel-indicators">';
	var li='';
	var div='';
	var option;
	$.ajax({
		type : "post",
		url : "getVipPicture",
		data:{
			vid : vid
		},
		async : false,
		dataType: "json",
		success : function(data){
			for(var i=0;i<data.length;i++){
				var item = data[i];
				if(i==0){
					li += '<li data-target="#carousel-pic" data-slide-to="'+i+'" class="active"></li>';
					div += '<div class="item active">';
					div += '<img class="img-responsive" src="getPicture?aid='+item+'">';
					div += '</div>';
				}else{
					li += '<li data-target="#carousel-pic" data-slide-to="'+i+'"></li>';
					div += '<div class="item">';
					div += '<img class="img-responsive" src="getPicture?aid='+item+'">';
					div += '</div>';
				}
			}
		}
	});
	$.ajax({
		type : "post",
		url : "getWebSiteByMid",
		dataType: "json",
		async : false,
		success : function(data){
			for(var i=0;i<data.length;i++){
				var item = data[i];
				option += '<option value="'+item.wid+'">'+item.wname+'</option>';
			}
		}
	});
	$.ajax({
		type : "post",
		url : "getWebSiteByMid",
		dataType: "json",
		async : false,
		success : function(data){
			for(var i=0;i<data.length;i++){
				var item = data[i];
				option += '<option value="'+item.wid+'">'+item.wname+'</option>';
			}
		}
	});
	html += li;
	html += '</ol>';
	html += '<div class="carousel-inner" role="listbox">';
	html += div;
	html += '</div>';						
	html += '</div>';
	html += '<div style="height:100%;width:0px;border-left:1px solid #fff;border-right:1px solid #ddd;float:left;"></div>';
	html += '<div style="float:right;width:300px;">';
	html += ' <div style="height:50px;"></div>';
    html += '<div style="padding-left:5px;">';
    html += '<div><span id="buypic_packagename" style="font-family:\'Microsoft YaHei\';font-size:16px;font-weight:bold;color:#555;">'+name+'</span></div>';
    html += '<div style="font-size:14px;font-family:\'Microsoft YaHei\';margin-top:10px;">';
    html += '<span>￥</span>';
    html += '<span style="margin-left:2px;" id="buypic_price"></span>';        
    html += '</div>';        
    html += '<div style="font-size:14px;color:#444;font-family:\'Microsoft YaHei\';margin-top:10px;">';
    html += '<span id="buypic_used_count">'+count_used+'</span>';    
    html += '<span>个网站在使用</span>';        
    html += '</div>';        
    html += '<div style="font-size:16px; font-family:\'Microsoft YaHei\';margin-top:20px;overflow:hidden;">';    
    html += '<div class="buypic-select-single">';    
    html += '<select id="buypic_selecttime" class="buypic-select" onchange="updatePrice(this.value,'+unit_price+');" style="float:left;margin-top:5px;">';       
    html += '<option value="1">一个月</option>';           
    html += '<option value="3">三个月</option>';               
    html += '<option value="6">半年</option>';               
    html += '<option value="12">一年</option>';               
    html += '</select>';               
    html += '<span class="buypic-select-muli-dis" style="font-size: 13px; color: rgb(92, 91, 91); float: left; margin-left: 25px;margin-top: 6px;">选择网站:</span>';           
    html += '<select id="buypic_selectwebsite" class="buypic-select" style="float: left; margin-left: 4px; margin-top: 5px; width: 100px;">';   
    html += option;
    html += '</select>';           
    html += '</div> ';           
    html += '<div id="buypic_pay" class="buypic-paybut-single" style ="margin-top: 20px;">';       
    html += '<span style="color:#fff;line-height:30px;">马上付款</span>';        
    html += '</div>';            
    html += '</div>';        
    html += '<div style="font-family:\'Microsoft YaHei\';padding-top:20px;padding-right:10px;">';    
    html += '<p id="buypic_packagecomment" style="text-indent:28px;font-size:12px;color:#444;"></p>';    
    html += '</div>';        
    html += '</div>';    
    html += '</div>';
	$(".modal-body").html(html);
	updatePrice('1', unit_price);// 更新价钱
	$("#vippicModal").modal("show");// 显示窗口
	$('#carousel-pic').carousel({// 执行轮播
      interval: 2000
    });
	$("#buypic_pay").on("click",function(){
		var price = $("#buypic_price").text();
		var buypic_selecttime = $("#buypic_selecttime").val();
		var wid = $("#buypic_selectwebsite").val();
		console.log("wid="+wid);
		// 付款
		window.open("../pay/payOnline?vid="+vid+"&wid="+wid+"&price="+price+"&selecttime="+buypic_selecttime);
	});
}
// 更新价钱
function updatePrice(val, unit_price) {
    var days;
    switch (val) {
        case '1': days = 30; break;
        case '3': days = 90; break;
        case '6': days = 180; break;
        case '12': days = 360; break;
        default: alert("error"); break;
    }
    // if (TouGlobal.memberlevel === 1) {
    // $('#buypic_price').text(0);
    // } else {
        $('#buypic_price').text(days * unit_price);
    // }
}
$("a[class='tabclick']").each(function(){
	var that = $(this);
	that.one("click",function(){
		var _this = $(this);
		var pid = _this.attr("pid");
		var href = _this.prop("href");
		var innerdiv = href.substr(href.indexOf("#")+1,href.length);
		$.post("pictureClass",{pid:pid},function(result){
			var html = '';
		    for(var i=0;i<result.length;i++){
		    	var item = result[i];
		    	html += '<div class="buypic-banners" style="display: block; background-image: url(getClassPicture?vid='+item.vid+');">';
		    	html += '<div class="buypic-sliders">';
		    	html += '<div class="sliders-left">';
				html += '<span style="font-weight: bold; display: block;width:135px">'+item.name+'</span>';
				html += '<span style="font-size: 13px;">'+item.count_picture+'</span>';	
				html += '<span style="font-size: 12px;">张精美图片</span><br>';	
				html += '<span style="font-weight: bold">￥</span>';
				html += '<span style="margin-left: 4px; font-weight: bold">'+item.unit_price+'</span>';
				html += '<span style="font-weight: bold:margin-left:2px;">/</span>';
				html += '<span style="margin-left: 4px; font-weight: bold">天</span>';
				html += '</div>';		 
				html += '<div class="sliders-right" style="width: 105px;" onclick="install(\''+item.vid+'\',\''+item.name+'\',\''+item.unit_price+'\',\''+item.count_used+'\');">';	
				html += '<div></div>';	
				html += '</div>';		
				html += '</div>';		
				html += '</div>';	
		    }
		    $("#"+innerdiv).html(html);
		    $(".buypic-banners").mouseenter(function(){
				var _this = $(this);
				_this.find("div.sliders-left").addClass("sliders-left-cover");
				enter(_this);
			});
			$(".buypic-banners").mouseleave(function(){
				var _this = $(this);
				_this.find("div.sliders-left").addClass("sliders-left-cover");
				leave(_this);
			});
		  },'json');
	});
});
function enter(that){
	that.find("div.buypic-sliders").animate({width:"240px"}, 200, "swing");
}
function leave(that){
	that.find("div.buypic-sliders").animate({width:"130px"}, 400, "swing");
}