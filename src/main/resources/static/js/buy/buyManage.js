function levelUp(obj) {
	var level = obj.value;
	document.location.href = "bebalance?level=" + level + "&month=1";
}
function problem(obj) {
	document.location.href = "bebalance?level=" + obj + "&month=1";
}
function goHistory() {
	$.ajax({
		url : "../buy/checkPayStatus",
		type : "post",
		async : false,
		success : function(data) {
			if (data) {
				alert(data);
				document.location.href = "history";
			} else {
				alert("检验失败！请稍后重试。");
			}
		}
	});

}



function orderPage() {
	var month = $("#month").html();
	var level = $("#levelDiv").find(".btn-info").val();
	var contract = $("#contract").attr("checked") == null ? 0 : 1;
	var receipt = $("#receipt").attr("checked") == null ? 0 : 1;
	var prices = $("#priceSpan").html();
	prices = prices * 1;
	$.ajax({
		url : "../buy/orderPage",
		type : "post",
		data :{
			month : month,
			level : level,
			contract : contract,
			receipt : receipt,
			price : prices
		},
		success : function(data) {
			if (data) {
				document.location.href = "orderinfo?month="+month;
			} else {
				alert("服务器错误！请稍后重试。");
			}
		}
	});
}
function goAliPay(obj) {
	document.balance.submit();
}
function goAliPaytest(obj) {
	document.balancetest.submit();
}

function showAll() {
	$("#toggleDiv").toggle(800);
}
function contactservice() {
	$("#contactqq").toggle(800);
}

$(".btn-default").click(function() {
	sliderToDes(0);
	var level = $(this).val();
	changeMonth(0, level);
})
function goOrderPageFromHistory(hid){
	console.log(hid);
	$.ajax({
		url : "../buy/OrderInfoFromHistory",
		type : "post",
		data : {
			hid : hid
		},
		success : function(data) {
			if (data) {
				document.location.href = "setOrderinfo?hid="+hid;
			} else {
				alert("服务器错误！请稍后重试。");
			}
		}
	});
}
function deleteOrder(){
	var hid = $("#hid_history").val();
	console.log(hid);
	$.ajax({
		url : "../buy/deleteOrder",
		type : "post",
		data : {
			hid : hid
		},
		success : function(data) {
			if (data==1) {
				location.reload();
			} else {
				alert("删除失败");
			}
		}
	});
}
function changeMonth(month, level) {
	// var level = $("#levelDiv").find(".btn-info").val();
	$("#month").html(month + 1);
	if (level == null) {
		var level = $("#levelDiv").find(".btn-info").val();
	}
	$.ajax({
		url : "../buy/balance",
		type : "post",
		data : {
			level : level,
			month : month + 1
		},
		success : function(data) {
			if (data) {
				$("#priceSpan").text(data);
			} else {
			}
		}
	});
}
var sliderToDes = null;
;
(function($) {
	$.fn.sliderDate = function(setting) {
		var defaults = {
			callback : false
		// 默认回调函数为false
		}
		// 如果setting为空，就取default的值
		var setting = $.extend(defaults, setting);
		this.each(function() {
			// 插件实现代码
			// var $sliderDate = $(".slider-date");
			var $sliderDate = $(this);
			var $sliderBar = $sliderDate.find(".slider-bar");
			var $sliderBtn = $sliderDate.find(".slider-bar-btn");
			var liWid = 50 + 1; // 单个li的宽度

			// 滚动指定的位置
			sliderToDes = function(index) {

				// 最大不能超过11
				if (index > 11) {
					index = 11;
				}

				// 最小不能小于 0
				if (index < 0) {
					index = 0;
				}

				// 背景动画
				$sliderBar.animate({
					"width" : liWid * (index + 1)
				}, 500);

				// 执行回调
				if (setting.callback) {
					setting.callback(index);
				}

			};

			// 点击 - 滚动到指定位置
			$sliderDate.on('click', "li", function(e) {
				// 执行滚动方法
				sliderToDes($(this).index());
			});

			// 拖动 - 滚动到指定位置
			$sliderBtn.on('mousedown', function(e) {
				var $this = $(this);
				var pointX = e.pageX - $this.parent().width();
				var wid = null;

				// 拖动事件
				$(document).on('mousemove', function(ev) {
					wid = ev.pageX - pointX
					if (wid > 20 && wid < 620) {
						$sliderBar.css("width", wid);
					}
				}).on('mouseup', function(e) {
					$(this).off('mousemove mouseup');
					var index = Math.ceil(wid / liWid) - 1;
					sliderToDes(index);
				});
			});
		});
	}
})(jQuery);
$(function() {
	function a(index) {
		console.log(index + 1);
	}
	$("#slider-date-1").sliderDate({
		callback : changeMonth
	});

});
