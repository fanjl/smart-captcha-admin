﻿/// <reference path="../script/jquery-1.8.0.min.js" />
/// <reference path="jslib.js" />
/// <reference path="../script/jquery.easyui.min.js" />

String.prototype.trim = function () { return this.replace(/(^\s*)|(\s*$)/g, ""); };

$(document).ready(function ()
{
    
    var touclick_post_data = {
        website_type: -1, //网站类型
        role:'',
        email: null,
        password: false,
        password2:false,
        realname: null,
        qq: null,
        website_name: null,
        tel: '',
        website_domain: null,
        invite_code:'',
        verify:''
    };
    var locker = new lock();
    (function ()
    {
        var win_height = $(window).height();
        win_height = win_height < 850 ? 850 : win_height;
        var win_width = $(window).width();
        $('#contain').width(win_width);
    })();//页面高度
    $('.block input[type=text]').val('');
    $('#website_type').blur(function ()
    {
        if (touclick_post_data.website_type === -1)
        {
            note_to_false(this);
        } else
        {
            note_to_ok(this);
        }
    }).focus(function ()
    {
        //note_to_normal(this);
    }).find('input[type=radio]').click(function ()
    {
        //网站类型
        var _this = $(this);
        var tou_id = parseInt(_this.attr('tou_id'));
        if (touclick_post_data.website_type === -1)
        {
            note_to_ok($('#website_type')[0]);
        } else if (touclick_post_data.website_type != tou_id)
        {
            var website_type = $('#website_type');
            website_type.find('input[tou_id=' + touclick_post_data.website_type + ']').attr('checked', false);
            website_type.find('input[tou_id=' + tou_id + ']').attr('checked', true);
        }
        touclick_post_data.website_type = tou_id;
    }).each(function (index, value)
    {
        $(value).attr('checked', false);
    });
   
    $('#register_button').click(function () {

       

        $(this).removeClass('register_button_focus');
        var isok = true;
        for (var proprity in touclick_post_data) {
            var default_val;
            if (proprity == 'website_type') {
                default_val = -1;
            } else if (proprity == 'password' || proprity == 'password2') {
                default_val = false;
            } else {
                default_val = null;
            }
            if(proprity=='role'){
            	if($('select[tou_id=role]')==''){
            		note_to_false($('select[tou_id=role]'));
                	isok &= false;
            	}
            }
            if(proprity=='verify'){
            	if($('#verify').val()==''){
            		note_to_false($("input[tou_id=verify]"));
					isok &= false;
            	}
            }
            //默认值在此处赋值
            if (touclick_post_data[proprity] === default_val) {
                if (proprity == 'email') {
                    $('.block input[tou_id=email]').blur();
                } else {
                    note_to_false($('.block input[tou_id=' + proprity + ']'));
                }
                isok &= false;
            }
        }
        //检查网站类型是否选择
        if (window.website_types.length === 0) {
            isok &= false;
            note_to_false(website_realtype_hidden[0]);
        } else {
            touclick_post_data['website_realtype'] = window.website_types.join(',');
        }
        //

        if (isok === true) {
        	
        	/*window.TouClick.Start({
                website_key: '212d8812-46ca-4554-b370-4d9ffc82c5f5',
                position_code: 0,
                onSuccess: function (args, check_obj) {
                	var wtype='';
                	$('#website_realtype .selected').each(function(){
                		if(typeof($(this).attr('tou_id'))!='undefined'){
                			wtype=wtype+$(this).attr('tou_id')+',';
                		}
                	});
                	var reg=/,$/gi;
                	wtype=wtype.replace(reg,"");
                	$('#wtype').val(wtype);
                	var formdata = $('form').serialize();
                    touclick_post_data.check_key = check_obj.check_key;
                    touclick_post_data.check_address = check_obj.check_address[0] + "," + check_obj.check_address[1];
                    locker.show();
                    TouClick.Close();
                    $.post('member/regist',formdata, function (data) {
                    	console.log(data);
                        locker.close();
                        if (data && data.isok === true) {
                        	console.log("here");
                            var div = document.createElement('div');
                            div.id = 'register_success';
                            div.style.padding = "10px";
                            document.body.appendChild(div);
                            $(div).window({
                                width: 550,
                                height: 470,
                                modal: true,
                                collapsible: false,
                                minimizable: false,
                                title: "注册成功",
                                onClose: function () {
                                    window.location.href = "http://www.touclick.com";
                                },
                                onOpen: function () {
                                    window.key_obj = data;
                                    delete window.key_obj.isok;
                                }
                            });
                            $(div).window('refresh', 'success.html');

                        }
                        else {
                            $.messager.alert('注册页提醒', '没有注册成功,可能是系统出错,请联系我们,谢谢<br/>qq:874767484');
                        }
                    }, 'json');
                }
            });*/
        }
        else {
            $.messager.alert('注册页提醒', '请检查不合格输入项');
        }
    }).focus(function () {
        $(this).addClass('register_button_focus');
    }).blur(function () {
        $(this).removeClass('register_button_focus');
    }).keydown(function (e) {
        if (e.keyCode == 13) {
            $(this).addClass('register_button_focus');
            $(this).click();
        }
    });

    $('.block input[tou_id=password]').blur(function ()
    {
        //校验密码
        var password = $(this).val();
        var length = password.split('').length;
        if (length < 6 || length >= 20)
        {
            note_to_false(this);
            touclick_post_data.password = false;
        }
        else
        {
            note_to_ok(this);
            touclick_post_data.password = true;
        }
        touclick_post_data.password2 = false;
        note_to_false($('.block input[tou_id=password2]')[0]);
    }).focus(function ()
    {
        note_to_normal(this);
    });
    $('.block input[tou_id=password2]').blur(function ()
    {
        //确认密码
        var password = $('.block input[tou_id=password]').val();
        var password2 = $(this).val();
        if (password2 != password)
        {
            note_to_false(this);
        }
        else
        {
            note_to_ok(this);
            if (touclick_post_data.password === true)
            {
                touclick_post_data.password = password;
                touclick_post_data.password2 = true;
            }
            else
            {
                touclick_post_data.password = false;
                touclick_post_data.password2 = false;
            }
        }
    }).focus(function ()
    {
        note_to_normal(this);
    });
    $('select[tou_id=role]').change(function(){
    	
    	 var $_this=$(this);
         var identity = $_this.attr('tou_id');
         var val=$_this.val().trim();
         if (val != '')
         {
             note_to_ok(this);
             touclick_post_data[identity] = val;
         }
         else
         {
             touclick_post_data[identity] = null;
             note_to_false(this);
         }
    });
    $('.block input[tou_id=realname]').add('.block input[tou_id=qq]').add('.block input[tou_id=website_name]').blur(function ()
    {
        var $_this=$(this);
        var identity = $_this.attr('tou_id');
        var val=$_this.val().trim();
        if (val != '')
        {
            note_to_ok(this);
            touclick_post_data[identity] = val;
        }
        else
        {
            touclick_post_data[identity] = null;
            note_to_false(this);
        }
    }).focus(function ()
    {
        note_to_normal(this);
    });

    $('.block input[tou_id=website_domain]').blur(function ()
    {
        var url = $(this).val().trim();
        var _this = this;
        var reg = "^((http)?://)?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\.){3}[0-9]{1,3}|([0-9a-z_!~*'()-]+\.)*([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.[a-z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
        var re = new RegExp(reg);
        if ( re.test(url) === true)
        {
            $.post('member/existWebSite', { way: 'exist_website', waddress: url },
                function (data)
                {
                    if (data && data.isexist === false)
                    {
                        touclick_post_data.website_domain = url;
                        note_to_ok(_this);
                    } else
                    {
                        //已存在
                        var item_note = $(_this).parents('.item').children('.item-note');
                        item_note.children().css({ 'display': 'none' });
                        item_note.find('span.other-note-not1').css({ 'display': 'inline' });
                    }
                }, 'json');
        } else
        {
            touclick_post_data.website_domain = null;
            note_to_false(this);
        }
    }).focus(function ()
    {
        note_to_normal(this);
    });
    $('.block input[tou_id=tel]').blur(function ()
    {
        var val = $(this).val().trim();
        if (val == '')
        {
            note_to_ok(this);
            touclick_post_data.tel = val;
            return;
        }
        var reg = /^[0-9]+$/;
        if (reg.test(val) === true)
        {
            touclick_post_data.tel = val;
            note_to_ok(this);
        }
        else
        {
            touclick_post_data.tel = null;
            note_to_false(this);
        }
    });

    $('.block input[tou_id=email]').blur(function ()
    {
        var email = $(this).val().trim();
        //var reg = /^[a-z]([a-z0-9]*[-_]?[a-z0-9]+)*@([a-z0-9]*[-_]?[a-z0-9]+)+[\.][a-z]{2,3}([\.][a-z]{2})?$/i;
        var reg = /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/i;
        if (email == '')
        {
            var item_note = $(this).parents('.item').children('.item-note');
            item_note.children().css({ 'display': 'none' });
            item_note.find('.other-note-not').css({ 'display': 'inline' });
            return;
        }
        if (reg.test(email) === true)
        {
            //发起远程
            var item_note = $(this).parents('.item').children('.item-note');
            item_note.children().css({ 'display': 'none' });
            item_note.find('.other-note-yes2').css({ 'display': 'block' });
            $.post('member/existEmail', {email:email,way:'exist'}, function (data)
            {
                item_note.children().css({ 'display': 'none' });
                if (data && data.isexist === false)
                {
                    item_note.find('.other-note-yes1').css({ 'display': 'block' });
                    touclick_post_data.email = email;
                }
                else
                {
                    item_note.find('span.other-note-not1').css({ 'display': 'inline' });
                }
            }, 'json');
        } else
        {
            var item_note = $(this).parents('.item').children('.item-note');
            item_note.children().css({ 'display': 'none' });
            item_note.find('span.other-note-not2').css({ 'display': 'inline' });
        }
    }).focus(function ()
    {
        var item_note = $(this).parents('.item').children('.item-note');
        item_note.children().css({ 'display': 'none' });
        item_note.find('span.note').css({ 'display': 'inline' });
    });

    $('.block input[tou_id=invite_code]').blur(function () {
        var invite_code = $(this).val().trim();
        if (invite_code.length ==0) {
            //note_to_false(this);
            touclick_post_data.invite_code = '';
        } else {
            var _this = this;
            $.post('existInviteCode', { way: 'exist_invite_code', invite_code: invite_code }, function (data) {
                if (data && data.isexist === true) {
                    note_to_ok(_this);
                    touclick_post_data.invite_code = invite_code;
                    //$(_this).parents('.item').css('display', 'none');
                } else {
                    touclick_post_data.invite_code = '';
                    note_to_false(_this);
                }
            }, 'json');
        }
    }).focus(function () {
        note_to_normal(this);
    });
    
    //检测是否包含 邀请码
    var vars = [], hash;
    var q = document.URL.split('?')[1];
    if (q != undefined) {
        q = q.split('&');
        for (var i = 0; i < q.length; i++) {
            hash = q[i].split('=');
            vars.push(hash[1]);
            vars[hash[0]] = hash[1];
        }
    }
    if (typeof (vars['invite_code']) != 'undefined') {
        var invite_input = $('.block input[tou_id=invite_code]');
        invite_input.attr('loadok', '1');
        $.messager.alert('欢迎使用点触验证码', '欢迎受邀使用点触验证码，注册成功后可向工作人员索要10元优惠券', 'info');
        invite_input.val(vars['invite_code']).blur();
    }





    //var div = document.createElement('div');
    //div.id = 'register_success';
    //div.style.padding = "10px";
    //document.body.appendChild(div);
    //$(div).window({
    //    width: 550,
    //    height: 470,
    //    modal: true,
    //    collapsible: false,
    //    minimizable: false,
    //    title: "注册成功",
    //    onClose: function () {
    //        window.location.href = "http://www.touclick.com";
    //    },
    //    onOpen: function () {
    //        window.key_obj = {};
    //        delete window.key_obj.isok;
    //    }
    //});
    //$(div).window('refresh', '/template/register/success.html');


});
//提醒切换
function note_to_ok(item_input)
{
    var item_note = $(item_input).parents('.item').children('.item-note');
    item_note.children().css({ 'display': 'none' });
    item_note.find('.other-note-yes').css({ 'display': 'block' });
}
function note_to_false(item_input, id)
{
    var item_note = $(item_input).parents('.item').children('.item-note');
    item_note.children().css({ 'display': 'none' });
    item_note.find('span.other-note-not').css({ 'display': 'inline' });
}
function note_to_normal(item_input)
{
    var item_note = $(item_input).parents('.item').children('.item-note');
    item_note.children().css({ 'display': 'none' });
    item_note.find('span.note').css({ 'display': 'inline' });
}
function lock()
{
    var div_dom = null;
    this.show = function (isie6)
    {
        div_dom.style.display = 'block'
        if (isie6 === true) { var style = div_dom.style; style.position = 'absolute'; style.height = document.body.clientHeight + 'px'; };
        $('#register_loading').css({ 'display': 'block' });
    };
    this.close = function ()
    {
        div_dom.style.display = 'none'
        $('#register_loading').css({ 'display': 'none' });
    };
    div_dom = document.createElement("div");
    var style = div_dom.style;
    style.display = "none";
    style.position = "fixed";
    style.left = "0px";
    style.top = "0px";
    style.zIndex = "9999";
    style.height = style.width = "100%";
    var back_shade_color = '#fff';
    if (typeof (background_shade_color) != 'undefined')
    {
        back_shade_color = background_shade_color
    };
    style.backgroundColor = back_shade_color;
   
    style.opacity = "0.5";
    style.filter = "alpha(opacity=50)";
    document.body.appendChild(div_dom)
};
function downloadverify(){
	var waddress = $("#waddress").val();
	if(waddress==""){
		alert("请先填写网站地址");
	}else{
		window.open("member/downloadVerify?waddress="+waddress);
	}
}
function clickverify(){
	var waddress = $("#waddress").val();
	if(waddress==""){
		alert("请先填写网站地址");
		return false;
	}else{
		$.ajax({
			type : "post",
			url : "member/verify",
			data : {waddress:waddress},
			success:function(data){
				if(data){
					note_to_ok($("input[tou_id=verify]"));
					$('#verify').val(1);
				}else{
					note_to_false($("input[tou_id=verify]"));
				}
			}
		});
	}
}
