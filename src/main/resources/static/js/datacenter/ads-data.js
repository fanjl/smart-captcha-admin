
/**      
 * 对Date的扩展，将 Date 转化为指定格式的String      
 * 月(M)、日(d)、12小时(h)、24小时(H)、分(m)、秒(s)、周(E)、季度(q) 可以用 1-2 个占位符      
 * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)      
 * eg:      
 * (new Date()).pattern("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423      
 * (new Date()).pattern("yyyy-MM-dd E HH:mm:ss") ==> 2009-03-10 二 20:09:04      
 * (new Date()).pattern("yyyy-MM-dd EE hh:mm:ss") ==> 2009-03-10 周二 08:09:04      
 * (new Date()).pattern("yyyy-MM-dd EEE hh:mm:ss") ==> 2009-03-10 星期二 08:09:04      
 * (new Date()).pattern("yyyy-M-d h:m:s.S") ==> 2006-7-2 8:9:4.18      
 */        
Date.prototype.pattern=function(fmt) {         
    var o = {         
    "M+" : this.getMonth()+1, //月份         
    "d+" : this.getDate(), //日         
    "h+" : this.getHours()%24 == 0 ? 00 : this.getHours()%24, //小时         
    "H+" : this.getHours(), //小时         
    "m+" : this.getMinutes(), //分         
    "s+" : this.getSeconds(), //秒         
    "q+" : Math.floor((this.getMonth()+3)/3), //季度         
    "S" : this.getMilliseconds() //毫秒         
    };         
    var week = {         
    "0" : "/u65e5",         
    "1" : "/u4e00",         
    "2" : "/u4e8c",         
    "3" : "/u4e09",         
    "4" : "/u56db",         
    "5" : "/u4e94",         
    "6" : "/u516d"        
    };         
    if(/(y+)/.test(fmt)){         
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));         
    }         
    if(/(E+)/.test(fmt)){         
        fmt=fmt.replace(RegExp.$1, ((RegExp.$1.length>1) ? (RegExp.$1.length>2 ? "/u661f/u671f" : "/u5468") : "")+week[this.getDay()+""]);         
    }         
    for(var k in o){         
        if(new RegExp("("+ k +")").test(fmt)){         
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));         
        }         
    }         
    return fmt;         
}    
function setoptions(data, format) {
	options = {
		"dom" : 'T<"clear">lfrtip',
		"tableTools" : {
			"sSwfPath" : "../plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
		},
		"data" : data,
		"columns" : [ {
			"data" : "date"
		}, {
			"data" : "g"
		}, {
			"data" : "vy"
		}, {
			"data" : "vn"
		}, {
			"data" : "sv"
		}, {
			"data" : "ad"
		}, {
			"data" : "adr"
		} ],
		"columnDefs" : [ {
			"render" : function(data, type, row) {
//				return Highcharts.dateFormat(format, data);
				return new Date(data).pattern(format);
			},
			"targets" : 0
		} ],
		"bPaginate" : true,
		"bLengthChange" : false,
		"bFilter" : false,
		"bSort" : true,
		"bInfo" : true,
		"bAutoWidth" : false,
		"oLanguage" : {
			"sProcessing" : "正在加载中......",
			"sLengthMenu" : "每页显示 _MENU_ 条记录",
			"sZeroRecords" : "对不起，查询不到相关数据！",
			"sEmptyTable" : "表中无数据存在！",
			"sInfo" : "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录",
			"sInfoFiltered" : "数据表中共为 _MAX_ 条记录",
			"sSearch" : "搜索",
			"oPaginate" : {
				"sFirst" : "首页",
				"sPrevious" : "上一页",
				"sNext" : "下一页",
				"sLast" : "末页"
			}
		}
	}
}
var selectDateRangeDay = function() {
	var dateRange = $("#reservation_day").val();
	$.ajax({
		type : "POST",
		url : "getCtypesDataByDateRange",
		dataType : "json",
		data : {
			dateRange : dateRange,
			datetype : "day",
			wid : $("#currentwebsite").val()
		},
		success : function(data) {
			if(data.length>0){
				daytable.fnClearTable();
				daytable.fnAddData(data);
				daytable.fnDraw();
			}else{
				daytable.fnClearTable();
			}
		}
	});
}
var selectDateRangeMonth = function() {
	var dateRange = $("#reservation_month").val();
	$.ajax({
		type : "POST",
		url : "getCtypesDataByDateRange",
		dataType : "json",
		data : {
			dateRange : dateRange,
			datetype : "month",
			wid : $("#currentwebsite").val()
		},
		success : function(data) {
			if(data.length>0){
				monthtable.fnClearTable();
				monthtable.fnAddData(data);
				monthtable.fnDraw();
			}else{
				monthtable.fnClearTable();
			}
		}
	});
}
var selectDateRangeYear = function() {
	var dateRange = $("#reservation_year").val();
	$.ajax({
		type : "POST",
		url : "getCtypesDataByDateRange",
		dataType : "json",
		data : {
			dateRange : dateRange,
			datetype : "year",
			wid : $("#currentwebsite").val()
		},
		success : function(data) {
			if(data.length>0){
				yeartable.fnClearTable();
				yeartable.fnAddData(data);
				yeartable.fnDraw();
			}else{
				yeartable.fnClearTable();
			}
		}
	});
}
function changewebsite(){
	selectDateRangeDay();
	selectDateRangeMonth();
	selectDateRangeYear();
}
