function del(wid) {
	$.ajax({
		type : 'post',
		url : 'delWebsite',
		data : {
			wid : wid
		},
		success : function(data) {
			if (data) {
				alert("删除成功");
				location.reload();
			} else {
				alert("删除失败，用户至少拥有一个网站");
			}
		}
	});
}
function save() {
	var wname = $("#wname").val();
	var waddress = $("#waddress").val();
	var platform = $("#platform").val();
	if (wname == "") {
		alert("网站名称不能为空");
		return false;
	} else if (waddress == "") {
		alert("网站地址不能为空");
		return false;
	} else {
		waddressvalidate();
	}
}
function resetToken(wid) {
	if (window.confirm("确定要重置秘钥吗？")) {
		$.ajax({
			type : "post",
			url : "resetToken",
			data : {
				wid : wid
			},
			success : function(data) {
				if (data != "") {
					alert("您的新秘钥为:" + data);
					location.reload();
				}
			}
		});
	} else {
		return false;
	}
}
function downloadverify(waddress) {
	var waddress = $("#waddress").val();
	if (waddress == "") {
		alert("请先填写网站地址");
	} else {
		window.open("../member/downloadVerify?waddress=" + waddress);
	}
}
function waddressvalidate() {
	var _this = $("#waddress");
	var url = _this.val().trim();
	var reg = "^((http|https)\\://)?([a-zA-Z0-9\\.\\-]+(\\:[a-zA-"   
        + "Z0-9\\.&%\\$\\-]+)*@)?((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{"   
        + "2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}"   
        + "[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|"   
        + "[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-"   
        + "4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0"   
        + "-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.[a-zA-Z]{2,4})(\\:[0-9]+)?(/"   
        + "[^/][a-zA-Z0-9\\.\\,\\?\\'\\\\/\\+&%\\$\\=~_\\-@]*)*$";
	var re = new RegExp(reg);
	if (re.test(url) === true) {
		$.ajax({
			url : "../member/existWebSite",
			type : "post",
			async : false,
			data : {
				waddress : url
			},
			success : function(data) {
				if (data) {
					// 已存在
					_this.siblings("font").remove();
					_this.siblings("br").remove();
					_this.after('<font class="pull-left" color="red">网站地址已存在</font><br>');
				}else{
					submit1();
				}
			}
		});
	}else{
		_this.siblings("font").remove();
		_this.siblings("br").remove();
		_this.after('<font class="pull-left" color="red">网站地址格式错误</font><br>');
		return false;
	}
}
function clickverify(wid, waddress) {
	$.ajax({
		type : "post",
		url : "verify",
		data : {
			wid : wid,
			waddress : waddress
		},
		success : function(data) {
			if (data) {
				alert("验证通过");
				location.reload();
			} else {
				alert("验证未通过,请检查网站地址并将下载的文件放在网站的根目录下，以便验证网站");
			}
		}
	});
}
function submit1(){
	var wname = $("#wname").val();
	var waddress = $("#waddress").val();
	var platform = $("#platform").val();
	var l = Ladda.create( document.querySelector( '.ladda-button' ) );
	l.start();
	setTimeout(function(){
		$.ajax({
			type : 'post',
			url : 'addWebsite',
			data : {
				wname : wname,
				waddress : waddress,
				platform : platform
			},
			success : function(data) {
				switch (data) {
				case 0:
					alert("保存失败,请重试");
					break;
				case 1:
					alert("保存成功");
					location.reload();
					break;
				case 2:
					alert("验证未通过");
					break;
				case 3:
					alert("您当前版本的网站个数已到达最大值");
					break;
				}
				l.stop();
			}
		});
	},500);
}