/*
 * Fullscreen background
 */
//$.backstretch("https://captcha-static.touclick.com/admin/login20160217.jpg");
$('html').keydown(function(e) {
	if (e.keyCode == 13) {
		login();
	}
});
//var l = Ladda.create( document.querySelector( '.ladda-button' ) );

TouClick.ready(function(){
	var check_obj_tmp = {};
    var tc = TouClick("captcha-target",{
    	readyCheck: function(){
    		var val = TouClick.$('email').value;
    		if(val == ''){
    			return {status:false};
    		}
    		return {status:true,ckCode:val};
    	},
        onSuccess : function(check_obj){
        	$("#_submit").css('background','#4aaf51');
        	check_obj_tmp = check_obj;
        },
        behaviorDom:'email'
    });
    
	TouClick.$('_submit').onclick = function(){
		if(!tc.getStatus()){
			//点击验证码
			return;
		}else{
			//l.start();
			var email = $("#email").val();
			var pwd = $("#password").val();
			if (email.length == 0) {
				alert("\"用户名\"不能为空,请输入!");
				document.getElementById("email").value = "";
				document.getElementById("email").focus();
				return false;
			}
			if (pwd.length == 0) {
				document.getElementById("password").value = "";
				alert("\"密码\"不能为空,请输入!");
				document.getElementById("password").focus();
				return false;
			}
			$.post("member/login", {
				email : email,
				password : pwd,
				token: check_obj_tmp.token,
				checkAddress: check_obj_tmp.checkAddress,
				sid: check_obj_tmp.sid
			}, function(returnvalue) {
				console.log(returnvalue);
				if(returnvalue.meta.code == 200){
					member.userInfo.mid = returnvalue.data.mid;
					member.userInfo.email = returnvalue.data.email;
					member.userInfo.level = returnvalue.data.level;
					member.userInfo.token = returnvalue.data.token;
					member.userInfo.realName = returnvalue.data.realname;
					console.log(member.userInfo);
					$.cookie("Authorization",null);
					$.cookie("Authorization",member.userInfo.token);
					document.location.href = "member/index";
				}else{
					alert(returnvalue.meta.message);
				}


				// if(returnvalue=='0'){
				// 	alert("验证失败！");
				// 	// alert("\"用户名\"不存在,请重新输入!");
				// 	document.getElementById("password").value = "";
				// 	document.getElementById("password").focus();
				// }else if(returnvalue=='2'){
				// 	alert("\"用户名\"或\"密码\"错误，请重新输入!");
				// 	// alert("\"用户名\"不存在,请重新输入!");
				// 	document.getElementById("password").value = "";
				// 	document.getElementById("password").focus();
				// }else{
				// 	console.info(returnvalue);
				// 	$.cookie("Authorization",null);
				// 	$.cookie("Authorization",returnvalue);
				// 	document.location.href = "member/index";
				// }

				//l.stop();
			});
		}
	};
});
