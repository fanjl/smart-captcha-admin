    	$("#password2").keyup(function(){
			loadPasswordStrong("#password2");
		});
		function loadPasswordStrong(div){
		    var  content = $(div).val();
		    if(content.length>=6){
		        $.post(
		            "https://password.touclick.com/api/eval/evaluate",
		            {password:content},
		            function(data) {
		                var level = data.data.strengthText;
		                var count = 0;
		                switch(level){
		                    case "极弱":
		                        count = "15%";
		                        break;
		                    case "弱":
		                        count = "35%";
		                        break;
		                    case "中":
		                        count = "55%";
		                        break;
		                    case "强":
		                        count = "75%";
		                        break;
		                    case "极强":
		                        count = "98%";
		                        break;
		                }
		                $("#strong").css("display","block").children(".progress-bar").css({
		                    "width":count
		                });
		            }
		        )
		    }
		}
