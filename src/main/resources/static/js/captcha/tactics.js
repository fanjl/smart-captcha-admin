// checkbox btn effect
inputChecked($("#logostatus"), "../captcha/updateRemoveLogoStatus", 1);
inputChecked($("#helpstatus"), "../captcha/updateRemoveHelpStatus", 2);
inputChecked($("#promptTextStatus"), "../captcha/updateCustomTxtStatus", 3);
inputChecked($("#behaviorstatus"));
// click checkbox btn fun
function inputChecked(box, url, paramNum) {
    box.on("click touchstart", "input", function (event) {
        // 判断有效请求
        if (url) {
            var that = $(this);
            var wid = $("#currentwebsite").val();
            var btnSwitch = null;
            if ($(this).attr("checked") === "checked") {
                btnSwitch = false;
            } else {
                btnSwitch = true;
            }
            switch (paramNum) {
                case 1:
                    var data = {
                        isRemoveLogo: btnSwitch,
                        wid: wid
                    }
                    break;
                case 2:
                    var data = {
                        isRemoveHelp: btnSwitch,
                        wid: wid
                    }
                    break;
                case 3:
                    var data = {
                        isCustomTxt: btnSwitch,
                        wid: wid
                    }
                    break;
            }
            updateRemoveFun(wid, that, url, data, btnSwitch);
        } else {
            $("#errorText").text("敬请期待");
            $("#ErrorAlert").modal("show");
            that.removeAttr("checked");
        }
    });

}

$("#currentwebsite").change(function () {
    console.log($("#currentwebsite").val());
    $.ajax({
        url: "../captcha/WebSiteTastic",
        type: "post",
        data: {'wid': $("#currentwebsite").val()},
        success: function (data) {
            var json = JSON.parse(data);
            changeWidTasticsShow(json.isEnableRemoveLogo, $("#logotext"));
            changeWidTasticsShow(json.isEnableRemoveHelp, $("#helptext"));
            changeWidTasticsShow(json.isCustomText, $("#promptTextText"));
            changeWidTasticsShow(json.isSupportBehavior, $("#behaviortext"));
        }
    })
})
function changeWidTasticsShow(status, node) {
        if (status == "true") {
            node.html("开启");
            node.siblings().prop("checked","checked",true);
        } else {
            node.html("关闭");
            node.siblings().removeAttr("checked");
        }

}
function updateRemoveFun(wid, that, url, data, status) {
    $.ajax({
        url: url,
        type: "post",
        data: data,
        success: function (data) {
            switch (data) {
                case 1:
                    if (status) {
                        that.siblings().html("开启");
                        that.attr("checked", "checked");
                    } else {
                        that.siblings().html("关闭");
                        that.removeAttr("checked");
                    }
                    break;
                case 2:
                    alert("服务器错误，请稍后重试！");
                    that.removeAttr("checked");
                    break;
                case 3:
                    that.removeAttr("checked");
                    $("#errorText").text("权限不足！请升级产品。");
                    $("#ErrorAlert").modal("show");
                    break;
                default:
                    break;
            }
        }
    });
}