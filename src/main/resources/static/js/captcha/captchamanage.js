var currentstep = 0;
var jumptwostep = false;
var can_upload_pic = false;
var smpic_numbers = 1;
var printflag;
var submitflag = true;
var sliderOptions = {
	formatter : function(value) {
		return '当前透明度: ' + value;
	}
};
var slider1 = new Slider('#slider1', sliderOptions);
var slider2 = new Slider('#slider2', sliderOptions);
var slider3 = new Slider('#slider3', sliderOptions);
function printfont() {
	// var group = $("#adgroups").val();
	// if(group==""){
	// alert("没有广告组，请先添加广告组");
	// return false;
	// }
	printflag = "font";// 印刷文字型
	$("#custom-step" + currentstep++).css("display", "none");
	$("#custom-step" + currentstep).css("display", "block");
	$("#captcha-div-bottom").css("display", "block");
	$("#over").css("display", "none");
	$("#step1img").css("background-position", "0px 0px");
	jumptwostep = false;
	$("#imgtexttype").val("0");
}
function printimg() {
	// var group = $("#adgroups").val();
	// if(group==""){
	// alert("没有广告组，请先添加广告组");
	// return false;
	// }
	printflag = "img";// 印刷图片型
	$("#custom-step" + currentstep++).css("display", "none");
	$("#custom-step" + currentstep).css("display", "block");
	$("#captcha-div-bottom").css("display", "block");
	$("#over").css("display", "none");
	$("#step1img").css("background-position", "0px -128px");
	jumptwostep = true;
	$("#imgtexttype").val("1");
}
$('input[name="ctype"]').change(function() {
	var _this = $(this);
	var ctype = _this.val();
	var whiteImg = $("#skin_white");
	var blackImg = $("#skin_black");
	switch (ctype) {
	case "1":
		whiteImg.prop("src", "../image/classical-white_new.png");
		blackImg.prop("src", "../image/classical-black_new.png");
		blackImg.parent().parent().css("display", "block");
		break;
	case "2":
		whiteImg.prop("src", "../image/new/block-white.png");
		blackImg.prop("src", "../image/new/block-black.png");
		blackImg.parent().parent().css("display", "block");
		break;
	case "4":
		whiteImg.prop("src", "../image/rotate/rotate.png");
		blackImg.parent().parent().css("display", "none");
		$('input:radio[value="white"]').click();
		break;
	case "3":
		whiteImg.prop("src", "../image/ctype/3/5.2.1/drag_skin_white.png");
		blackImg.parent().parent().css("display", "none");
		$('input:radio[value="white"]').click();
		break;
	case "13":
		whiteImg.prop("src", "../image/ctype/13/5.2.1/click_skin_white.png");
		blackImg.parent().parent().css("display", "none");
		$('input:radio[value="white"]').click();
		break;
	case "14":
		whiteImg.prop("src", "../image/ctype/14/5.2.1/block_skin_white.png");
		blackImg.parent().parent().css("display", "none");
		$('input:radio[value="white"]').click();
		break;
	}
});
$("#captcha-div-bottom div")
		.click(
				function() {
					console.log($("#currentwebsite").val());
					var tou_id = $(this).attr('tou_id');
					if (tou_id == '1') {
						document.getElementById("custom-step" + currentstep--).style.display = 'none';
						if (jumptwostep && currentstep > 1) {
							currentstep--;
						}
						document.getElementById("custom-step" + currentstep).style.display = 'block';
						if (currentstep == 0) {
							$("#captcha-div-bottom").css("display", "none");
						} else if (currentstep == 1) {
							$("#nextstep").css("display", "block");
							$("#over").css("display", "none");
						}
					} else if (tou_id == '2') {
						if (currentstep == 1 && $("#backcroppic").val() == "") {
							alert("请上传底图，并做好裁剪");
							return false;
						}
						document.getElementById("custom-step" + currentstep++).style.display = 'none';
						if (jumptwostep) {
							currentstep++;
						}
						document.getElementById("custom-step" + currentstep).style.display = 'block';
						if (currentstep > 1) {
							$("#nextstep").css("display", "none");
							$("#over").css("display", "block");
						}
					} else {
						var formData = new FormData();
						formData.append("tempurl", $("#backcroppic").val());// 底图的base64
						var clickfont = "";
						var otherfont = "";
						$("#captcha-div2-wordpool div").each(
								function() {
									var that = $(this);
									var wordclass = that.attr("class");
									if (wordclass.indexOf("wordsel") != -1) {
										clickfont = clickfont
												+ that.find("span").text();
									} else {
										otherfont = otherfont
												+ that.find("span").text();
									}
								});
						formData.append("newprintfont", clickfont + otherfont);// 点击文字放前边，其他放后边的印刷文字
						formData.append("printfont", $("#captcha-div2-input")
								.val());// 原顺序印刷文字
						formData.append("clickfont", clickfont);// 点击文字
						formData.append("imgtexttype", $("#imgtexttype").val());// 自定义图片类型
						if (printflag == "img") {
							if (typeof ($("#smallpic1").get(0).files[0]) != "undefined") {
								formData.append("smallpic1", $("#smallpic1")
										.get(0).files[0]);
								if ($("#remind1").val() == "") {
									alert("请输入提醒文字");
									return false;
								}
							} else if (printflag == "img") {
								alert("请至少上传一个小图");
								return false;
							}
							if (typeof ($("#smallpic2").get(0).files[0]) != "undefined") {
								formData.append("smallpic2", $("#smallpic2")
										.get(0).files[0]);
								if ($("#remind2").val() == "") {
									alert("请输入提醒文字");
									return false;
								}
							}
							if (typeof ($("#smallpic3").get(0).files[0]) != "undefined") {
								formData.append("smallpic3", $("#smallpic3")
										.get(0).files[0]);
								if ($("#remind3").val() == "") {
									alert("请输入提醒文字");
									return false;
								}
							}
						}
						formData.append("transparent1", slider1.getValue());
						formData.append("transparent2", slider2.getValue());
						formData.append("transparent3", slider3.getValue());
						formData.append("remind1", $("#remind1").val());
						formData.append("remind2", $("#remind2").val());
						formData.append("remind3", $("#remind3").val());
						var src = $(".croppedImg").prop("src");
						formData.append("background", src.substr(src
								.indexOf(",") + 1, src.length));
						// formData.append("adgroups",$("#adgroups").val());
						formData.append("wid", $("#currentwebsite").val());
						if (printflag == "font") {
							if ($("#captcha-div2-input").val() == "") {
								alert("请输入印刷文字");
								return false;
							}
							if (clickfont == "") {
								alert("请选择点击文字");
								return false;
							}
						}
						if (submitflag) {
							submitflag = false;
							$
									.ajax({
										type : "post",
										url : "saveCustomPic",
										data : formData,
										processData : false,
										contentType : false,
										success : function(data) {
											if (data) {
												alert("保存成功");
												document.location.href = "captchaManage";
											} else {
												alert("保存失败，请重试");
											}
											submitflag = true;
										}
									});
						} else {
							alert("服务器正在处理，请不要重复提交");
						}
					}
				});
$('#captcha-div4-inputpool .div4-block input[type=file]').change(
		function() {
			var _this = this;
			function fail() {
				$(_this).parent().siblings('.div4-img').text($(_this).val());
			}
			var __temp_files = this.files;
			var resultFile = __temp_files ? __temp_files[0] : false;
			if (resultFile && FileReader) {
				try {
					var reader = new FileReader();
					reader.readAsDataURL(resultFile);
					reader.onload = function() {
						$(_this).parent().siblings('.div4-img').children('img')
								.css('display', 'block').attr('src',
										reader.result).load(
										function() {
											var __this = $(this);
											if (__this.height() > 60
													|| __this.width() > 60) {
												alert('请上传尺寸小于 60*60 像素的图片!');
												__this.css('display', 'none')
														.attr('src', '');
											}
										});
					}
				} catch (e) {
					fail();
				}
			} else {
				fail();
			}
		});
function trim(str) { // 删除左右两端的空格
	return str.replace(/(^\s*)|(\s*$)/g, "");
}
var croppicHeaderOptions = {
	cropUrl : 'cropimg',
	customUploadButtonId : 'cropContainerHeaderButton',
	modal : false,
	processInline : true,
	outputUrlId : 'backcroppic'
// loaderHtml : '<div class="loader bubblingG"><span
// id="bubblingG_1"></span><span id="bubblingG_2"></span><span
// id="bubblingG_3"></span></div> '
// onBeforeImgUpload : function() {
// console.log('onBeforeImgUpload')
// },
// onAfterImgUpload : function() {
// console.log('onAfterImgUpload')
// },
// onImgDrag : function() {
// console.log('onImgDrag')
// },
// onImgZoom : function() {
// console.log('onImgZoom')
// },
// onBeforeImgCrop : function() {
// console.log('onBeforeImgCrop')
// },
// onAfterImgCrop : function() {
// console.log('onAfterImgCrop')
// },
// onReset : function() {
// console.log('onReset')
// },
// onError : function(errormessage) {
// console.log('onError:' + errormessage)
// }
}
var rotateCroppicHeaderOptions = {
	cropUrl : 'cropimg',
	customUploadButtonId : 'rotateCropContainerHeaderButton',
	modal : false,
	processInline : true,
	outputUrlId : 'backrotatecroppic'
// loaderHtml : '<div class="loader bubblingG"><span
// id="bubblingG_1"></span><span id="bubblingG_2"></span><span
// id="bubblingG_3"></span></div> '
}
var croppic = new Croppic('croppic', croppicHeaderOptions);
var rotateCroppic = new Croppic('rotateCroppic', rotateCroppicHeaderOptions);
function saverotate() {
	var backrotatecroppic = $("#backrotatecroppic").val();
	if (backrotatecroppic == "") {
		alert("请剪切好图片再试");
		return false;
	}
	backrotatecroppic = backrotatecroppic.substr(
			backrotatecroppic.indexOf(",") + 1, backrotatecroppic.length);
	$.ajax({
		url : "saveRotateCustom",
		type : "post",
		data : {
			rotatePic : backrotatecroppic,
			wid : $("#currentwebsite").val()
		},
		success : function(msg) {
			if (msg) {
				alert("新增成功");
				location.reload();
			} else {
				alert("新增失败，请重试");
			}
		}
	});
}

//function savedrag() {
//	var backrotatecroppic = $("#backrotatecroppic").val();
//	if (backrotatecroppic == "") {
//		alert("请剪切好图片再试");
//		return false;
//	}
//	backrotatecroppic = backrotatecroppic.substr(
//			backrotatecroppic.indexOf(",") + 1, backrotatecroppic.length);
//	$.ajax({
//		url : "saveDragPic",
//		type : "post",
//		data : {
//			dragPic : backrotatecroppic,
//			wid : $("#currentwebsite").val()
//		},
//		success : function(msg) {
//			if (msg) {
//				alert("新增成功");
//				location.reload();
//			} else {
//				alert("新增失败，请重试");
//			}
//		}
//	});
//}
$('#captcha-div2-input').keyup(function() {
	var str = trim($(this).val());
	var arr = str.split('');
	var arr_len = arr.length;
	if (arr_len > 7) {
		$(this).val(str.substring(0, 7));
		return;
	}
	var wordpool = $('#captcha-div2-wordpool');
	wordpool.children().remove()
	for (var i = 0; i < arr_len; i++) {
		var ar = arr[i];
		if (ar == "" || ar == " ") {
			continue;
		}
		;
		var div = document.createElement('div');
		div.className = 'word';
		$(div).click(function() {
			if (this.className.indexOf('wordsel') != -1) {
				this.className = 'word';
			} else {
				this.className += ' wordsel';
			}
		});
		var span = document.createElement('span');
		div.appendChild(span);
		span.innerHTML = ar;
		wordpool.append(div);
	}
	;
}).keydown(function(event) {
	if (event.keyCode == 32) {
		return stopDefault();
	}
});
(function() {
	var par_input = $('#captcha-div4-inputpool');
	par_input.children('.div4-add').hover(function() {
		$(this).children().addClass('sel');
	}, function() {
		$(this).children().removeClass('sel');
	}).click(function() {
		if (++smpic_numbers == 3) {
			$(this).css('display', 'none');
		}
		addsmallpic();

	});
	par_input.find('.sub').click(function() {
		var block = $(this).parent().css('display', 'none');
		block.find('input[type=file]').attr('value', '');
		block.children('.div4-img').html('').append('<img/>');
		block.children('.easyui-slider').slider('setValue', 100);
		--smpic_numbers;
		par_input.children('.div4-add').css('display', 'block');
	});
	$.post("../group/getAdGroups", {}, function(result) {
		var options = "";
		for (var i = 0; i < result.length; i++) {
			var item = result[i];
			options += '<option value="' + item.gid + '">' + item.gname
					+ '</option>';
		}
		if (options == "") {
			$("#adgroups").html('<option value="">无广告组</option>');
		} else {
			$("#adgroups").html(options);
		}
	}, "json");
})();
function addsmallpic() {
	var blocks = $('#captcha-div4-inputpool .sub').parent();
	var blocks_len = blocks.length;
	for (var i = 0; i < blocks_len; i++) {
		var block = blocks[i];
		if (block.style.display != 'block') {
			$(block).css('display', 'block').insertBefore(
					$('#captcha-div4-inputpool .div4-add'));
			break;
		}
	}
}
function updateCtypeAndSkin() {
	var currentwebsite = $("#currentwebsite").val();
	var val = $('input:radio[name="skin"]:checked').val();
	var ctype = $('input:radio[name="ctype"]:checked').val();
	$.ajax({
		type : "post",
		url : "updateCtypeAndSkin",
		data : {
			wid : currentwebsite,
			skin : val,
			ctype : ctype
		},
		success : function(data) {
			if (data) {
				alert("保存成功");
			} else {
				alert("保存失败");
			}
		}
	});
}
changewebsite();
function changewebsite() {
	var websiteTmp = $("#currentwebsite").val();
	var website = parseInt(websiteTmp.replace(",",""))
	var cversion = $("#currentwebsite  option:selected").attr("cversion");
	console.log(cversion);
	$("#cversion").html(cversion);
	var cversionMapper = eval($("#cversionMapper").val());
	console.log(cversionMapper);
	for(var i = 0;i < cversionMapper.length;i++){
		if(cversion == cversionMapper[i].cversion){
			var ctype = cversionMapper[i].ctype.split(",");
			var noctype = cversionMapper[i].noctype.split(",");
			for(var i = 0;i<ctype.length;i++){
				$("div[ctype='"+ctype[i]+"']").show();
			}
			for(var j = 0;j<noctype.length;j++){
				$("div[ctype='"+noctype[j]+"']").hide();
			}
			 if(cversion == '5.0.1'){
                 $("#skin_black").parent().parent().show();
	         }else{
	             $("#skin_black").parent().parent().hide();
	         }

		}
	}
	$.ajax({
		type : "post",
		url : "getWebsiteCtypeSkin",
		data : {
			wid : website
		},
		success : function(data) {
			var arr = data.split(",");
			$('input:radio[value="' + arr[0] + '"]').click();
			$('input:radio[value="' + arr[1] + '"]').click();
		}
	});
}
function imgText() {
	var currentwebsite = $("#currentwebsite").val().replace(",","");
	window.location.href = 'imgText?currentwebsite=' + currentwebsite;
}
function rotateManage() {
	var currentwebsite = $("#currentwebsite").val().replace(",","");
	window.location.href = 'rotateManage?currentwebsite=' + currentwebsite;
}
function groupList() {
	window.location.href = '../group/groupList';
}