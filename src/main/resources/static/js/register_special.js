﻿/// <reference path="../script/jquery-1.8.0.min.js" />
/// <reference path="jslib.js" />
/// <reference path="../script/jquery.easyui.min.js" />

String.prototype.trim = function () { return this.replace(/(^\s*)|(\s*$)/g, ""); };

$(document).ready(function ()
{
    
    var touclick_post_data = {
        website_type: -1, //网站类型
        website_name: null,
        website_domain: null,
    };
    var locker = new lock();
    (function ()
    {
        var win_height = $(window).height();
        win_height = win_height < 850 ? 850 : win_height;
        var win_width = $(window).width();
        $('#contain').width(win_width);
    })();//页面高度
    $('.block input[type=text]').val('');
    $('#website_type').blur(function ()
    {
        if (touclick_post_data.website_type === -1)
        {
            note_to_false(this);
        } else
        {
            note_to_ok(this);
        }
    }).focus(function ()
    {
        //note_to_normal(this);
    }).find('input[type=radio]').click(function ()
    {
        //网站类型
        var _this = $(this);
        var tou_id = parseInt(_this.attr('tou_id'));
        if (touclick_post_data.website_type === -1)
        {
            note_to_ok($('#website_type')[0]);
        } else if (touclick_post_data.website_type != tou_id)
        {
            var website_type = $('#website_type');
            website_type.find('input[tou_id=' + touclick_post_data.website_type + ']').attr('checked', false);
            website_type.find('input[tou_id=' + tou_id + ']').attr('checked', true);
        }
        touclick_post_data.website_type = tou_id;
    }).each(function (index, value)
    {
        $(value).attr('checked', false);
    });
   
    $('#register_button').click(function () {

       

        $(this).removeClass('register_button_focus');
        var isok = true;
        for (var proprity in touclick_post_data) {
            var default_val;
            if (proprity == 'website_type') {
                default_val = -1;
            } else {
                default_val = null;
            }
        }
        //检查网站类型是否选择
        if (window.website_types.length === 0) {
            isok &= false;
            note_to_false(website_realtype_hidden[0]);
        } else {
            touclick_post_data['website_realtype'] = window.website_types.join(',');
        }
        //

        if (isok === true) {
            window.TouClick.Start({
                website_key: '212d8812-46ca-4554-b370-4d9ffc82c5f5',
                position_code: 0,
                onSuccess: function (args, check_obj) {
                	var wtype='';
                	$('#website_realtype .selected').each(function(){
                		if(typeof($(this).attr('tou_id'))!='undefined'){
                			wtype=wtype+$(this).attr('tou_id')+',';
                		}
                	});
                	var reg=/,$/gi;
                	wtype=wtype.replace(reg,"");
                	$('#wtype').val(wtype);
                	var formdata = $('form').serialize();
                    touclick_post_data.check_key = check_obj.check_key;
                    touclick_post_data.check_address = check_obj.check_address[0] + "," + check_obj.check_address[1];
                    locker.show();
                    TouClick.Close();
                    $.post('member/regist_special',formdata, function (data) {
                    	console.log(data);
                        locker.close();
                        if (data && data.isok === true) {
                            var div = document.createElement('div');
                            div.id = 'register_success';
                            div.style.padding = "10px";
                            document.body.appendChild(div);
                            $(div).window({
                                width: 550,
                                height: 470,
                                modal: true,
                                collapsible: false,
                                minimizable: false,
                                title: "注册成功",
                                onClose: function () {
                                    window.location.href = "http://www.touclick.com";
                                },
                                onOpen: function () {
                                    window.key_obj = data;
                                    delete window.key_obj.isok;
                                }
                            });
                            $(div).window('refresh', 'success.html');

                        }
                        else {
                            $.messager.alert('注册页提醒', '没有注册成功,可能是系统出错,请联系我们,谢谢<br/>qq:874767484');
                        }
                    }, 'json');
                }
            });
        }
        else {
            $.messager.alert('注册页提醒', '请检查不合格输入项');
        }
    }).focus(function () {
        $(this).addClass('register_button_focus');
    }).blur(function () {
        $(this).removeClass('register_button_focus');
    }).keydown(function (e) {
        if (e.keyCode == 13) {
            $(this).addClass('register_button_focus');
            $(this).click();
        }
    });


    $('.block input[tou_id=website_domain]').blur(function ()
    {
        var url = $(this).val().trim();
        var _this = this;
        var reg = "^((http)?://)?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\.){3}[0-9]{1,3}|([0-9a-z_!~*'()-]+\.)*([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.[a-z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
        var re = new RegExp(reg);
        if ( re.test(url) === true)
        {
            $.post('member/existWebSite', { way: 'exist_website', waddress: url },
                function (data)
                {
                    if (data && data.isexist === false)
                    {
                        touclick_post_data.website_domain = url;
                        note_to_ok(_this);
                    } else
                    {
                        //已存在
                        var item_note = $(_this).parents('.item').children('.item-note');
                        item_note.children().css({ 'display': 'none' });
                        item_note.find('span.other-note-not1').css({ 'display': 'inline' });
                    }
                }, 'json');
        } else
        {
            touclick_post_data.website_domain = null;
            note_to_false(this);
        }
    }).focus(function ()
    {
        note_to_normal(this);
    });


    //检测是否包含 邀请码
    var vars = [], hash;
    var q = document.URL.split('?')[1];
    if (q != undefined) {
        q = q.split('&');
        for (var i = 0; i < q.length; i++) {
            hash = q[i].split('=');
            vars.push(hash[1]);
            vars[hash[0]] = hash[1];
        }
    }

});
//提醒切换
function note_to_ok(item_input)
{
    var item_note = $(item_input).parents('.item').children('.item-note');
    item_note.children().css({ 'display': 'none' });
    item_note.find('.other-note-yes').css({ 'display': 'block' });
}
function note_to_false(item_input, id)
{
    var item_note = $(item_input).parents('.item').children('.item-note');
    item_note.children().css({ 'display': 'none' });
    item_note.find('span.other-note-not').css({ 'display': 'inline' });
}
function note_to_normal(item_input)
{
    var item_note = $(item_input).parents('.item').children('.item-note');
    item_note.children().css({ 'display': 'none' });
    item_note.find('span.note').css({ 'display': 'inline' });
}
function lock()
{
    var div_dom = null;
    this.show = function (isie6)
    {
        div_dom.style.display = 'block'
        if (isie6 === true) { var style = div_dom.style; style.position = 'absolute'; style.height = document.body.clientHeight + 'px'; };
        $('#register_loading').css({ 'display': 'block' });
    };
    this.close = function ()
    {
        div_dom.style.display = 'none'
        $('#register_loading').css({ 'display': 'none' });
    };
    div_dom = document.createElement("div");
    var style = div_dom.style;
    style.display = "none";
    style.position = "fixed";
    style.left = "0px";
    style.top = "0px";
    style.zIndex = "9999";
    style.height = style.width = "100%";
    var back_shade_color = '#fff';
    if (typeof (background_shade_color) != 'undefined')
    {
        back_shade_color = background_shade_color
    };
    style.backgroundColor = back_shade_color;
   
    style.opacity = "0.5";
    style.filter = "alpha(opacity=50)";
    document.body.appendChild(div_dom)
};
