<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>

<#include "../common/base.ftl">
<style>
	body {
		overflow: auto;
	}
</style>
</head>
<body>
	<table class="table table-bordered table-hover" id="tab">
		<thead>
			<tr>
				<th>网站名称</th>
				<th>网站地址</th>
				<th>总计请求量</th>
				<th>日均请求量</th>
				<th>时均请求量</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${websiteList }" var="website">
				<tr>
					<td><a href="#" onclick="javascript:login('${website.wid}')">${website.wname }</a></td>
					<td><a href="http://${website.waddress}" target="_blank">${website.waddress }</a></td>
					<td>${website.all }</td>
					<td>${website.day }</td>
					<td>${website.hour }</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
<script type="text/javascript">
	$(function() {
		$('#tab').dataTable({
			"bPaginate" : true,
			"bLengthChange" : true,
			"bFilter" : true,
			"bSort" : false,
			"bInfo" : true,
			"bAutoWidth" : false,
			"oLanguage" : {
				"sProcessing" : "正在加载中......",
				"sLengthMenu" : "每页显示 _MENU_ 条记录",
				"sZeroRecords" : "对不起，查询不到相关数据！",
				"sEmptyTable" : "表中无数据存在！",
				"sInfo" : "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录",
				"sInfoFiltered" : "数据表中共为 _MAX_ 条记录",
				"sSearch" : "搜索",
				"oPaginate" : {
					"sFirst" : "首页",
					"sPrevious" : "上一页",
					"sNext" : "下一页",
					"sLast" : "末页"
				}
			}
		});
	});
	function login(wid) {
		$.post("${ctx}/member/websitelogin", {
			wid : wid
		}, function(returnvalue) {
			switch (returnvalue) {
			case 1:
				parent.document.location.href = "${ctx}/member/index";
				break;
			case 2:
				alert("\"用户名\"或\"密码\"错误，请重新输入!");
				// alert("\"用户名\"不存在,请重新输入!");
				document.getElementById("password").value = "";
				document.getElementById("password").focus();
				break;
			default:
				alert("服务器无响应，请稍候重试！");
			}
		});
	}
</script>
</html>