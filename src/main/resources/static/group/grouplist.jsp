<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<head>
<#include "../common/base.ftl">
<style>
body {
	overflow: scroll;
}
</style>
<script type="text/javascript">
function del(wid){
	$.ajax({
		type:'post',
		url:'${ctx}/website/delWebsite',
		data:{
			wid:wid
		},
		success:function(data){
			if(data){
				alert("删除成功");
				location.reload();
			}else{
				alert("删除失败,请重试");
			}
		}
	});
}
function save(){
	var gname = $("#gname").val();
	var count = $("#count").val();
	if(gname==""){
		alert("广告组名称不能为空");
		return false;
	}else if(count==""){
		alert("每日展示量不能为空");
		return false;
	}else{
		$.ajax({
			type:'post',
			url:'${ctx}/group/save',
			data:{
				gname:gname,
				count:count
			},
			success:function(data){
				if(data){
					alert("保存成功");
					location.reload();
				}else{
					alert("保存失败,请重试");
				}
			}
		});
	}
}
</script>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>广告组列表</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">广告组列表</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">广告组列表</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="account">
								<thead>
									<tr>
										<th>广告组名称</th>
										<th>每日展示量(次/日)</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="list" items="${groupList}">
										<tr>
											<td>${list.gname}</td>
											<td>${list.count}</td>
											<td>
												<button class="btn btn-danger btn-sm" onclick="del('${list.gid}')" type="button">删除</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
						<div class="box-footer btn-group">
							<button class="btn btn-default" data-toggle="modal"
								data-target="#groupModel">新增广告组</button>
							<!-- <button class="btn btn-default" onclick="history.go(-1)">返回上一页</button> -->
						</div>
					</div>
					<!-- /.box -->
				</section>
				<!-- ./col -->
			</div>
			<!-- /.row -->
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="groupModel" tabindex="-1" role="dialog" data-backdrop="false"
				aria-labelledby="websiteModel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="websiteModel">新增广告组</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>广告组名称:</label> 
								<input class="form-control pull-right" id="gname"/>
							</div>
							<div class="form-group">
								<label>广告组每日展示量(次/日):</label>
								<input class="form-control pull-right" id="count" onkeyup="value=value.replace(/[^\d]/g,'')"/> 
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary"
								onclick="save();">提交</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal -->
			</div>
		</section>
		<!-- /.content -->
	</div>
</body>
</html>
