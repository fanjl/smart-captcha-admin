<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<#include "../common/base.ftl">
	<style>
body {
	overflow: scroll;
}
</style>
<script type="text/javascript">
	function save(){
		$.ajax({
			type:'post',
			url:'${ctx}/account/addaccount',
			data:$('#accountform').serialize(),
			async:false,
			success:function(data){
				if(data=='true'){
					alert('新增成功');
					document.location.href='${ctx}/account/accountlist';
				}else{
					alert('该账号已存在');
				}
			}
		});
	}
	function del(id){
		if(window.confirm('确认删除该帐号吗?')){
			$.ajax({
				type:'post',
				url:'${ctx}/account/delaccount',
				data:{
					id:id
				},
				async:false,
				success:function(data){
					if(data=='true'){
						alert('删除成功');
						document.location.href='${ctx}/account/accountlist';
					}else{
						alert('删除失败');
					}
				}
			});
		}
	}
	function defaultAccount(id){
		$.ajax({
			type:'post',
			url:'${ctx}/account/defaultAccount',
			data:{
				id:id
			},
			async:false,
			success:function(data){
				if(data=='true'){
					alert('设置成功');
					document.location.href='${ctx}/account/accountlist';
				}else{
					alert('设置失败');
				}
			}
		});
	}
</script>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>账户管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">账户管理</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">账户列表</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="account">
								<thead>
									<tr>
										<th>账号类别</th>
										<th>账号</th>
										<th>状态</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="list" items="${list}">
										<tr>
											<td>${list.accounttype}</td>
											<td>${list.value}</td>
											<td>${list.useflag==1?'使用中':''}</td>
											<td>
												<c:if test="${list.useflag!=1}">	
													<button class="btn btn-primary btn-sm" onclick="defaultAccount('${list.id}')" type="button">默认使用</button>
													<button class="btn btn-danger btn-sm" onclick="del('${list.id}')" type="button">删除</button>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
						<div class="box-footer btn-group">
							<button class="btn btn-default" data-toggle="modal"
								data-target="#myModal">新增账号</button>
							<button class="btn btn-default" onclick="history.go(-1)">返回上一页</button>
						</div>
					</div>
					<!-- /.box -->
				</section>
			</div>
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">新增交易账户</h4>
						</div>
						<div class="modal-body">
						<form action="" id="accountform">
							<div class="form-group">
								<label>账号类别:</label> <select class="form-control pull-right"
									id="accounttype" name="accounttype">
									<option value="1">支付宝</option>
								</select>
							</div>
							<div class="form-group">
								<label>账号:</label> <input class="form-control pull-right"
									id="account" name="account" />
							</div>
						</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary"
								onclick="save();">提交</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal -->
			</div>
		</section>
	</div>
	<script type="text/javascript">
	var accounttable;
		$(function() {
			$('#reservation').daterangepicker();
			accounttable = $('#account').dataTable({
				"bPaginate" : true,
				"bLengthChange" : false,
				"bFilter" : false,
				"bSort" : true,
				"bInfo" : true,
				"bAutoWidth" : false,
				"oLanguage" : {
					"sProcessing" : "正在加载中......",
					"sLengthMenu" : "每页显示 _MENU_ 条记录",
					"sZeroRecords" : "对不起，查询不到相关数据！",
					"sEmptyTable" : "表中无数据存在！",
					"sInfo" : "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录",
					"sInfoFiltered" : "数据表中共为 _MAX_ 条记录",
					"sSearch" : "搜索",
					"oPaginate" : {
						"sFirst" : "首页",
						"sPrevious" : "上一页",
						"sNext" : "下一页",
						"sLast" : "末页"
					}
				}
			});
		});
	</script>
</body>
</html>