<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<head>
<#include "../common/base.ftl">
<link rel="stylesheet"
	href="${ctx }/plugins/ladda/ladda-themeless.min.css">
<style>
body {
	overflow: scroll;
}
</style>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>网站列表</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">网站列表</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">频次设置</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<div>
								<span class="sli font left">时间段:</span> <input id="slider1"
									width="80px" data-slider-id='slider1' type="text"
									data-slider-min="0" data-slider-max="100" data-slider-step="1"
									data-slider-value="100" /><span class="sli font right" style="margin-left: 10px;">分钟</span>
							</div>
							<div>
								<span class="sli font left">频次:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><input id="slider2"
									data-slider-id='ex1Slider' type="text" data-slider-min="0"
									data-slider-max="100" data-slider-step="1"
									data-slider-value="100" />
							</div>
							<div>
								<button class="btn btn-info" onclick="save();">保存</button>
							</div>
						</div>
					</div>
					<!-- /.box -->
				</section>
				<!-- ./col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
</body>
<script src="${ctx }/plugins/ladda/spin.min.js"></script>
<script src="${ctx }/plugins/ladda/ladda.min.js"></script>
<link href="${ctx }/plugins/bootstrap-slider/bootstrap-slider.css"
	rel="stylesheet">
<script src="${ctx }/plugins/bootstrap-slider/bootstrap-slider.js"></script>
<script type="text/javascript">
	var sliderOptions = {
		formatter : function(value) {
			return '当前透明度: ' + value;
		}
	};
	var slider1 = new Slider('#slider1', sliderOptions);
	var slider2 = new Slider('#slider2', sliderOptions);
	function save(){
		$.ajax({
			url : 'security/saveVisitCount',
			type : 'post',
			data : {
				time : $("#slider1").val(),
				count : $("#slider2").val()
			},
			success : function(data){
				if(data){
					alert("保存成功");
				}else{
					alert("保存失败");
				}
			}
		});
	}
</script>
</html>
