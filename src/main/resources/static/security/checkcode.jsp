<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<head>
<#include "../common/base.ftl">
<link rel="stylesheet" href="${ctx }/plugins/ladda/ladda-themeless.min.css">
<style>
body {
	overflow: scroll;
}
</style>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>网站列表</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">网站列表</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">网站列表</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="account">
								<thead>
									<tr>
										<th>网站名称</th>
										<th>网址</th>
										<th>平台</th>
										<th>公钥</th>
										<th>私钥</th>
										<th>验证状态</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="list" items="${list}">
										<tr>
											<td>${list.wname}</td>
											<td>${list.waddress}</td>
											<td>${list.platform}</td>
											<td>${list.pubkey}</td>
											<%-- <td>${fn:substring(list.token,0,8)}******${fn:substring(list.token,24,36)}</td> --%>
											<td>${list.token }</td>
											<td>${list.verifyflag==1?'已验证':'未验证' }</td>
											<td>
												<%-- <button class="btn btn-primary btn-sm"
													onclick="resetToken('${list.wid}')" type="button">重置私钥</button> --%>
												<c:if test="${list.verifyflag!=1 }">
													<button class="btn btn-primary btn-sm"
														onclick="clickverify('${list.wid}','${list.waddress}')"
														type="button">验证网站</button>
													<button class="btn btn-primary btn-sm"
														onclick="alert('验证提示:请将下载的文件放在网站的根目录下，以便验证网站');window.open('../member/downloadVerify?waddress=${list.waddress}');"
														type="button">下载验证文件</button>
												</c:if>
												<button class="btn btn-danger btn-sm"
													onclick="del('${list.wid}')" type="button">删除</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
						<div class="box-footer btn-group">
							<button class="btn btn-default" data-toggle="modal"
								data-target="#websiteModel">新增网站</button>
							<button class="btn btn-default" onclick="history.go(-1)">返回上一页</button>
						</div>
					</div>
					<!-- /.box -->
				</section>
				<!-- ./col -->
			</div>
			<!-- /.row -->
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="websiteModel" tabindex="-1" role="dialog"
				data-backdrop="false" aria-labelledby="websiteModel"
				aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="websiteModel">新增网站</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>网站名称:</label> <input class="form-control pull-right"
									id="wname" />
							</div>
							<div class="form-group">
								<label>网站地址:</label> <input class="form-control pull-right"
									id="waddress" />
							</div>
							<div class="form-group">
								<font color="red">验证提示:请将下载的文件放在网站的根目录下，以便验证网站</font> <a
									href="javascript:downloadverify();">点击下载验证文件</a>
							</div>
							<div class="form-group">
								<label>平台:</label> <select class="form-control pull-right"
									id="platform">
									<option value="discuz">Discuz</option>
									<option value="pw">PHPWind</option>
									<option value="api">自建网站</option>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary ladda-button" onclick="save();" data-style="slide-up">提交</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal -->
			</div>
		</section>
		<!-- /.content -->
	</div>
</body>
<script src="${ctx }/plugins/ladda/spin.min.js"></script>
<script src="${ctx }/plugins/ladda/ladda.min.js"></script>
<script type="text/javascript" src="${ctx }/js/website/website.js?v=1.000"></script>
</html>
