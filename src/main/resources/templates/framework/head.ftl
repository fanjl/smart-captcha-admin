<script src="//cdn.bootcss.com/vue/1.0.24/vue.js"></script>
<script src="/js/login/member.js"></script>
<header class="main-header">
	<!-- Logo -->
			<div  class="logo"  style="background-position:center center;background-image: url('/image/leftmenu/top-logo.png'); background-repeat:no-repeat;"></div>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
			role="button"> <span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
			<li class="dropdown user user-menu">
                <span  name='level' v-model="userInfo.level"></span>

			</li>
			<li class="dropdown user user-menu" >
			<a href="/member/updateMember"  target="mainFrame" class="dropdown-toggle" >
				 <img src="/image/top-shezhi.png" class="user-image" alt="User Image" />
				 <span class="hidden-xs">设置</span>
				 </a>
			</li>
			<li class="dropdown user user-menu" >
			<a href="/member/logout" class="dropdown-toggle" >
				<img src="/image/top-tuichu.png" class="user-image" alt="User Image" />
				<span class="hidden-xs">退出</span>
				</a>
			</li>
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown">
					 <img
						src="/image/logo.png" class="user-image" alt="User Image" />
						<span class="hidden-xs" v-model="userInfo.realName"></span>
				</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header"><img src="/image/logo.png"
							class="img-circle" alt="User Image" />
							<p>
                                <span class="hidden-xs" v-model="userInfo.realName"></span> <small>注册时间:<p v-model="userInfo.createtime"></p></small>
							</p></li>
						<!-- Menu Body -->
						<!-- <li class="user-body">
							<div class="col-xs-4 text-center">
								<a href="#">Followers</a>
							</div>
							<div class="col-xs-4 text-center">
								<a href="#">Sales</a>
							</div>
							<div class="col-xs-4 text-center">
								<a href="#">Friends</a>
							</div>
						</li> -->
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">返回</a>
							</div>
							<div class="pull-left" style="position: relative; left: 20px;">
								<a href="/member/updateMember" target="mainFrame" class="btn btn-default btn-flat">修改个人信息</a>
							</div>
							<div class="pull-right">
								<a href="/member/logout"
									class="btn btn-default btn-flat">退出</a>
							</div>
						</li>
					</ul></li>
			</ul>
		</div>
	</nav>
</header>