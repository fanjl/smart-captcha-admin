<!DOCTYPE html>
<html>
<head>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
	<#include "../common/base.ftl">
<script type="text/javascript" type="text/javascript"
	src="../js/highcharts/highcharts.js"></script>
<script type="text/javascript"
	src="../js/highcharts/modules/exporting.js"></script>
<style>
body{
	overflow: scroll;
}
</style>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>控制面板</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">控制面板</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<!-- ./col -->
				<div class="col-lg-3 col-xs-3">
					<!-- small box -->
					<div class="small-box bg-green">
						<div class="inner" style="height: 100px;">
							<h3>
								<sup style="font-size: 20px">元</sup>
							</h3>
							<p>收益</p>
						</div>
						<a href="draw/drawMoney" class="small-box-footer">提现<i
							class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<div class="col-lg-3 col-xs-3">
					<!-- small box -->
					<div class="small-box bg-aqua">
						<div class="inner" style="height: 100px;">
							<h3>
								0<sup style="font-size: 20px">个</sup>
							</h3>
							<p>正在投放广告</p>
						</div>
						<a href="#" class="small-box-footer">广告管理<i
							class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<div class="col-lg-3 col-xs-3">
					<!-- small box -->
					<div class="small-box bg-yellow">
						<div class="inner" style="height: 100px;">
							<h3>
								${sv}<sup style='font-size: 20px'>次</sup>
							</h3>
							<p>今日广告完成量</p>
						</div>
						<a href="datacenter/viewAdsData" class="small-box-footer">数据查看
							<i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
				<!-- ./col -->
				<div class="col-lg-3 col-xs-3">
					<!-- small box -->
					<div class="small-box bg-red">
						<div class="inner" style="height: 100px;">
							<h5>最新公告</h5>
							<#--<p style="text-indent: 2em;">-->
								<#--<c:if test="${fn:length(lastnotice.content)>'28'}">  -->
					            	<#--${fn:substring(lastnotice.content,0,28)}...  -->
					            <#--</c:if>-->
								<#--<c:if test="${fn:length(lastnotice.content)<='28'}">  -->
					                <#--${lastnotice.content}-->
					            <#--</c:if>-->
							<#--</p>-->
						</div>
						<a href="notice/noticeList" class="small-box-footer">更多
							<i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
				<!-- ./col -->
			</div>
			<!-- /.row -->
			<!-- Main row -->
			<div class="row">
				<section class="col-lg-12 connectedSortable">
					<!-- Custom tabs (Charts with tabs)-->
					<div class="nav-tabs-custom">
						<!-- Tabs within a box -->
						<ul id="activeul" class="nav nav-tabs pull-right">
							<li datetype="year"><a href="#year-chart" data-toggle="tab"
								id='year'>本年</a></li>
							<li datetype="month"><a href="#month-chart"
								data-toggle="tab" id='month'>本月</a></li>
							<li datetype="day" class="active"><a href="#today-chart"
								data-toggle="tab">今天</a></li>
							<li class="pull-left header"><i class="fa fa-inbox"></i>
								我的网站 <select id="website" onchange="changewebsite();">
									<#--<c:forEach var="websiteList" items="${websiteList}">-->
										<#--<option value="${websiteList.wid }">${websiteList.wname }</option>-->
									<#--</c:forEach>-->
								<#list websiteList as websiteList>
                                    <option value="${websiteList.wid }">${websiteList.wname }</option>
								</#list>
							</select></li>
						</ul>
						<div class="tab-content no-padding">
							<div class="chart tab-pane" id="year-chart"
								style="position: relative; height: 300px;"></div>
							<div class="chart tab-pane" id="month-chart"
								style="position: relative; height: 300px;"></div>
							<div class="chart tab-pane active" id="today-chart"
								style="position: relative; height: 300px;"></div>
						</div>
					</div>
					<!-- /.nav-tabs-custom -->

				</section>

				<section class="col-lg-6 col-xs-6 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">当天数据</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="daytable">
								<thead>
									<tr>
										<th>时间</th>
										<th>请求量</th>
										<th>正确量</th>
										<th>错误量</th>
										<th>完成量</th>
										<th>广告跳转量</th>
										<th>跳转率</th>
									</tr>
								</thead>
								<tbody>
									<#--<#if daydata??>-->
									<#--<#list daydata as daydata>-->
                                    <#--<tr>-->
                                        <#--<td>${daydata.time }</td>-->
                                        <#--<td>${daydata.g}</td>-->
                                        <#--<td>${daydata.VY}</td>-->
                                        <#--<td>${daydata.VN}</td>-->
                                        <#--<td>${daydata.SV}</td>-->
                                        <#--<td>${daydata.AD}</td>-->
                                        <#--<td>${daydata.adr}</td>-->
                                    <#--</tr>-->
									<#--</#list>-->
									<#--</#if>-->

								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- right col -->
				<section class="col-lg-6 col-xs-6 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">当月数据</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="monthtable">
								<thead>
									<tr>
										<th>时间</th>
										<th>请求量</th>
										<th>正确量</th>
										<th>错误量</th>
										<th>完成量</th>
										<th>广告跳转量</th>
										<th>跳转率</th>
									</tr>
								</thead>
								<tbody>
								<#--<#if daydata??>-->
									<#--<#list daydata as daydata>-->
                                    <#--<tr>-->
                                        <#--<td>${daydata.time }</td>-->
                                        <#--<td>${daydata.g}</td>-->
                                        <#--<td>${daydata.VY}</td>-->
                                        <#--<td>${daydata.VN}</td>-->
                                        <#--<td>${daydata.SV}</td>-->
                                        <#--<td>${daydata.AD}</td>-->
                                        <#--<td>${daydata.adr}</td>-->
                                    <#--</tr>-->
									<#--</#list>-->
								<#--</#if>-->
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- right col -->
				<section class="col-lg-6 col-xs-6 connectedSortable">

					<div class="box">
						<div class="box-header">
							<h3 class="box-title">当年数据</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="yeartable">
								<thead>
									<tr>
										<th>时间</th>
										<th>请求量</th>
										<th>正确量</th>
										<th>错误量</th>
										<th>完成量</th>
										<th>广告跳转量</th>
										<th>跳转率</th>
									</tr>
								</thead>
								<tbody>
							<#--<#if daydata??>-->
									<#--<#list daydata as daydata>-->
                                    <#--<tr>-->
                                        <#--<td>${daydata.time }</td>-->
                                        <#--<td>${daydata.g}</td>-->
                                        <#--<td>${daydata.VY}</td>-->
                                        <#--<td>${daydata.VN}</td>-->
                                        <#--<td>${daydata.SV}</td>-->
                                        <#--<td>${daydata.AD}</td>-->
                                        <#--<td>${daydata.adr}</td>-->
                                    <#--</tr>-->
									<#--</#list>-->
							<#--</#if>-->
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- right col -->
			</div>
			<!-- /.row (main row) -->

		</section>
		<!-- /.content -->
	</div>
	<script type="text/javascript">
		var daytable;
		var monthtable;
		var yeartable;
		var options;
		var time_arr = [];
		var g_arr = [];
		var vy_arr = [];
		var vn_arr = [];
		var sv_arr = [];
		var asv_arr = [];
		var ad_arr = [];
		var options;
		var monthchart;
		var yearchat;
		$(function() {
			Highcharts.setOptions({
				global : {
					useUTC : false
				}
			});
			setdata(eval('${jsondaydata}'));
			setoptions('%Y-%m-%d %H时', 5, 'today-chart');
			var daychart = new Highcharts.Chart(options);

			settableoptions(eval('${jsondaydata}'), '%Y-%m-%d %H时');
			daytable = $('#daytable').dataTable(options);
			settableoptions(eval('${jsonmonthdata}'), '%Y-%m-%d');
			monthtable = $('#monthtable').dataTable(options);
			settableoptions(eval('${jsonyeardata}'), '%Y-%m');
			yeartable = $('#yeartable').dataTable(options);
		});
	</script>
	<script type="text/javascript" src="../js/member/websiter.js"></script>
</body>
</html>
