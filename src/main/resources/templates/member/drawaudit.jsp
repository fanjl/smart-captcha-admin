<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<#include "../common/base.ftl">
<style type="text/css">
.tip-section2 {
  background-color: #fffeee;
  color: #ff8a10;
  font: 14px/18px "microsoft yahei";
  padding: 6px 10px 6px 14px;
}
.pdl-10 {
  padding-left: 10px !important;
}
.pdr-10 {
  padding-right: 10px !important;
}
.ued-table-3 {
  color: #949393;
  font-size: 12px;
  font-family: "microsoft yahei";
  border: 1px solid #e1e1e1;
  border-collapse: collapse;
  max-width: 100%;
  background-color: transparent;
  border-spacing: 0;
}
.ued-table-3 th {
  background: #f3f3f3;
  line-height: 32px;
  padding: 0 15px;
  border-right: 1px solid #e1e1e1;
  border-bottom: 1px solid #e1e1e1;
  vertical-align: middle;
  text-align: center;
}
</style>
<script type="text/javascript">
var pay = function (money){
	$("#feeId").text(money);
	$('#payModal').modal('show');
}
</script>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>提现审核</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">提现审核</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">提现审核</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="earnings">
								<thead>
									<tr>
										<th>时间</th>
										<th>提取人</th>
										<th>收益</th>
										<th>提取状态</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="list" items="${list}">
										<tr>
											<td><fmt:formatDate pattern="yyyy年MM月dd日 hh:mm:ss"
													value="${list.createtime}" /></td>
											<td>${list.realname}</td>
											<td>${list.money}</td>
											<td>
												${list.drawflag==2?'待审核':(list.drawflag==1?'提取成功':'提取失败')}
											</td>
											<td>
												<c:if test="${list.drawflag==2}">
													<a href="${ctx}/pay/payEarnings?drawid=${list.drawid}" target="_blank" onclick="pay('${list.money}');">支付</a>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
						<div class="box-footer btn-group">
							<button class="btn btn-default" onclick="history.go(-1)">返回上一页</button>
						</div>
					</div>
					<!-- /.box -->
				</section>
			</div>
			<!-- 模态框（Modal） -->
			<div class="modal fade" id="payModal" tabindex="-1" role="dialog"
				data-backdrop="false" aria-labelledby="payModal"
				aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">支付等待</h4>
						</div>
						<div class="modal-body">
							<div class="tip-section2">支付完成前请不要关闭此页面</div>
							<div class="pdl-10 pdr-10">
					            <table width="100%" cellpadding="0" cellspacing="0" class="ued-table-3">
					                <tbody>
					                <tr>
					                    <th>交费号码</th>
					                    <th>交费金额</th>
					                    <th>支付类型</th>
					                </tr>
					                <tr>
					                    <td id="nameId">支付宝支付</td>
										<td><span id="feeId" class="red"></span></td>
										<td><img id="bankFlag" src="${ctx }/image/pay/alipay.gif"></td>
					                </tr>
					            </tbody></table>
					        </div>
						</div>
						<div class="modal-footer" style="text-align: center;">
							<button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:location.reload();">支付成功</button>	
							<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="javascript:location.reload();">支付遇到问题</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal -->
			</div>
		</section>
	</div>
</body>
<script type="text/javascript">
	$(function(){
		$('#earnings').dataTable({
			"bPaginate": true,
	          "bLengthChange": false,
	          "bFilter": false,
	          "bSort": true,
	          "bInfo": true,
	          "bAutoWidth": false,
	          "oLanguage": { 
                  "sProcessing": "正在加载中......", 
                  "sLengthMenu": "每页显示 _MENU_ 条记录", 
                  "sZeroRecords": "对不起，查询不到相关数据！", 
                  "sEmptyTable": "表中无数据存在！", 
                  "sInfo": "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录", 
                  "sInfoFiltered": "数据表中共为 _MAX_ 条记录", 
                  "sSearch": "搜索", 
                  "oPaginate":  
                  { 
                      "sFirst": "首页", 
                      "sPrevious": "上一页", 
                      "sNext": "下一页", 
                      "sLast": "末页" 
                  } 
              }
		});
	});
</script>
</html>