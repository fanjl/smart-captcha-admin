<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<#include "../common/base.ftl">
<script type="text/javascript" type="text/javascript"
	src="${ctx}/js/highcharts/highcharts.js"></script>
<script type="text/javascript"
	src="${ctx}/js/highcharts/modules/exporting.js"></script>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>控制面板</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">控制面板</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-lg-3 col-xs-3">
					<!-- small box -->
					<div class="small-box bg-aqua">
						<div class="inner">
							<h3>
								150<sup style="font-size: 20px">次</sup>
							</h3>
							<p>投放广告</p>
						</div>
						<div class="icon">
							<i class="ion ion-bag"></i>
						</div>
						<a href="#" class="small-box-footer">创建广告组</a>
					</div>
				</div>
				<!-- ./col -->
				<div class="col-lg-3 col-xs-3">
					<!-- small box -->
					<div class="small-box bg-green">
						<div class="inner">
							<h3>
								53<sup style="font-size: 20px">元</sup>
							</h3>
							<p>余额</p>
						</div>
						<div class="icon">
							<i class="ion ion-stats-bars"></i>
						</div>
						<a href="#" class="small-box-footer">提取</a>
					</div>
				</div>
				<div class="col-lg-3 col-xs-3">
					<!-- small box -->
					<div class="small-box bg-yellow">
						<div class="inner">
							<h3>44</h3>
							<p>User Registrations</p>
						</div>
						<div class="icon">
							<i class="ion ion-person-add"></i>
						</div>
						<a href="#" class="small-box-footer">More info <i
							class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<!-- ./col -->
				<div class="col-lg-3 col-xs-3">
					<!-- small box -->
					<div class="small-box bg-red">
						<div class="inner">
							<h3>65</h3>
							<p>Unique Visitors</p>
						</div>
						<div class="icon">
							<i class="ion ion-pie-graph"></i>
						</div>
						<a href="#" class="small-box-footer">More info <i
							class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<!-- ./col -->
			</div>
			<!-- /.row -->
			<!-- Main row -->
			<div class="row">
				<section class="col-lg-12 connectedSortable">
					<!-- Custom tabs (Charts with tabs)-->
					<div class="nav-tabs-custom">
						<!-- Tabs within a box -->
						<ul class="nav nav-tabs pull-right">
							<li><a href="#year-chart" data-toggle="tab" id='year'>本年</a></li>
							<li><a href="#month-chart" data-toggle="tab" id='month'>本月</a></li>
							<li class="active"><a href="#today-chart" data-toggle="tab">今天</a></li>
							<li class="pull-left header"><i class="fa fa-inbox"></i>
								我的广告</li>
						</ul>
						<div class="tab-content no-padding">
							<div class="chart tab-pane" id="year-chart"
								style="position: relative; height: 300px;"></div>
							<div class="chart tab-pane" id="month-chart"
								style="position: relative; height: 300px;"></div>
							<div class="chart tab-pane active" id="today-chart"
								style="position: relative; height: 300px;"></div>
						</div>
					</div>
					<!-- /.nav-tabs-custom -->

				</section>

				<section class="col-lg-6 col-xs-6 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">当天数据</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-condensed" id="daytable">
								<thead>
									<tr>
										<th style="width: 20%">时间</th>
										<th style="width: 16%">展示量</th>
										<th style="width: 16%">互动量</th>
										<th style="width: 16%">互动完成量</th>
										<th style="width: 16%">跳转量</th>
										<th style="width: 16%">跳转率</th>
									</tr>
									<c:forEach var="daydata" items="${daydata}">
										<tr>
											<td>${daydata.time }</td>
											<td>${daydata.g}</td>
											<td>${daydata.vyn}</td>
											<td>${daydata.VY}</td>
											<td>${daydata.AD}</td>
											<td>${daydata.adr}</td>
										</tr>
									</c:forEach>
								</thead>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- right col -->
				<section class="col-lg-6 col-xs-6 connectedSortable">

					<div class="box">
						<div class="box-header">
							<h3 class="box-title">当月数据</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-condensed" id="monthtable">
								<tr>
									<th style="width: 20%">时间</th>
									<th style="width: 16%">展示量</th>
									<th style="width: 16%">互动量</th>
									<th style="width: 16%">互动完成量</th>
									<th style="width: 16%">跳转量</th>
									<th style="width: 16%">跳转率</th>
								</tr>
								<c:forEach var="monthdata" items="${monthdata}">
									<tr>
										<td>${monthdata.time }</td>
										<td>${monthdata.g}</td>
										<td>${monthdata.vyn}</td>
										<td>${monthdata.VY}</td>
										<td>${monthdata.AD}</td>
										<td>${monthdata.adr}</td>
									</tr>
								</c:forEach>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- right col -->
				<section class="col-lg-6 col-xs-6 connectedSortable">

					<div class="box">
						<div class="box-header">
							<h3 class="box-title">当年数据</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-condensed" id="yeartable">
								<tr>
									<th style="width: 20%">时间</th>
									<th style="width: 16%">展示量</th>
									<th style="width: 16%">互动量</th>
									<th style="width: 16%">互动完成量</th>
									<th style="width: 16%">跳转量</th>
									<th style="width: 16%">跳转率</th>
								</tr>
								<c:forEach var="yeardata" items="${yeardata}">
									<tr>
										<td>${yeardata.time }</td>
										<td>${yeardata.g}</td>
										<td>${yeardata.vyn}</td>
										<td>${yeardata.VY}</td>
										<td>${yeardata.AD}</td>
										<td>${yeardata.adr}</td>
									</tr>
								</c:forEach>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- right col -->
			</div>
			<!-- /.row (main row) -->

		</section>
		<!-- /.content -->
	</div>
<script type="text/javascript" src="${ctx}/js/member/advertiser.js"></script>
</body>
</html>
