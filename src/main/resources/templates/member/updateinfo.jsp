<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<#include "../common/base.ftl">
<body>
	<style>
body {
	overflow: scroll;
}

input {
	width: 70%;
}
</style>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>修改个人信息</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">修改个人信息</li>
			</ol>
		</section>

		<section class="content">

			<div class='row'>
				<div class='col-md-6'>
					<div class='box box-info'>
						<div class='box-header'>
							<h3 class='box-title'>个人信息</h3>
							<!-- tools box -->
							<div class="pull-right box-tools">
								<button class="btn btn-info btn-sm" data-widget='collapse'
									data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i>
								</button>
								<button class="btn btn-info btn-sm" data-widget='remove'
									data-toggle="tooltip" title="Remove">
									<i class="fa fa-times"></i>
								</button>
							</div>
							<!-- /. tools -->
						</div>
						<!-- /.box-header -->
						<div class='box-body pad'>
							<form>
								<div class="form-group">
									<label>真实姓名</label>
									<input type="text" id="realname" class="form-control" value="${member.realname }">
								</div>
								<div class="form-group">
									<label>QQ</label>
									<input type="text" id="qq" class="form-control" value="${member.qq }" onkeyup="value=value.replace(/[^\d]/g,'')">
								</div>
								<div class="form-group">
									<label>联系电话</label>
									<input type="text" id="tel" class="form-control" value="${member.tel }">
								</div>
							</form>
						</div>
						<div class="box-footer text-center">
							<button class="btn btn-primary" onclick="javascript:updateinfo();">保存个人信息</button>
						</div>
					</div>
					<!-- /.box -->

				</div>
				<!-- /.col-->
				<div class="col-md-6">
					<div class='box'>
						<div class='box-header'>
							<h3 class='box-title'>修改密码</h3>
							<!-- tools box -->
							<div class="pull-right box-tools">
								<button class="btn btn-default btn-sm" data-widget='collapse'
									data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i>
								</button>
								<button class="btn btn-default btn-sm" data-widget='remove'
									data-toggle="tooltip" title="Remove">
									<i class="fa fa-times"></i>
								</button>
							</div>
							<!-- /. tools -->
						</div>
						<!-- /.box-header -->
						<div class='box-body pad'>
							<form id="pwdform">
								<div class="form-group">
									<label>旧密码</label><input type="password" id="oldpwd" name="oldpwd" class="form-control">
								</div>
								<div class="form-group">
									<label>新密码</label><input type="password" id="newpwd" name="newpwd" class="form-control">
								</div>
								<div class="form-group">
									<label>确认密码</label> <input type="password" id="secpwd" class="form-control">
								</div>
							</form>
						</div>
						<div class="box-footer text-center">
							<button class="btn btn-primary" onclick="javascript:updatepwd();">保存密码</button>
						</div>
					</div>
				</div>
			</div>
			<!-- ./row -->
		</section>
		<!-- /.content -->
	</div>
</body>
<script type="text/javascript">
function updateinfo(){
	var realname=$("#realname").val();
	var qq = $("#qq").val();
	var tel = $("#tel").val();
	if(realname==""){
		alert("真是姓名不能为空");
		return false;
	}
	if(qq==""){
		alert("QQ不能为空");
		return false;
	}
	$.ajax({
		type:"post",
		url:"${ctx}/member/updateinfo",
		async:false,
		data:{
			realname:realname,
			qq:qq,
			tel:tel
		},
		success:function(data){
			if(data=="true"){
				alert("个人信息修改成功");
			}else{
				alert("网络连接失败");
			}
		}
	});
}
function updatepwd(){
	var oldpwd = $("#oldpwd").val();
	var newpwd = $("#newpwd").val();
	var secpwd = $("#secpwd").val();
	var length = newpwd.split('').length;
	if(oldpwd==""){
		alert("请输入旧密码");
		return false;
	}else if(newpwd==""){
		alert("请输入新密码");
		return false;
	}else if(secpwd==""){
		alert("请输入确认密码");
		return false;
	}
	if(newpwd!=secpwd){
		alert("两次输入密码不一致");
		return false;
	}
	 if (length < 6 || length >= 20){
         alert("密码长度错误,请输入8-20位，字母、数字、下划线");
         return false;
     }
	 $.ajax({
		 type:"post",
		 url:"${ctx}/member/updatepwd",
		 async:false,
		 data:{
			 oldpwd:oldpwd,
			 newpwd:newpwd
		 },
		 success:function(data){
			 if(data=="true"){
				 alert("密码修改成功");
				 $("#pwdform")[0].reset();
			 }else{
				 alert("旧密码错误");
			 }
		 }
	 });
}
</script>
</html>