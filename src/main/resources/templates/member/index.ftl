<#--<%@ page contentType="text/html; charset=UTF-8" language="java"%>-->
<#--<%@ include file="/common/taglibs.jsp"%>-->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>广告管理平台</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<#--<#include "../common/base.ftl">-->
<#include "/common/base.ftl">
<link rel="stylesheet" href="/plugins/checkbrowser/checkbrowser.css">
<#--<script type="text/javascript">-->
	<#--(function(e, t, n, r) {-->
		<#--function i(e, n) {-->
			<#--e = t.createElement("script");-->
			<#--e.async = 1;-->
			<#--e.src = r;-->
			<#--n = t.getElementsByTagName("script")[0];-->
			<#--n.parentNode.insertBefore(e, n)-->
		<#--}-->
		<#--e[n] = e[n] || function() {-->
			<#--(e[n].q = e[n].q || []).push(arguments)-->
		<#--};-->
		<#--e.attachEvent ? e.attachEvent("onload", i) : e.addEventListener("load",-->
				<#--i, false)-->
	<#--})(window, document, "security", "/security.js")-->
	<#--security('appid', '123456');-->
	<#--security('userid', '123', {-->
		<#--name : '123', // optional-->
		<#--email : '123' // optional-->
	<#--});-->
<#--</script>-->

</head>
<body class="skin-blue sidebar-mini">
	<div class="top-wrap" id="wrap"></div>
	<div class="wrapper">
	<#include "/framework/head.ftl">
	<#include "/framework/left.ftl">
		<div id="main-content-wrapper" class="content-wrapper">


			<!-- Content Wrapper. Contains page content -->
			<iframe name="mainFrame" id="mainFrame" src="" frameborder="false"
				scrolling="auto" class="mainframe"
				style="border: none; width: 100%;"> </iframe>
			<!-- /.content-wrapper -->
		</div>
		<#--<jsp:include page="/framework/foot.jsp"></jsp:include>-->
		<#include "../framework/foot.ftl">
		<!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
		<div class='control-sidebar-bg'></div>
	</div>
	<!-- ./wrapper -->
	<script src="/plugins/checkbrowser/checkbrowser.js"></script>
	<script src="/js/member/index.js"></script>
	<script type="text/javascript">
	$(function() {

		var resizeMainFrame = function() {
			console.log("resize fire")
			var $that = $('#main-content-wrapper');
			var mainFrame = $('#mainFrame');
			mainFrame.css({
				height : $that.css('height')
			});
		}
		$(window).resize(resizeMainFrame);
		resizeMainFrame();
		$.ajax({
			type : "get",
			url : "/left/leftmenu",
			cache : false,
			async : false
		}).done(function(html) {
			$("#leftmenu").html(html);
			var href = $("#sid_1").attr("href");
			$("#sid_1").addClass('active');
			var src = $("#sid_1").find('img').attr('src');
			var src_load = src.substring(0,src.length-4)+"_1.png";
			$("#sid_1").find('img').attr('src',src_load); 
			$("#sid_1").find("span").css('color','#3c8dbc');
			$("#mainFrame").attr("src", href);
			
		});
		  $("#mainFrame").load(
				function() {
					
				});  
	});
	</script>
</body>
</html>