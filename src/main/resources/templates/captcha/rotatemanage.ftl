<!DOCTYPE html>
<html>
<head>
<#include "../common/base.ftl">
<style>
body {
	overflow: scroll;
}
.tablecell{
	vertical-align: middle!important;
}
.captcha_manage_smallpic_div {
    width: 60px;
    height: 60px;
    border: 1px dashed #cdcdcd;
    margin: 3px;
    line-height: 60px;
    text-align: center;
}
.captcha_manage_trans_div {
    width: 60px;
    height: 60px;
    text-align: center;
    line-height: 60px;
    font-size: 20px;
    border: 1px dashed #cdcdcd;
    margin: 3px;
    color: #666;
    font-family: 'Microsoft YaHei';
}
.captcha_manage_tword_div {
    width: 60px;
    height: 60px;
    text-align: center;
    font-size: 14px;
    border: 1px dashed #cdcdcd;
    margin: 3px;
    word-break: break-all;
    white-space: normal;
    overflow: hidden;
    color: #666;
}
</style>
<script type="text/javascript">
	var options = {
		"bPaginate" : true,
		"bLengthChange" : false,
		"bFilter" : false,
		"bSort" : true,
		"bInfo" : true,
		"bAutoWidth" : false,
		"oLanguage" : {
			"sProcessing" : "正在加载中......",
			"sLengthMenu" : "每页显示 _MENU_ 条记录",
			"sZeroRecords" : "对不起，查询不到相关数据！",
			"sEmptyTable" : "表中无数据存在！",
			"sInfo" : "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录",
			"sInfoFiltered" : "数据表中共为 _MAX_ 条记录",
			"sSearch" : "搜索",
			"oPaginate" : {
				"sFirst" : "首页",
				"sPrevious" : "上一页",
				"sNext" : "下一页",
				"sLast" : "末页"
			}
		}
	};
	$(function() {
		$('#defultimg').dataTable(options);
		$('#custom').dataTable(options);
		if(${custompic}){
			$("#custompic").prop("checked",true);
		}
		$('#custompic').click(function(){
			var that = $(this);
			$.ajax({
				type : "post",
				url : "${ctx}/captcha/updateCustomPic",
				data : {
					custompic : that.is(":checked"),
					wid : $("#wid").val()
				},
				success : function(data){
					if(data){
						alert("修改成功");
					}else{
						alert("修改失败，请重试");
					}
				}
			});
		});
	});
	function del(bid){
		if(window.confirm("是否删除该图片组")){
			$.ajax({
				type : "post",
				url : "${ctx}/captcha/delBds",
				data : {
					bid : bid
				},
				success : function(msg){
					if(msg){
						alert("删除成功");
						location.reload();
					}
				}
			});
		}
	}
</script>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>图片管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">图片管理</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">网站专属</h3>
							<input type="checkbox" id="custompic">是否仅使用网站专属图片
							<input type="hidden" id="wid" value="${wid }">
							<!-- tools box -->
							<div class="pull-right box-tools">
								<button class="btn btn-info btn-sm" data-widget='collapse'
									data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i>
								</button>
								<button class="btn btn-info btn-sm" data-widget='remove'
									data-toggle="tooltip" title="Remove">
									<i class="fa fa-times"></i>
								</button>
							</div>
							<!-- /. tools -->
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="custom">
								<thead>
									<tr>
										<th>验证图类型</th>
										<th>验证码底图</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="bds" items="${bds}">
										<tr>
											<td class="tablecell">网站专属</td>
											<td><img width="250px" height="125px" alt="未加载" src="data:image/jpg;base64,${bds.backgroundbase64 }"></td>
											<td>
												<button class="btn btn-danger btn-sm" onclick="del('${bds.bid}')" type="button">删除</button>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
						<div class="box-footer btn-group">
							<button class="btn btn-default" onclick="history.go(-1)">返回上一页</button>
						</div>
					</div>
					<!-- /.box -->
				</section>
			</div>
		</section>
	</div>
</body>
</html>