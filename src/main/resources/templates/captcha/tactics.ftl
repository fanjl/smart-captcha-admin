<!DOCTYPE html>
<html>
<head>
<#include "../common/base.ftl">
<link href="../plugins/croppic/croppic.css" rel="stylesheet">
<link href="../plugins/croppic/main.css?v=1.006" rel="stylesheet">
<link href="../css/captcha/tactics.css" rel="stylesheet">
<link href="../plugins/bootstrap-slider/bootstrap-slider.css"
	rel="stylesheet">
<link rel="stylesheet" href="../css/timeManage.css">
<link rel="stylesheet" href="../css/titatoggle-dist.css">


</head>
<body>
	<div class="wrapper">
		<div class="tactics container-fluid">
			<!-- 标题 -->
			<h3 class="tatics-title">
				<strong> 应用场景选择 </strong> <select id="currentwebsite"
					class="tatics-select">
					<c:forEach var="list" items="${website}" varStatus="status">
						<c:choose>
							<c:when test="${status['index'] == 0}">
								<option value="${list.wid }" cversion="${list.cversion }"
									selected="selected">${list.wname }</option>
							</c:when>
							<c:otherwise>
								<option value="${list.wid }" cversion="${list.cversion }">${list.wname }</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</h3>
			<!-- 单项功能设置 -->
			 <div class="tactics-Individual">
				<h3 class="tactics-h3">单项功能设置：</h3>
				<ul class="tactics-option">
					<li>
						<ul class="row">
							<li class="col-sm-2 col-xs-6 tactics-option-title">去除点触logo：</li>
							<li class="col-sm-2 col-xs-6">
								 <c:if test="${logo == true}">
									<div
										class="checkbox  checkbox-slider-md checkbox-slider--a  checkbox-slider-info payPage-checkbox"
										id="logostatus">
										<label> <input type="checkbox" checked=""><span
											id="logotext">开启</span>
										</label>
									</div>
									</c:if>
									<c:if test="${logo == false}">
									<div
										class="checkbox  checkbox-slider-md checkbox-slider--a  checkbox-slider-info payPage-checkbox"
										id="logostatus">
										<label> <input type="checkbox" ><span
											id="logotext">关闭</span>
										</label>
									</div>
									</c:if>
								
							</li>
							<li class="col-sm-2 col-xs-6 tactics-option-title">去除点触help链接：</li>
							<li class="col-sm-2 col-xs-6">
							<c:if test="${help == true}">
									<div
										class="checkbox  checkbox-slider-md checkbox-slider--a  checkbox-slider-info payPage-checkbox"
										id="helpstatus">
										<label> <input type="checkbox" checked=""><span
											id="helptext">开启</span>
										</label>
									</div>
									</c:if>
									<c:if test="${help == false}">
									<div
									class="checkbox checkbox-slider-md checkbox-slider--a  checkbox-slider-info payPage-checkbox"
									id="helpstatus">
									<label> <input type="checkbox"><span
										id="helptext">关闭</span>
									</label>
								</div>
									</c:if>
							</li>
							
							<li class="col-sm-2 col-xs-6 tactics-option-title">行为特征监控：</li>
							<li class="col-sm-2 col-xs-6">
								 <c:if test="${behavior == true}">
									<div
										class="checkbox  checkbox-slider-md checkbox-slider--a  checkbox-slider-info payPage-checkbox"
										id="behaviorstatus">
										<label> <input type="checkbox" checked=""><span
											id="behaviortext">开启</span>
										</label>
									</div>
									</c:if>
									<c:if test="${behavior == false}">
									<div
										class="checkbox  checkbox-slider-md checkbox-slider--a  checkbox-slider-info payPage-checkbox"
										id="behaviorstatus">
										<label> <input type="checkbox" ><span
											id="behaviortext">关闭</span>
										</label>
									</div>
									</c:if>
								
							</li>


							<li class="col-sm-2 col-xs-6 tactics-option-title">自定义提示文字：</li>
							<li class="col-sm-2 col-xs-6">
								<c:if test="${promptText == true}">
									<div
											class="checkbox  checkbox-slider-md checkbox-slider--a  checkbox-slider-info payPage-checkbox"
											id="promptTextStatus">
										<label> <input type="checkbox" checked=""><span
												id="promptTextText">开启</span>
										</label>
									</div>
								</c:if>
								<c:if test="${promptText == false}">
									<div
											class="checkbox  checkbox-slider-md checkbox-slider--a  checkbox-slider-info payPage-checkbox"
											id="promptTextStatus">
										<label> <input type="checkbox" ><span
												id="promptTextText">关闭</span>
										</label>
									</div>
								</c:if>

							</li>
							<!-- 	<li class="col-sm-3 tactics-aissez-passer">免验证设置：
								<select name="" id="">
									<option value="">2</option>
									<option value="">2</option>
									<option value="">2</option>
								</select>
								次
							</li> -->
							<!-- 	<li class="col-sm-5">
								<i class="icon iconfont">&#xe600;</i>
								同一设备指纹使用前
								<span>3</span>
								次免验证
							</li> -->
						</ul>
					</li>
				</ul>
			</div> 
			<!-- 综合功能设置 -->
			<div class="tactics-synthesize">
				<h3 class="tactics-h3">
					综合功能设置:<sub>（您可以根据自己场景的需求在开发中自主设置安全策略，系统已默认为您提供了建议设置，此处不提供该项目的配置）</sub>
				</h3>
				<ul class="tactics-option">
					<li>当同一用户以新的设备指纹使用系统时，进行<select name="" id="">
							<option value="">验证码/强验证、短信验证</option>
					</select>
					</li>
					<li>当用户行为特征为机器特征时，进行<select name="" id="">
							<option value="">验证码/强验证、短信验证</option>
					</select>
					</li>
				</ul>
			</div>

		</div>
		<div class="modal fade" id="ErrorAlert" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div>
							<div style="text-align: center;">
								<span class="lead" id="errorText"></span>
							</div>
						</div>
						<div></div>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
		</div>
	</div>
</body>
<script src="../js/captcha/tactics.js"></script>
</html>