<!DOCTYPE html>
<html>
<head>
<#include "../common/base.ftl">
<style>
body {
	overflow: scroll;
}
</style>
<script type="text/javascript" src="../js/highcharts/highcharts.js"></script>
<script type="text/javascript" src="../js/datacenter/validate-data.js?v=1.002"></script>
<script type="text/javascript">
var daytable;
var monthtable;
var yeartable;
var options;
var datepickeroptions = {
	format : 'YYYY-MM-DD',
	locale : {
		applyLabel : '确定',
		cancelLabel : '取消',
		fromLabel : '起始时间',
		toLabel : '结束时间',
		daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
        monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
                '七月', '八月', '九月', '十月', '十一月', '十二月' ],
	}
};
$(document).ready(function() {
	$('#reservation_day').daterangepicker(datepickeroptions);
	$('#reservation_month').daterangepicker(datepickeroptions);
	$('#reservation_year').daterangepicker(datepickeroptions);
	setoptions(eval('${daydata}'), 'yyyy-MM-dd hh时');
	daytable = $('#daytable').dataTable(options);
	setoptions(eval('${monthdata}'), 'yyyy-MM-dd');
	monthtable = $('#monthtable').dataTable(options);
	setoptions(eval('${yeardata}'), 'yyyy-MM');
	yeartable = $('#yeartable').dataTable(options);
});
</script>
</head>
<body>
	<div class="wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				主页 <small>数据中心</small>
			</h1>
			<h4>
				当前网站： 
				<select id="currentwebsite" onchange="changewebsite();">
					<#--<c:forEach var="websiteList" items="${websiteList}">-->
						<#--<option value="${websiteList.wid }">${websiteList.wname }</option>-->
					<#--</c:forEach>-->
				<#if websiteList??>
					<#list websiteList as list>
                        <option value="${list.wid }">${list.wname }</option>

					</#list>
				</#if>
				</select>

			</h4>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
				<li class="active">数据中心</li>
			</ol>
		</section>
		<!-- Main row -->
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<section class="col-lg-12 col-xs-12 connectedSortable">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">验证数据(天)</h3>
							<div class="pull-right">
								<i class="fa fa-calendar"></i> <input type="text"
									class="dropdown-menu-right" id="reservation_day" />
								<button class="btn btn-default dropdown-menu-right"
									onclick="selectDateRangeDay();">查询</button>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="daytable">
								<thead>
									<tr>
										<th>时间</th>
										<th>请求量</th>
										<th>正确量</th>
										<th>错误量</th>
										<th>完成量</th>
										<th>广告跳转量</th>
										<th>跳转率</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- right col -->
				<section class="col-lg-12 col-xs-12 connectedSortable">

					<div class="box">
						<div class="box-header">
							<h3 class="box-title">验证数据(月)</h3>
							<div class="pull-right">
								<i class="fa fa-calendar"></i> <input type="text"
									class="dropdown-menu-right" id="reservation_month" />
								<button class="btn btn-default dropdown-menu-right"
									onclick="selectDateRangeMonth();">查询</button>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-bordered table-hover" id="monthtable">
								<thead>
									<tr>
										<th>时间</th>
										<th>请求量</th>
										<th>正确量</th>
										<th>错误量</th>
										<th>完成量</th>
										<th>广告跳转量</th>
										<th>跳转率</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- right col -->
				<section class="col-lg-12 col-xs-12 connectedSortable">

					<div class="box">
						<div class="box-header">
							<h3 class="box-title">验证数据(年)</h3>
							<div class="pull-right">
								<i class="fa fa-calendar"></i> <input type="text"
									class="dropdown-menu-right" id="reservation_year" />
								<button class="btn btn-default dropdown-menu-right"
									onclick="selectDateRangeYear();">查询</button>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-condensed table-hover" id="yeartable">
								<thead>
									<tr>
										<th>时间</th>
										<th>请求量</th>
										<th>正确量</th>
										<th>错误量</th>
										<th>完成量</th>
										<th>广告跳转量</th>
										<th>跳转率</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- right col -->
			</div>
			<!-- /.row (main row) -->

		</section>
		<!-- /.content -->
	</div>
</body>
</html>