<link rel="icon" href="/favicon.ico" type="image/x-icon">
<!-- Bootstrap 3.3.4 -->
<link href="/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- FontAwesome 4.3.0 -->
<link
	href="/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" /> 
<!-- Ionicons 2.0.0 -->
<!-- <link
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
	rel="stylesheet" type="text/css" />
 --><!-- Theme style -->
<link href="/dist/css/AdminLTE.css" rel="stylesheet"
	type="text/css" />
<!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
<link href="/dist/css/skins/_all-skins.min.css"
	rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link
	href="/plugins/daterangepicker/daterangepicker-bs3.css"
	rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link
	href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	rel="stylesheet" type="text/css" />
<!-- jquery datatables -->
<link
	href="/plugins/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.min.css"
	rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- jQuery 2.1.4 -->
<script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.2 -->
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.2 JS -->
<script src="/bootstrap/js/bootstrap.min.js"
	type="text/javascript"></script>
<!-- daterangepicker -->
<script src="/js/moment.min.js" type="text/javascript"></script>
<script src="/plugins/daterangepicker/daterangepicker.js"
	type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script
	src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="/dist/js/app.min.js" type="text/javascript"></script>
<!-- jquery datatables -->
<script src='/plugins/datatables/jquery.dataTables.min.js'></script>
<script src='/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js'></script>
<script src='/plugins/datatables/dataTables.fnReloadAjax.js'></script>

